/*
	Description: 	
	
		Basic tables containing AI equipment classnames and skin models used by AI units. 
		Some settings may be overridden by map-specific configuration files in the world_classname_configs folder.
		To make changes for a specific DayZ mod, edit the appropriate config file in for the map/mod being used.

	How it works:
	
		DZAI will first load global classnames defined by global_classnames.sqf, then load the map/mod-specific file to modify global settings.
		Example: DZAI will always first load global_classnames.sqf, then if DayZ Epoch is detected, DZAI will overwrite settings specified by epoch\dayz_epoch.sqf. 

		In this case, you may need to edit both global_classnames.sqf and \epoch\dayz_epoch.sqf to make your wanted classname changes.

	Last upated: 9:16 PM 2/19/2014
*/


//Default weapon classname tables - DZAI will only use these tables if the dynamic weapon list (DZAI_dynamicWeaponList) is disabled, otherwise they are ignored and overwritten if it is enabled.
//Note: Low-level AI may use pistols listed in DZAI_Pistols0 or DZAI_Pistols1. Mid/high level AI will carry pistol weapons but not use them - they will use rifle weapons instead.
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_Pistols0 = []; 	//Weapongrade 0 pistols
DZAI_Pistols1 = []; 	//Weapongrade 1 pistols
DZAI_Pistols2 = []; 	//Weapongrade 2 pistols
DZAI_Pistols3 = []; 	//Weapongrade 3 pistols

DZAI_Rifles0 = ["Winchester1866","MR43","bwc_SKS","Remington870_lamp","M1014_DZed","LeeEnfield_DZed","SVD_Ironsight_DayZed","RH_M249","RH_rpk74","RH_MK12","huntingrifle","RH_bizon","RH_mp5a5","RH_ak105","RH_UMP","RH_gr1","C1987_Famas_f1","G36C","RH_aks74u","RH_hk416s","RH_akm","RH_M4a1","RH_M4sbr","RH_akms","RH_acrb","RH_M16a4","RH_AK107","RH_hk416","RH_p90","RH_aks74","RH_rk95","RH_ak74","RH_an94"];		//Weapongrade 0 rifles
DZAI_Rifles1 = ["Winchester1866","MR43","bwc_SKS","Remington870_lamp","M1014_DZed","LeeEnfield_DZed","SVD_Ironsight_DayZed","RH_M249","RH_rpk74","RH_MK12","huntingrifle","RH_bizon","RH_mp5a5","RH_ak105","RH_UMP","RH_gr1","C1987_Famas_f1","G36C","RH_aks74u","RH_hk416s","RH_akm","RH_M4a1","RH_M4sbr","RH_akms","RH_acrb","RH_M16a4","RH_AK107","RH_hk416","RH_p90","RH_aks74","RH_rk95","RH_ak74","RH_an94"];		//Weapongrade 1 rifles
DZAI_Rifles2 = ["Winchester1866","MR43","bwc_SKS","Remington870_lamp","M1014_DZed","LeeEnfield_DZed","SVD_Ironsight_DayZed","RH_M249","RH_rpk74","RH_MK12","huntingrifle","RH_bizon","RH_mp5a5","RH_ak105","RH_UMP","RH_gr1","C1987_Famas_f1","G36C","RH_aks74u","RH_hk416s","RH_akm","RH_M4a1","RH_M4sbr","RH_akms","RH_acrb","RH_M16a4","RH_AK107","RH_hk416","RH_p90","RH_aks74","RH_rk95","RH_ak74","RH_an94"];		//Weapongrade 2 rifles
DZAI_Rifles3 = ["Winchester1866","MR43","bwc_SKS","Remington870_lamp","M1014_DZed","LeeEnfield_DZed","SVD_Ironsight_DayZed","RH_M249","RH_rpk74","RH_MK12","huntingrifle","RH_bizon","RH_mp5a5","RH_ak105","RH_UMP","RH_gr1","C1987_Famas_f1","G36C","RH_aks74u","RH_hk416s","RH_akm","RH_M4a1","RH_M4sbr","RH_akms","RH_acrb","RH_M16a4","RH_AK107","RH_hk416","RH_p90","RH_aks74","RH_rk95","RH_ak74","RH_an94"]; //Weapongrade 3 rifles

	
//Note: Custom rifle tables can be defined below this line (DZAI_Rifles4 - DZAI_Rifles9). Custom rifle tables can only be used with custom-defined spawns (spawns created using the DZAI_spawn function). 
//Instructions: Replace "nil" with the wanted rifle array. Refer to the above rifle arrays for examples on how to define custom rifle tables.
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_Rifles4 = nil; //weapongrade 4 weapons
DZAI_Rifles5 = nil; //weapongrade 5 weapons
DZAI_Rifles6 = nil; //weapongrade 6 weapons
DZAI_Rifles7 = nil; //weapongrade 7 weapons
DZAI_Rifles8 = nil; //weapongrade 8 weapons
DZAI_Rifles9 = nil; //weapongrade 9 weapons


//AI skin classnames. DZAI will use any of these classnames for AI spawned. Note: Additional skins may be used on a per-map or per-mod basis - see appropriate folders in \world_classname_configs
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_BanditTypes = ["Bandit1_DZ"]; //List of skins for AI units to use


//AI Backpack types.
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_Backpacks0 = [];
DZAI_Backpacks1 = [];
DZAI_Backpacks2 = [];
DZAI_Backpacks3 = [];


//AI Food/Medical item types.
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_Edibles = []; //List of all edible items
DZAI_Medicals1 = []; //List of common medical items
DZAI_Medicals2 = []; //List of all medical items


//AI Miscellaneous item types.
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_MiscItemS = []; //List of random miscellaneous items (1 inventory space)
DZAI_MiscItemL = []; //List of random miscellaneous items (>1 inventory space)


//AI toolbelt item types. Toolbelt items are added to AI inventory upon death. Format: [item classname, item probability]
//NOTE: To remove an item, set its chance to 0, don't delete it from the array. To add a toolbelt item as loot, add it to the end of the array.
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_tools0 = [["ItemFlashlight",0],["ItemWatch",0],["ItemKnife",0],["ItemHatchet",0],["ItemCompass",0],["ItemMap",0],["ItemToolbox",0],["ItemGPS",0],["ItemRadio",0]];
DZAI_tools1 = [["ItemFlashlight",0],["ItemWatch",0],["ItemKnife",0],["ItemHatchet",0],["ItemCompass",0],["ItemMap",0],["ItemToolbox",0],["ItemGPS",0],["ItemRadio",0]];


//AI-useable toolbelt item types. These items are added to AI inventory at unit creation and may be used by AI. Format: [item classname, item probability]
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DZAI_gadgets0 = [["binocular",0],["NVGoggles",0]];
DZAI_gadgets1 = [["binocular",0],["NVGoggles",0]];


//Nothing to edit past this point
diag_log "[DZAI] Global classname tables loaded.";
