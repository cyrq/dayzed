/*
	Chernarus static spawn configuration 
	
	Last updated: 3:37 PM 12/24/2013
	
*/

#include "spawn_markers\markers_chernarus.sqf"	//Load manual spawn point definitions file.

if ((DZAI_maxHeliPatrols > 0) or {(DZAI_maxLandPatrols > 0)}) then {
	"DZAI_centerMarker" setMarkerPos [7652.9634, 7870.8076];
	"DZAI_centerMarker" setMarkerSize [5500, 5500];
};

waitUntil {sleep 0.1; !isNil "DZAI_classnamesVerified"};	//Wait for DZAI to finish verifying classname arrays or finish building classname arrays if verification is disabled.

if (DZAI_staticAI) then {
	#include "spawn_areas\areas_chernarus.sqf"		//Load spawn area definitions file.
	
	//marker name, [minimum AI, max additional AI], [markers for manual spawn points] (leave as empty array to use nearby buildings as spawn points), equipType (optional, required if number of AI groups is defined), number of AI groups (optional)
	["DZAI_DevilsCastle",[0,4],['DevilsCastle','DevilsCastle2','Devils3','Devils4'],1] call DZAI_static_spawn;
	["DZAI_BalotaAirstrip",[0,4],['Balota1','Balota2','Balota3','Balota4','Balota5'],1] call DZAI_static_spawn;
	["DZAI_NEAF",[0,4],['NEAF1','NEAF2','NEAF3','NEAF4','NEAF5'],2] call DZAI_static_spawn;
	["DZAI_StarySobor",[0,4],[],2,2] call DZAI_static_spawn;
	["DZAI_NWAF2",[0,4],[],3] call DZAI_static_spawn;
	["DZAI_NWAF3",[0,4],[],3] call DZAI_static_spawn;
	["DZAI_NWAF4",[0,4],['NWAF4_1','NWAF4_2','NWAF4_3','NWAF4_4'],2] call DZAI_static_spawn;
	["DZAI_NWAF5",[0,4],['NWAF5_1','NWAF5_2','NWAF5_3','NWAF5_4'],2] call DZAI_static_spawn;
	["DZAI_NWAF6",[0,4],['NWAF6_1','NWAF6_2','NWAF6_3','NWAF6_4'],2] call DZAI_static_spawn;
	["DZAI_NWAF7",[0,4],['NWAF7_1','NWAF7_2','NWAF7_3','NWAF7_4'],2] call DZAI_static_spawn;
	["DZAI_GreenMountain",[0,4],[],2] call DZAI_static_spawn;
};

#include "custom_markers\cust_markers_chernarus.sqf"
#include "custom_spawns\cust_spawns_chernarus.sqf"

diag_log "Chernarus static spawn configuration loaded.";
