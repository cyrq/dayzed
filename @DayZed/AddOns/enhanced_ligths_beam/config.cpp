////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 4.13
//Sun Sep 29 11:28:50 2013 : Source 'file' date Sun Sep 29 11:28:50 2013
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

class CfgPatches
{
	class enhanced_ligths_beam
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"CAData"};
		version = "2012-12-04";
		fileName = "two_ligths_beam.pbo";
		author = "Magician";
		mail = "";
	};
};
class CfgVehicles
{
	class All
	{
		enableGPS = "true";
	};
	class Logic: All{};
	class AllVehicles: All{};
	class Land: AllVehicles{};
	class LandVehicle: Land
	{
		class Reflectors
		{
			class Left
			{
				color[] = {0.9,0.8,0.8,1};
				ambient[] = {0.1,0.1,0.1,1};
				position = "L svetlo";
				direction = "konec L svetla";
				hitpoint = "L svetlo";
				selection = "L svetlo";
				size = 0.5;
				brightness = 0.5;
			};
			class Right
			{
				color[] = {0.9,0.8,0.8,1};
				ambient[] = {0.1,0.1,0.1,1};
				position = "P svetlo";
				direction = "konec P svetla";
				hitpoint = "P svetlo";
				selection = "P svetlo";
				size = 0.5;
				brightness = 0.5;
			};
		};
		aggregateReflectors[]={};
	};
};