class CfgPatches
{
 class DNK_skymod
 {
  units[] =   {};
  weapons[] = {};
  requiredVersion = 1.00;
  requiredAddons[] = {};
 };
};

class CfgWorlds {

class DefaultWorld {
		class Weather {
			class Overcast {
				class Weather1;	// External class reference
				class Weather2;	// External class reference
				class Weather3;	// External class reference
				class Weather4;	// External class reference
				class Weather5;	// External class reference
			};
		};
	};
	
	class CAWorld : DefaultWorld {
		class Weather : Weather {
			class Overcast : Overcast {
				class Weather1 : Weather1 {
					sky = "ca\data\data\sky_clear_sky.paa";
					skyR = "ca\data\data\sky_clear_lco.paa";
					horizon = "ca\data\data\sky_clear_horizont_sky.paa";
					overcast = 0;
					alpha = 0;
					size = 0;
					height = 1.0;
					bright = 0.7;
					speed = 0;
					through = 1.0;
					diffuse = 1.0;
					cloudDiffuse = 0.95;
					waves = 0.2;
					lightingOvercast = 0.0;
				};
				
				class Weather7 : Weather1 {
					sky = "ca\data\data\sky_veryclear_sky.paa";
					skyR = "ca\data\data\sky_clear_lco.paa";
					horizon = "ca\data\data\sky_veryclear_horizont_sky.paa";
					overcast = 0.1;
					alpha = 0.5;
					size = 0;
					height = 0.95;
					bright = 0.5;
					speed = 0.1;
					through = 1.0;
					diffuse = 1.0;
					cloudDiffuse = 0.95;
					waves = 0.2;
					lightingOvercast = 0.0;
				};
				
				class Weather2 : Weather2 {
					sky = "ca\data\data\sky_almostclear_sky.paa";
					skyR = "ca\data\data\sky_almostclear_lco.paa";
					horizon = "ca\data\data\sky_almostclear_horizont_sky.paa";
					overcast = 0.25;
					alpha = 0.6;
					size = 0;
					height = 0.9;
					bright = 0.45;
					speed = 0.25;
					through = 1.0;
					lightingOvercast = 0.1;
					diffuse = 1.0;
					waves = 0.22;
				};
				
				class Weather3 : Weather3 {
					sky = "ca\data\data\sky_semicloudy_sky.paa";
					skyR = "ca\data\data\sky_semicloudy_lco.paa";
					horizon = "ca\data\data\sky_semicloudy_horizont_sky.paa";
					overcast = 0.3;
					alpha = 0.8;
					size = 0;
					height = 0.8;
					bright = 0.4;
					speed = 0.45;
					through = 0.7;
					lightingOvercast = 0.5;
					diffuse = 0.8;
					waves = 0.32;
				};
				
				class Weather4 : Weather4 {
					sky = "dnk_skymod\dnk_sky_cloudy_sky.paa";
					skyR = "ca\data\data\sky_cloudy_lco.paa";
					horizon = "";
					overcast = 0.4;
					alpha = 0.99;
					size = 0;
					height = 0.75;
					bright = 0.5;
					speed = 0.55;
					through = 0.4;
					lightingOvercast = 0.85;
					diffuse = 0.6;
					waves = 0.32;
				};
				
				class Weather5 : Weather5 {
					sky = "dnk_skymod\dnk_sky_mostlycloudy_sky.paa";
					skyR = "ca\data\data\sky_mostlycloudy_lco.paa";
					horizon = "";
					overcast = 0.5;
					alpha = 0.55;
					size = 0;
					height = 0.7;
					bright = 0.33;
					speed = 0.75;
					through = 0;
					lightingOvercast = 0.98;
					diffuse = 0.3;
					waves = 0.52;
				};
				class Weather6 : Weather5 {
					sky = "dnk_skymod\dnk_sky_overcast_sky.paa";
					skyR = "ca\data\data\sky_mostlycloudy_lco.paa";
					horizon = "";
					overcast = 0.7;
					alpha = 0.55;
					size = 0;
					height = 0.7;
					bright = 0.33;
					speed = 0.75;
					through = 0;
					lightingOvercast = 0.999;
					diffuse = 0.3;
					waves = 0.67;
				};
				class Weather8 : Weather5 {
					sky = "dnk_skymod\dnk_sky_rain_sky.paa";
					skyR = "ca\data\data\sky_overcast_lco.paa";
					horizon = "";
					overcast = 0.85;
					alpha = 0.55;
					size = 0;
					height = 0.7;
					bright = 0.33;
					speed = 0.75;
					through = 0;
					lightingOvercast = 0.999;
					diffuse = 0.3;
					waves = 0.75;
				};
				
			};
			
			class ThunderboltNorm;
			
			class ThunderboltHeavy;
		};
		
		class Rain;
	};
};