// VopSound v2.1.0 // 10/07/2009//

#define true	1
#define false	0

#define VSoft		0
#define VArmor		1
#define VAir		2

#define private		0
#define protected		1
#define public		2

#define TEast		0
#define TWest		1
#define TGuerrila		2
#define TCivilian		3
#define TSideUnknown		4
#define TEnemy		5
#define TFriendly		6
#define TLogic		7

#define ReadAndWrite		0
#define ReadAndCreate		1
#define ReadOnly		2
#define ReadOnlyVerified		3

#define LockNo		0
#define LockCadet		1
#define LockYes		2

class CfgPatches
{
	class Vops_c_Environmental
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"CAData","CAAir","CAAir2","CAAir3","CAA10","CACharacters","CASounds","CAWeapons","CAWeapons2","CAWheeled","CAWheeled2","CAWheeled3","CATracked","CATracked2"};
	};

};

	class CfgSFX {
		class AlarmSfx {
		Alarm[] = {"\Vops_s_Environmental\alarm1.wss", 3.16228, 1, 300, 1, 0.0, 0.0, 0.0};
		empty[] = {"", 0, 0, 0, 0, 0, 0, 0};
		};
	
		class AirAlarmSfx {
		Alarm[] = {"\Vops_s_Environmental\alarm2.wss", 3.16228, 1, 300, 1, 0.0, 0.0, 0.0};
		empty[] = {"", 0, 0, 0, 0, 0, 0, 0};
		};
	};

	class CfgEnvSounds {
    			class Default {
				name = $STR_CFG_ENVSOUNDS_DEFAULT;
				sound[] = {$DEFAULT$, 0, 1};
				soundNight[] = {$DEFAULT$, 0, 1};
			};


			class Rain {
				sound[] = {"\Vops_s_Environmental\rain_houses.wss", db-5, 1};
				soundNight[] = {"\Vops_s_Environmental\rain.wss", 0.177828, 1};
			};
	

			class RainHouses {
				sound[] = {"\Vops_s_Environmental\rain_city.wss", 0.016500, 1};
				soundNight[] = {"\Vops_s_Environmental\rain_city.wss", 0.016500, 1};
				volume = "rain*houses";
			};
	

			class WindNoForestHigh {
				sound[] = {"\Vops_s_Environmental\wind_noforest1.wss", db-10, 1};
				volume = "(1-forest)*(windy factor[0,1])*(0.1+(hills factor[0,1])*0.9)-(night*0.25)";
			};

			class WindForestHigh {
				sound[] = {"\Vops_s_Environmental\wind_forest1.wss", db-5, 1};
				volume = "forest*(windy factor[0,1])*(0.1+(hills factor[0,1])*0.9)-(night*0.25)";
			};


			class WindHouses {
				sound[] = {"\Vops_s_Environmental\wind_noforest1.wss", 0.030000, 1};
				soundNight[] = {"\Vops_s_Environmental\wind_noforest1.wss", 0.030300, 1};
				volume = "houses *(1-forest) *(1-sea) *windy*0.5 ";
			};


			class WindOcean {
			};


			class Sea {
				sound[] = {"\Vops_s_Environmental\light_sea.wss", db-15, 1};
				soundNight[] = {"\Vops_s_Environmental\sea_night.wss", db-15, 1};
			};

			class Forest {
				sound[] = {"\Vops_s_Environmental\forest_amb1.wss", db-5, 1};
				volume = "forest*(1-night)";
				randSamp11[] = {"\Vops_s_Environmental\forest_sfx1.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp10[] = {"ca\sounds\Ambient\forest\forest-sfx-12datel", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp0[] = {"\Vops_s_Environmental\forest_sfx2.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp1[] = {"ca\sounds\Ambient\forest\forest-sfx-6bird", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp2[] = {"\Vops_s_Environmental\forest_sfx3.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp3[] = {"\Vops_s_Environmental\forest_sfx4.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp8[] = {"\Vops_s_Environmental\cuckoo.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp9[] = {"ca\sounds\Ambient\forest\forest-sfx-11holub-flapping", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp4[] = {"\Vops_s_Environmental\forest_sfx6.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp5[] = {"\Vops_s_Environmental\forest_sfx2.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp6[] = {"\Vops_s_Environmental\forest_sfx5.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				randSamp7[] = {"\Vops_s_Environmental\forest_sfx1.wss", 0.1, 1, 30, 0.04, 10, 20, 40};
				random[] = {"randSamp11", "randSamp10", "randSamp0", "randSamp1", "randSamp2", "randSamp3", "randSamp8", "randSamp9", "randSamp4", "randSamp5", "randSamp6", "randSamp7"};
			};

			class ForestNight {
				sound[] = {"\Vops_s_Environmental\forest_night.wss", db-25, 1};
				volume = "forest*night";
				randSamp1[] = {"ca\sounds\Ambient\forest\forest-night-sfx-1", 0.1, 1, 25, 0.15, 8, 15, 20};
				randSamp2[] = {"ca\sounds\Ambient\forest\forest-night-sfx-2", 0.1, 1, 25, 0.15, 8, 15, 20, 30};
				randSamp3[] = {"ca\sounds\Ambient\forest\forest-sfx-10bird-flapping", 0.1, 1, 25, 0.15, 15, 30, 40};
				randSamp4[] = {"ca\sounds\Ambient\forest\forest-sfx-11holub-flapping", 0.1, 1 25, 0.15, 15, 30, 40};
				randSamp5[] = {"ca\sounds\Ambient\forest\forest-sfx-1", 0.0562341, 1, 25, 0.1, 8, 15, 20};
				randSamp6[] = {"ca\sounds\Ambient\forest\forest-sfx-2", 0.0562341, 1, 25, 0.1, 8, 10, 20};
				randSamp7[] = {"ca\sounds\Ambient\forest\forest-sfx-3", 0.0562341, 1, 25, 0.1, 8, 10, 20};
				randSamp8[] = {"\Vops_s_Environmental\forest_sfx6.wss", 0.0562341, 1, 25, 0.1, 8, 10, 20};
				random[] = {"randSamp1", "randSamp2", "randSamp3", "randSamp4", "randSamp5", "randSamp6", "randSamp7", "randSamp8"};
			};


			class Houses {
				randSamp11[] = {"\Vops_s_Environmental\houses_sfx1.wss", 0.177828, 1, 25, 0.077, 10, 25, 40};
				randSamp10[] = {"\Vops_s_Environmental\houses_sfx2.wss", 0.177828, 1, 25, 0.077, 10, 25, 40};
				randSamp0[] = {"", 0.177828, 1, 25, 0.077, 10, 25, 40};
				randSamp1[] = {"ca\sounds\Ambient\houses\houses-sfx-04", 0.177828, 1, 25, 0.077, 10, 35, 60};
				randSamp2[] = {"ca\sounds\Ambient\houses\houses-sfx-05", 0.177828, 1, 25, 0.077, 10, 25, 40};
				randSamp3[] = {"ca\sounds\Ambient\houses\houses-sfx-06", 0.177828, 1, 25, 0.077, 10, 25, 40};
				randSamp8[] = {"", 0.177828, 1, 25, 0.077, 10, 55, 80};
				randSamp9[] = {"ca\sounds\Ambient\houses\houses-sfx-08", 0.177828, 1, 25, 0.077, 20, 30, 40};
				randSamp4[] = {"ca\sounds\Ambient\houses\houses-sfx-09", 0.177828, 1, 25, 0.077, 10, 25, 40};
				randSamp5[] = {"ca\sounds\Ambient\houses\houses-sfx-10", 0.177828, 1, 25, 0.077, 10, 75, 40};
				randSamp6[] = {"ca\sounds\Ambient\houses\houses-sfx-11", 0.177828, 1, 25, 0.077, 10, 25, 70};
				randSamp7[] = {"ca\sounds\Ambient\houses\houses-sfx-12", 0.177828, 1, 25, 0.077, 10, 25, 40};
				random[] = {"randSamp11", "randSamp10", "randSamp0", "randSamp1", "randSamp2", "randSamp3", "randSamp8", "randSamp9", "randSamp4", "randSamp5", "randSamp6", "randSamp7"};
			};
	
			class Meadows {
				sound[] = {"\Vops_s_Environmental\field_amb.wss", db-20, 1};
				volume = "(1-forest)*(1-houses)*(1-night)*(1-sea)";
				randSamp1[] = {"\Vops_s_Environmental\meadow_sfx1.wss", 0.1, 1, 25, 0.15, 8, 15, 20};
				randSamp2[] = {"\Vops_s_Environmental\forest_sfx1.wss", 0.1, 1, 25, 0.15, 8, 15, 20};
				randSamp3[] = {"\Vops_s_Environmental\forest_sfx2.wss", 0.1, 1, 25, 0.15, 15, 30, 40};
				randSamp4[] = {"\Vops_s_Environmental\forest_sfx3.wss", 0.01, 1, 25, 0.15, 15, 30, 40};
				randSamp5[] = {"\Vops_s_Environmental\forest_sfx4.wss", 0.00562341, 1, 25, 0.1, 8, 15, 20};
				randSamp6[] = {"ca\sounds\Ambient\meadows\meadow-sfx-06", 0.01, 1, 25, 0.1, 8, 10, 20};
				random[] = {"randSamp1", "randSamp2", "randSamp3", "randSamp4", "randSamp5", "randSamp6"};
			};

			class MeadowsNight {
				sound[] = {"\Vops_s_Environmental\meadow_night.wss", 0.006300, 1};
				volume = "(1-forest)*(1-houses)*night*(1-sea)";
				randSamp1[] = {"ca\sounds\Ambient\meadows\meadow_sfx_01", 0.1, 1, 25, 0.15, 8, 15, 20};
				randSamp2[] = {"ca\sounds\Ambient\meadows\meadow_sfx_02", 0.1, 1, 25, 0.15, 8, 15, 20};
				randSamp3[] = {"ca\sounds\Ambient\meadows\meadow_sfx_03", 0.1, 1, 25, 0.15, 15, 30, 40};
				randSamp4[] = {"ca\sounds\Ambient\meadows\meadow_sfx_04_crickets", 0.01, 1, 25, 0.15, 15, 30, 40};
				randSamp5[] = {"ca\sounds\Ambient\meadows\meadow_sfx_05_crickets", 0.00562341, 1, 25, 0.1, 8, 15, 20};
				random[] = {"randSamp1", "randSamp2", "randSamp3", "randSamp4", "randSamp5"};
			};

			class BugsForestNight {
				sound[] = {"\Vops_s_Environmental\bugsforest_night.wss", 0.028300, 1};
				volume = "forest *(1-meadow) *(1-houses) *(1-rain) *night";
			};


			class BirdsForest {
				sound[] = {"\Vops_s_Environmental\birds_forest.wss", 0.024500, 1};
				volume="forest *(1-meadow) *(1-houses) *(1-rain) *(1-night)";
			};


			class BirdsForestNight {
				sound[] = {"\Vops_s_Environmental\birds_forest_night.wss", 0.016100, 1};
				volume="forest *(1-meadow) *(1-houses) *(1-rain) *night";
			};

			class Combat {
				sound[] = {"\Vops_s_Environmental\combat_amb.wss", db-25, 1};
				soundNight[] = {"\Vops_s_Environmental\combat_amb.wss", 0.0562341, 1};
			};
		};


	class CfgWorlds {
	
	class DefaultWorld {
		class Weather {
			class Overcast {};
		};
	};

	class CAWorld : DefaultWorld {
		class Weather : Weather {
			
				class ThunderboltNorm {
				soundNear[] = {"\Vops_s_Environmental\thundernormal.wss", db0, 1};
				soundFar[] = {"\Vops_s_Environmental\thunder_far.wss", db0, 1};
				};
			
				class ThunderboltHeavy {
				soundNear[] = {"\Vops_s_Environmental\thunderhard.wss", db+5, 1};
				soundFar[] = {"\Vops_s_Environmental\thunder_far.wss", db+5, 1};
				};
			};
		};
	};

	class cfgNonAiVehicles {
	class Insect;	// External class reference
	class Bird;	// External class reference

	class SeaGull : Bird {
	};
	
	class Hawk : Bird {
	};
	
	class DragonFly : Insect {
	};
	
	class ButterFly : Insect {
	};
	
	class HouseFly : Insect {
		flySound[] = {"\Vops_s_Environmental\fly.wss", 0.01, 1, 1.2};
	};
	
	class HoneyBee : Insect {
		flySound[] = {"\Vops_s_Environmental\bumblebee.wss", 0.01, 1, 1.2};
	};
	
	class Mosquito : Insect {
		flySound[] = {"\Vops_s_Environmental\mosquito.wss", 0.01, 1, 1.2};
	};

	class Crow : SeaGull {
		singSound[] = {"\Vops_s_Environmental\crow1.wss", 0.00177828, 1, 100};
	};
};