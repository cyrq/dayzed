#define _ARMA_
#define NO_SIDE -1
#define EAST 0 // (Russian)
#define WEST 1 // (Nato)
#define RESISTANCE 2 // Guerilla 
#define CIVILIAN 3
#define NEUTRAL 4
#define ENEMY 5
#define FRIENDLY 6
#define LOGIC 7
//Class P:\SMD\smd_units\config.bin{
class CfgPatches
{
	class dayzed_models
	{
		units[] = 
		{
			"smd_Ch2_Citizen",
			"smd_Ch2_Doctor",
			"smd_Ch2_Functionary",
			"smd_Ch2_Pilot",
			"smd_Ch2_Policeman",
			"smd_Ch2_Priest",
			"smd_Ch2_Profiteer",
			"smd_Ch2_Rocker",
			"smd_Ch2_Villiger",
			"smd_Ch2_Woodlander",
			"smd_Ch2_Worker",
			"smd_Citizen1",
			"smd_Citizen2",
			"smd_Citizen3",
			"smd_Citizen4",
			"smd_RU_Citizen1",
			"smd_RU_Citizen2",
			"smd_RU_Citizen3",
			"smd_RU_Citizen4",
			"smd_Worker",
			"smd_Worker1",
			"smd_Worker2",
			"smd_Worker3",
			"smd_Worker4",
			"smd_RU_Worker1",
			"smd_RU_Worker2",
			"smd_RU_Worker3",
			"smd_RU_Worker4",
			"smd_Profiteer",
			"smd_Profiteer1",
			"smd_Profiteer2",
			"smd_Profiteer3",
			"smd_Profiteer4",
			"smd_Rocker",
			"smd_Rocker1",
			"smd_Rocker2",
			"smd_Rocker3",
			"smd_Rocker4",
			"smd_RU_Rocker1",
			"smd_RU_Rocker2",
			"smd_RU_Rocker3",
			"smd_RU_Rocker4",			
			"smd_Woodlander",
			"smd_Woodlander1",
			"smd_Woodlander2",
			"smd_Woodlander3",
			"smd_Woodlander4",
			"smd_RU_Woodlander1",
			"smd_RU_Woodlander2",
			"smd_RU_Woodlander3",
			"smd_RU_Woodlander4",
			"smd_Functionary",
			"smd_Functionary1",
			"smd_Functionary2",
			"smd_RU_Functionary1",
			"smd_RU_Functionary2",
			/*"smd_Villager",
			"smd_Villager1",
			"smd_Villager2",
			"smd_Villager3",
			"smd_Villager4",
			"smd_RU_Villager1",
			"smd_RU_Villager2",
			"smd_RU_Villager3",
			"smd_RU_Villager4",*/
			"smd_Priest",
			"smd_RU_Priest",
			"smd_Doctor",
			"smd_RU_Doctor",
			"smd_SchoolTeacher",
			"smd_RU_SchoolTeacher",
			"smd_Assistant",
			"smd_Pilot",
			"smd_RU_Pilot",
			"smd_Policeman",
			"smd_RU_Policeman",
			"smd_civ1_pants",
			"smd_civ2_pants",
			"smd_civ3_pants",
			"smd_civ4_pants",
			"smd_civ5_pants",
			"smd_civ6_pants",
			"smd_civ7_pants",
			"smd_civ1_shorts",
			"smd_civ2_shorts",
			"smd_civ3_shorts",
			"smd_civ4_shorts",
			"smd_civ5_shorts",
			"smd_civ6_shorts",
			"smd_civ7_shorts",
			"smdz_civ1_pants",
			"smdz_civ2_pants",
			"smdz_civ3_pants",
			"smdz_civ4_pants",
			"smdz_civ5_pants",
			"smdz_civ6_pants",
			"smdz_civ7_pants",
			"smdz_civ1_shorts",
			"smdz_civ2_shorts",
			"smdz_civ3_shorts",
			"smdz_civ4_shorts",
			"smdz_civ5_shorts",
			"smdz_civ6_shorts",
			"smdz_civ7_shorts"	
		};
		weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {};
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class dayzed_models
		{
			list[] = {"dayzed_models"};
		};
	};
};
class CfgFactionClasses
{
	class smd_bi_civ_faction
	{
		displayName = "SMD Chernarus Civs";
		priority = 90;
		scope = 2;
		side = 3;
	};
	class smd_civ_faction
	{
		displayName = "SMD Sahrani Civs";
		priority = 90;
		scope = 2;
		side = 3;
	};
	class smdz_civ_faction
	{
		displayName = "SMDZ Sahrani Civs";
		priority = 90;
		scope = 2;
		side = 3;
	};
	class smd_pmc_faction
	{
		displayName = "SMD BI Modded";
		priority = 5;
		scope = 2;
		side = 1;
	};
};
class CfgVehicleClasses
{
	class smd_bi_civ_class
	{
		displayName = "SMD BI CIVS";
	};
	class smd_civ_class
	{
		displayName = "SMD CIVZ PANTS";
	};
	class smd_civ_class_shorts
	{
		displayName = "SMD CIVZ SHORTS";
	};
	class smdz_civ_class
	{
		displayName = "SMD CIVZ PANTS";
	};
	class smdz_civ_class_shorts
	{
		displayName = "SMD CIVZ SHORTS";
	};
	class smd_pmc
	{
		displayName = "SMD PMC";
	};	
};		
	class CfgVehicles 
	{
		class All;
		class AllVehicles;
		class ThingEffect;
		class Land;
		class Man;
		class CAManBase;
		class USMC_Soldier2;	
		class Soldier_Crew_PMC;
		class SoldierEB;
		class BAF_Soldier_SniperH_W;
		class GUE_Soldier_Scout;		
		class smd_Ch2_Citizen: USMC_Soldier2
		{
			faction = "smd_bi_civ_faction";
			side = 3;
			model = "\SMD\smd_units\smd_bi_modded\smd_Ch2_CITIZEN.p3d";
			canDeactivateMines = "true";
			weapons[] = {"Throw","Put"};
			class Wounds
			{
				tex[]={};
				mat[]={
					"ca\characters2\civil\citizen\data\citizen.rvmat",
					"ca\characters2\civil\citizen\data\W1_citizen.rvmat",
					"ca\characters2\civil\citizen\data\W2_citizen.rvmat",

					"ca\characters\heads\male\defaulthead\data\hhl.rvmat",
					"ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat",
					"ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat",
				};
			};
			rarityUrban=0.5;
			hiddenSelections[] =
			{
				"Camo"
			};
		};
		class smd_Citizen1: smd_Ch2_Citizen
		{
			scope = 2;
			accuracy = 1000;
			displayName = "$STR_DN_CIV_CITIZEN1";
			hiddenSelectionsTextures[] = {"\ca\characters2\civil\citizen\data\citizen_co.paa"};
		};
		class smd_Citizen2: smd_Ch2_Citizen
		{
			scope = 2;
			accuracy = 1000;
			displayName = "$STR_DN_CIV_CITIZEN2";
			hiddenSelectionsTextures[] = {"\ca\characters2\civil\citizen\data\Citizen_v2_co.paa"};
		 };
		 class smd_Citizen3: smd_Ch2_Citizen
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_CITIZEN3";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\citizen\data\Citizen_v3_co.paa"};
		 };
		 class smd_Citizen4: smd_Ch2_Citizen
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_CITIZEN4";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\citizen\data\Citizen_v4_co.paa"};
		 };
		 class smd_RU_Citizen1: smd_Citizen1
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Citizen2: smd_Citizen2
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Citizen3: smd_Citizen3
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Citizen4: smd_Citizen4
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_Worker: smd_Ch2_Citizen
		 {
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_CZ"};
		  scope = 0;
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_WORKER";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Worker";
		  hiddenSelections[] = {"Camo"};
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters2\civil\worker\data\worker.rvmat","ca\characters2\civil\worker\data\W1_worker.rvmat","ca\characters2\civil\worker\data\W2_worker.rvmat"};
		  };
		  rarityUrban = 0.4;
		 };
		 class smd_Worker1: smd_Worker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WORKER1";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\worker\data\worker_co.paa"};
		  rarityUrban = 0.4;
		 };
		 class smd_Worker2: smd_Worker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WORKER2";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\worker\data\Worker_v2_CO.paa"};
		  rarityUrban = 0.5;
		 };
		 class smd_Worker3: smd_Worker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WORKER3";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\worker\data\Worker_v3_CO.paa"};
		 };
		 class smd_Worker4: smd_Worker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WORKER4";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\worker\data\Worker_v4_CO.paa"};
		 };
		 class smd_RU_Worker1: smd_Worker1
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Worker2: smd_Worker2
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Worker3: smd_Worker3
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Worker4: smd_Worker4
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_Profiteer: smd_Ch2_Citizen
		 {
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Civ_SunGlasses","Language_CZ"};
		  scope = 0;
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_PROFITEER";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_profiteer";
		  hiddenSelections[] = {"Camo"};
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters2\civil\profiteer\data\profiteer.rvmat","ca\characters2\civil\profiteer\data\W1_profiteer.rvmat","ca\characters2\civil\profiteer\data\W2_profiteer.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat"};
		  };
		  rarityUrban = 0.6;
		 };
		 class smd_Profiteer1: smd_Profiteer
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_PROFITEER1";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\profiteer\data\profiteer_co.paa"};
		 };
		 class smd_Profiteer2: smd_Profiteer
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_PROFITEER2";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\profiteer\data\profiteer_v2_co.paa"};
		 };
		 class smd_Profiteer3: smd_Profiteer
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_PROFITEER3";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\profiteer\data\profiteer_v3_co.paa"};
		 };
		 class smd_Profiteer4: smd_Profiteer
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_PROFITEER4";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\profiteer\data\profiteer_v4_co.paa"};
		 };
		 class smd_RU_Profiteer1: smd_Profiteer1
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Profiteer2: smd_Profiteer2
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Profiteer3: smd_Profiteer3
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Profiteer4: smd_Profiteer4
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_Rocker: smd_Ch2_Citizen
		 {
		  scope = 0;
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Civ_SunGlasses","Language_CZ"};
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_ROCKER";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Rocker";
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters2\civil\Rocker\data\Rocker.rvmat","ca\characters2\civil\Rocker\data\W1_Rocker.rvmat","ca\characters2\civil\Rocker\data\W2_Rocker.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat"};
		  };
		  hiddenSelections[] = {"Camo","CamoB"};
		  rarityUrban = 0.5;
		 };
		 class smd_Rocker1: smd_Rocker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_ROCKER1";
		  hiddenSelectionsTextures[] = {"\ca\characters2\Civil\Rocker\Data\rocker_co.paa","\ca\characters2\Civil\Rocker\Data\rockerhair_ca.paa"};
		 };
		 class smd_Rocker2: smd_Rocker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_ROCKER2";
		  hiddenSelectionsTextures[] = {"\ca\characters2\Civil\Rocker\Data\rocker_v2_co.paa","\ca\characters2\Civil\Rocker\Data\rockerhair_ca.paa"};
		 };
		 class smd_Rocker3: smd_Rocker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_ROCKER3";
		  hiddenSelectionsTextures[] = {"\ca\characters2\Civil\Rocker\Data\rocker_v3_co.paa","\ca\characters2\Civil\Rocker\Data\rockerhair_v2_ca.paa"};
		 };
		 class smd_Rocker4: smd_Rocker
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_ROCKER4";
		  hiddenSelectionsTextures[] = {"\ca\characters2\Civil\Rocker\Data\rocker_v4_co.paa","\ca\characters2\Civil\Rocker\Data\rockerhair_v3_ca.paa"};
		 };
		 class smd_RU_Rocker1: smd_Rocker1
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Rocker2: smd_Rocker2
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Rocker3: smd_Rocker3
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Rocker4: smd_Rocker4
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_Woodlander: smd_Ch2_Citizen
		 {
		  scope = 0;
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_CZ"};
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_WOODLANDER";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Woodlander";
		  hiddenSelections[] = {"Camo"};
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters2\civil\Woodlander\data\Woodlander.rvmat","ca\characters2\civil\Woodlander\data\W1_Woodlander.rvmat","ca\characters2\civil\Woodlander\data\W2_Woodlander.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat"};
		  };
		  rarityUrban = 0.4;
		 };
		 class smd_Woodlander1: smd_Woodlander
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WOODLANDER1";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\woodlander\data\woodlander_co.paa"};
		 };
		 class smd_Woodlander2: smd_Woodlander
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WOODLANDER2";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\woodlander\data\woodlander_v2_co.paa"};
		 };
		 class smd_Woodlander3: smd_Woodlander
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WOODLANDER3";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\woodlander\data\woodlander_v3_co.paa"};
		 };
		 class smd_Woodlander4: smd_Woodlander
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_WOODLANDER4";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\woodlander\data\woodlander_v4_co.paa"};
		 };
		 class smd_RU_Woodlander1: smd_Woodlander1
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Woodlander2: smd_Woodlander2
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Woodlander3: smd_Woodlander3
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Woodlander4: smd_Woodlander4
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_Functionary: smd_Ch2_Citizen
		 {
		  scope = 0;
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Civ_SunGlasses","Language_CZ"};
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_FUNCTIONARY";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Functionary";
		  hiddenSelections[] = {"Camo"};
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\functionary\data\functionary_co.paa"};
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters2\civil\Functionary\data\Functionary.rvmat","ca\characters2\civil\Functionary\data\W1_Functionary.rvmat","ca\characters2\civil\Functionary\data\W2_Functionary.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat"};
		  };
		  rarityUrban = 0.8;
		 };
		 class smd_Functionary1: smd_Functionary
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_FUNCTIONARY1";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\functionary\data\functionary_co.paa"};
		 };
		 class smd_Functionary2: smd_Functionary
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_FUNCTIONARY2";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\functionary\data\functionary2_co.paa"};
		  rarityUrban = 0.7;
		 };
		 class smd_RU_Functionary1: smd_Functionary1
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
		 class smd_RU_Functionary2: smd_Functionary2
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Language_RU","Civ_Glasses","Civ_SunGlasses"};
		 };
/*		 class smd_Villager: smd_Ch2_Citizen
		 {
		  scope = 0;
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_CZ"};
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_VILLAGER";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Villager";
		  hiddenSelections[] = {"Camo"};
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\villager\data\villager_co.paa"};
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters2\Civil\Villager\Data\villager.RVmat","ca\characters2\Civil\Villager\Data\villager_w1.RVmat","ca\characters2\Civil\Villager\Data\villager_w2.RVmat"};
		  };
		  rarityUrban = 0.2;
		 };
		 class smd_Villager1: smd_Villager
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_VILLAGER1";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\villager\data\villager_co.paa"};
		 };
		 class smd_Villager2: smd_Villager
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_VILLAGER2";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\villager\data\villager_v2_co.paa"};
		 };
		 class smd_Villager3: smd_Villager
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_VILLAGER3";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\villager\data\villager_v3_co.paa"};
		 };
		 class smd_Villager4: smd_Villager
		 {
		  scope = 2;
		  accuracy = 1000;
		  displayName = "$STR_DN_CIV_VILLAGER4";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\villager\data\villager_v4_co.paa"};
		 };
		 class smd_RU_Villager1: smd_Villager1
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Villager2: smd_Villager2
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Villager3: smd_Villager3
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_RU_Villager4: smd_Villager4
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };*/
		 class smd_Priest: smd_Ch2_Citizen
		 {
		  scope = 2;
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_CZ"};
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_PRIEST";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Priest";
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters2\civil\priest\data\priest.rvmat","ca\characters2\civil\priest\data\W1_priest.rvmat","ca\characters2\civil\priest\data\W2_priest.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat"};
		  };
		 };
		 class smd_RU_Priest: smd_Priest
		 {
		  faction = "smd_civ_faction";
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_Doctor_base: smd_Ch2_Citizen
		 {
		  scope = 0;
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_CZ"};
		  vehicleClass = "Men";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Doctor";
		  hiddenSelections[] = {"Camo"};
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\doctor\data\doctor_co.paa"};
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters2\civil\doctor\data\doctor.rvmat","ca\characters2\civil\doctor\data\W1_doctor.rvmat","ca\characters2\civil\doctor\data\W2_doctor.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_Wounds.rvmat"};
		  };
		 };
		 class smd_Doctor: smd_Doctor_base
		 {
		  scope = 2;
		  accuracy = 3.9;
		  displayName = "$STR_DN_CIV_DOCTOR";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\doctor\data\doctor_co.paa"};
		  attendant = 1;
		 };
		 class smd_RU_Doctor: smd_Doctor
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_SchoolTeacher: smd_Doctor_base
		 {
		  scope = 2;
		  accuracy = 3.9;
		  displayName = "$STR_DN_CIV_SCHOOLTEACHER";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\doctor\data\doctor2_co.paa"};
		  rarityUrban = 0.5;
		 };
		 class smd_RU_SchoolTeacher: smd_SchoolTeacher
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_Assistant: smd_Doctor_base
		 {
		  scope = 2;
		  accuracy = 3.9;
		  displayName = "$STR_DN_CIV_ASSISTANT";
		  hiddenSelectionsTextures[] = {"\ca\characters2\civil\doctor\data\doctor_3_co.paa"};
		  rarityUrban = 0.5;
		 };
		 class smd_RU_Assistant: smd_Assistant
		 {
		  faction = "smd_civ_faction";
		  languages[] = {"RU"};
		  genericNames = "RussianMen";
		  identityTypes[] = {"Head_CIV","Civ_Glasses","Language_RU"};
		 };
		 class smd_Pilot: smd_Ch2_Citizen
		 {
		  scope = 2;
		  identityTypes[] = {"Head_CIV","pilot_sunglasses","Language_CZ"};
		  accuracy = 3.9;
		  vehicleClass = "Men";
		  displayName = "$STR_DN_CIV_PILOT";
		  model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Pilot";
		  class Wounds
		  {
		   tex[] = {};
		   mat[] = {"ca\characters2\Civil\Pilot\Data\camelpilot.rvmat","ca\characters2\Civil\Pilot\Data\Pilot_W1.rvmat","ca\characters2\Civil\Pilot\Data\Pilot_W2.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_wounds.rvmat"};
		  };
		 };
		class smd_RU_Pilot: smd_Pilot
		{
			faction = "smd_civ_faction";
			languages[] = {"RU"};
			genericNames = "RussianMen";
			identityTypes[] = {"Head_CIV","pilot_sunglasses","Language_RU"};
		};
		class smd_Policeman: smd_Ch2_Citizen
		{
			scope = 2;
			identityTypes[] = {"Head_CIV","pilot_sunglasses","Language_CZ"};
			accuracy = 3.9;
			vehicleClass = "Men";
			displayName = "$STR_DN_CIV_POLICEMAN";
			model = "\smd\smd_units\smd_bi_modded\smd_Ch2_Policeman";
		class Wounds
			{
				tex[] = {};
				mat[] = {"ca\characters2\civil\policeman\data\policeman.rvmat","ca\characters2\civil\policeman\data\w1_policeman.rvmat","ca\characters2\civil\policeman\data\w2_policeman.rvmat","ca\characters\heads\male\defaulthead\data\hhl.rvmat","ca\characters\heads\male\defaulthead\data\hhl_wounds.rvmat","ca\characters\heads\male\defaulthead\data\hhl_wounds.rvmat"};
			};
		};
		class smd_RU_Policeman: smd_Policeman
		{
			faction = "smd_civ_faction";
			languages[] = {"RU"};
			genericNames = "RussianMen";
			identityTypes[] = {"Head_CIV","pilot_sunglasses","Language_RU"};
		};
		class smd_civ1_pants: USMC_Soldier2
		{
			displayName = "1 Blue Jersey";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_1_tshirt.p3d";
			canDeactivateMines = "true";
			weapons[] = {"Throw","Put"};
			magazines[] = {};
			respawnweapons[] = {};
			respawnmagazines[] = {};
			side = 3;
			vehicleClass = "smd_civ_class";
			faction = "smd_civ_faction";
			picture = "\Ca\characters\data\Ico\i_null_CA.paa";
			identityTypes[] = {"Head_USMC"};
			icon = "\Ca\characters2\data\icon\i_soldier_CA.paa";
			class Wounds
			{
				tex[] = {};
				mat[] = {"SMD\smd_units\smd_icewindo_civs\data\civil_tshirt_body.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_tshirt_body_wound1.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_tshirt_body_wound2.rvmat","ca\characters\heads\male\defaulthead\data\hhl_white.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_1_tshirt_hhl_wound1.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_1_tshirt_hhl_wound2.rvmat"};
			};
		};
		class smd_civ2_pants: smd_civ1_pants
		{
			displayName = "2 Black Metal";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_2_tshirt.p3d";
		};
		class smd_civ3_pants: smd_civ1_pants
		{
			displayName = "3 Green Banana";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_3_tshirt.p3d";
		};
		class smd_civ4_pants: smd_civ1_pants
		{
			displayName = "4 Orange Cowboy";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_4_tshirt.p3d";
		};
		class smd_civ5_pants: smd_civ1_pants
		{
			displayName = "5 White Nagova";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_5_tshirt.p3d";
		};
		class smd_civ6_pants: smd_civ1_pants
		{
			displayName = "6 Orange Pepe";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_6_tshirt.p3d";
		};
		class smd_civ7_pants: smd_civ1_pants
		{
			displayName = "7 Black Star";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_7_tshirt.p3d";
		};
		class smd_civ1_shorts: USMC_Soldier2
		{
			displayName = "1 Blue Jersey";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_1_tshirt_shorts.p3d";
			canDeactivateMines = "true";
			weapons[] = {"Throw","Put"};
			magazines[] = {};
			respawnweapons[] = {};
			respawnmagazines[] = {};
			side = 3;
			vehicleClass = "smd_civ_class_shorts";
			faction = "smd_civ_faction";
			picture = "\Ca\characters\data\Ico\i_null_CA.paa";
			identityTypes[] = {"Head_USMC"};
			icon = "\Ca\characters2\data\icon\i_soldier_CA.paa";
			class Wounds
			{
				tex[] = {};
				mat[] = {"SMD\smd_units\smd_icewindo_civs\data\civil_tshirt_shorts_body.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_tshirt_shorts_body_wound1.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_tshirt_shorts_body_wound2.rvmat","ca\characters\heads\male\defaulthead\data\hhl_white.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_1_tshirt_hhl_wound1.rvmat","SMD\smd_units\smd_icewindo_civs\data\civil_1_tshirt_hhl_wound2.rvmat"};
			};
		};
		class smd_civ2_shorts: smd_civ1_shorts
		{
			displayName = "2 Black Metal";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_2_tshirt_shorts.p3d";
		};
		class smd_civ3_shorts: smd_civ1_shorts
		{
			displayName = "3 Green Banana";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_3_tshirt_shorts.p3d";
		};
		class smd_civ4_shorts: smd_civ1_shorts
		{
			displayName = "4 Orange Cowboy";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_4_tshirt_shorts.p3d";
		};
		class smd_civ5_shorts: smd_civ1_shorts
		{
			displayName = "5 White Nagova";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_5_tshirt_shorts.p3d";
		};
		class smd_civ6_shorts: smd_civ1_shorts
		{
			displayName = "6 Orange Pepe";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_6_tshirt_shorts.p3d";
		};
		class smd_civ7_shorts: smd_civ1_shorts
		{
			displayName = "7 Black Star";
			model = "\SMD\smd_units\smd_icewindo_civs\civil_7_tshirt_shorts.p3d";
		};
		class smdz_civ1_pants: USMC_Soldier2
		{
			displayName = "1 Blue Jersey";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_1_tshirt.p3d";
			canDeactivateMines = "true";
			weapons[] = {"Throw","Put"};
			magazines[] = {};
			respawnweapons[] = {};
			respawnmagazines[] = {};
			side = 3;
			vehicleClass = "smdz_civ_class";
			faction = "smdz_civ_faction";
			picture = "\Ca\characters\data\Ico\i_null_CA.paa";
			identityTypes[] = {"Head_USMC"};
			icon = "\Ca\characters2\data\icon\i_soldier_CA.paa";
			class Wounds
			{
				tex[] = {};
				mat[] = {"SMD\smd_units\smd_icewindo_civz\data\civil_tshirt_body.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_tshirt_body_wound1.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_tshirt_body_wound2.rvmat","ca\characters\heads\male\defaulthead\data\hhl_white.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_1_tshirt_hhl_wound1.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_1_tshirt_hhl_wound2.rvmat"};
			};
		};
		class smdz_civ2_pants: smdz_civ1_pants
		{
			displayName = "2 Black Metal";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_2_tshirt.p3d";
		};
		class smdz_civ3_pants: smdz_civ1_pants
		{
			displayName = "3 Green Banana";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_3_tshirt.p3d";
		};
		class smdz_civ4_pants: smdz_civ1_pants
		{
			displayName = "4 Orange Cowboy";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_4_tshirt.p3d";
		};
		class smdz_civ5_pants: smdz_civ1_pants
		{
			displayName = "5 White Nagova";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_5_tshirt.p3d";
		};
		class smdz_civ6_pants: smdz_civ1_pants
		{
			displayName = "6 Orange Pepe";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_6_tshirt.p3d";
		};
		class smdz_civ7_pants: smdz_civ1_pants
		{
			displayName = "7 Black Star";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_7_tshirt.p3d";
		};
		class smdz_civ1_shorts: USMC_Soldier2
		{
			displayName = "1 Blue Jersey";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_1_tshirt_shorts.p3d";
			canDeactivateMines = "true";
			weapons[] = {"Throw","Put"};
			magazines[] = {};
			respawnweapons[] = {};
			respawnmagazines[] = {};
			side = 3;
			vehicleClass = "smdz_civ_class_shorts";
			faction = "smdz_civ_faction";
			picture = "\Ca\characters\data\Ico\i_null_CA.paa";
			identityTypes[] = {"Head_USMC"};
			icon = "\Ca\characters2\data\icon\i_soldier_CA.paa";
			class Wounds
			{
				tex[] = {};
				mat[] = {"SMD\smd_units\smd_icewindo_civz\data\civil_tshirt_shorts_body.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_tshirt_shorts_body_wound1.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_tshirt_shorts_body_wound2.rvmat","ca\characters\heads\male\defaulthead\data\hhl_white.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_1_tshirt_hhl_wound1.rvmat","SMD\smd_units\smd_icewindo_civz\data\civil_1_tshirt_hhl_wound2.rvmat"};
			};
		};
		class smdz_civ2_shorts: smdz_civ1_shorts
		{
			displayName = "2 Black Metal";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_2_tshirt_shorts.p3d";
		};
		class smdz_civ3_shorts: smdz_civ1_shorts
		{
			displayName = "3 Green Banana";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_3_tshirt_shorts.p3d";
		};
		class smdz_civ4_shorts: smdz_civ1_shorts
		{
			displayName = "4 Orange Cowboy";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_4_tshirt_shorts.p3d";
		};
		class smdz_civ5_shorts: smdz_civ1_shorts
		{
			displayName = "5 White Nagova";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_5_tshirt_shorts.p3d";
		};
		class smdz_civ6_shorts: smdz_civ1_shorts
		{
			displayName = "6 Orange Pepe";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_6_tshirt_shorts.p3d";
		};
		class smdz_civ7_shorts: smdz_civ1_shorts
		{
			displayName = "7 Black Star";
			model = "\SMD\smd_units\smd_icewindo_civz\civil_7_tshirt_shorts.p3d";
		};
/*		class smd_annie : Soldier_Crew_PMC
		{
			displayName = "SMD Annie";
			side = 1;
			weapons[] = {"Throw","Put"};
			model = "smd\smd_units\smd_bi_modded\women\smd_annie";
			magazines[] = {};
			vehicleClass = "smd_civ_class_shorts";
			faction = "smd_civ_faction";
			respawnWeapons[] = {"Throw","Put"};
			respawnMagazines[] = {};
			  weaponSlots = "1	 + 	4	 + 6*		256	 + 2*	4096	 + 	2	 + 4*	16  + 6*131072"; //"1	 + 	4	 + 12*		256	 + 2*	4096	 + 	2	 + 8*	16  + 12*131072";
			  canHideBodies = 1;
			  identityTypes[] = {"Language_W_EN_EP1","Woman"};
			  languages[] = {"EN"};
			  class TalkTopics
			  {
			   core = "Core_E";
			   core_en = "Core_Full_E";
			  };
			  genericNames = "EnglishWomen";
			  class SpeechVariants
			  {
			   class Default
			   {
				speechSingular[] = {"veh_woman"};
				speechPlural[] = {"veh_women"};
			   };
			   class EN: Default{};
			   class CZ
			   {
				speechSingular[] = {"veh_woman_CZ"};
				speechPlural[] = {"veh_women_CZ"};
			   };
			   class CZ_Akuzativ
			   {
				speechSingular[] = {"veh_woman_CZ4P"};
				speechPlural[] = {"veh_women_CZ4P"};
			   };
			   class RU
			   {
				speechSingular[] = {"veh_woman_RU"};
				speechPlural[] = {"veh_women_RU"};
			   };
			  };
			  TextPlural = "Women";
			  TextSingular = "Woman";
			  nameSound = "veh_woman";
			  class HitDamage
			  {
			   class Group0
			   {
				hitSounds[] = {
				 { 
				  { "ca\sounds\Characters\Noises\Damage\banz-hit-01",0.177828,1,120 },0.2 },
				 { 
				  { "ca\sounds\Characters\Noises\Damage\banz-hit-02",0.177828,1,120 },0.2 },
				 { 
				  { "ca\sounds\Characters\Noises\Damage\banz-hit-03",0.177828,1,120 },0.2 },
				 { 
				  { "ca\sounds\Characters\Noises\Damage\banz-hit-04",0.177828,1,120 },0.1 },
				 { 
				  { "ca\sounds\Characters\Noises\Damage\banz-hit-05",0.177828,1,120 },0.1 },
				 { 
				  { "ca\sounds\Characters\Noises\Damage\banz-hit-06",0.177828,1,120 },0.1 },
				 { 
				  { "ca\sounds\Characters\Noises\Damage\banz-hit-07",0.177828,1,120 },0.1 }};
				damageSounds[] = {
				 { "body",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-01",0.0562341,1,120,0.25,5,6,10 } },
				 { "body",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-02",0.0562341,1,120,0.25,5,7.5,10 } },
				 { "body",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-03",0.0562341,1,120,0.25,5,6,10 } },
				 { "body",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-04",0.0562341,1,120,0.25,5,7.5,10 } },
				 { "hands",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-07-arm",0.0562341,1,120,0.5,0,2.5,5 } },
				 { "hands",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-08-arm",0.0562341,1,120,0.5,0,2.5,5 } },
				 { "legs",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-05-leg",0.0562341,1,120,0.5,0,1,2 } },
				 { "legs",
				  { "ca\sounds\Characters\Noises\Damage\banz-damage-g1-06-leg",0.0562341,1,120,0.5,0,1,2 } }};
			   };
			  };
			  class SoundBreath
			  {
			   breath0[] = {
				{ 
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run-breath-01",0.0562341,1,8 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run-breath-02",0.0562341,1,8 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run-breath-03",0.0562341,1,8 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run-breath-04",0.125893,1,8 },0.25 } },
				{ 
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run2-breath-01",0.0562341,1,15 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run2-breath-02",0.0562341,1,15 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run2-breath-03",0.0562341,1,15 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-run2-breath-04",0.125893,1,15 },0.25 } },
				{ 
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-sprint-breath-01",0.1,1,20 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-sprint-breath-02",0.1,1,20 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-sprint-breath-03",0.1,1,20 },0.25 },
				 { 
				  { "\ca\sounds\Characters\Noises\Breath\hanz-sprint-breath-04",0.1,1,20 },0.25 } }};
			  };
			  class SoundGear
			  {
			   primary[] = {
				{ "walk",
				 { "",0.00177828,1,10 } },
				{ "run",
				 { "",0.00316228,1,15 } },
				{ "sprint",
				 { "",0.00562341,1,20 } }};
			   secondary[] = {
				{ "walk",
				 { "",0.00177828,1,10 } },
				{ "run",
				 { "",0.00316228,1,10 } },
				{ "sprint",
				 { "",0.00562341,1,10 } }};
			  };
			  class SoundEquipment
			  {
			   soldier[] = {
				{ "walk",
				 { "",0.00177828,1,13 } },
				{ "run",
				 { "",0.00316228,1,20 } },
				{ "sprint",
				 { "",0.00398107,1,25 } }};
			   civilian[] = {
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-01",0.177828,1,8 } },
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-02",0.177828,1,8 } },
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-03",0.177828,1,8 } },
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-04",0.177828,1,8 } },
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-05",0.177828,1,8 } },
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-06",0.177828,1,8 } },
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-07",0.177828,1,8 } },
				{ "walk",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-walk-08",0.177828,1,8 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-01",0.1,1,15 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-02",0.1,1,15 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-03",0.1,1,15 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-04",0.1,1,15 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-05",0.1,1,15 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-06",0.1,1,15 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-07",0.1,1,15 } },
				{ "run",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-run-08",0.1,1,15 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-01",0.0562341,1,20 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-02",0.0562341,1,20 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-03",0.0562341,1,20 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-04",0.0562341,1,20 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-05",0.0562341,1,20 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-06",0.0562341,1,20 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-07",0.0562341,1,20 } },
				{ "sprint",
				 { "\ca\sounds\Characters\Noises\Equipment\civil-equipment-sprint-08",0.0562341,1,20 } }};
			};
			hiddenSelections[] = {};
		};
		class smd_hooker: smd_annie
		{
			displayName = "SMD Annie";
			side = 1;
			model = "smd\smd_units\smd_bi_modded\women\smd_hooker";
		};*/
		class SMD_Sniper_D : BAF_Soldier_SniperH_W   //Desert Ghillie (Coverall) //Textures by Pliskin
		{
			side = 1;
			faction = "smd_pmc_faction";
			vehicleclass = "smd_pmc";
			displayName = "Desert Ghillie (Overall)";
			weapons[] = {"M24_des_EP1", "Throw", "Put", "ItemMap", "Binocular_Vector", "ItemWatch", "ItemRadio"};
			magazines[] = {"5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24"};
			backpack = "";
			model = "\SMD\smd_units\smd_bi_modded\smd_Ghillie_Overall";
			hiddenSelectionsTextures[] = {"\SMD\smd_units\smd_pliskins\smd_ghillie_des_coverall_co.paa"};
		};	
		class SMD_Sniper_G : SMD_Sniper_D
		{
			displayName = "Grass Ghillie (Overall)";
			weapons[] = {"M24_des_EP1", "Throw", "Put", "ItemMap", "Binocular_Vector", "ItemWatch", "ItemRadio"};
			magazines[] = {"5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24"};
			backpack = "";
			model = "\SMD\smd_units\smd_bi_modded\smd_Ghillie_Overall";
			hiddenSelectionsTextures[] = {"\ca\characters_W_baf\data\Ghillie_Overall2_co.paa"};
		};	
		class SMD_Sniper_L : SMD_Sniper_D
		{
			displayName = "Light Ghillie (Overall)";
			weapons[] = {"M24_des_EP1", "Throw", "Put", "ItemMap", "Binocular_Vector", "ItemWatch", "ItemRadio"};
			magazines[] = {"5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24"};
			backpack = "";
			model = "\SMD\smd_units\smd_bi_modded\smd_Ghillie_Overall";
			hiddenSelectionsTextures[] = {"\ca\characters_E\Ghillie\Data\ghillie_overall1_desert_co.paa"};
		};			
		class SMD_Sniper_D_Upper : GUE_Soldier_Scout  //Desert Ghillie (Upper Only) //Textures by Pliskin
		{
			side = 1;
			model = "\SMD\smd_units\smd_bi_modded\smd_Ghillie_Top";
			faction = "smd_pmc_faction";
			vehicleclass = "smd_pmc";
			displayName = "Desert Ghillie (top)";
			weapons[] = {"M24_des_EP1", "Throw", "Put", "ItemMap", "Binocular_Vector", "ItemWatch", "ItemRadio"};
			magazines[] = {"5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24"};
			backpack = "";
			primaryLanguage = "EN";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"\SMD\smd_units\smd_pliskins\smd_ghillie_des_tophalf_co.paa"};
		};	
		class SMD_Sniper_G_Upper : SMD_Sniper_D_Upper
		{
			displayName = "Grass Ghillie (top)";
			weapons[] = {"M24_des_EP1", "Throw", "Put", "ItemMap", "Binocular_Vector", "ItemWatch", "ItemRadio"};
			magazines[] = {"5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24", "5Rnd_762x51_M24"};
			backpack = "";
			model = "\SMD\smd_units\smd_bi_modded\smd_Ghillie_Top";
			hiddenSelectionsTextures[] = {"\ca\characters_E\Ghillie\Data\Ghillie_Top_desert_co.paa"};
		};	
	};
//};