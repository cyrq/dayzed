////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 4.13
//Tue Dec 31 02:18:31 2013 : Source 'file' date Tue Dec 31 02:18:31 2013
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

class CfgPatches
{
	class RH_m40
	{
		//units[] = {"RHm40ammobox"};
		weapons[] = {"RH_M40A3","RH_M40A5"/*,"RH_M40A1"*/};
		requiredAddons[] = {"CAweapons"};
		requiredVersion = 1.0;
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class RH_m40
		{
			list[] = {"RH_m40"};
		};
	};
};
class cfgWeapons
{
	class Default;
	class RifleCore;
	class Rifle;
	class M24;
	class M40A3;
	class RH_M40A3: M40A3
	{
		model = "\RH_m40\RH_m40a3.p3d";
		displayName = "M40A3";
		descriptionShort = "Bolt-action sniper rifle used by the United States Marine Corps.<br/>Magazine: 5Rnd. 7.62x51mm";
		picture = "\RH_m40\inv\m40a3.paa";
	};
	/*class RH_M40A1: M24
	{
		model = "\RH_m40\RH_m40a1.p3d";
		displayName = "M40A1";
		picture = "\RH_m40\inv\m40a1.paa";
		modelOptics = "\RH_m40\fnc_10x_round_mildot";
		opticsZoomMin = 0.029624;
		opticsZoomMax = 0.029624;
		distanceZoomMin = 329;
		distanceZoomMax = 329;
	};*/
	class RH_M40A5: M24
	{
		model = "\RH_m40\RH_m40a5.p3d";
		displayName = "M40A5";
		descriptionShort = "Bolt-action sniper rifle used by the United States Marine Corps.<br/>Magazine: 5Rnd. 7.62x51mm";
		picture = "\RH_m40\inv\m40a5.paa";
		modelOptics="\RH_m40\fnc_12x_gen2_mildot";
		opticsZoomMin=0.025180999;
		opticsZoomMax=0.081537001;
		opticsZoomInit=0.081537001;
	};
};
/*class CfgVehicles
{
	class All;
	class AllVehicles;
	class ThingEffect;
	class Land;
	class Man;
	class CAManBase;
	class ReammoBox;
	class SoldierWB;
	class USBasicAmmunitionBox: ReammoBox{};
	class RHm40ammobox: USBasicAmmunitionBox
	{
		displayName = "RH M40 Ammo Box";
		class TransportMagazines
		{
			class xx5Rnd_762x51_M24
			{
				magazine = "5Rnd_762x51_M24";
				count = 75;
			};
		};
		class TransportWeapons
		{
			class xxRH_m40a1
			{
				weapon = "RH_m40a1";
				count = 5;
			};
			class xxRH_m40a3
			{
				weapon = "RH_m40a3";
				count = 5;
			};
			class xxRH_m40a5
			{
				weapon = "RH_m40a5";
				count = 5;
			};
		};
	};
};
//};
