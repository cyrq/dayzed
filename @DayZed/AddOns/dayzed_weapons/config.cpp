class CfgPatches {
	class dayzed_weapons {
		Units[] = {};
		requiredVersion = "0.2.9.8";
		requiredAddons[] = {};
		DayZedV = "0.2.9.8";
			class CfgAK {
				#include "CfgAK\config.cpp"
			};
			class CfgBWC {
				#include "CfgBWC\config.cpp"
			};
			class CfgFAMAS {
				#include "CfgFAMAS\config.cpp"
			};
			class CfgG3 {
				#include "CfgG3\config.cpp"
			};
			class CfgHK {
				#include "CfgHK\config.cpp"
			};
			class CfgM4 {
				#include "CfgM4\config.cpp"
			};
			class CfgM14 {
				#include "CfgM14\config.cpp"
			};
			class CfgM40 {
				#include "CfgM40\config.cpp"
			};
			class CfgMISC {
				#include "CfgMISC\config.cpp"
			};
			class CfgSEC {
				#include "CfgSEC\config.cpp"
			};
			class CfgSMG {
				#include "CfgSMG\config.cpp"
			};
			class CfgM200 {
				#include "CfgM200\config.cpp"
			};
	};
};