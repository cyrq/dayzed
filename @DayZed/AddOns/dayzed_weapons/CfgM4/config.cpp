////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 4.13
//Fri Oct 11 14:39:35 2013 : Source 'file' date Fri Oct 11 14:39:35 2013
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

class CfgPatches
{
	class RH_m4_cfg
	{
		//units[] = {"RHm4m16ammobox"};
		weapons[] = {"RH_m16a2","RH_m4","RH_m4r","RH_m4aim","RH_m4eotech","RH_m4acog","RH_m4a1r","RH_m4sbr","RH_m4sbraim","RH_m4sbreotech","RH_m4sbracog","RH_m16a4","RH_m16a4gl","RH_m16a4acog","RH_m16a4glacog","RH_m16a4aim","RH_m16a4glaim","RH_m16a4eotech","RH_m16a4gleotech","RH_M4sdaim_wdl","RH_mk12","RH_M249","RH_M249elcan","RH_Mk48mod1","RH_Mk48mod1elcan","RH_m4a1","RH_m4a1aim","RH_m4a1eotech","RH_m4a1acog","RH_m4sd","RH_m4sdaim","RH_m4sdeotech","RH_m4gl","RH_m4glaim","RH_m4glaeotech","RH_m4glacog","RH_m4sdgl","RH_m4sdglaim","RH_m4sdgleotech"/*,"RH_ar10","RH_ar10s","RH_m4sdacog","RH_m4sdglacog","RH_m4m","RH_m4maim","RH_m4meotech","RH_m4macog","RH_M4aim_wdl","RH_M4gleotech_wdl","RH_M4sdgleotech_wdl","RH_m16a1","RH_m16a1gl","RH_m16a1s","RH_m16a1sgl","RH_m16a2gl","RH_m16a2aim","RH_m16a2glaim","RH_m16a2s","RH_m16a2sgl","RH_m16a3","RH_m16a3gl","RH_m16a3aim","RH_m16a3s","RH_m16a3sgl","RH_m16a3c","RH_m16a3cs","RH_mk12mod1","RH_mk12sd","RH_mk12mod1sd","RH_M249acog","RH_M249p","RH_M249pacog","RH_M249pelcan","RH_Mk48mod1acog"*/};
		requiredVersion = 1.52;
		requiredAddons[] = {};
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class RH_m4_cfg
		{
			list[] = {"RH_m4_cfg"};
		};
	};
};
class CfgRecoils
{
	RH_assaultRiflesBase[] = {0,0.005,0.005,0.005,0.01,0.011,0.09,0.005,-0.0005,0.14,0,0};
	RH_assaultRiflesBaseProne[] = {0,0.005,0.005,0.005,0.01,0.009,0.075,0.005,-0.0003,0.13,0,0};
	RH_M4A1Recoil[] = {0,0.005,0.005,0.005,0.01,0.011,0.09,0.005,-0.0004,0.13,0,0};
	RH_M203GLBase[] = {0,0.03,0.05,0.13,0.01,-0.001,0.26,0,0};
	RH_M249_Recoil[] = {0,0.006,-0.003,0.05,0.006,0.006,0.03,0.006,0.006,0.17,0,0};
	RH_M249_RecoilProne[] = {0,0.005,0.002,0.07,0.005,-0.0015,0.12,0,0};
	RH_Mk48_Recoil[] = {0,0.01,0.002,0.05,0.01,0.003,0.04,0.006,0.02,0.17,0,0};
	RH_Mk48_RecoilProne[] = {0,0.01,0.0012,0.09,0.009,-0.001,0.15,0,0};
};
class CfgMagazines
{
	class Default;
	class CA_Magazine: Default{};
	class 20Rnd_762x51_FNFAL: CA_Magazine{};
	class RH_20Rnd_762x51_AR10: 20Rnd_762x51_FNFAL
	{
		displayName = "20Rnd. AR-10";
		descriptionShort = "Caliber: 7.62x51mm NATO<br/>Rounds: 20<br/>Used in: AR-10";
		picture = "\rh_m4\inv\m_ar10.paa";
		initSpeed = 820;
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class CfgWeapons
{
	class Default;
	class RifleCore;
	class GrenadeLauncher: Default{};
	class Rifle: RifleCore
	{
		class M203Muzzle: GrenadeLauncher{};
	};
	class M16_base: Rifle
	{
		magazines[] = {"30Rnd_556x45_Stanag"/*, "20Rnd_556x45_Stanag", "30Rnd_556x45_G36"*/};
		class Single: Mode_SemiAuto{};
		class Burst: Mode_Burst{};
		class FullAuto: Mode_FullAuto{};
		class RH_M203Muzzle: M203Muzzle
		{
			sound[] = {"\RH_m4\sound\M203.wss",0.1,1,400};
			reloadMagazineSound[] = {"\RH_m4\sound\M203_Reload.wss",0.000316228,1,20};
			recoil = "RH_M203GLbase";
			minRangeProbab = 0;
			midRangeProbab = 0;
			maxRangeProbab = 0;
		};
		class RH_M203Muzzle_AI: RH_M203Muzzle
		{
			cameraDir = "";
			memoryPointCamera = "";
			showToPlayer = 0;
			minRangeProbab = 0.5;
			midRangeProbab = 0.8;
			maxRangeProbab = 0.1;
		};
	};
	class M16A2: M16_base{};
	/*class RH_M16a1: M16A2
	{
		displayName = "M16A1";
		model = "\RH_m4\RH_m16a1.p3d";
		picture = "\RH_m4\inv\m16a1.paa";
		reloadMagazineSound[] = {"\RH_m4\sound\M4_Reload.wss",0.056234,1,20};
		handAnim[] = {};
		modes[] = {"Single","FullAuto"};
		ace_weight = 2.9;
		dexterity = 1.8;
		irDistance = 0;
		class Single: Single
		{
			begin1[] = {"\RH_m4\sound\m16a1.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.08;
			recoil = "RH_assaultRiflesBase";
			recoilProne = "RH_assaultRiflesBaseProne";
			dispersion = 0.002;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 400;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 200;
			midRangeProbab = 0.5;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_m4\sound\m16a1.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.08;
			recoil = "RH_assaultRiflesBase";
			recoilProne = "RH_assaultRiflesBaseProne";
			dispersion = 0.002;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 100;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 80;
			maxRangeProbab = 0.05;
		};
	};
	class RH_M16a1s: RH_M16a1
	{
		displayName = "M16A1 Scope";
		model = "\RH_m4\RH_M16a1s.p3d";
		picture = "\RH_m4\inv\m16a1s.paa";
		modelOptics = "\RH_m4\NWD_colt_4x20.p3d";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 3.4;
		dexterity = 1.6;
		class Single: Single
		{
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 600;
		};
	};
	class RH_M16a1gl: RH_M16a1
	{
		displayName = "M16A1 M203";
		model = "\RH_m4\RH_m16a1gl.p3d";
		picture = "\RH_m4\inv\m16a1gl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 4.3;
		dexterity = 1.4;
		descriptionShort = "Assault rifle with grenade launcher<br/>Caliber: 5.56x45mm NATO";
	};
	class RH_M16a1sgl: RH_M16a1gl
	{
		displayName = "M16A1 M203 Scope";
		model = "\RH_m4\RH_m16a1sgl.p3d";
		picture = "\RH_m4\inv\m16a1sgl.paa";
		muzzles[] = {"RH_M16a1sMuzzle","RH_M203Muzzle","RH_M203Muzzle_AI"};
		class RH_M16a1sMuzzle: RH_M16a1s{};
		ace_weight = 4.7;
		dexterity = 1.3;
	};*/
	class RH_m16a2: M16A2
	{
		model = "\RH_m4\RH_m16a2.p3d";
		picture = "\RH_m4\inv\m16a2.paa";
		reloadMagazineSound[] = {"\RH_m4\sound\M4_Reload.wss",0.056234,1,20};
		handAnim[] = {};
		ace_weight = 3.5;
		dexterity = 1.6;
		irDistance = 0;
		class Single: Single
		{
			begin1[] = {"\RH_m4\sound\m16s.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			recoil = "RH_assaultRiflesBase";
			recoilProne = "RH_assaultRiflesBaseProne";
			dispersion = 0.00175;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 400;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 200;
			midRangeProbab = 0.5;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		class Burst: Burst
		{
			begin1[] = {"\RH_m4\sound\m16s.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			recoil = "RH_assaultRiflesBase";
			recoilProne = "RH_assaultRiflesBaseProne";
			dispersion = 0.00175;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 200;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 120;
			maxRangeProbab = 0.05;
		};
	};
	/*class RH_M16a2gl: RH_m16a2
	{
		displayName = "M16A2 M203";
		model = "\RH_m4\RH_m16a2gl.p3d";
		picture = "\RH_m4\inv\m16a2gl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 4.9;
		dexterity = 1.3;
		descriptionShort = "Assault rifle with grenade launcher<br/>Caliber: 5.56x45mm NATO";
	};
	class RH_m16a2aim: RH_m16a2
	{
		displayName = "M16A2 Aimpoint";
		model = "\RH_m4\RH_m16a2aim.p3d";
		picture = "\RH_m4\inv\m16a2aim.paa";
		ace_weight = 3.8;
		dexterity = 1.5;
	};
	class RH_M16a2glaim: RH_M16a2gl
	{
		displayName = "M16A2 M203 Aimpoint";
		model = "\RH_m4\RH_m16a2glaim.p3d";
		picture = "\RH_m4\inv\m16a2glaim.paa";
		ace_weight = 5.1;
		dexterity = 1.2;
	};
	class RH_M16a2s: RH_m16a2
	{
		displayName = "M16A2 Scope";
		model = "\RH_m4\RH_M16a2s.p3d";
		picture = "\RH_m4\inv\m16a2s.paa";
		modelOptics = "\RH_m4\NWD_colt_4x20.p3d";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 4.0;
		dexterity = 1.4;
		class Single: Single
		{
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 600;
		};
	};
	class RH_M16a2sgl: RH_M16a2s
	{
		displayName = "M16A2 M203 Scope";
		model = "\RH_m4\RH_m16a2sgl.p3d";
		picture = "\RH_m4\inv\m16a2sgl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 5.3;
		dexterity = 1.2;
		descriptionShort = "Assault rifle with grenade launcher<br/>Caliber: 5.56x45mm NATO";
	};
	class RH_M16a3: RH_M16a1
	{
		displayName = "M16A3";
		model = "\RH_m4\RH_m16a3.p3d";
		picture = "\RH_m4\inv\m16a3.paa";
		ace_weight = 3.5;
		dexterity = 1.6;
		class Single: Single
		{
			begin1[] = {"\RH_m4\sound\m16s.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			dispersion = 0.00175;
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_m4\sound\m16s.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			dispersion = 0.00175;
		};
	};
	class RH_M16a3c: RH_M16a3
	{
		model = "\RH_m4\RH_M16a3c.p3d";
		picture = "\RH_m4\inv\M16a3c.paa";
	};
	class RH_M16a3gl: RH_M16a3
	{
		displayName = "M16A3 M203";
		model = "\RH_m4\RH_m16a2gl.p3d";
		picture = "\RH_m4\inv\m16a2gl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 4.9;
		dexterity = 1.3;
		descriptionShort = "Assault rifle with grenade launcher<br/>Caliber: 5.56x45mm NATO";
	};
	class RH_M16a3aim: RH_M16a3
	{
		displayName = "M16A3 Aimpoint";
		model = "\RH_m4\RH_M16a3aim.p3d";
		picture = "\RH_m4\inv\M16a3aim.paa";
		ace_weight = 3.8;
		dexterity = 1.5;
	};
	class RH_M16a3s: RH_M16a3
	{
		displayName = "M16A3 Scope";
		model = "\RH_m4\RH_M16a3s.p3d";
		picture = "\RH_m4\inv\M16a3s.paa";
		modelOptics = "\RH_m4\NWD_colt_4x20.p3d";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 4.0;
		dexterity = 1.4;
		class Single: Single
		{
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 600;
		};
	};
	class RH_M16a3cs: RH_M16a3s
	{
		model = "\RH_m4\RH_M16a3cs.p3d";
		picture = "\RH_m4\inv\M16a3cs.paa";
	};
	class RH_M16a3sgl: RH_M16a3s
	{
		displayName = "M16A3 M203 Scope";
		model = "\RH_m4\RH_M16a3sgl.p3d";
		picture = "\RH_m4\inv\M16a3sgl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 5.3;
		dexterity = 1.2;
		descriptionShort = "Assault rifle with grenade launcher<br/>Caliber: 5.56x45mm NATO";
	};*/
	class RH_M16a4: RH_m16a2
	{
		displayName = "M16A4";
		descriptionShort="5.56x45mm caliber assault rifle.<br/>Magazine: 30Rnd. Stanag Mag.";
		model = "\RH_m4\RH_m16a4.p3d";
		picture = "\RH_m4\inv\m16a4.paa";
		magazines[] = {"30Rnd_556x45_Stanag"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons_E\SCAR\Data\Anim\SCAR.rtm"};
		irDistance = 0;
		ace_weight = 3.8;
		dexterity = 1.6;
	};
	class RH_M16A4aim: RH_M16a4
	{
		displayName = "M16A4 Aimpoint";
		model = "\RH_m4\RH_m16a4aim.p3d";
		picture = "\RH_m4\inv\m16a4aim.paa";
		ace_weight = 4.3;
		dexterity = 1.5;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	class RH_M16A4eotech: RH_M16a4
	{
		displayName = "M16A4 EOTech";
		model = "\RH_m4\RH_m16a4eotech.p3d";
		picture = "\RH_m4\inv\m16a4eotech.paa";
		ace_weight = 4.2;
		dexterity = 1.5;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
		};
	};
	class RH_M16A4acog: RH_M16a4
	{
		displayName = "M16A4 ACOG";
		model = "\RH_m4\RH_m16a4acog.p3d";
		picture = "\RH_m4\inv\m16a4acog.paa";
		modelOptics = "\RH_m4\fnc_acog_ta31rco";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 4.2;
		dexterity = 1.5;
		class Single: Single
		{
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 600;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove ACOG Scope";
				script = "spawn player_removeACOG;";
			};
		};
	};
	class RH_M16a4gl: RH_M16a4
	{
		displayName = "M16A4 M203";
		model = "\RH_m4\RH_m16a4gl.p3d";
		picture = "\RH_m4\inv\m16a4gl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 5.1;
		dexterity = 1.2;
		class ItemActions
		{
			class Use
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};		
	};
	class RH_M16A4glaim: RH_M16a4gl
	{
		displayName = "M16A4 M203 Aimpoint";
		model = "\RH_m4\RH_M16A4glaim.p3d";
		picture = "\RH_m4\inv\m16a4glaim.paa";
		ace_weight = 5.6;
		dexterity = 1.2;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
			class Use2
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};		
	};
	class RH_M16A4gleotech: RH_M16a4gl
	{
		displayName = "M16A4 M203 EOTech";
		model = "\RH_m4\RH_M16A4gleotech.p3d";
		picture = "\RH_m4\inv\m16a4glaim.paa";
		ace_weight = 5.5;
		dexterity = 1.2;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
			class Use2
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};	
	};
	class RH_M16A4glacog: RH_M16A4acog
	{
		displayName = "M16A4 M203 ACOG";
		model = "\RH_m4\RH_m16a4glacog.p3d";
		picture = "\RH_m4\inv\m16a4glacog.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 5.4;
		dexterity = 1.2;
		class ItemActions
		{
			class Use
			{
				text = "Remove ACOG Scope";
				script = "spawn player_removeACOG;";
			};
			class Use2
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};	
	};
	/*class FN_FAL: M16A2
	{
		class Single;
	};
	class RH_ar10: FN_FAL
	{
		displayName = "AR-10";
		model = "\RH_m4\RH_AR10.p3d";
		picture = "\RH_m4\inv\ar10.paa";
		magazines[] = {"RH_20Rnd_762x51_AR10"};
		reloadMagazineSound[] = {"\RH_m4\sound\ar10_reload.wss",0.056234,1,20};
		handAnim[] = {};
		modes[] = {"Single","FullAuto"};
		ace_weight = 3.3;
		dexterity = 1.6;
		irDistance = 0;
		class Single: Single
		{
			reloadTime = 0.085;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 400;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 200;
			midRangeProbab = 0.5;
			maxRange = 400;
			maxRangeProbab = 0.05;
			begin1[] = {"\RH_m4\sound\ar10.wss",2.5,1,2000};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: Single
		{
			autoFire = 1;
			displayName = "$STR_DN_MODE_FULLAUTO";
			aiRateOfFire = 2;
			aiRateOfFireDistance = 100;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class Library
		{
			libTextDesc = "";
		};
	};
	class RH_ar10s: RH_ar10
	{
		displayName = "AR-10 Scope";
		model = "\RH_m4\RH_AR10s.p3d";
		picture = "\RH_m4\inv\ar10s.paa";
		modelOptics = "\RH_m4\NWD_colt_4x20.p3d";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 3.8;
		dexterity = 1.5;
		class Single: Single
		{
			aiRateOfFire = 3;
			aiRateOfFireDistance = 700;
			maxRange = 700;
		};
	};*/
	class M4A1: M16_base
	{
		class Single: Single{};
		class Burst: Burst{};
		class FullAuto: FullAuto{};
	};
	class RH_M4: M4A1
	{
		displayName = "M4";
		descriptionShort="Fully automatic variant of the basic M4 carbine intended for special operations use.<br/>Magazine: 30Rnd. Stanag Mag.";
		model = "\RH_m4\RH_m4a1.p3d";
		picture = "\RH_m4\inv\m4a1.paa";
		magazines[] = {"30Rnd_556x45_Stanag"};
		reloadMagazineSound[] = {"\RH_m4\sound\M4_Reload.wss",0.056234,1,20};
		handAnim[] = {};
		modes[] = {"Single","Burst"};
		ace_weight = 2.7;
		dexterity = 1.9;
		irDistance = 0;
		class Single: Single
		{
			begin1[] = {"\RH_m4\sound\m4.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.075;
			dispersion = 0.00225;
			recoil = "RH_M4A1Recoil";
			recoilProne = "RH_assaultRiflesBaseProne";
			aiRateOfFire = 1;
			aiRateOfFireDistance = 400;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 200;
			midRangeProbab = 0.5;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		class Burst: Burst
		{
			begin1[] = {"\RH_m4\sound\m4.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.075;
			dispersion = 0.00225;
			recoil = "RH_M4A1Recoil";
			recoilProne = "RH_assaultRiflesBaseProne";
			aiRateOfFire = 1;
			aiRateOfFireDistance = 200;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 120;
			maxRangeProbab = 0.05;
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_m4\sound\m4.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.075;
			dispersion = 0.00225;
			recoil = "RH_M4A1Recoil";
			recoilProne = "RH_assaultRiflesBaseProne";
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 100;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 80;
			maxRangeProbab = 0.05;
		};
	};
	class RH_M4a1: RH_M4
	{
		displayName = "M4A1";
		modes[] = {"Single","FullAuto"};
	};
	class RH_M4r: RH_M4
	{
		displayName = "M4 RIS";
		model = "\RH_m4\RH_m4.p3d";
		picture = "\RH_m4\inv\m4.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons_E\SCAR\Data\Anim\SCAR.rtm"};
		ace_weight = 2.8;
	};
	class RH_M4a1r: RH_M4r
	{
		displayName = "M4A1 RIS";
		modes[] = {"Single","FullAuto"};
	};
	class RH_M4aim: RH_M4r
	{
		displayName = "M4 Aimpoint";
		model = "\RH_m4\RH_m4aim.p3d";
		picture = "\RH_m4\inv\m4a1aim.paa";
		irDistance = 0;
		ace_weight = 3.5;
		dexterity = 1.7;
	};
	class RH_M4a1aim: RH_M4aim
	{
		displayName = "M4A1 Aimpoint";
		modes[] = {"Single","FullAuto"};
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	class RH_M4eotech: RH_M4aim
	{
		displayName = "M4 EOTech";
		model = "\RH_m4\RH_m4eotech.p3d";
		picture = "\RH_m4\inv\m4a1eotech.paa";
		ace_weight = 3.4;		
	};
	class RH_M4a1eotech: RH_M4eotech
	{
		displayName = "M4A1 EOTech";
		modes[] = {"Single","FullAuto"};
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
		};	
	};
	class RH_M4acog: RH_M4aim
	{
		displayName = "M4 ACOG";
		model = "\RH_m4\RH_m4acog.p3d";
		picture = "\RH_m4\inv\m4a1acog.paa";
		modelOptics = "\RH_m4\fnc_acog_ta31rco.p3d";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 3.3;
		dexterity = 1.7;
		class Single: Single
		{
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 600;
		};
	};
	class RH_M4a1acog: RH_M4acog
	{
		displayName = "M4A1 ACOG";
		modes[] = {"Single","FullAuto"};
		class ItemActions
		{
			class Use
			{
				text = "Remove ACOG Scope";
				script = "spawn player_removeACOG;";
			};
		};	
	};
	class RH_M4gl: RH_M4
	{
		displayName = "M4 M203";
		model = "\RH_m4\RH_m4gl.p3d";
		picture = "\RH_m4\inv\m4a1gl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 4.0;
		dexterity = 1.4;
	};
	class RH_M4a1gl: RH_M4gl
	{
		displayName = "M4A1 M203";
		modes[] = {"Single","FullAuto"};
		class ItemActions
		{
			class Use
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};		
	};
	class RH_M4glaim: RH_M4gl
	{
		displayName = "M4 M203 Aimpoint";
		model = "\RH_m4\RH_m4glaim.p3d";
		picture = "\RH_m4\inv\m4a1glaim.paa";
		irDistance = 0;
		ace_weight = 4.7;
		dexterity = 1.3;
	};
	class RH_M4a1glaim: RH_M4glaim
	{
		displayName = "M4A1 M203 Aimpoint";
		modes[] = {"Single","FullAuto"};
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
			class Use2
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};
	};
	class RH_M4gleotech: RH_M4glaim
	{
		displayName = "M4 M203 EOTech";
		model = "\RH_m4\RH_m4gleotech.p3d";
		picture = "\RH_m4\inv\m4a1gleotech.paa";
	};
	class RH_M4a1gleotech: RH_M4gleotech
	{
		displayName = "M4A1 M203 EOTech";
		modes[] = {"Single","FullAuto"};
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
			class Use2
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};
	};
	class RH_M4glacog: RH_M4acog
	{
		displayName = "M4 M203 ACOG";
		model = "\RH_m4\RH_m4glacog.p3d";
		picture = "\RH_m4\inv\m4a1glacog.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 4.6;
		dexterity = 1.3;
	};
	class RH_M4a1glacog: RH_M4glacog
	{
		displayName = "M4A1 M203 ACOG";
		modes[] = {"Single","FullAuto"};
		class ItemActions
		{
			class Use
			{
				text = "Remove ACOG Scope";
				script = "spawn player_removeACOG;";
			};
			class Use2
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};
	};
	class RH_M4sd: RH_M4a1r
	{
		displayName = "M4A1 SD";
		model = "\RH_m4\RH_m4sd.p3d";
		picture = "\RH_m4\inv\m4a1sd.paa";
		magazines[] = {"30Rnd_556x45_StanagSD"/*, "30Rnd_556x45_G36SD"*/};
		irDistance = 0;
		ace_weight = 3.9;
		dexterity = 1.6;
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		ace_suppressed = 1;
		descriptionShort = "M4A1 suppressed assault rifle.<br/>Magazine: 30Rnd. StanagSD Mag.";
		class Single: Single
		{
			begin1[] = {"\RH_m4\sound\m4sd.wss",1,1,120};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_m4\sound\m4sd.wss",1,1,120};
			soundBegin[] = {"begin1",1};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_M4sdaim: RH_M4sd
	{
		displayName = "M4A1 SD Aimpoint";
		model = "\RH_m4\RH_m4sdaim.p3d";
		picture = "\RH_m4\inv\m4a1sdaim.paa";
		magazines[] = {"30Rnd_556x45_StanagSD"/*, "30Rnd_556x45_G36SD"*/};
		ace_weight = 4.4;
		dexterity = 1.5;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_M4sdeotech: RH_M4sdaim
	{
		displayName = "M4A1 SD EOTech";
		model = "\RH_m4\RH_m4sdeotech.p3d";
		picture = "\RH_m4\inv\m4a1sdeotech.paa";
		magazines[] = {"30Rnd_556x45_StanagSD"/*, "30Rnd_556x45_G36SD"*/};
		handAnim[] = {"OFP2_ManSkeleton","\rh_m4\anim\RH_m4t.rtm"};
		ace_weight = 4.3;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	/*class RH_M4sdacog: RH_M4sd
	{
		displayName = "M4A1 SD ACOG";
		model = "\RH_m4\RH_m4sdacog.p3d";
		picture = "\RH_m4\inv\m4a1sdacog.paa";
		magazines[] = {"30Rnd_556x45_StanagSD", "30Rnd_556x45_G36SD"};
		modelOptics = "\RH_m4\fnc_acog_ta31rco";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 4.2;
		dexterity = 1.5;
		class Single: Single
		{
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 600;
		};
	};*/
	class RH_M4sdgl: RH_M4sd
	{
		displayName = "M4A1 M203 SD";
		model = "\RH_m4\RH_m4sdgl.p3d";
		picture = "\RH_m4\inv\m4a1sdgl.paa";
		magazines[] = {"30Rnd_556x45_StanagSD"/*, "30Rnd_556x45_G36SD"*/};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 4.7;
		dexterity = 1.3;
		descriptionShort = "M4A1 suppressed assault rifle.<br/>Magazine: 30Rnd. StanagSD Mag.";
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
			class Use2
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};
	};
	class RH_M4sdglaim: RH_M4sdgl
	{
		displayName = "M4A1 M203 SD Aimpoint";
		model = "\RH_m4\RH_m4sdglaim.p3d";
		picture = "\RH_m4\inv\m4a1sdglaim.paa";
		magazines[] = {"30Rnd_556x45_StanagSD"/*, "30Rnd_556x45_G36SD"*/};
		irDistance = 0;
		ace_weight = 5.4;
		dexterity = 1.2;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
			class Use3
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};
	};
	class RH_M4sdgleotech: RH_M4sdglaim
	{
		displayName = "M4A1 M203 SD EOTech";
		model = "\RH_m4\RH_m4sdgleotech.p3d";
		picture = "\RH_m4\inv\m4a1sdgleotech.paa";
		magazines[] = {"30Rnd_556x45_StanagSD"/*, "30Rnd_556x45_G36SD"*/};
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
			class Use3
			{
				text = "Remove M203 Launcher";
				script = "spawn player_removeM203;";
			};
		};
	};
	/*class RH_M4sdglacog: RH_M4sdacog
	{
		displayName = "M4A1 M203 SD ACOG";
		model = "\RH_m4\RH_m4sdglacog.p3d";
		picture = "\RH_m4\inv\m4a1sdglacog.paa";
		magazines[] = {"30Rnd_556x45_StanagSD", "30Rnd_556x45_G36SD"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"this","RH_M203Muzzle","RH_M203Muzzle_AI"};
		ace_weight = 5.3;
		dexterity = 1.2;
	};
	class RH_M4m: RH_M4a1
	{
		displayName = "M4A1 Magpul";
		model = "\RH_m4\RH_m4m.p3d";
		picture = "\RH_m4\inv\m4m.paa";
		handAnim[] = {"OFP2_ManSkeleton","\rh_m4\anim\RH_AFG_anim.rtm"};
		ace_weight = 2.9;
		dexterity = 1.8;
		class FlashLight
		{
			color[] = {0.9,0.9,0.7,0.9};
			ambient[] = {0.1,0.1,0.1,1.0};
			position = "flash dir";
			direction = "flash";
			angle = 30;
			scale[] = {1,1,0.5};
			brightness = 0.1;
		};
	};
	class RH_M4maim: RH_M4m
	{
		displayName = "M4A1 Magpul Aimpoint";
		model = "\RH_m4\RH_m4maim.p3d";
		picture = "\RH_m4\inv\m4maim.paa";
		ace_weight = 3.4;
		dexterity = 1.6;
	};
	class RH_M4meotech: RH_M4a1
	{
		displayName = "M4A1 Magpul EOTech";
		model = "\RH_m4\RH_m4meotech.p3d";
		picture = "\RH_m4\inv\m4meotech.paa";
		handAnim[] = {"OFP2_ManSkeleton","\rh_m4\anim\RH_AFG_anim.rtm"};
		irDistance = 150;
		ace_weight = 3.3;
		dexterity = 1.6;
	};
	class RH_M4macog: RH_M4a1acog
	{
		displayName = "M4A1 Magpul ACOG";
		model = "\RH_m4\RH_m4macog.p3d";
		picture = "\RH_m4\inv\m4macog.paa";
		handAnim[] = {"OFP2_ManSkeleton","\rh_m4\anim\RH_AFG_anim.rtm"};
		ace_weight = 3.2;
		dexterity = 1.7;
	};
	class RH_M4aim_wdl: RH_M4a1
	{
		displayName = "M4A1 Aimpoint Camo";
		model = "\RH_m4\RH_m4aim_wdl.p3d";
		picture = "\RH_m4\inv\m4a1aim_Wdl.paa";
		handAnim[] = {"OFP2_ManSkeleton","\rh_m4\anim\RH_m4t.rtm"};
		irDistance = 150;
		ace_weight = 3.3;
		dexterity = 1.7;
	};
	class RH_M4gleotech_wdl: RH_M4a1gleotech
	{
		displayName = "M4A1 M203 EOTech Camo";
		model = "\RH_m4\RH_m4gleotech_wdl.p3d";
		picture = "\RH_m4\inv\m4a1gleotech_wdl.paa";
	};*/
	class RH_M4sdaim_wdl: RH_M4sdaim
	{
		displayName = "M4A1 SD Aimpoint Camo";
		descriptionShort = "M4A1 suppressed assault rifle with woodland camouflage.<br/>Magazine: 30Rnd. StanagSD Mag.";
		model = "\RH_m4\RH_m4sdaim_wdl.p3d";
		picture = "\RH_m4\inv\m4a1sdaim_wdl.paa";
		magazines[] = {"30Rnd_556x45_StanagSD"/*, "30Rnd_556x45_G36SD"*/};
		ace_weight = 4.1;
		dexterity = 1.5;
		class ItemActions {};
	};
	/*class RH_M4sdgleotech_wdl: RH_M4sdgleotech
	{
		displayName = "M4A1 M203 SD EOTech Camo";
		model = "\RH_m4\RH_m4sdgleotech_wdl.p3d";
		picture = "\RH_m4\inv\m4a1sdgleotech_wdl.paa";
		magazines[] = {"30Rnd_556x45_StanagSD", "30Rnd_556x45_G36SD"};
	};*/
	class RH_M4sbr: RH_M4a1
	{
		displayName = "M4A1 SBR";
		model = "\RH_m4\RH_m4sbr.p3d";
		picture = "\RH_m4\inv\m4sbr.paa";
		magazines[] = {"30Rnd_556x45_Stanag"};
		handAnim[] = {"OFP2_ManSkeleton","\RH_m4\anim\RH_M4sbr.rtm"};
		ace_weight = 2.5;
		dexterity = 2.1;
		ace_mv[] = {"B_556x45_Ball",740,"ACE_B_556x45_T",740,"ACE_B_556x45_SB",680,"ACE_B_556x45_S",740};
		descriptionShort = "5.56x45mm caliber short barrel rifle.<br/>Magazine: 30Rnd. Stanag Mag.";
		class Single: Single
		{
			dispersion = 0.00275;
			midRange = 100;
			maxRange = 300;
		};
		class FullAuto: FullAuto
		{
			dispersion = 0.00275;
		};
	};
	class RH_M4sbraim: RH_M4sbr
	{
		displayName = "M4A1 SBR Aimpoint";
		model = "\RH_m4\RH_m4sbraim.p3d";
		picture = "\RH_m4\inv\m4sbraim.paa";
		ace_weight = 2.8;
		dexterity = 1.9;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	class RH_M4sbreotech: RH_M4sbr
	{
		displayName = "M4A1 SBR EOTech";
		model = "\RH_m4\RH_m4sbreotech.p3d";
		picture = "\RH_m4\inv\m4sbreotech.paa";
		ace_weight = 3.0;
		dexterity = 1.9;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
		};
	};
	class RH_M4sbracog: RH_M4sbr
	{
		displayName = "M4A1 SBR ACOG";
		model = "\RH_m4\RH_m4sbracog.p3d";
		picture = "\RH_m4\inv\m4sbracog.paa";
		modelOptics = "\RH_m4\fnc_acog_ta31rco";
		ace_weight = 2.9;
		dexterity = 1.9;
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		class Single: Single
		{
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 400;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove ACOG Scope";
				script = "spawn player_removeACOG;";
			};
		};
	};
	class M4SPR: M4A1
	{
		class Single: Single{};
	};
	class RH_MK12: M4SPR
	{
		displayName = "Mk12 Mod0";
		descriptionShort = "Special purpose rifle, designated marksman rifle.<br/>Magazine: 20Rnd. Stanag Mag.";
		model = "\RH_m4\RH_mk12.p3d";
		picture = "\RH_m4\inv\mk12.paa";
		magazines[] = {"20Rnd_556x45_Stanag"};
		handAnim[] = {};
		ace_weight = 4.0;
		dexterity = 1.4;
		modes[] = {"Single"};
		irDistance = 0;
		distanceZoomMin = 360;
		distanceZoomMax = 360;
		modelOptics = "\ca\weapons\2Dscope_MilDot_10";
		opticsZoomMin = 0.029624;
		opticsZoomMax = 0.08464;
		opticsZoomInit = 0.08464;
		discretefov[] = {0.08464,0.029624};
		discreteInitIndex = 0;
		opticsPPEffects[] = {"OpticsCHAbera2","OpticsBlur1"};
		opticsFlare = 1;
		visionMode[] = {"Normal"};
		class Single: Single
		{
			begin1[] = {"\RH_m4\sound\m4.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			dispersion = 0.0003;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			maxRange = 600;
		};
		class Library
		{
			libTextDesc = "$STR_LIB_M4_SPR";
		};
	};
	/*class RH_MK12mod1: RH_MK12
	{
		displayName = "Mk12 Mod1";
		model = "\RH_m4\RH_mk12mod1.p3d";
		picture = "\RH_m4\inv\mk12mod1.paa";
	};
	class RH_MK12sd: RH_MK12
	{
		displayName = "Mk12 Mod0 SD";
		model = "\RH_m4\RH_mk12sd.p3d";
		picture = "\RH_m4\inv\mk12sd.paa";
		magazines[] = {"30Rnd_556x45_StanagSD", "30Rnd_556x45_G36SD"};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		ace_suppressed = 1;
		ace_weight = 4.6;
		dexterity = 1.3;
		class Single: Single
		{
			begin1[] = {"\RH_m4\sound\m4sd.wss",1,1,120};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_MK12mod1sd: RH_MK12sd
	{
		displayName = "Mk12 Mod1 SD";
		model = "\RH_m4\RH_mk12mod1sd.p3d";
		picture = "\RH_m4\inv\mk12mod1sd.paa";
		magazines[] = {"30Rnd_556x45_StanagSD", "30Rnd_556x45_G36SD"};
	};*/
	class M249: Rifle
	{
		class manual: Mode_FullAuto{};
		class close: manual{};
		class short: close{};
		class medium: close{};
		class far: close{};
	};
	class RH_M249: M249
	{
		type = "1";
		scope = 2;
		displayName = "M249";
		model = "\RH_m4\RH_M249.p3d";
		picture = "\RH_m4\inv\M249.paa";
		descriptionShort = "Light machine gun.<br/>Magazine: 200Rnd. M249 Belt, 30Rnd. Stanag Mag.";
		magazines[]={"200Rnd_556x45_M249","30Rnd_556x45_Stanag"};
		reloadMagazineSound[] = {"\RH_m4\sound\M249_Reload.wss",0.056234,1,20};
		ace_weight = 7.5;
		dexterity = 1.0;
		modes[] = {"manual","close","short","medium"};
		irDistance = 0;
		weaponInfoType = "RscWeaponZeroing";
		discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
		discreteDistanceInitIndex = 3;
		class manual: manual
		{
			begin1[] = {"\RH_m4\sound\m249.wss",1.8,1,1500};
			soundBegin[] = {"begin1",1};
			dispersion = 0.0035;
			recoil = "RH_M249_Recoil";
			recoilProne = "RH_M249_RecoilProne";
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 60;
			maxRangeProbab = 0.05;
		};
		class close: close
		{
			burst = "8 + round random 4";
			aiRateOfFire = 1;
			aiRateOfFireDistance = 200;
			minRange = 50;
			minRangeProbab = 0.8;
			midRange = 100;
			midRangeProbab = 0.7;
			maxRange = 200;
			maxRangeProbab = 0.05;
		};
		class short: short
		{
			burst = "6 + round random 2";
			aiRateOfFire = 2;
			aiRateOfFireDistance = 400;
			minRange = 100;
			minRangeProbab = 0.8;
			midRange = 200;
			midRangeProbab = 0.7;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		class medium: medium
		{
			burst = "4 + round random 2";
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.8;
			midRange = 400;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.05;
		};
		class far: far
		{
			burst = "2 + round random 2";
			aiRateOfFire = 3;
			aiRateOfFireDistance = 800;
			minRange = 400;
			minRangeProbab = 0.8;
			midRange = 600;
			midRangeProbab = 0.7;
			maxRange = 800;
			maxRangeProbab = 0.05;
		};
	};
	/*class RH_M249acog: RH_M249
	{
		displayName = "M249 ACOG";
		model = "\RH_m4\RH_m249acog.p3d";
		picture = "\RH_m4\inv\m249acog.paa";
		modelOptics = "\RH_m4\fnc_acog_ta31rco";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 7.8;
		modes[] = {"manual","close","short","medium","far"};
		weaponInfoType = "RscWeaponEmpty";
	};*/
	class RH_M249elcan: RH_M249
	{
		displayName = "M249 M145";
		model = "\RH_m4\RH_m249elcan.p3d";
		picture = "\RH_m4\inv\m249elcan.paa";
		magazines[]={"200Rnd_556x45_M249","30Rnd_556x45_Stanag"};
		modelOptics = "\RH_m4\fnc_m145_m249.p3d";
		class OpticsModes
		{
			class M145
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.085333;
				opticsZoomMax = 0.085333;
				opticsZoomInit = 0.085333;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 308;
				distanceZoomMax = 308;
				cameraDir = "";
			};
			class Kolimator: M145
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 8.3;
		modes[] = {"manual","close","short","medium","far"};
		weaponInfoType = "RscWeaponEmpty";
		class ItemActions
		{
			class Use
			{
				text = "Remove ELCAN Scope";
				script = "spawn player_removeELCAN;";
			};
		};
	};
	/*class RH_M249p: RH_M249
	{
		displayName = "M249 Para";
		model = "\RH_m4\RH_m249p.p3d";
		picture = "\RH_m4\inv\m249p.paa";
		ace_weight = 7.2;
		ace_mod_mv = 1;
		ace_mv[] = {"B_556x45_Ball",880,"ACE_B_556x45_T",880,"ACE_B_556x45_SB",790};
	};
	class RH_M249pacog: RH_M249p
	{
		displayName = "M249 Para ACOG";
		model = "\RH_m4\RH_m249pacog.p3d";
		picture = "\RH_m4\inv\m249pacog.paa";
		modelOptics = "\RH_m4\fnc_acog_ta31rco";
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 7.6;
		modes[] = {"manual","close","short","medium","far"};
		weaponInfoType = "RscWeaponEmpty";
	};
	class RH_M249pelcan: RH_M249p
	{
		displayName = "M249 Para M145";
		model = "\RH_m4\RH_m249pelcan.p3d";
		picture = "\RH_m4\inv\m249pelcan.paa";
		modelOptics = "\RH_m4\fnc_m145_m249.p3d";
		irDistance = 150;
		class OpticsModes
		{
			class M145
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.085333;
				opticsZoomMax = 0.085333;
				opticsZoomInit = 0.085333;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 308;
				distanceZoomMax = 308;
				cameraDir = "";
			};
			class Kolimator: M145
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		ace_weight = 8.3;
		modes[] = {"manual","close","short","medium","far"};
		weaponInfoType = "RscWeaponEmpty";
	};*/
	class M240: Rifle
	{
		class manual: Mode_FullAuto{};
		class close: manual{};
		class short: close{};
		class medium: close{};
		class far: close{};
	};
	class Mk_48: M240
	{
		type = "1";
		class manual: manual{};
		class close: close{};
		class short: short{};
		class medium: medium{};
		class far: far{};
	};
	class RH_Mk48mod1: Mk_48
	{
		scope = 2;
		displayName = "Mk48 Mod1";
		descriptionShort = "Modified M249 light machine gun.<br/>Magazine: 100Rnd. M240 Belt";
		model = "\RH_m4\RH_Mk48mod1.p3d";
		picture = "\RH_m4\inv\Mk48mod1.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons_E\SCAR\Data\Anim\SCAR.rtm"};
		reloadMagazineSound[] = {"\RH_m4\sound\M249_Reload.wss",0.056234,1,20};
		ace_weight = 8.4;
		dexterity = 1.0;
		modes[] = {"manual","close","short","medium"};
		irDistance = 0;
		weaponInfoType = "RscWeaponZeroing";
		discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000};
		discreteDistanceInitIndex = 3;
		class manual: manual
		{
			begin1[] = {"\RH_m4\sound\Mk48.wss",2.5,1,2000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.08;
			dispersion = 0.003;
			recoil = "RH_Mk48_Recoil";
			recoilProne = "RH_Mk48_RecoilProne";
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 60;
			maxRangeProbab = 0.05;
		};
		class close: close
		{
			dispersion = 0.003;
			recoil = "RH_Mk48_Recoil";
			recoilProne = "RH_Mk48_RecoilProne";
			burst = "8 + round random 4";
			aiRateOfFire = 1;
			aiRateOfFireDistance = 200;
			minRange = 50;
			minRangeProbab = 0.8;
			midRange = 100;
			midRangeProbab = 0.7;
			maxRange = 200;
			maxRangeProbab = 0.05;
		};
		class short: short
		{
			dispersion = 0.003;
			recoil = "RH_Mk48_Recoil";
			recoilProne = "RH_Mk48_RecoilProne";
			burst = "6 + round random 2";
			aiRateOfFire = 2;
			aiRateOfFireDistance = 400;
			minRange = 100;
			minRangeProbab = 0.8;
			midRange = 200;
			midRangeProbab = 0.7;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		class medium: medium
		{
			dispersion = 0.003;
			recoil = "RH_Mk48_Recoil";
			recoilProne = "RH_Mk48_RecoilProne";
			burst = "4 + round random 2";
			aiRateOfFire = 2;
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.8;
			midRange = 400;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.05;
		};
		class far: far
		{
			dispersion = 0.003;
			recoil = "RH_Mk48_Recoil";
			recoilProne = "RH_Mk48_RecoilProne";
			burst = "2 + round random 2";
			aiRateOfFire = 3;
			aiRateOfFireDistance = 800;
			minRange = 400;
			minRangeProbab = 0.8;
			midRange = 600;
			midRangeProbab = 0.7;
			maxRange = 800;
			maxRangeProbab = 0.05;
		};
	};
	/*class RH_Mk48mod1acog: RH_Mk48mod1
	{
		displayName = "Mk48 Mod1 ACOG";
		model = "\RH_m4\RH_Mk48mod1acog.p3d";
		picture = "\RH_m4\inv\Mk48mod1acog.paa";
		modelOptics = "\RH_m4\fnc_acog_ta31rco";
		ace_weight = 9.0;
		irDistance = 150;
		class OpticsModes
		{
			class ACOG
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.071945;
				opticsZoomMax = 0.071945;
				opticsZoomInit = 0.071945;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 189;
				distanceZoomMax = 189;
				cameraDir = "";
			};
			class Kolimator: ACOG
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		modes[] = {"manual","close","short","medium","far"};
		weaponInfoType = "RscWeaponEmpty";
	};*/
	class RH_Mk48mod1elcan: RH_Mk48mod1
	{
		displayName = "Mk48 Mod1 M145";
		model = "\RH_m4\RH_Mk48mod1elcan.p3d";
		picture = "\RH_m4\inv\Mk48mod1elcan.paa";
		modelOptics = "\RH_m4\fnc_m145_m249.p3d";
		ace_weight = 9.4;
		irDistance = 0;
		class OpticsModes
		{
			class M145
			{
				opticsID = 1;
				useModelOptics = 1;
				opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
				opticsZoomMin = 0.085333;
				opticsZoomMax = 0.085333;
				opticsZoomInit = 0.085333;
				memoryPointCamera = "opticView";
				visionMode[] = {"Normal"};
				opticsFlare = 1;
				opticsDisablePeripherialVision = 1;
				distanceZoomMin = 308;
				distanceZoomMax = 308;
				cameraDir = "";
			};
			class Kolimator: M145
			{
				opticsID = 2;
				useModelOptics = 0;
				opticsFlare = 0;
				opticsDisablePeripherialVision = 0;
				opticsZoomMin = 0.25;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.5;
				memoryPointCamera = "eye";
				visionMode[] = {};
			};
		};
		modes[] = {"manual","close","short","medium","far"};
		weaponInfoType = "RscWeaponEmpty";
		class ItemActions
		{
			class Use
			{
				text = "Remove ELCAN Scope";
				script = "spawn player_removeELCAN;";
			};
		};
	};
};
/*class CfgVehicles
{
	class ReammoBox;
	class RHm4m16ammobox: ReammoBox
	{
		scope = 2;
		accuracy = 1000;
		model = "\ca\weapons\AmmoBoxes\USBasicWeapons.p3d";
		displayName = "RH M4/M16 weapons box";
		class TransportMagazines
		{
			class _xx_RH_20Rnd_762x51_AR10
			{
				magazine = "RH_20Rnd_762x51_AR10";
				count = 200;
			};
			class _xx_30Rnd_556x45_Stanag
			{
				magazine = "30Rnd_556x45_Stanag";
				count = 200;
			};
			class _xx_30Rnd_556x45_StanagSD
			{
				magazine = "30Rnd_556x45_StanagSD";
				count = 200;
			};
			class _xx_100Rnd_556x45_BetaCMag
			{
				magazine = "100Rnd_556x45_BetaCMag";
				count = 200;
			};
			class _xx_100Rnd_762x51_M240
			{
				magazine = "100Rnd_762x51_M240";
				count = 200;
			};
			class _xx_200Rnd_556x45_M249
			{
				magazine = "200Rnd_556x45_M249";
				count = 200;
			};
			class _xx_1rnd_HE_M203
			{
				magazine = "1rnd_HE_M203";
				count = 30;
			};
			class _xx_HandGrenade_West
			{
				magazine = "HandGrenade_West";
				count = 50;
			};
			class _xx_FlareWhite_M203
			{
				magazine = "FlareWhite_M203";
				count = 6;
			};
			class _xx_FlareRed_M203
			{
				magazine = "FlareRed_M203";
				count = 6;
			};
			class _xx_FlareGreen_M203
			{
				magazine = "FlareGreen_M203";
				count = 6;
			};
			class _xx_FlareYellow_M203
			{
				magazine = "FlareYellow_M203";
				count = 6;
			};
		};
		class TransportWeapons
		{
			class _xx_RH_ar10
			{
				weapon = "RH_ar10";
				count = 6;
			};
			class _xx_RH_ar10s
			{
				weapon = "RH_ar10s";
				count = 6;
			};
			class _xx_RH_M4m
			{
				weapon = "RH_M4m";
				count = 4;
			};
			class _xx_RH_M4maim
			{
				weapon = "RH_M4maim";
				count = 4;
			};
			class _xx_RH_M4meotech
			{
				weapon = "RH_M4meotech";
				count = 4;
			};
			class _xx_RH_M4macog
			{
				weapon = "RH_M4macog";
				count = 4;
			};
			class _xx_RH_M4
			{
				weapon = "RH_M4";
				count = 6;
			};
			class _xx_RH_M4A1
			{
				weapon = "RH_M4a1";
				count = 6;
			};
			class _xx_RH_M4r
			{
				weapon = "RH_M4r";
				count = 6;
			};
			class _xx_RH_M4a1r
			{
				weapon = "RH_M4a1r";
				count = 6;
			};
			class _xx_RH_M4aim
			{
				weapon = "RH_M4aim";
				count = 6;
			};
			class _xx_RH_M4a1aim
			{
				weapon = "RH_M4a1aim";
				count = 6;
			};
			class _xx_RH_M4eotech
			{
				weapon = "RH_M4eotech";
				count = 6;
			};
			class _xx_RH_M4a1eotech
			{
				weapon = "RH_M4a1eotech";
				count = 6;
			};
			class _xx_RH_M4acog
			{
				weapon = "RH_M4acog";
				count = 6;
			};
			class _xx_RH_M4a1acog
			{
				weapon = "RH_M4a1acog";
				count = 6;
			};
			class _xx_RH_M4gl
			{
				weapon = "RH_M4gl";
				count = 6;
			};
			class _xx_RH_M4a1gl
			{
				weapon = "RH_M4a1gl";
				count = 6;
			};
			class _xx_RH_M4glaim
			{
				weapon = "RH_M4glaim";
				count = 6;
			};
			class _xx_RH_M4a1glaim
			{
				weapon = "RH_M4a1glaim";
				count = 6;
			};
			class _xx_RH_M4gleotech
			{
				weapon = "RH_M4gleotech";
				count = 6;
			};
			class _xx_RH_M4a1gleotech
			{
				weapon = "RH_M4a1gleotech";
				count = 6;
			};
			class _xx_RH_M4glacog
			{
				weapon = "RH_M4glacog";
				count = 6;
			};
			class _xx_RH_M4a1glacog
			{
				weapon = "RH_M4a1glacog";
				count = 6;
			};
			class _xx_RH_M4sd
			{
				weapon = "RH_M4sd";
				count = 6;
			};
			class _xx_RH_M4sdaim
			{
				weapon = "RH_M4sdaim";
				count = 6;
			};
			class _xx_RH_M4sdeotech
			{
				weapon = "RH_M4sdeotech";
				count = 6;
			};
			class _xx_RH_M4sdacog
			{
				weapon = "RH_M4sdacog";
				count = 6;
			};
			class _xx_RH_M4sdgl
			{
				weapon = "RH_M4sdgl";
				count = 6;
			};
			class _xx_RH_sdglaim
			{
				weapon = "RH_M4sdglaim";
				count = 6;
			};
			class _xx_RH_M4sdglLeotech
			{
				weapon = "RH_M4sdgleotech";
				count = 6;
			};
			class _xx_RH_M4sdglacog
			{
				weapon = "RH_M4sdglacog";
				count = 6;
			};
			class _xx_RH_M4aim_wdl
			{
				weapon = "RH_M4aim_wdl";
				count = 6;
			};
			class _xx_RH_M4gleotech_wdl
			{
				weapon = "RH_M4gleotech_wdl";
				count = 6;
			};
			class _xx_RH_M4sdaim_wdl
			{
				weapon = "RH_M4sdaim_wdl";
				count = 6;
			};
			class _xx_RH_M4sdgleotech_wdl
			{
				weapon = "RH_M4sdgleotech_wdl";
				count = 6;
			};
			class _xx_RH_M4sbr
			{
				weapon = "RH_M4sbr";
				count = 6;
			};
			class _xx_RH_M4sbraim
			{
				weapon = "RH_M4sbraim";
				count = 6;
			};
			class _xx_RH_M4sbreotech
			{
				weapon = "RH_M4sbreotech";
				count = 6;
			};
			class _xx_RH_M4sbracog
			{
				weapon = "RH_M4sbracog";
				count = 6;
			};
			class _xx_RH_M16A1
			{
				weapon = "RH_M16A1";
				count = 6;
			};
			class _xx_RH_M16A1GL
			{
				weapon = "RH_M16A1GL";
				count = 6;
			};
			class _xx_RH_M16A1s
			{
				weapon = "RH_M16A1s";
				count = 6;
			};
			class _xx_RH_M16A1sGL
			{
				weapon = "RH_M16A1sGL";
				count = 6;
			};
			class _xx_RH_M16A2
			{
				weapon = "RH_M16A2";
				count = 6;
			};
			class _xx_RH_M16A2GL
			{
				weapon = "RH_M16A2GL";
				count = 6;
			};
			class _xx_RH_M16A2aim
			{
				weapon = "RH_M16A2aim";
				count = 6;
			};
			class _xx_RH_M16A2GLaim
			{
				weapon = "RH_M16A2GLaim";
				count = 6;
			};
			class _xx_RH_M16A2s
			{
				weapon = "RH_M16A2s";
				count = 6;
			};
			class _xx_RH_M16A2sGL
			{
				weapon = "RH_M16A2sGL";
				count = 6;
			};
			class _xx_RH_M16A3
			{
				weapon = "RH_M16A3";
				count = 6;
			};
			class _xx_RH_M16A3GL
			{
				weapon = "RH_M16A3GL";
				count = 6;
			};
			class _xx_RH_M16A3aim
			{
				weapon = "RH_M16A3aim";
				count = 6;
			};
			class _xx_RH_M16A3s
			{
				weapon = "RH_M16A3s";
				count = 6;
			};
			class _xx_RH_M16A3sGL
			{
				weapon = "RH_M16A3sGL";
				count = 6;
			};
			class _xx_RH_M16A3c
			{
				weapon = "RH_M16A3c";
				count = 6;
			};
			class _xx_RH_M16A3cs
			{
				weapon = "RH_M16A3cs";
				count = 6;
			};
			class _xx_RH_M16A4
			{
				weapon = "RH_M16A4";
				count = 6;
			};
			class _xx_RH_M16A4aim
			{
				weapon = "RH_M16A4aim";
				count = 6;
			};
			class _xx_RH_M16A4acog
			{
				weapon = "RH_M16A4acog";
				count = 6;
			};
			class _xx_RH_M16A4GL
			{
				weapon = "RH_M16A4GL";
				count = 6;
			};
			class _xx_RH_M16A4eotech
			{
				weapon = "RH_M16A4eotech";
				count = 6;
			};
			class _xx_RH_M16A4glacog
			{
				weapon = "RH_M16A4glacog";
				count = 6;
			};
			class _xx_RH_M16A4GLaim
			{
				weapon = "RH_M16A4GLaim";
				count = 6;
			};
			class _xx_RH_M16A4GLeotech
			{
				weapon = "RH_M16A4GLeotech";
				count = 6;
			};
			class _xx_RH_Mk12
			{
				weapon = "RH_Mk12";
				count = 6;
			};
			class _xx_RH_Mk12mod1
			{
				weapon = "RH_Mk12mod1";
				count = 6;
			};
			class _xx_RH_Mk12sd
			{
				weapon = "RH_Mk12sd";
				count = 6;
			};
			class _xx_RH_Mk12mod1sd
			{
				weapon = "RH_Mk12mod1sd";
				count = 6;
			};
			class _xx_RH_M249
			{
				weapon = "RH_M249";
				count = 6;
			};
			class _xx_RH_M249acog
			{
				weapon = "RH_M249acog";
				count = 6;
			};
			class _xx_RH_M249elcan
			{
				weapon = "RH_M249elcan";
				count = 6;
			};
			class _xx_RH_M249p
			{
				weapon = "RH_M249p";
				count = 6;
			};
			class _xx_RH_M249pacog
			{
				weapon = "RH_M249pacog";
				count = 6;
			};
			class _xx_RH_M249pelcan
			{
				weapon = "RH_M249pelcan";
				count = 6;
			};
			class _xx_RH_Mk48mod1
			{
				weapon = "RH_Mk48mod1";
				count = 6;
			};
			class _xx_RH_Mk48mod1acog
			{
				weapon = "RH_Mk48mod1acog";
				count = 6;
			};
			class _xx_RH_Mk48mod1elcan
			{
				weapon = "RH_Mk48mod1elcan";
				count = 6;
			};
		};
	};
};*/
//};
