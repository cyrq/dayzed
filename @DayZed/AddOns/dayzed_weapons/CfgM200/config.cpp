class CfgPatches {
	class m200_cfg {
		units[] = {};
		weapons[] = {"M200_Intervention_DZed"};
		requiredVersion = 1.00;
		requiredAddons[] = {};
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class m200_cfg
		{
			list[] = {"m200_cfg"};
		};
	};
};
class CfgAmmo {

	class Default;
	class BulletCore;
	class BulletBase;	
	class 375_350_M200: BulletBase {
		hit = 18;
		indirectHit = 1.300000;
		indirectHitRange = 0.010000;
		airFriction = -0.00038;
		timetolive = 10;
		deflecting = 20;
		caliber = 2.25;
		airLock = 1;
		cost = 12;
		muzzleEffect = "BIS_Effects_HeavySniper";
	};
};

class CfgMagazines {
	class Default;
	class CA_Magazine;	
	class 7Rnd_M200_DZed : CA_Magazine {
		
		scope=2;
		picture="\CA\weapons\data\Equip\M_107_CA.paa";
		model="\ca\CommunityConfigurationProject_E\Gameplay_ActualModelsOfWeaponMagazinesVisibleOnTheGround\p3d\10Rnd_127x99_m107.p3d";
		//picture = "\ussr_m200\data\mags\mag1.paa"; original 
		displayName = "CheyTac M200 Mag.";
		descriptionShort="Caliber: .375<br/>Rounds: 7<br/>Used in: CheyTac M200";		
		ammo = "375_350_M200";
		//reloadAction = "ManActGestureHiC";
		count = 7;
		initSpeed = 976;
		class ItemActions {
			class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
			};
		};
	};
};


class CfgRecoils {
	cheytacRecoil[] = {0, 0, 0, 0.09, 0.08, 0.07, 0.006, -0.04, -0.3, 0.01, 0, 0};
	cheytacRecoilProne[] = {0, 0, 0, 0.1, 0.1, 0.005, 0.05, -0.2, -0.01, 0.05, 0, 0};
};

class CfgWeapons {
	class Default;
	class RifleCore;
	class Rifle;
	class M200_Intervention_DZed : Rifle {
		
		scope = 2;
		type = 1;
		autoFire=0;
		ModelOptics = "\ussr_m200\optika_nightforce.p3d";
		model = "\ussr_m200\m200cht.p3d";
		displayName = "CheyTac M200";
		descriptionShort = "An American bolt action sniper rifle manufactured by CheyTac LLC. It is fed by a 7-round detachable single stack magazine.";
		dexterity = 0.45;
		opticsPPEffects[] = {"OpticsCHAbera1", "OpticsBlur1"};
		opticsFlare = true;
		opticsDisablePeripherialVision = true;
		optics = true;
		opticsZoomMin = 0.02;
		opticsZoomMax = 0.12;
		distanceZoomMin = 300;
		distanceZoomMax = 300;
		picture = "\ussr_m200\data\w_m200_ca375.paa";
		UiPicture = "\CA\weapons\data\Ico\i_sniper_CA.paa";
		magazines[] = {"7Rnd_M200_DZed"};
		reloadMagazineSound[] = {"\ca\Weapons\Data\Sound\M21_reload_v4", 0.0316228, 1};
		handAnim[]={"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M24.rtm"};
		begin1[] = {"\ussr_m200\data\sound\chshot.ogg", 1, 1, 1000};
		begin2[] = {"\ussr_m200\data\sound\chshot.ogg", 1, 1, 1000};
		soundBegin[] = {"begin1", 0.5, "begin2", 0.5};
		reloadTime = 2;
		dispersion = 0.0001;
		minRange = 2;
		minRangeProbab = 0.1;
		midRange = 1200;
		midRangeProbab = 0.7;
		maxRange = 1800;
		maxRangeProbab = 0.05;
		recoil = "cheytacRecoil";
		recoilProne = "cheytacRecoilProne";
		class Library {
			libTextDesc = ".375 CheyTac M200 Intervention";
		};
		
	};
};