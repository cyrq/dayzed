class CfgPatches
{
	class Misc_Dzed_Cfg
	{
		weapons[] = {"LeeEnfield_DZed","BAF_LRR_scoped_W_DZed","G36C","M24","M40A3","M1014_DZed"};
		requiredVersion = 1.02;
		requiredAddons[] = {"CAweapons"};
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class Misc_Dzed_Cfg
		{
			list[] = {"Misc_Dzed_Cfg"};
		};
	};
};
class CfgAmmo
{
	class BulletBase;
	class B_303_Ball: BulletBase
	{
		hit=11;
		indirectHit=0;
		indirectHitRange=0;
		visibleFire=18;
		audibleFire=18;
		visibleFireTime=2;
		cost=1.2;
		airLock=1;
		tracerStartTime=-1;
		airFriction=-0.00071225001;
		typicalSpeed=750;
		caliber=0.89999998;
	};
	class B_86x70_Ball_noTracer : BulletBase {
		hit=17;
		indirectHit=0;
		indirectHitRange=0;
		cartridge="FxCartridge_127";
		visibleFire=22;
		audibleFire=22;
		visibleFireTime=3;
		cost=20;
		airLock=1;
		model="\ca\Weapons\Data\bullettracer\tracer_red";
		caliber=2.07;
		tracerColor[]={0,0,0,0};
		tracerColorR[]={0,0,0,0};
		tracerStartTime=-1;
		airfriction = -0.0005875;
		muzzleEffect="BIS_Effects_HeavySniper";
	};
	class B_556x45_Ball : BulletBase //Stanag
	{
		hit=8;
		visibleFire=17;
		audibleFire=17;
	};
	class B_556x45_SD : BulletBase //StanagSD
	{ 
		hit=6;
		visibleFire=0.07;
		audibleFire=0.07;
	};
	class B_545x39_Ball : BulletBase //AK
	{
		visibleFire=17;
		audibleFire=17;
	};
};
class CfgMagazines
{
	class Default;
	class CA_Magazine;
	class 10x_303 : CA_Magazine {};
	class 10x_303_DZed : 10x_303
	{
		scope=2;
		displayName="10Rnd. .303";
		descriptionShort="Caliber: .303<br/>Rounds: 10<br/>Used in: Lee-Enfield No.4 Mk I";
		picture="\Ca\weapons\Data\Equip\m_M24_CA.paa";
		count=10;
		ammo="B_303_Ball";
		initSpeed=750;
		class ItemActions {
		class CombineMag {
			text = $STR_MAG_COMBINE;
			script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 5Rnd_86x70_L115A1 : CA_Magazine {};
	class 5Rnd_86x70_L115A1_DZed : 5Rnd_86x70_L115A1
	{
		scope=2;
		ammo="B_86x70_Ball_noTracer";
		count=5;
		initSpeed=936;
		model="\bwc_weapons\bwc_R1s_mag.p3d";
		picture="\bwc_weapons\data\ico\m_R1s_ca.paa";	
		displayName="Lapua Magnum Mag.";
		descriptionShort="Caliber: .338<br/>Rounds: 5<br/>Used in: L115A2 LRR";
		class ItemActions {
		class CombineMag {
			text = $STR_MAG_COMBINE;
			script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 15Rnd_9x19_M9 : CA_Magazine
	{
		descriptionShort="Caliber: 9x19mm Parabellum<br/>Rounds: 15<br/>Used in: Beretta M9";
		};
	class 15Rnd_9x19_M9SD: 15Rnd_9x19_M9
	{
		descriptionShort="Caliber: 9x19mm<br/>Rounds: 15<br/>Used in: Beretta M9SD";
		};
	class 30Rnd_9x19_MP5 : CA_Magazine
	{
		descriptionShort="Caliber: 9x19mm Parabellum<br/>Rounds: 30<br/>Used in: HK MP5";
		};
	class 30Rnd_9x19_MP5SD : CA_Magazine
	{
		descriptionShort="Caliber: 9x19mm Parabellum<br/>Rounds: 30<br/>Used in suppressed: HK MP5";
		model="\ca\CommunityConfigurationProject_E\Gameplay_ActualModelsOfWeaponMagazinesVisibleOnTheGround\p3d\30Rnd_9x19_MP5.p3d";
		initSpeed=400;
		};
	class 64Rnd_9x19_Bizon : CA_Magazine
	{
		descriptionShort="Caliber: 9x18mm<br/>Rounds: 64<br/>Used in: Bizon PP-19";
		model = "\rh_aks\mags\mag_bizon.p3d";
		picture = "\rh_aks\inv\m_bizon.paa";
		};
	class 64Rnd_9x19_SD_Bizon : CA_Magazine
	{
		displayName="BizonSD Mag.";
		descriptionShort="Caliber: 9x18mm<br/>Rounds: 64<br/>Used in suppressed: Bizon PP-19";
		model = "\rh_aks\mags\mag_bizon.p3d";
		picture = "\rh_aks\inv\m_bizon.paa";
		initSpeed=350;
		};
	class 20Rnd_556x45_Stanag : CA_Magazine
	{
		displayName="20Rnd. Stanag Mag.";
		descriptionShort="Caliber: 5.56x45mm NATO<br/>Rounds: 20<br/>Used in: Mk12 Mod0";
		};
	class 30Rnd_556x45_Stanag : 20Rnd_556x45_Stanag
	{
		displayName="30Rnd. Stanag Mag.";
		descriptionShort="Caliber: 5.56x45mm NATO<br/>Rounds: 30<br/>Used in: M4A1, M16A4, M4SBR, FAMAS F1, M249";
		};
	class 30Rnd_556x45_StanagSD : 30Rnd_556x45_Stanag
	{
		ammo="B_556x45_SD";
		displayName="30Rnd. StanagSD Mag.";
		descriptionShort="Caliber: 5.56x45mm NATO<br/>Rounds: 30<br/>Used in suppressed: M4A1, M16A4, FAMAS F1";
		initSpeed=930;
		};
	class 30Rnd_556x45_G36 : 30Rnd_556x45_Stanag
	{
		displayName = "G36 Mag.";
		descriptionShort="Caliber: 5.56x45mm NATO<br/>Rounds: 30<br/>Used in: G36C, HK416, HK416 Cqb, Bushmaster ACR";
		};
	class 30Rnd_556x45_G36SD : 30Rnd_556x45_G36
	{
		displayName = "G36SD Mag.";
		descriptionShort="Caliber: 5.56x45mm NATO<br/>Rounds: 30<br/>Used in suppressed: HK416, G36C Eotech";
		initSpeed=930;
		};
	class 30Rnd_545x39_AK : CA_Magazine
	{
		displayName="AK Mag.";
		descriptionShort="Caliber: 5.45x39mm<br/>Rounds: 30<br/>Used in: AK-74, AKS-74, AK-105, AK-107, AN-94";
		};
	class 30Rnd_545x39_AKSD : 30Rnd_545x39_AK
	{
		displayName="AKSD Mag.";
		descriptionShort="Caliber: 5.45x39mm<br/>Rounds: 30<br/>Used in suppressed: AKS-74U";
		initSpeed=900;
		};
	class 30Rnd_762x39_AK47 : CA_Magazine
	{
		model = "\rh_aks\mags\mag_ak47.p3d";
		picture = "\rh_aks\inv\m_ak.paa";
		descriptionShort="Caliber: 7.62x39mm<br/>Rounds: 30<br/>Used in: AKM, AKMS, Groza, RK-95";
		};
	class 5Rnd_762x51_M24 : CA_Magazine
	{
		displayName="5Rnd. 7.62x51mm";
		descriptionShort="Caliber: 7.62x51mm NATO<br/>Rounds: 5<br/>Used in: M24, M40A3, M40A3 Ghillie, M40A5";
		};
	class 20Rnd_762x51_DMR : CA_Magazine
	{
		displayName = "DMR Mag.";
		descriptionShort="Caliber: 7.62x51mm NATO<br/>Rounds: 20<br/>Used in: M21, M14";
		};
	class 100Rnd_762x51_M240 : CA_Magazine
	{
		displayName="100Rnd. M240 Belt";
		descriptionShort="Caliber: 7.62x51mm NATO<br/>Rounds: 100<br/>Used in: Mk48 Mod1, M240";
		};
	class 100Rnd_762x54_PK : CA_Magazine {
		displayName="PKM Mag.";
		descriptionShort="Caliber: 7.62x54mm<br/>Rounds: 100<br/>Used in: PKT";
	};
	class 50Rnd_127x107_DSHKM : CA_Magazine {
		displayName="DShKM Mag.";
		descriptionShort="Caliber: 12.7x108mm<br/>Rounds: 50<br/>Used in: DShKM";
		type=256;
		reloadAction="";
		scope=2;
		ammo="B_127x107_Ball";
		count=50;
		initSpeed=850;
		maxLeadSpeed=200;
		tracersEvery=3;
		lastRoundsTracer=5;
		nameSound="mgun";
		model = "\ca\CommunityConfigurationProject_E\Gameplay_ActualModelsOfWeaponMagazinesVisibleOnTheGround\p3d\100Rnd_762x54_PK.p3d";
		picture = "\rh_aks\inv\m_pkm.paa";
	};
	class 8Rnd_B_Beneli_74Slug : CA_Magazine {};
	class 8Rnd_B_Beneli_74Slug_DZed : 8Rnd_B_Beneli_74Slug
	{
		model="\dayz_weapons\models\8shells_slugs.p3d";
		picture="\dayz_weapons\textures\equip_shells_ca.paa";
		descriptionShort="Rounds: 8<br/>Used in: M1014, Remington 870";
		displayName="8Rnd. Slugs";
		displayNameShort="Slug";
		class ItemActions
		{
			class ReloadMag
			{
				text="Split into 4 x 2 rounds";
				script="spawn player_reloadMag;";
				use[]=
				{
					"8Rnd_B_Beneli_74Slug_DZed"
				};
				output[]=
				{
					"2Rnd_shotgun_74Slug",
					"2Rnd_shotgun_74Slug",
					"2Rnd_shotgun_74Slug",
					"2Rnd_shotgun_74Slug"
				};
			};
			class CombineMag
			{
			text = $STR_MAG_COMBINE;
			script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
			};
		};
	};	
	class 8Rnd_B_Beneli_Pellets: CA_Magazine {};
	class 8Rnd_B_Beneli_Pellets_DZed : 8Rnd_B_Beneli_Pellets
	{
		model="\dayz_weapons\models\8shells_pellets.p3d";
		picture="\dayz_weapons\textures\equip_pellets_ca.paa";
		descriptionShort="Rounds: 8<br/>Used in: M1014, Remington 870";
		displayName="8Rnd. Pellets";
		displayNameShort="Pellet";
		class ItemActions
		{
			class ReloadMag
			{
				text="Split into 4x2 rounds";
				script="spawn player_reloadMag;";
				use[]=
				{
					"8Rnd_B_Beneli_Pellets_DZed"
				};
				output[]=
				{
					"2Rnd_shotgun_74Pellets",
					"2Rnd_shotgun_74Pellets",
					"2Rnd_shotgun_74Pellets",
					"2Rnd_shotgun_74Pellets"
				};
			};
			class CombineMag
			{
			text = $STR_MAG_COMBINE;
			script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
			};
		};
	};
	class 2Rnd_shotgun_74Slug: 8Rnd_B_Beneli_74Slug
	{
		model="\z\addons\dayz_communityassets\models\2shells_slugshot.p3d";
		picture="\z\addons\dayz_communityassets\pictures\equip_2shells_slugshot_CA.paa";
	};
	class 2Rnd_shotgun_74Pellets: 8Rnd_B_Beneli_Pellets
	{
		model="\z\addons\dayz_communityassets\models\2shells_pellet.p3d";
		picture="\z\addons\dayz_communityassets\pictures\equip_2shells_pellet_CA.paa";
	};
};
class CfgWeapons
{
	class Rifle;
	class SVD_Ironsight_DayZed : Rifle
	{
		scope=2;
		model="\dayzed_weapons\SVD_Ironsight_DayZed.p3d";
		dexterity=1.5700001;
		displayName="SVD Ironsight";
		descriptionShort = "Semi-automatic rifle.<br/>Magazine: SVD Dragunov Mag.";
		picture="\dayzed_weapons\Equip\svd_ironsight.paa";
		UiPicture="\CA\weapons\data\Ico\i_sniper_CA.paa";
		opticsFlare=0;
		opticsDisablePeripherialVision=0;
		opticsPPEffects[]={};
		opticsZoomMin=0.25;
		opticsZoomMax=1.1;
		opticsZoomInit=0.5;
		distanceZoomMin=300;
		distanceZoomMax=300;
		begin1[]=
		{
			"\RH_aks\Sound\SVD.wss",
			1.7782789,
			1,
			1200
		};
		soundBegin[]=
		{
			"begin1",
			1
		};
		reloadMagazineSound[]=
		{
			"\RH_aks\sound\svd_reload.wss",
			0.056233998,
			1,
			20
		};
		dispersion=0.00025000001;
		recoil="RH_SVDRecoil";
		recoilProne="RH_SVDRecoilProne";
		minRange=0;
		minRangeProbab=0.1;
		midRange=400;
		midRangeProbab=0.69999999;
		maxRange=800;
		maxRangeProbab=0.050000001;
		reloadTime=0.1;
		autoFire=0;
		value=1000;
		aiRateOfFire=9;
		aiRateOfFireDistance=1300;
		magazines[]=
		{
			"10Rnd_762x54_SVD"
		};
		htMin=1;
		htMax=420;
		afMax=0;
		mfMax=0;
		mFact=1;
		tBody=100;
		visionMode[]={};
		class Library
		{
			libTextDesc="SVD was designed not as a standart sniper rifle. In fact, main role of the SVD ir Soviet and Russian Army is to extend effective range of fire of every infantry squad up to 600 meters and to provide special fire support. SVD is a lightweight and quite accurate for it's class rifle, cabable of semi-auto fire. First request for new sniper rifle was issued in 1958. In 1963 SVD Snaiperskaya Vintovka Dragunova, or Dragunov Sniper Rifle was accepted by Soviet Military. SVD can use any kind of standart 7.62x54R ammo, but primary round is specially developed for SVD sniper-grade cartridge with steel-core bullet. ";
		};
	};
	class SVD_NSPU_DayZed : Rifle {
		scope=2;
		model="\ca\weapons_E\SVD\SVD_nspu.p3d";
		dexterity=1.5700001;
		displayName="SVD NSPU";
		descriptionShort = "Semi-automatic sniper rifle with built-in night vision scope.<br/>Magazine: SVD Dragunov Mag.";
		picture="\CA\weapons_E\Data\icons\w_svd_nspu_CA.paa";
		UiPicture="\CA\weapons\data\Ico\i_sniper_CA.paa";
		modelOptics="\Ca\weapons_E\NV_nspu_optic.p3d";
		class OpticsModes {
			class NSPU {
				opticsID=1;
				useModelOptics=1;
				opticsZoomMin=0.0708215;
				opticsZoomMax=0.0708215;
				opticsZoomInit=0.0708215;
				opticsPPEffects[]={};
				opticsDisablePeripherialVision=1;
				memoryPointCamera="opticView";
				visionMode[]={"NVG"};
				opticsFlare=1;
				distanceZoomMin=300;
				distanceZoomMax=300;
				cameraDir="";
			};
			class Ironsights {
				opticsID=2;
				useModelOptics=0;
				opticsZoomMin=0.25;
				opticsZoomMax=1.1;
				opticsZoomInit=0.5;
				opticsPPEffects[]={};
				opticsDisablePeripherialVision=0;
				memoryPointCamera="eye";
				visionMode[]={};
				opticsFlare=0;
				distanceZoomMin=300;
				distanceZoomMax=300;
				cameraDir="";
			};
		};
		begin1[]=
		{
			"\RH_aks\Sound\SVD.wss",
			1.7782789,
			1,
			1200
		};
		soundBegin[]=
		{
			"begin1",
			1
		};
		reloadMagazineSound[]=
		{
			"\RH_aks\sound\svd_reload.wss",
			0.056233998,
			1,
			20
		};
		dispersion=0.00025000001;
		recoil="RH_SVDRecoil";
		recoilProne="RH_SVDRecoilProne";
		minRange=0;
		minRangeProbab=0.1;
		midRange=400;
		midRangeProbab=0.7;
		maxRange=800;
		maxRangeProbab=0.05;
		reloadTime = 0.1;
		autoFire = 0;
		value = 1000;
		aiRateOfFire=9;
		aiRateOfFireDistance=1300;
		magazines[]=
		{
			"10Rnd_762x54_SVD"
		};
		htMin=1;
		htMax=420;
		afMax=0;
		mfMax=0;
		mFact=1;
		tBody=100;		
		class Library
		{
			libTextDesc="SVD was designed not as a standart sniper rifle. In fact, main role of the SVD ir Soviet and Russian Army is to extend effective range of fire of every infantry squad up to 600 meters and to provide special fire support. SVD is a lightweight and quite accurate for it's class rifle, cabable of semi-auto fire. First request for new sniper rifle was issued in 1958. In 1963 SVD Snaiperskaya Vintovka Dragunova, or Dragunov Sniper Rifle was accepted by Soviet Military. SVD can use any kind of standart 7.62x54R ammo, but primary round is specially developed for SVD sniper-grade cartridge with steel-core bullet. ";
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove NSPU Scope";
				script = "spawn player_removeNSPU;";
			};
		};
	};
	class LeeEnfield_DZed: Rifle
	{
		htMin=1;
		htMax=420;
		afMax=0;
		mfMax=0;
		mFact=1;
		tBody=100;
		scope=2;
		displayName="Lee-Enfield No.4 Mk I";
		model="\CA\weapons_E\LeeEnfield\LeeEnfield_no4mk1";
		picture="\ca\weapons_E\Data\icons\leeEnfield_CA.paa";
		UiPicture="\CA\weapons\data\Ico\i_sniper_CA.paa";
		handAnim[]=
		{
			"OFP2_ManSkeleton",
			"\Ca\weapons_E\Data\Anim\LeeEnfield.rtm"
		};
		dexterity=1.61;
		magazines[]=
		{
			"10x_303_DZed"
		};
		opticsDisablePeripherialVision=0;
		opticsPPEffects[]={};
		distanceZoomMin=300;
		distanceZoomMax=300;
		minRange=0;
		minRangeProbab=0.1;
		midRange=150;
		midRangeProbab=0.69999999;
		maxRange=300;
		maxRangeProbab=0.050000001;
		aiRateOfFire=8;
		aiRateOfFireDistance=600;
		dispersion=0.00050000002;
		class Library
		{
			libTextDesc="$STR_EP1_LIB_LeeEnfield";
		};
		descriptionShort="Bolt-action, magazine-fed, repeating rifle.<br/>Magazine: 10Rnd. .303";
		begin1[]=
		{
			"Ca\Sounds_E\Weapons_E\Enfield\Enfield_4",
			1.7782794,
			1,
			1500
		};
		soundBegin[]=
		{
			"begin1",
			1
		};
		reloadMagazineSound[]=
		{
			"Ca\sounds\Weapons\rifles\M1014-reload",
			0.0099999998,
			1,
			20
		};
		drySound[]=
		{
			"Ca\sounds\Weapons\rifles\dry",
			0.0099999998,
			1,
			10
		};
		reloadTime=2;
		backgroundReload=1;
		recoil="recoil_single_primary_6outof10";
		recoilProne="recoil_single_primary_prone_5outof10";
		value=1000;
	};
	class BAF_LRR_scoped {};
	class BAF_LRR_scoped_W : BAF_LRR_scoped {};
	class BAF_LRR_scoped_W_DZed : BAF_LRR_scoped_W
	{
	picture="\dayzed_weapons\CfgMISC\textures\lrr_scoped_W.paa";
	displayname="L115A2 LRR";
	descriptionShort="Long range bolt-action sniper rifle.<br/>Magazine: Lapua Magnum Mag.";
	magazines[]={"5Rnd_86x70_L115A1_DZed"};
	};
	class G36C : Rifle
	{
	magazines[]={"30Rnd_556x45_G36"};
	displayName="G36C";
	descriptionShort="Compact carbine with a 9 inch barrel.<br/>Magazine: G36 Mag.";
	};
	class M24 : Rifle
	{
	descriptionShort="Bolt-action repeating sniper rifle equipped with Leupold scope.<br/>Magazine: 5Rnd. 7.62x51mm";
	};
	class M40A3 : M24
	{
	displayName="M40A3 Ghillie";
	descriptionShort="Bolt-action sniper rifle equipped with a ghillie webbing.<br/>Magazine: 5Rnd. 7.62x51mm";
	};
	class M1014 : Rifle {};
	class M1014_DZed : M1014 
	{
	descriptionShort="Semi-automatic combat shotgun.<br/>Ammunition: 8Rnd. Slugs, 8Rnd. Pellets";
	magazines[]={"8Rnd_B_Beneli_74Slug_DZed","8Rnd_B_Beneli_Pellets_DZed"};
	};
	class G36_C_SD_eotech : G36C
                {
                value=6;
                model="\ca\weapons\G36\G36_C_SD_eotech";
                displayName="G36C Eotech SD";
                picture="\ca\weapons\G36\Data\Equip\w_G36_C_SD_eotech_ca.paa";
                UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
                magazines[]={"30Rnd_556x45_G36SD"};
                fireLightDuration=0;
                fireLightIntensity=0;
                distanceZoomMin=100;
                distanceZoomMax=100;
                modes[]={"Single","Burst","Fullauto"};
                class Single {
                        begin1[]={"ca\sounds\weapons\rifles\rifle-silence-single2",1,1,300};
                        soundBegin[]={"begin1",1};
                        minRange=2;
                        minRangeProbab=0.1;
                        midRange=100;
                        midRangeProbab=0.7;
                        maxRange=150;
                        maxRangeProbab=0.05;
                        };

                class Burst {
                        begin1[]={"ca\sounds\weapons\rifles\rifle-silence-single2",1,1,300};
                        soundBegin[]={"begin1",1};
                        minRange=1;
                        minRangeProbab=0.1;
                        midRange=50;
                        midRangeProbab=0.7;
                        maxRange=100;
                        maxRangeProbab=0.05;
                        };

                class FullAuto {
                        begin1[]={"ca\sounds\weapons\rifles\rifle-silence-single2",1,1,300};
                        soundBegin[]={"begin1",1};
                        minRange=0;
                        minRangeProbab=0.1;
                        midRange=20;
                        midRangeProbab=0.7;
                        maxRange=30;
                        maxRangeProbab=0.05;
                        };

                class Library
                        {
                        libTextDesc="The G36 is a 5.56mm caliber assault rifle, manufactured in Germany. This weapon has been used by the German Armed Forces since 1997 and these days many armies have adopted it. <br/>The G36C (C – Compact) is a further development of this weapon. It features a shorter barrel and a shorter flash suppressor. This type also uses rail-mounted iron sights instead of dual optic sights and has an additional suppressor.";
                        };

                descriptionShort="Silenced assault rifle <br/>Magazine: G36SD Mag.";
                };
    class NVGoggles;
    class NVGoggles_Broken : NVGoggles {
        displayName = "NV Goggles (Broken)";
        simulation= "";
        descriptionShort = "Broken Night Vision Goggles";
		picture="\dayzed_weapons\Equip\equip_broken_nvg.paa";
        class ItemActions
        {
            class Use
            {
                text = "Fix NV Goggles";
                script = "spawn player_fixEQ;";
            };
        };
    };
	class ItemGPS;
	class ItemGPS_Broken : ItemGPS {
		displayName = "GPS (Broken)";
		simulation= "";
		descriptionShort = "Broken GPS";
		picture="\dayzed_weapons\Equip\equip_broken_gps.paa";
        class ItemActions
        {
            class Use
            {
                text = "Fix GPS";
                script = "spawn player_fixEQ;";
            };
        };
	};
	class Binocular_Vector;
	class Binocular_Vector_Broken : Binocular_Vector {
		displayName = "Rangefinder (Broken)";
		simulation= "";
		showEmpty=0;
		descriptionShort = "Broken device that measures distance from the observer to a target";
		picture="\dayzed_weapons\Equip\equip_broken_binocular_vector.paa";
        class ItemActions
        {
            class Use
            {
                text = "Fix Rangefinder";
                script = "spawn player_fixEQ;";
            };
        };
	};
};