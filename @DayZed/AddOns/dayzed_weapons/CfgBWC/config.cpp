class CfgPatches
{
	class bwc_Weapons
	{
		units[]={};
		weapons[]={"bwc_SKS"/*,"bwc_T33","bwc_Star","bwc_Z88","bwc_R1","bwc_R1_HEAT","bwc_R1_FRAG","bwc_R1_S","bwc_R2","bwc_R2_F","bwc_R4","bwc_R4_HEAT","bwc_R4_FRAG","bwc_R4_F","bwc_R5","bwc_R5_F","bwc_FNMAG","bwc_M79","bwc_AK47","bwc_AK47_GL","bwc_AKS47","bwc_AKS47_F","bwc_AKM","bwc_AKM_GL","bwc_AKMS","bwc_AKMS_F","bwc_RPK","bwc_RPK_D","bwc_SVD"*/};
		requiredVersion=1.08;
		requiredAddons[]=
		{
			"CAWeapons"
		};
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class BWC_weapons
		{
			list[] = {"BWC_weapons"};
		};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class cfgRecoils
{
	bwc_762mm_PistolRecoil[]={0,0.0070000002,0.050000001,0.0049999999,0.0070000002,0.050000001,0.090000004,0,-0.003,0.15000001,0,0};
	bwc_9mm_PistolRecoil[]={0,0.0070000002,0.050000001,0.0049999999,0.0070000002,0.050000001,0.090000004,0,-0.003,0.15000001,0,0};
	bwc_R4Recoil[]={0,0.0080000004,0.0099999998,0.0049999999,0.0099999998,0.0099999998,0.090000004,0.0049999999,-9.9999997e-005,0.14,0,0};
	bwc_R4RecoilProne[]={0,0.0049999999,0.0049999999,0.0049999999,0.0080000004,0.0060000001,0.075000003,0.0049999999,-9.9999997e-005,0.13,0,0};
	bwc_R5Recoil[]={0,0.0040000002,0.0099999998,0.0049999999,0.0099999998,0.0099999998,0.090000004,0.0049999999,0,0.12,0,0};
	bwc_R5RecoilProne[]={0,0.003,0.0070000002,0.0049999999,0.0049999999,0.0070000002,0.050000001,0.003,-9.9999997e-005,0.12,0,0};
	bwc_FNMAGRecoil[]={0,0.0099999998,0.014,0.14,0.0060000001,0,0.2,0,0};
	bwc_FNMAGRecoilProne[]={0,0.0099999998,0.0012000001,0.090000004,0.0089999996,-0.001,0.15000001,0,0};
	bwc_m79Recoil[]={0,0,0,0.059999999,0.0099999998,0.012,0.1,0,-0.02,0.1,-0.0099999998,0.0099999998,0.050000001,0,0};
	bwc_m79RecoilProne[]={0,0,0,0.059999999,0.0099999998,0.012,0.1,0,-0.02,0.1,-0.0099999998,0.0099999998,0.050000001,0,0};
	bwc_R1Recoil[]={0,0.0080000004,0.0080000004,0.0049999999,0.011,0.015,0.090000004,0.0049999999,-9.9999997e-005,0.16,0,0};
	bwc_R1RecoilProne[]={0,0.0080000004,0.0049999999,0.0049999999,0.011,0.0089999996,0.090000004,0.0049999999,-0.00019999999,0.15000001,0,0};
	bwc_R1SRecoil[]={0,0.013,0.015,0.02,0.013,0.015,0.1,0.0089999996,0.015,0.12,0,0};
	bwc_R1SRecoilProne[]={0,0.012,0.0099999998,0.013,0.012,0.0099999998,0.07,0.0070000002,0,0.12,0,0};
	bwc_AK47Recoil[]={0,0.0080000004,0.0080000004,0.0049999999,0.011,0.015,0.090000004,0.0049999999,-9.9999997e-005,0.16,0,0};
	bwc_AK47RecoilProne[]={0,0.0080000004,0.0049999999,0.0049999999,0.011,0.0089999996,0.090000004,0.0049999999,-0.00019999999,0.15000001,0,0};
	bwc_AKMRecoil[]={0,0.0080000004,0.0080000004,0.0049999999,0.011,0.014,0.090000004,0.0049999999,-9.9999997e-005,0.16,0,0};
	bwc_AKMRecoilProne[]={0,0.0080000004,0.0049999999,0.0049999999,0.011,0.0089999996,0.090000004,0.0049999999,-0.00019999999,0.15000001,0,0};
	bwc_GP25Recoil[]={0,0.029999999,0.050000001,0.13,0.0099999998,-0.001,0.25999999,0,0};
	bwc_AKSRecoil[]={0,0.0080000004,0.0080000004,0.0049999999,0.014,0.035,0.1,0.0070000002,0,0.19,0,0};
	bwc_AKSRecoilProne[]={0,0.0080000004,0.0049999999,0.0049999999,0.014,0.017000001,0.1,0.0070000002,-9.9999997e-005,0.18000001,0,0};
	bwc_SKSRecoil[]={0,0.0099999998,0.017000001,0.0049999999,0.0099999998,0.017000001,0.14,0.0049999999,0,0.14,0,0};
	bwc_SKSRecoilProne[]={0,0.0080000004,0.0070000002,0.0049999999,0.0099999998,0.0070000002,0.1,0.0049999999,-9.9999997e-005,0.13,0,0};
	bwc_SVDRecoil[]={0,0.013,0.015,0.02,0.013,0.015,0.1,0.0089999996,0.015,0.12,0,0};
	bwc_SVDRecoilProne[]={0,0.012,0.0099999998,0.013,0.012,0.0099999998,0.07,0.0070000002,0,0.12,0,0};
	bwc_RPKRecoil[]={0,0.0099999998,0.013,0.0099999998,0.0099999998,0.013,0.039999999,0.0049999999,0.0049999999,0.15000001,0,0};
	bwc_RPKRecoilProne[]={0,0.0070000002,0.0044999998,0.0099999998,0.0070000002,0.0044999998,0.07,0.0070000002,0,0.14,0,0};
};
class CfgAmmo
{
	class Default;
	class Grenade;
	class Mine;
	class BulletCore;
	class BulletBase;
	/*class bwc_9x19: BulletBase
	{
		hit=9;
		indirectHit=0;
		indirectHitRange=0;
		model="\ca\Weapons\shell";
		cartridge="FxCartridge_Small";
		visibleFire=18;
		audibleFire=18;
		visibleFireTime=2;
		cost=5;
		deflecting=10;
		typicalSpeed=350;
		tracerColor[]={0,0,0,0};
		tracerColorR[]={0,0,0,0};
	};
	class bwc_762x25: bwc_9x19
	{
		typicalSpeed=370;
	};
	class bwc_556x45: BulletBase
	{
		hit=8;
		indirectHit=0;
		indirectHitRange=0;
		cartridge="FxCartridge_Small";
		cost=1;
		tracerColor[]={0.80000001,0.1,0.1,0.039999999};
		tracerColorR[]={0.80000001,0.1,0.1,0.039999999};
	};*/
	class bwc_762x39: BulletBase
	{
		hit=9;
		indirectHit=0;
		indirectHitRange=0;
		typicalSpeed=578;
		airFriction=-0.00152;
		visibleFire=18;
		audibleFire=18;
		visibleFireTime=3;
		tracerColor[]={0.2,0.80000001,0.1,0.039999999};
		tracerColorR[]={0.2,0.80000001,0.1,0.039999999};
	};
	/*class bwc_762x51: BulletBase
	{
		hit=11;
		indirectHit=0;
		indirectHitRange=0;
		visibleFire=22;
		audibleFire=18;
		visibleFireTime=3;
		typicalspeed=750;
		airFriction=-0.00105;
		cost=1.2;
		airLock=1;
		tracerColor[]={0.80000001,0.1,0.1,0.039999999};
		tracerColorR[]={0.80000001,0.1,0.1,0.039999999};
	};
	class bwc_762x51S: bwc_762x51
	{
		airLock=0;
		tracerColor[]={0,0,0,0};
		tracerColorR[]={0,0,0,0};
	};
	class bwc_762x51_3RndBurst: bwc_762x51
	{
		hit=25;
		indirectHit=0;
		indirectHitRange=0;
		visibleFire=25;
		audibleFire=22;
		visibleFireTime=4;
		tracerColor[]={0.80000001,0.1,0.1,0.039999999};
		tracerColorR[]={0.80000001,0.1,0.1,0.039999999};
	};
	class bwc_762x54R: BulletBase
	{
		hit=11;
		indirectHit=0;
		indirectHitRange=0;
		typicalSpeed=680;
		airFriction=-0.00152;
		airLock=0;
		visibleFire=18;
		audibleFire=18;
		visibleFireTime=3;
		cost=1;
		tracerColor[]={0,0,0,0};
		tracerColorR[]={0,0,0,0};
	};
	class GrenadeCore;
	class GrenadeBase;
	class FlareCore;
	class FlareBase;
	class bwc_VOG_25: GrenadeBase
	{
		model="\bwc_Weapons\bwc_VOG-25";
		hit=13;
		indirectHit=10;
		indirectHitRange=5;
		soundHit[]=
		{
			"\bwc_weapons\data\snd\m79_explode.ogg",
			100.00001,
			1
		};
		visibleFire=18;
		audibleFire=18;
		visibleFireTime=3;
		explosive=1;
		cost=8;
		deflecting=10;
	};
	class bwc_VOG_25P: GrenadeBase
	{
		model="\bwc_Weapons\bwc_VOG-25P";
		hit=15;
		indirectHit=13;
		indirectHitRange=6;
		soundHit[]=
		{
			"\bwc_weapons\data\snd\vog25p_explode.ogg",
			316.22778,
			1
		};
		visibleFire=18;
		audibleFire=18;
		visibleFireTime=3;
		explosive=1;
		cost=8;
		deflecting=10;
	};
	class bwc_40mm_HE: GrenadeBase
	{
		model="\bwc_Weapons\bwc_40mm";
		hit=14;
		indirectHit=12;
		indirectHitRange=8;
		soundHit[]=
		{
			"\bwc_weapons\data\snd\m79_explode.ogg",
			316.22778,
			1
		};
		visibleFire=16;
		audibleFire=18;
		visibleFireTime=3;
		explosive=1;
		cost=8;
		deflecting=10;
	};
	class bwc_40mm_White: FlareCore
	{
		model="\ca\Weapons\granat";
		lightColor[]={1,1,1,0};
	};
	class bwc_40mm_Green: bwc_40mm_White
	{
		lightColor[]={0,1,0,0};
	};
	class bwc_40mm_Red: bwc_40mm_White
	{
		lightColor[]={1,0,0,0};
	};
	class bwc_40mm_Yellow: bwc_40mm_White
	{
		lightColor[]={1,1,0,0};
	};
	class bwc_75mm_HE: GrenadeBase
	{
		model="\bwc_weapons\bwc_75mmHEAT";
		hit=210;
		indirectHit=20;
		indirectHitRange=9;
		soundHit[]=
		{
			"\bwc_weapons\data\snd\75heat_explode.ogg",
			100.00001,
			1
		};
		visibleFire=17;
		audibleFire=19;
		visibleFireTime=4;
		explosive=1;
		cost=5;
		deflecting=10;
	};
	class bwc_75mm_FR: bwc_75mm_HE
	{
		model="\bwc_weapons\bwc_75mmFRAG";
		hit=20;
		indirectHit=10;
		indirectHitRange=8;
		soundHit[]=
		{
			"\bwc_weapons\data\snd\75frag_explode.ogg",
			100.00001,
			1
		};
		cost=3;
	};*/
};
class CfgMagazines
{
	class Default;
	class CA_Magazine;
	/*class bwc_T33_mag: CA_Magazine
	{
		scope=2;
		displayName="T33 Magazine";
		type=16;
		model="\bwc_weapons\bwc_pistol_mag";
		picture="\bwc_weapons\data\ico\m_T33_ca.paa";
		ammo="bwc_762x25";
		count=8;
		selectionFireAnim="zasleh";
		initSpeed=420;
	};
	class bwc_Z88_mag: CA_Magazine
	{
		scope=2;
		displayName="Z88 Magazine";
		type=16;
		model="\bwc_weapons\bwc_pistol_mag";
		picture="\bwc_weapons\data\ico\m_Z88_ca.paa";
		ammo="bwc_9x19";
		count=15;
		selectionFireAnim="zasleh";
		initSpeed=360;
	};
	class bwc_Star_mag: CA_Magazine
	{
		scope=2;
		displayName="Star Magazine";
		type=16;
		model="\bwc_weapons\bwc_pistol_mag";
		picture="\bwc_weapons\data\ico\m_Star_ca.paa";
		ammo="bwc_9x19";
		count=8;
		selectionFireAnim="zasleh";
		initSpeed=360;
	};*/
	class bwc_AK47_mag: CA_Magazine
	{
		scope=2;
		displayName="AK47 Magazine";
		model="\bwc_weapons\bwc_AK47_mag";
		picture="\bwc_weapons\data\ico\m_AK47_ca.paa";
		ammo="bwc_762x39";
		count=30;
		initSpeed=715;
		selectionFireAnim="zasleh";
	};
	class bwc_AKM_mag: CA_Magazine
	{
		scope=2;
		displayName="AKM Magazine";
		model="\bwc_weapons\bwc_AKM_mag";
		picture="\bwc_weapons\data\ico\m_AKM_ca.paa";
		ammo="bwc_762x39";
		count=30;
		initSpeed=715;
		selectionFireAnim="zasleh";
	};
	/*class bwc_RPK_mag: CA_Magazine
	{
		scope=2;
		displayName="RPK Magazine";
		model="\bwc_weapons\bwc_RPK_mag";
		picture="\bwc_weapons\data\ico\m_AK47_ca.paa";
		ammo="bwc_762x39";
		count=40;
		initSpeed=745;
		selectionFireAnim="zasleh";
	};
	class bwc_RPK_D_mag: CA_Magazine
	{
		scope=2;
		type="2*		256";
		displayName="RPK Drum Magazine";
		model="\bwc_weapons\bwc_RPK_D_mag";
		picture="\bwc_weapons\data\ico\m_RPK_D_ca.paa";
		ammo="bwc_762x39";
		count=75;
		initSpeed=745;
		selectionFireAnim="zasleh";
	};*/
	class bwc_SKS_mag: CA_Magazine
	{
		scope=2;
		displayName="10Rnd. SKS Clip";
		descriptionShort="Caliber: 7.62x39mm<br/>Rounds: 10<br/>Used in: SKS";
		model="\bwc_weapons\bwc_SKS_mag";
		picture="\bwc_weapons\data\ico\m_SKS_ca.paa";
		ammo="bwc_762x39";
		count=10;
		initSpeed=715;
		selectionFireAnim="zasleh";
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
			};
		};
	};
	/*class bwc_SVD_mag: CA_Magazine
	{
		scope=2;
		displayName="SVD Magazine";
		model="\bwc_weapons\bwc_SVD_mag";
		picture="\bwc_weapons\data\ico\m_SVD_ca.paa";
		ammo="bwc_762x54R";
		count=10;
		initSpeed=830;
		selectionFireAnim="zasleh";
	};
	class bwc_R4_mag: CA_Magazine
	{
		scope=2;
		displayName="R4 Magazine";
		model="\bwc_weapons\bwc_R4_mag";
		picture="\bwc_weapons\data\ico\m_R4_ca.paa";
		ammo="bwc_556x45";
		count=35;
		initSpeed=850;
		selectionFireAnim="zasleh";
	};
	class bwc_R1_mag: CA_Magazine
	{
		scope=2;
		displayName="R1 Magazine";
		model="\bwc_weapons\bwc_R1_mag";
		picture="\bwc_weapons\data\ico\m_R1_ca.paa";
		ammo="bwc_762x51";
		count=20;
		initSpeed=900;
		selectionFireAnim="zasleh";
	};
	class bwc_R1s_mag: bwc_R1_mag
	{
		model="\bwc_weapons\bwc_R1s_mag";
		picture="\bwc_weapons\data\ico\m_R1s_ca.paa";
		ammo="bwc_762x51S";
		count=10;
	};
	class bwc_FNMAG_mag: CA_Magazine
	{
		scope=2;
		displayName="FNMAG Ammo Belt";
		model="\bwc_weapons\bwc_FNMAG_belt";
		picture="\bwc_weapons\data\ico\m_FNMAG_ca.paa";
		count=100;
		type="2*		256";
		ammo="bwc_762x51";
		initSpeed=900;
		selectionFireAnim="zasleh";
	};
	class bwc_M79_round: CA_Magazine
	{
		scope=2;
		displayName="40mm Grenade";
		model="\bwc_weapons\bwc_M79_round";
		picture="\bwc_weapons\data\ico\m_M79_ca.paa";
		ammo="bwc_40mm_HE";
		initSpeed=80;
		count=1;
	};
	class bwc_M79_FlareWhite: CA_Magazine
	{
		scope=2;
		type=16;
		displayName="40mm White Flare";
		picture="\Ca\weapons\Data\Equip\m_FlareWhite_CA.paa";
		ammo="bwc_40mm_White";
		initSpeed=90;
		count=1;
	};
	class bwc_M79_FlareGreen: bwc_M79_FlareWhite
	{
		displayName="40mm Green Flare";
		ammo="bwc_40mm_Green";
		picture="\Ca\weapons\Data\Equip\m_FlareGreen_CA.paa";
	};
	class bwc_M79_FlareRed: bwc_M79_FlareWhite
	{
		displayName="40mm Red Flare";
		ammo="bwc_40mm_Red";
		picture="\Ca\weapons\Data\Equip\m_FlareRed_CA.paa";
	};
	class bwc_M79_FlareYellow: bwc_M79_FlareWhite
	{
		displayName="40mm Yellow Flare";
		ammo="bwc_40mm_Yellow";
		picture="\Ca\weapons\Data\Equip\m_FlareYelow_CA.paa";
	};
	class bwc_VOG_25P_round: CA_Magazine
	{
		scope=2;
		type=16;
		displayName="VOG-25P (Frog) Grenade";
		model="\bwc_weapons\bwc_VOG-25P_mag";
		modelSpecial="\bwc_weapons\bwc_AKM_GLL";
		picture="\bwc_weapons\data\ico\m_VOG-25P_ca.paa";
		ammo="bwc_VOG_25P";
		initSpeed=100;
		count=1;
	};
	class bwc_VOG_25_round: CA_Magazine
	{
		scope=2;
		type=16;
		displayName="VOG-25 Grenade";
		model="\bwc_weapons\bwc_VOG-25_mag";
		modelSpecial="\bwc_weapons\bwc_AK47_GLL";
		picture="\bwc_weapons\data\ico\m_VOG-25_ca.paa";
		ammo="bwc_VOG_25";
		initSpeed=100;
		count=1;
	};
	class bwc_GP25_FlareWhite: CA_Magazine
	{
		scope=2;
		type=16;
		displayName="40mm White Flare";
		picture="\Ca\weapons\Data\Equip\m_FlareWhite_CA.paa";
		ammo="bwc_40mm_White";
		initSpeed=80;
		count=1;
	};
	class bwc_GP25_FlareGreen: bwc_GP25_FlareWhite
	{
		displayName="40mm Green Flare";
		ammo="bwc_40mm_Green";
		picture="\Ca\weapons\Data\Equip\m_FlareGreen_CA.paa";
	};
	class bwc_GP25_FlareRed: bwc_GP25_FlareWhite
	{
		displayName="40mm Red Flare";
		ammo="bwc_40mm_Red";
		picture="\Ca\weapons\Data\Equip\m_FlareRed_CA.paa";
	};
	class bwc_GP25_FlareYellow: bwc_GP25_FlareWhite
	{
		displayName="40mm Yellow Flare";
		ammo="bwc_40mm_Yellow";
		picture="\Ca\weapons\Data\Equip\m_FlareYelow_CA.paa";
	};
	class bwc_R4HE_mag: CA_Magazine
	{
		scope=2;
		type="2*		256";
		model="\bwc_weapons\bwc_75mmHEAT_mag";
		modelSpecial="\bwc_weapons\bwc_R4_RGheat";
		displayName="75mm HEAT Rifle Grenade";
		picture="\bwc_weapons\data\ico\m_75heat_ca.paa";
		ammo="bwc_75mm_HE";
		initSpeed=80;
		count=1;
	};
	class bwc_R4FR_mag: CA_Magazine
	{
		scope=2;
		type="2*		256";
		model="\bwc_weapons\bwc_75mmFRAG_mag";
		modelSpecial="\bwc_weapons\bwc_R4_RGfrag";
		displayName="75mm FRAG Rifle Grenade";
		picture="\bwc_weapons\data\ico\m_75frag_ca.paa";
		ammo="bwc_75mm_FR";
		initSpeed=80;
		count=1;
	};
	class bwc_R1HE_mag: CA_Magazine
	{
		scope=2;
		type="2*		256";
		model="\bwc_weapons\bwc_75mmHEAT_mag";
		modelSpecial="\bwc_weapons\bwc_R1_HEAT";
		displayName="75mm HEAT Rifle Grenade";
		picture="\bwc_weapons\data\ico\m_75heat_ca.paa";
		ammo="bwc_75mm_HE";
		initSpeed=80;
		count=1;
	};
	class bwc_R1FR_mag: CA_Magazine
	{
		scope=2;
		type="2*		256";
		model="\bwc_weapons\bwc_75mmFRAG_mag";
		modelSpecial="\bwc_weapons\bwc_R1_FRAG";
		displayName="75mm FRAG Rifle Grenade";
		picture="\bwc_weapons\data\ico\m_75frag_ca.paa";
		ammo="bwc_75mm_FR";
		initSpeed=80;
		count=1;
	};*/
};
class cfgWeapons
{
	class bwc_Single : Mode_SemiAuto
	{
		multiplier=1;
		burst=1;
		displayName="Single";
		dispersion=0.00019999999;
		sound[]=
		{
			"",
			10,
			1
		};
		soundContinuous=0;
		soundBurst=0;
		reloadTime=0.15000001;
		ffCount=1;
		ffMagnitude=0.5;
		ffFrequency=11;
		flash="gunfire";
		flashSize=0.1;
		recoil="Empty";
		recoilProne="Empty";
		autoFire=0;
		useAction=0;
		useActionTitle="";
		showToPlayer=1;
		minRange=9.9999997e-006;
		minRangeProbab=9.9999997e-006;
		midRange=9.9999997e-006;
		midRangeProbab=9.9999997e-006;
		maxRange=9.9999997e-006;
		maxRangeProbab=9.9999997e-006;
		aiRateOfFire=0;
		aiRateOfFireDistance=0;
	};
	class bwc_Burst: bwc_Single
	{
		displayName="Burst";
		sound[]=
		{
			"",
			10,
			1
		};
		soundBurst=0;
		burst=3;
	};
	class bwc_Auto: bwc_Single
	{
		displayName="Automatic";
		sound[]=
		{
			"",
			10,
			1
		};
		soundContinuous=0;
		autoFire=1;
	};
	/*class bwc_AI_Single: bwc_Single
	{
		displayName="AI Semi-Automatic";
		showToPlayer=0;
	};
	class bwc_AI_Burst: bwc_Burst
	{
		displayName="AI Burst";
		showToPlayer=0;
	};
	class bwc_AI_Auto: bwc_Auto
	{
		displayName="AI Automatic";
		showToPlayer=0;
	};*/
	class Default;
	class PistolCore;
	class RifleCore;
	class MGunCore;
	class GrenadeCore;
	class Pistol;
	class Rifle;
	class GrenadeLauncher;
	class bwc_AK47: Rifle
	{
		scope=2;
		value=0;
		model="\bwc_weapons\bwc_AK47";
		optics=1;
		dexterity=1.7;
		flash="gunfire";
		flashSize=0.5;
		displayName="AK-47";
		picture="\bwc_weapons\data\ico\w_AK47_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\AK47_reload.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_AK47_mag",
			"bwc_AKM_mag"
		};
		modes[]=
		{
			"Single",
			"Auto"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			reloadTime=0.092;
			recoil="bwc_AK47Recoil";
			recoilProne="bwc_AK47RecoilProne";
			aiRateOfFire=2.5;
			aiRateOfFireDistance=400;
			dispersion=0.0027999999;
			minRange=2;
			minRangeProbab=0.1;
			midRange=250;
			midRangeProbab=0.69999999;
			maxRange=400;
			maxRangeProbab=0.5;
		};
		class Auto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			soundBurst=0;
			recoil="bwc_AK47Recoil";
			recoilProne="bwc_AK47RecoilProne";
			dispersion=0.0049999999;
			reloadTime=0.092;
			aiRateOfFire=0.1;
			aiRateOfFireDistance=100;
			minRange=2;
			minRangeProbab=0.1;
			midRange=70;
			midRangeProbab=0.57999998;
			maxRange=100;
			maxRangeProbab=0.039999999;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_AK47";
		};
	};
	class bwc_SKS: bwc_AK47
	{
		scope=2;
		value=0;
		model="\bwc_weapons\bwc_SKS";
		opticsZoomInit=0.375;
		dexterity=1.66;
		displayName="SKS";
		descriptionShort="Soviet semi-automatic carbine chambered for the 7.62x39mm round.<br/>Magazine: 10Rnd. SKS Clip";
		picture="\bwc_weapons\data\ico\w_SKS_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		optics=1;
		drySound[]={"\bwc_weapons\data\snd\rifle_empty.ogg",0.0099999998,1,10};
		modes[]={"Single"};
		magazineReloadTime=3;
		reloadMagazineSound[]={"\bwc_weapons\data\snd\SKS_reload.ogg",0.0099999998,1,20};
		magazines[]={"bwc_SKS_mag"};
		class Single: bwc_Single
		{
			sound[]={"\bwc_weapons\data\snd\SKS_fire.ogg",10,1};
			reloadTime=1;
			recoil="bwc_SKSRecoil";
			recoilProne="bwc_SKSRecoilProne";
			dispersion=0.0024999999;
			aiRateOfFire=5;
			minRange=2;
			minRangeProbab=0.1;
			midRange=250;
			midRangeProbab=0.5;
			maxRange=400;
			maxRangeProbab=0.039999999;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_SKS";
		};
	};	
	/*class bwc_Star: Pistol
	{
		scope=2;
		model="\bwc_weapons\bwc_Star";
		modelOptics="-";
		picture="\bwc_weapons\data\ico\w_Star_ca.paa";
		minRange=2;
		minRangeProbab=0.1;
		midRange=30;
		midRangeProbab=0.80000001;
		maxRange=50;
		maxRangeProbab=0.039999999;
		optics=1;
		distanceZoomMin=50;
		distanceZoomMax=50;
		displayName="Star Pistol";
		sound[]=
		{
			"\bwc_weapons\data\snd\9mm_fire.ogg",
			10,
			1
		};
		drySound[]=
		{
			"\bwc_weapons\data\snd\pistol_empty.ogg",
			1.1220185,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\pistol_load.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_Star_mag"
		};
		dispersion=0.02;
		ffCount=1;
		recoil="bwc_9mm_PistolRecoil";
		aiRateOfFire=0.5;
		aiRateOfFireDistance=50;
		class Library
		{
			libTextDesc="$STR_BWC_Lib_Star";
		};
	};
	class bwc_Z88: Pistol
	{
		scope=2;
		model="\bwc_weapons\bwc_Z88";
		modelOptics="-";
		picture="\bwc_weapons\data\ico\w_Z88_ca.paa";
		minRange=2;
		minRangeProbab=0.1;
		midRange=30;
		midRangeProbab=0.80000001;
		maxRange=50;
		maxRangeProbab=0.039999999;
		optics=1;
		distanceZoomMin=50;
		distanceZoomMax=50;
		displayName="Z88 Pistol";
		sound[]=
		{
			"\bwc_weapons\data\snd\9mm_fire.ogg",
			10,
			1
		};
		drySound[]=
		{
			"\bwc_weapons\data\snd\pistol_empty.ogg",
			1.1220185,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\pistol_load.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_Z88_mag"
		};
		dispersion=0.02;
		ffCount=1;
		recoil="bwc_9mm_PistolRecoil";
		aiRateOfFire=0.5;
		aiRateOfFireDistance=50;
		class Library
		{
			libTextDesc="$STR_BWC_Lib_Z88";
		};
	};
	class bwc_T33: Pistol
	{
		scope=2;
		model="\bwc_weapons\bwc_T33";
		modelOptics="-";
		picture="\bwc_weapons\data\ico\w_T33_ca.paa";
		minRange=2;
		minRangeProbab=0.1;
		midRange=30;
		midRangeProbab=0.80000001;
		maxRange=70;
		maxRangeProbab=0.039999999;
		optics=1;
		distanceZoomMin=50;
		distanceZoomMax=50;
		displayName="Tokarev T33 Pistol";
		sound[]=
		{
			"\bwc_weapons\data\snd\T33_fire.wss",
			31.622778,
			1
		};
		drySound[]=
		{
			"\bwc_weapons\data\snd\pistol_empty.ogg",
			1.1220185,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\pistol_load.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_T33_mag"
		};
		dispersion=0.02;
		ffCount=1;
		recoil="bwc_762mm_PistolRecoil";
		aiRateOfFire=0.5;
		aiRateOfFireDistance=50;
		class Library
		{
			libTextDesc="$STR_BWC_Lib_T33";
		};
	};
	class bwc_R1: Rifle
	{
		scope=2;
		value=0;
		model="\bwc_weapons\bwc_R1";
		optics=1;
		dexterity=1.64;
		flash="gunfire";
		flashSize=0.5;
		displayName="R1 7.62mm";
		picture="\bwc_weapons\data\ico\w_R1_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\R1_reload.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_R1_mag"
		};
		modes[]=
		{
			"Single",
			"Auto"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\R1_fire.ogg",
				10,
				1
			};
			reloadTime=0.15000001;
			recoil="bwc_R1Recoil";
			recoilProne="bwc_R1RecoilProne";
			dispersion=0.0015;
			minRange=2;
			minRangeProbab=0.1;
			midRange=250;
			midRangeProbab=0.69999999;
			maxRange=400;
			maxRangeProbab=0.5;
		};
		class Auto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\R1_fire.ogg",
				10,
				1
			};
			soundBurst=0;
			recoil="bwc_R1Recoil";
			recoilProne="bwc_R1RecoilProne";
			dispersion=0.0040000002;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R1";
		};
	};
	class bwc_R1_S: Rifle
	{
		scope=2;
		model="\bwc_weapons\bwc_R1_Sniper";
		picture="\bwc_weapons\data\ico\w_R1s_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_sniper_CA.paa";
		displayName="R1 Sniper 7.62mm";
		dexterity=1.6;
		modelOptics="\bwc_Weapons\bwc_R1_Opt";
		optics=1;
		opticsZoomMin=0.039999999;
		opticsZoomMax=0.12;
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		distanceZoomMin=400;
		distanceZoomMax=400;
		sound[]=
		{
			"\bwc_weapons\data\snd\R1_fire.ogg",
			10,
			1
		};
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\R1_reload.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_R1s_mag",
			"bwc_R1_mag"
		};
		reloadTime=0.15000001;
		soundContinuous=0;
		backgroundReload=1;
		recoil="bwc_R1sRecoil";
		recoilProne="bwc_R1sRecoilProne";
		dispersion=9.9999997e-005;
		minRange=2;
		minRangeProbab=0.1;
		midRange=500;
		midRangeProbab=0.69999999;
		maxRange=800;
		maxRangeProbab=0.1;
		aiRateOfFire=5;
		aiRateOfFireDistance=800;
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R1";
		};
	};
	class bwc_R1_HEAT: bwc_R1
	{
		displayName="R1 Rifle Grenade 7.62mm";
		model="\bwc_weapons\bwc_R1_RG";
		modelSpecial="\bwc_weapons\bwc_R1_HEAT";
		muzzles[]=
		{
			"R1Muzzle",
			"R1_HEATMuzzle"
		};
		picture="\bwc_weapons\data\ico\w_R1H_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		dexterity=1.46;
		class R1Muzzle: bwc_R1
		{
		};
		class R1_HEATMuzzle: GrenadeLauncher
		{
			displayName="75mm HEAT Rifle Grenade";
			magazines[]=
			{
				"bwc_R1HE_mag"
			};
			sound[]=
			{
				"\bwc_weapons\data\snd\75_fire.ogg",
				3.1622777,
				1
			};
			reloadMagazineSound[]=
			{
				"\bwc_weapons\data\snd\75_reload.ogg",
				3.1622777,
				1
			};
			magazineReloadTime=0;
			reloadTime=0.1;
			recoil="bwc_m79Recoil";
			recoilProne="bwc_m79RecoilProne";
			optics=1;
			modelOptics="-";
			cameraDir="GL look";
			memoryPointCamera="GL eye";
			muzzlePos="usti granatometu";
			muzzleEnd="konec granatometu";
			opticsZoomMin=0.22;
			opticsZoomMax=0.94999999;
			opticsZoomInit=0.41999999;
		};
	};
	class bwc_R1_FRAG: bwc_R1
	{
		displayName="R1 Rifle Grenade 7.62mm";
		model="\bwc_weapons\bwc_R1_RG";
		modelSpecial="\bwc_weapons\bwc_R1_FRAG";
		muzzles[]=
		{
			"R1Muzzle",
			"R1_FRAGMuzzle"
		};
		picture="\bwc_weapons\data\ico\w_R1F_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		dexterity=1.46;
		class R1Muzzle: bwc_R1
		{
		};
		class R1_FRAGMuzzle: GrenadeLauncher
		{
			displayName="75mm FRAG Rifle Grenade";
			magazines[]=
			{
				"bwc_R1FR_mag"
			};
			sound[]=
			{
				"\bwc_weapons\data\snd\75_fire.ogg",
				3.1622777,
				1
			};
			reloadMagazineSound[]=
			{
				"\bwc_weapons\data\snd\75_reload.ogg",
				3.1622777,
				1
			};
			magazineReloadTime=0;
			reloadTime=0.1;
			recoil="bwc_m79Recoil";
			recoilProne="bwc_m79RecoilProne";
			optics=1;
			modelOptics="-";
			cameraDir="GL look";
			memoryPointCamera="GL eye";
			muzzlePos="usti granatometu";
			muzzleEnd="konec granatometu";
			opticsZoomMin=0.22;
			opticsZoomMax=0.94999999;
			opticsZoomInit=0.41999999;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R1";
		};
	};
	class bwc_R2: bwc_R1
	{
		model="\bwc_weapons\bwc_R2";
		optics=1;
		displayName="R2 Para 7.62mm";
		picture="\bwc_weapons\data\ico\w_R2_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\R1_reload.ogg",
			1.1220185,
			1
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R1";
		};
	};
	class bwc_R2_F: bwc_R2
	{
		model="\bwc_weapons\bwc_R2_f";
		optics=1;
		displayName="R2 Para 7.62mm";
		picture="\bwc_weapons\data\ico\w_R2f_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\R1_reload.ogg",
			1.1220185,
			1
		};
	};
	class bwc_R4: Rifle
	{
		scope=2;
		value=0;
		model="\bwc_weapons\bwc_R4";
		optics=1;
		dexterity=1.64;
		flash="gunfire";
		flashSize=0.5;
		displayName="R4 AR 5.56mm";
		picture="\bwc_weapons\data\ico\w_R4_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\R4_reload.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_R4_mag"
		};
		modes[]=
		{
			"Single",
			"Auto"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\R4_fire.ogg",
				7.9432826,
				1
			};
			reloadTime=0.15000001;
			recoil="bwc_R4Recoil";
			recoilProne="bwc_R4RecoilProne";
			dispersion=0.0015;
			minRange=2;
			minRangeProbab=0.1;
			midRange=250;
			midRangeProbab=0.69999999;
			maxRange=400;
			maxRangeProbab=0.050000001;
		};
		class Auto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\R4_fire.ogg",
				7.9432826,
				1
			};
			soundBurst=0;
			recoil="bwc_R4Recoil";
			recoilProne="bwc_R4RecoilProne";
			dispersion=0.0040000002;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R4";
		};
	};
	class bwc_R4_F: bwc_R4
	{
		model="\bwc_weapons\bwc_R4_f";
		picture="\bwc_weapons\data\ico\w_R4_fold_ca.paa";
	};
	class bwc_R4_HEAT: bwc_R4
	{
		displayName="R4 AR 5.56mm";
		model="\bwc_weapons\bwc_R4_RG";
		modelSpecial="\bwc_weapons\bwc_R4_RGheat";
		muzzles[]=
		{
			"R4Muzzle",
			"HEATMuzzle"
		};
		picture="\bwc_weapons\data\ico\w_R4_heat_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		dexterity=1.46;
		class R4Muzzle: bwc_R4
		{
		};
		class HEATMuzzle: GrenadeLauncher
		{
			displayName="75mm HEAT Rifle Grenade";
			magazines[]=
			{
				"bwc_R4HE_mag"
			};
			sound[]=
			{
				"\bwc_weapons\data\snd\75_fire.ogg",
				3.1622777,
				1
			};
			reloadMagazineSound[]=
			{
				"\bwc_weapons\data\snd\75_reload.ogg",
				3.1622777,
				1
			};
			magazineReloadTime=0;
			reloadTime=0.1;
			recoil="bwc_m79Recoil";
			recoilProne="bwc_m79RecoilProne";
			optics=1;
			modelOptics="-";
			cameraDir="GL look";
			memoryPointCamera="GL eye";
			muzzlePos="usti granatometu";
			muzzleEnd="konec granatometu";
			opticsZoomMin=0.22;
			opticsZoomMax=0.94999999;
			opticsZoomInit=0.41999999;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R4H";
		};
	};
	class bwc_R4_FRAG: bwc_R4
	{
		displayName="R4 AR 5.56mm";
		model="\bwc_weapons\bwc_R4_RG";
		modelSpecial="\bwc_weapons\bwc_R4_RGfrag";
		muzzles[]=
		{
			"R4Muzzle",
			"FRAGMuzzle"
		};
		picture="\bwc_weapons\data\ico\w_R4_frag_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		dexterity=1.46;
		class R4Muzzle: bwc_R4
		{
		};
		class FRAGMuzzle: GrenadeLauncher
		{
			displayName="75mm FRAG Rifle Grenade";
			magazines[]=
			{
				"bwc_R4FR_mag"
			};
			sound[]=
			{
				"\bwc_weapons\data\snd\75_fire.ogg",
				3.1622777,
				1
			};
			reloadMagazineSound[]=
			{
				"\bwc_weapons\data\snd\75_reload.ogg",
				3.1622777,
				1
			};
			magazineReloadTime=0;
			reloadTime=0.1;
			recoil="bwc_m79Recoil";
			recoilProne="bwc_m79RecoilProne";
			optics=1;
			modelOptics="-";
			cameraDir="GL look";
			memoryPointCamera="GL eye";
			muzzlePos="usti granatometu";
			muzzleEnd="konec granatometu";
			opticsZoomMin=0.22;
			opticsZoomMax=0.94999999;
			opticsZoomInit=0.41999999;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R4F";
		};
	};
	class bwc_R5: Rifle
	{
		scope=2;
		value=0;
		model="\bwc_weapons\bwc_R5";
		optics=1;
		dexterity=1.64;
		flash="gunfire";
		flashSize=0.5;
		displayName="R5 AR 5.56mm";
		picture="\bwc_weapons\data\ico\w_R5_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\R4_reload.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_R4_mag"
		};
		modes[]=
		{
			"Single",
			"Auto"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\R4_fire.ogg",
				7.9432826,
				1
			};
			reloadTime=0.15000001;
			recoil="bwc_R5Recoil";
			recoilProne="bwc_R5RecoilProne";
			dispersion=0.0015;
			minRange=2;
			minRangeProbab=0.090000004;
			midRange=250;
			midRangeProbab=0.60000002;
			maxRange=400;
			maxRangeProbab=0.039999999;
		};
		class Auto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\R4_fire.ogg",
				7.9432826,
				1
			};
			soundBurst=0;
			recoil="bwc_R5Recoil";
			recoilProne="bwc_R5RecoilProne";
			dispersion=0.0040000002;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_R5";
		};
	};
	class bwc_R5_F: bwc_R5
	{
		model="\bwc_weapons\bwc_R5_f";
		picture="\bwc_weapons\data\ico\w_R5_fold_ca.paa";
	};
	class bwc_M79: GrenadeLauncher
	{
		scope=2;
		displayName="M79 Grenade Launcher";
		model="\bwc_Weapons\bwc_M79.p3d";
		picture="\bwc_Weapons\data\ico\w_M79_ca.paa";
		type=1;
		cursor="GLCursor";
		cursorAim="\ca\Weapons\Data\clear_empty";
		cursorSize=4;
		value=3;
		canLock="LockNo";
		sound[]=
		{
			"\bwc_weapons\data\snd\M79_fire.ogg",
			31.622778,
			1
		};
		magazineReloadTime=1;
		reloadTime=0.1;
		autoReload=0;
		autoAimEnabled=0;
		ffMagnitude=0.1;
		ffFrequency=1;
		ffCount=1;
		muzzlePos="usti granatometu";
		muzzleEnd="konec granatometu";
		cartridgePos="";
		cartridgeVel="";
		optics=1;
		modelOptics="-";
		cameraDir="GL look";
		memoryPointCamera="GL eye";
		opticsZoomMin=0.22;
		opticsZoomMax=0.94999999;
		opticsZoomInit=0.41999999;
		recoil="bwc_m79Recoil";
		recoilProne="bwc_m79RecoilProne";
		dexterity=1.78;
		aiRateOfFire=3;
		aiRateOfFireDistance=280;
		minRange=10;
		minRangeProbab=0.60000002;
		midRange=200;
		midRangeProbab=0.80000001;
		maxRange=400;
		maxRangeProbab=0.60000002;
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_Weapons\data\snd\M79_load.ogg",
			1,
			1
		};
		magazines[]=
		{
			"bwc_M79_round",
			"bwc_M79_FlareWhite",
			"bwc_M79_FlareGreen",
			"bwc_M79_FlareRed",
			"bwc_M79_FlareYellow"
		};
		reloadAction="ManActReloadMagazine";
		dispersion=0.015;
		aiDispersionCoefY=2;
		class Library
		{
			libTextDesc="$STR_BWC_Lib_M79";
		};
	};
	class bwc_AKS47: bwc_AK47
	{
		model="\bwc_weapons\bwc_AKS47";
		displayName="AKS-47";
		picture="\bwc_weapons\data\ico\w_AKS47_ca.paa";
		modes[]=
		{
			"Single",
			"Auto"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			recoil="bwc_AKSRecoil";
			recoilProne="bwc_AKSRecoilProne";
			aiRateOfFire=2.5;
			aiRateOfFireDistance=400;
			dispersion=0.0027999999;
			reloadTime=0.092;
			minRange=2;
			minRangeProbab=0.1;
			midRange=250;
			midRangeProbab=0.69999999;
			maxRange=400;
			maxRangeProbab=0.5;
		};
		class Auto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			recoil="bwc_AKSRecoil";
			recoilProne="bwc_AKSRecoilProne";
			soundBurst=0;
			dispersion=0.0049999999;
			reloadTime=0.092;
			aiRateOfFire=0.1;
			aiRateOfFireDistance=100;
			minRange=2;
			minRangeProbab=0.1;
			midRange=70;
			midRangeProbab=0.57999998;
			maxRange=100;
			maxRangeProbab=0.039999999;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_AKS";
		};
	};
	class bwc_AKS47_F: bwc_AKS47
	{
		model="\bwc_weapons\bwc_AKS47_F";
		displayName="AKS-47";
		picture="\bwc_weapons\data\ico\w_AKS47_F_ca.paa";
	};
	class bwc_AK47_GL: bwc_AK47
	{
		scope=2;
		model="\bwc_weapons\bwc_AK47_GL";
		modelSpecial="\bwc_weapons\bwc_AK47_GLL";
		dexterity=1.5;
		displayName="AK-47w/GP25";
		picture="\bwc_weapons\data\ico\w_AK47_GL_ca.paa";
		muzzles[]=
		{
			"bwc_AK47Muzzle",
			"bwc_GP25Muzzle"
		};
		class bwc_AK47Muzzle: bwc_AK47
		{
		};
		class bwc_GP25Muzzle: GrenadeLauncher
		{
			displayName="GP25 Grenadelauncher";
			optics=1;
			modelOptics="-";
			cameraDir="GL look";
			memoryPointCamera="GL eye";
			opticsZoomMin=0.22;
			opticsZoomMax=0.94999999;
			opticsZoomInit=0.41999999;
			magazineReloadTime=0;
			reloadTime=0.1;
			recoil="bwc_GP25Recoil";
			recoilProne="bwc_GP25Recoil";
			sound[]=
			{
				"\bwc_weapons\data\snd\GP25_fire.ogg",
				3.1622777,
				1
			};
			drySound[]=
			{
				"\bwc_weapons\data\snd\rifle_empty.ogg",
				1,
				1
			};
			reloadMagazineSound[]=
			{
				"\bwc_Weapons\data\snd\GP25_reload.ogg",
				1,
				1
			};
			magazines[]=
			{
				"bwc_VOG_25_round",
				"bwc_GP25_FlareWhite",
				"bwc_GP25_FlareGreen",
				"bwc_GP25_FlareRed",
				"bwc_GP25_FlareYellow"
			};
			reloadAction="ManActReloadMagazine";
		};
	};
	class bwc_AKM: bwc_AK47
	{
		model="\bwc_weapons\bwc_AKM";
		displayName="AKM";
		picture="\bwc_weapons\data\ico\w_AKM_ca.paa";
		modes[]=
		{
			"Single",
			"Auto"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			recoil="bwc_AKMRecoil";
			recoilProne="bwc_AKMRecoilProne";
			aiRateOfFire=2.5;
			aiRateOfFireDistance=400;
			dispersion=0.0027999999;
			reloadTime=0.092;
			minRange=2;
			minRangeProbab=0.1;
			midRange=250;
			midRangeProbab=0.69999999;
			maxRange=400;
			maxRangeProbab=0.5;
		};
		class Auto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			recoil="bwc_AKMRecoil";
			recoilProne="bwc_AKMRecoilProne";
			soundBurst=0;
			dispersion=0.0049999999;
			reloadTime=0.092;
			aiRateOfFire=0.1;
			aiRateOfFireDistance=100;
			minRange=2;
			minRangeProbab=0.1;
			midRange=70;
			midRangeProbab=0.57999998;
			maxRange=100;
			maxRangeProbab=0.039999999;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_AKM";
		};
	};
	class bwc_AKMS: bwc_AKM
	{
		model="\bwc_weapons\bwc_AKMS";
		displayName="AKMS";
		picture="\bwc_weapons\data\ico\w_AKMS_ca.paa";
		modes[]=
		{
			"Single",
			"Auto"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			recoil="bwc_AKSRecoil";
			recoilProne="bwc_AKSRecoilProne";
			reloadTime=0.092;
			aiRateOfFire=2.5;
			aiRateOfFireDistance=400;
			dispersion=0.0027999999;
			minRange=2;
			minRangeProbab=0.1;
			midRange=250;
			midRangeProbab=0.69999999;
			maxRange=400;
			maxRangeProbab=0.5;
		};
		class Auto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_weapons\data\snd\AK47_fire.ogg",
				31.622778,
				1
			};
			recoil="bwc_AKSRecoil";
			recoilProne="bwc_AKSRecoilProne";
			soundBurst=0;
			dispersion=0.0049999999;
			reloadTime=0.092;
			aiRateOfFire=0.1;
			aiRateOfFireDistance=100;
			minRange=2;
			minRangeProbab=0.1;
			midRange=70;
			midRangeProbab=0.57999998;
			maxRange=100;
			maxRangeProbab=0.039999999;
		};
	};
	class bwc_AKMS_F: bwc_AKMS
	{
		model="\bwc_weapons\bwc_AKMS_F";
		displayName="AKMS";
		picture="\bwc_weapons\data\ico\w_AKMS_F_ca.paa";
	};
	class bwc_AKM_GL: bwc_AKM
	{
		scope=2;
		model="\bwc_weapons\bwc_AKM_GL";
		modelSpecial="\bwc_weapons\bwc_AKM_GLL";
		dexterity=1.5;
		displayName="AKM w/GP25";
		picture="\bwc_weapons\data\ico\w_AKM_GL_ca.paa";
		muzzles[]=
		{
			"bwc_AKMMuzzle",
			"bwc_AKMGP25Muzzle"
		};
		class bwc_AKMMuzzle: bwc_AKM
		{
		};
		class bwc_AKMGP25Muzzle: GrenadeLauncher
		{
			displayName="GP25 Grenadelauncher";
			optics=1;
			modelOptics="-";
			cameraDir="GL look";
			memoryPointCamera="GL eye";
			opticsZoomMin=0.22;
			opticsZoomMax=0.94999999;
			opticsZoomInit=0.41999999;
			magazineReloadTime=0;
			reloadTime=0.1;
			recoil="bwc_GP25Recoil";
			recoilProne="bwc_GP25Recoil";
			sound[]=
			{
				"\bwc_weapons\data\snd\GP25_fire.ogg",
				3.1622777,
				1
			};
			drySound[]=
			{
				"\bwc_weapons\data\snd\rifle_empty.ogg",
				1,
				1
			};
			reloadMagazineSound[]=
			{
				"\bwc_Weapons\data\snd\GP25_reload.ogg",
				1,
				1
			};
			magazines[]=
			{
				"bwc_VOG_25P_round",
				"bwc_GP25_FlareWhite",
				"bwc_GP25_FlareGreen",
				"bwc_GP25_FlareRed",
				"bwc_GP25_FlareYellow"
			};
			reloadAction="ManActReloadMagazine";
		};
	};
	class bwc_RPK: bwc_AK47
	{
		scope=2;
		value=0;
		opticsZoomInit=0.375;
		type="1	 + 	4";
		model="\bwc_weapons\bwc_RPK";
		displayName="RPK";
		picture="\bwc_weapons\data\ico\w_RPK_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_regular_CA.paa";
		optics=1;
		drySound[]=
		{
			"\bwc_weapons\data\snd\rifle_empty.ogg",
			1,
			1
		};
		dexterity=1.4;
		reloadMagazineSound[]=
		{
			"\bwc_Weapons\data\snd\RPK_reload.ogg",
			1,
			1
		};
		modes[]=
		{
			"Single",
			"AI_fullauto",
			"FullAuto"
		};
		magazines[]=
		{
			"bwc_RPK_mag",
			"bwc_AK47_mag",
			"bwc_AKM_mag"
		};
		class Single: bwc_Single
		{
			sound[]=
			{
				"\bwc_Weapons\data\snd\RPK_fire.ogg",
				30,
				1
			};
			reloadTime=0.092;
			recoil="bwc_RPKRecoil";
			recoilProne="bwc_RPKRecoilProne";
			dispersion=0.0024999999;
			aiRateOfFire=5;
			aiRateOfFireDistance=100;
			minRange=2;
			minRangeProbab=0.1;
			midRange=10;
			midRangeProbab=0.69999999;
			maxRange=20;
			maxRangeProbab=0.039999999;
		};
		class AI_FullAuto: bwc_AI_Auto
		{
			sound[]=
			{
				"\bwc_Weapons\data\snd\RPK_fire.ogg",
				30,
				1
			};
			reloadTime=0.092;
			ffCount=5;
			aiRateOfFire=0.25;
			aiRateOfFireDistance=500;
			recoil="bwc_RPKRecoil";
			recoilProne="bwc_RPKRecoilProne";
			dispersion=0.0070000002;
			minRange=2;
			minRangeProbab=0.1;
			midRange=150;
			midRangeProbab=0.69999999;
			maxRange=250;
			maxRangeProbab=0.050000001;
			showToPlayer=0;
		};
		class FullAuto: bwc_Auto
		{
			sound[]=
			{
				"\bwc_Weapons\data\snd\RPK_fire.ogg",
				30,
				1
			};
			reloadTime=0.092;
			ffCount=5;
			aiRateOfFire=0.25;
			aiRateOfFireDistance=500;
			recoil="bwc_RPKRecoil";
			recoilProne="bwc_RPKRecoilProne";
			dispersion=0.0055;
			minRange=2;
			minRangeProbab=0.1;
			midRange=10;
			midRangeProbab=0.69999999;
			maxRange=20;
			maxRangeProbab=0.050000001;
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_RPK";
		};
	};
	class bwc_RPK_D: bwc_RPK
	{
		scope=2;
		value=0;
		opticsZoomInit=0.375;
		type="1	 + 	4";
		model="\bwc_weapons\bwc_RPK_D";
		displayName="RPK (Drum Magazine)";
		picture="\bwc_weapons\data\ico\w_RPK_D_ca.paa";
		magazines[]=
		{
			"bwc_RPK_D_mag"
		};
	};
	class bwc_SVD: Rifle
	{
		scope=2;
		model="\bwc_weapons\bwc_SVD";
		picture="\bwc_weapons\data\ico\w_SVD_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_sniper_CA.paa";
		displayName="SVD Dragunov";
		dexterity=1.35;
		autoFire=0;
		modelOptics="\bwc_Weapons\bwc_SVD_Opt";
		optics=1;
		opticsZoomMin=0.050000001;
		opticsZoomMax=0.090000004;
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		distanceZoomMin=400;
		distanceZoomMax=400;
		sound[]=
		{
			"\bwc_weapons\data\snd\SVD_fire.ogg",
			10,
			1
		};
		drySound[]=
		{
			"\bwc_weapons\data\snd\SVD_empty.ogg",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"\bwc_weapons\data\snd\SVD_reload.ogg",
			1.1220185,
			1
		};
		magazines[]=
		{
			"bwc_SVD_mag"
		};
		reloadTime=0.15000001;
		soundContinuous=0;
		backgroundReload=1;
		recoil="bwc_SVDRecoil";
		recoilProne="bwc_SVDRecoilProne";
		dispersion=9.9999997e-005;
		minRange=2;
		minRangeProbab=0.1;
		midRange=500;
		midRangeProbab=0.69999999;
		maxRange=800;
		maxRangeProbab=0.1;
		aiRateOfFire=5;
		aiRateOfFireDistance=800;
		class Library
		{
			libTextDesc="$STR_BWC_Lib_SVD";
		};
	};
	class bwc_FNMAG: Rifle
	{
		scope=2;
		displayName="FNMAG";
		model="\bwc_weapons\bwc_FNMAG.p3d";
		picture="\bwc_weapons\data\ico\w_FNMAG_ca.paa";
		UiPicture="\CA\weapons\data\Ico\i_mg_CA.paa";
		cursor="MGCursor";
		cursoraim="\ca\Weapons\Data\w_lock";
		flash="gunfire";
		flashSize=0.5;
		modes[]=
		{
			"manual",
			"close",
			"short",
			"medium",
			"far"
		};
		class manual: bwc_Auto
		{
			reloadTime=0.07;
			recoil="bwc_FNMAGRecoil";
			recoilProne="bwc_FNMAGRecoilProne";
			dispersion=0.0038000001;
			sound[]=
			{
				"\bwc_Weapons\data\snd\fnmag_fire.ogg",
				5.6234136,
				1
			};
			soundContinuous=0;
			soundBurst=0;
			minRange=1;
			minRangeProbab=0.30000001;
			midRange=5;
			midRangeProbab=0.57999998;
			maxRange=10;
			maxRangeProbab=0.039999999;
			showToPlayer=1;
		};
		class close: manual
		{
			burst=10;
			aiRateOfFire=0.5;
			aiRateOfFireDistance=50;
			minRange=10;
			minRangeProbab=0.050000001;
			midRange=20;
			midRangeProbab=0.57999998;
			maxRange=50;
			maxRangeProbab=0.039999999;
			showToPlayer=0;
		};
		class short: close
		{
			burst=6;
			aiRateOfFire=2;
			aiRateOfFireDistance=200;
			minRange=50;
			minRangeProbab=0.050000001;
			midRange=100;
			midRangeProbab=0.57999998;
			maxRange=200;
			maxRangeProbab=0.039999999;
		};
		class medium: close
		{
			burst=8;
			aiRateOfFire=4;
			aiRateOfFireDistance=400;
			minRange=200;
			minRangeProbab=0.050000001;
			midRange=300;
			midRangeProbab=0.57999998;
			maxRange=400;
			maxRangeProbab=0.039999999;
		};
		class far: close
		{
			burst=7;
			aiRateOfFire=7;
			aiRateOfFireDistance=600;
			minRange=400;
			minRangeProbab=0.050000001;
			midRange=500;
			midRangeProbab=0.40000001;
			maxRange=600;
			maxRangeProbab=0.0099999998;
		};
		aiDispersionCoefY=7;
		aiDispersionCoefX=7;
		dexterity=1.21;
		type="1	 + 	4";
		reloadMagazineSound[]=
		{
			"\bwc_Weapons\data\snd\fnmag_reload.ogg",
			1,
			1
		};
		magazines[]=
		{
			"bwc_FNMAG_mag"
		};
		class Library
		{
			libTextDesc="$STR_BWC_Lib_FNMAG";
		};
	};*/
};
/*class CfgNonAIVehicles
{
	class ProxyWeapon
	{
	};
	class ProxySecWeapon
	{
	};
	class Proxybwc_R1: ProxyWeapon
	{
	};
	class Proxybwc_R1S: ProxyWeapon
	{
	};
	class Proxybwc_R1_HEAT: ProxyWeapon
	{
	};
	class Proxybwc_R1_FRAG: ProxyWeapon
	{
	};
	class Proxybwc_R2: ProxyWeapon
	{
	};
	class Proxybwc_R2F: ProxyWeapon
	{
	};
	class Proxybwc_R4: ProxyWeapon
	{
	};
	class Proxybwc_R4_f: ProxyWeapon
	{
	};
	class Proxybwc_R4_RGF: ProxyWeapon
	{
	};
	class Proxybwc_R4_RGH: ProxyWeapon
	{
	};
	class Proxybwc_R5: ProxyWeapon
	{
	};
	class Proxybwc_R5_f: ProxyWeapon
	{
	};
	class Proxybwc_AK47: ProxyWeapon
	{
	};
	class Proxybwc_AK47_GL: ProxyWeapon
	{
	};
	class Proxybwc_AKS47: ProxyWeapon
	{
	};
	class Proxybwc_AKS47_F: ProxyWeapon
	{
	};
	class Proxybwc_AKM: ProxyWeapon
	{
	};
	class Proxybwc_AKM_GL: ProxyWeapon
	{
	};
	class Proxybwc_AKMS: ProxyWeapon
	{
	};
	class Proxybwc_AKMS_F: ProxyWeapon
	{
	};
	class Proxybwc_SKS: ProxyWeapon
	{
	};
	class Proxybwc_RPK: ProxyWeapon
	{
	};
	class Proxybwc_RPK_D: ProxyWeapon
	{
	};
	class Proxybwc_SVD: ProxyWeapon
	{
	};
	class Proxybwc_M79: ProxyWeapon
	{
	};
	class Proxybwc_FNMAG: ProxyWeapon
	{
	};
	class ProxyHandGun: ProxyWeapon
	{
		simulation="ProxyHandGun";
	};
	class Proxybwc_Z88: ProxyHandGun
	{
	};
	class Proxybwc_Star: ProxyHandGun
	{
	};
	class Proxybwc_T33: ProxyHandGun
	{
	};
};*/
