////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 4.13
//Fri Oct 11 14:38:42 2013 : Source 'file' date Fri Oct 11 14:38:42 2013
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

class CfgPatches
{
	class RH_de_cfg
	{
		//units[] = {"RHpammobox"};
		weapons[] = {"RH_m9","RH_m9sd","RH_mk22","RH_mk22sd","RH_m1911old","RH_g17","RH_tec9","RH_vz61","RH_p38","RH_deagle","RH_usp","RH_uspsd","RH_mk2"/*"RH_deagleg","RH_deagles","RH_mk22v","RH_mk22vsd","RH_m1911","RH_m1911sd","RH_uspm","RH_m93r","RH_m9c","RH_m9sdc","RH_muzi","RH_g18","RH_tt33","RH_ppk","RH_p226","RH_p226s","RH_anac","RH_anacg","RH_bull","RH_python","RH_g19","RH_g19t"*/};
		requiredAddons[] = {"CAweapons"};
		requiredVersion = 1.02;
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class RH_de_cfg
		{
			list[] = {"RH_de_cfg"};
		};
	};
};
class cfgRecoils
{
	RH_45Base[] = {0,0.008,0.064,0.005,0.008,0.064,0.09,0,-0.0033,0.15,0,0};
	deagleBase[] = {0,0.01,0.11,0.005,0.01,0.11,0.1,0,0.003,0.17,0,0};
	RH_PistolBase_Auto[] = {0,0.008,0.035,0.02,0.008,0.035,0.08,0,-0.002,0.2,0,0};
	RH_PistolBase[] = {0,0.008,0.055,0.005,0.008,0.055,0.09,0,-0.003,0.15,0,0};
	RH_LowRecoil[] = {0,0.006,0.02,0.005,0.006,0.02,0.1,0,0,0.12,0,0};
};
class CfgAmmo
{
	class Default;
	class BulletCore;
	class BulletBase;
	class B_9x19_SD;
	class RH_50_AE_Ball: BulletBase
	{
		hit = 7.5;
		cartridge = "FxCartridge_Small";
		cost = 1;
		typicalSpeed = 421;
		airFriction = -0.00165;
		audibleFire=16; // DayZed
		visibleFire=16; //DayZed
	};
	class RH_32ACP: BulletBase
	{
		hit = 4.5;
		cost = 1;
		cartridge = "FxCartridge_Small";
		typicalSpeed = 300;
		airFriction = -0.0015;
		audibleFire=16; // DayZed
		visibleFire=16; //DayZed
	};
	class RH_45ACP: BulletBase
	{
		hit = 5.5;
		cost = 1;
		cartridge = "FxCartridge_Small";
		typicalSpeed = 260;
		airFriction = -0.0015;
		audibleFire=16; // DayZed
		visibleFire=16; //DayZed
	};
	class RH_44mag_ball: BulletBase
	{
		hit = 8.5;
		cost = 1;
		cartridge = "FxCartridge_Small";
		typicalSpeed = 260;
		airFriction = -0.0015;
	};
	class RH_357mag_ball: BulletBase
	{
		hit = 8.9;
		cost = 1;
		cartridge = "FxCartridge_Small";
		typicalSpeed = 260;
		airFriction = -0.0015;
	};
	class RH_762x25: BulletBase
	{
		hit = 7.5;
		cost = 1;
		cartridge = "FxCartridge_Small";
		typicalSpeed = 420;
		airFriction = -0.003;
	};
	class RH_B_9x19_Ball: BulletBase
	{
		cartridge = "FxCartridge_Small";
		cost = 5;
		typicalSpeed = 360;
		airFriction = -0.0017;
		hit = 5;
		audibleFire=16; //DayZed
		visibleFire=16; //DayZed
	};
	class RH_B_9x19_SD: BulletBase
	{
		cartridge = "FxCartridge_Small";
		visibleFire = 0.035;
		audibleFire = 0.035;
		visibleFireTime = 2;
		cost = 1;
		typicalSpeed = 278;
		airFriction = -0.000955;
		hit = 5;
	};
	class RH_B_22LR_SD: BulletBase
	{
		cartridge = "FxCartridge_Small";
		visibleFire = 0.035;
		audibleFire = 0.035;
		visibleFireTime = 2;
		cost = 1;
		typicalSpeed = 278;
		airFriction = -0.0015;
		hit = 6.0;
	};
};
class CfgMagazines
{
	class Default;
	class CA_Magazine;
	class 7Rnd_50_AE: CA_Magazine
	{
		scope = 2;
		displayName = "Desert Eagle Mag.";
		descriptionShort="Caliber: .50AE<br/>Rounds: 7<br/>Used in: Desert Eagle";
		picture = "\RH_de\inv\m_de.paa";
		model = "\RH_de\mags\mag_de.p3d";
		type = 16;
		ammo = "RH_50_AE_Ball";
		count = 7;
		initSpeed = 421;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 6Rnd_44_Mag: CA_Magazine
	{
		scope = 2;
		displayName = ".44 Magnum";
		picture = "\RH_de\inv\m_44m.paa";
		type = 16;
		ammo = "RH_44mag_ball";
		count = 6;
		initSpeed = 421;
	};
	class 6Rnd_357_Mag: CA_Magazine
	{
		scope = 2;
		displayName = ".357 Magnum";
		picture = "\RH_de\inv\m_44m.paa";
		type = 16;
		ammo = "RH_357mag_ball";
		count = 6;
		initSpeed = 421;
	};
	class 8Rnd_9x19_Mk: CA_Magazine
	{
		scope = 2;
		displayName = "Mk22 Mag.";
		descriptionShort="Caliber: 9mm<br/>Rounds: 8<br/>Used in: Mk22";
		model = "\RH_de\mags\mag_mk22.p3d";
		type = 16;
		picture = "\RH_de\inv\m_mk22.paa";
		ammo = "RH_B_9x19_Ball";
		count = 8;
		initSpeed = 365;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 19Rnd_9x19_g18: CA_Magazine
	{
		scope = 2;
		displayName = "Glock 18 mag";
		model = "\RH_de\mags\mag_g18.p3d";
		type = 16;
		picture = "\RH_de\inv\m_g18.paa";
		ammo = "RH_B_9x19_Ball";
		count = 19;
		initSpeed = 365;
	};
	class 17Rnd_9x19_g17: CA_Magazine
	{
		scope = 2;
		displayName = "Glock 17 Mag.";
		descriptionShort="Caliber: 9x19mm<br/>Rounds: 17<br/>Used in: Glock 17";
		model = "\RH_de\mags\mag_g17.p3d";
		type = 16;
		picture = "\RH_de\inv\m_g18.paa";
		ammo = "RH_B_9x19_Ball";
		count = 17;
		initSpeed = 365;
				class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};		
	};
	class 33Rnd_9x19_g18: CA_Magazine
	{
		scope = 2;
		displayName = "Glock 18 33Rnd mag";
		model = "\RH_de\mags\mag_g18.p3d";
		type = 16;
		picture = "\RH_de\inv\m_g18.paa";
		ammo = "RH_B_9x19_Ball";
		count = 33;
		initSpeed = 365;
	};
	class 20Rnd_9x19_M93: CA_Magazine
	{
		scope = 2;
		displayName = "M93R mag";
		model = "\RH_de\mags\mag_m93r.p3d";
		type = 16;
		picture = "\RH_de\inv\m_m93.paa";
		ammo = "RH_B_9x19_Ball";
		count = 20;
		initSpeed = 365;
	};
	class 32Rnd_9x19_Muzi: CA_Magazine
	{
		scope = 2;
		displayName = "MUzi mag";
		model = "\RH_de\mags\mag_muzi.p3d";
		type = 16;
		picture = "\RH_de\inv\m_muzi.paa";
		ammo = "RH_B_9x19_Ball";
		count = 32;
		initSpeed = 360;
	};
	class 30Rnd_9x19_tec: CA_Magazine
	{
		scope = 2;
		displayName = "TEC-9 Mag.";
		descriptionShort="Caliber: 9x19 mm Parabellum<br/>Rounds: 30<br/>Used in: Intratec TEC-9";
		model = "\RH_de\mags\mag_tec9.p3d";
		type = 16;
		picture = "\RH_de\inv\m_tec9.paa";
		ammo = "RH_B_9x19_Ball";
		count = 30;
		initSpeed = 360;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 8Rnd_9x19_Mksd: 8Rnd_9x19_Mk
	{
		displayName = "Mk22 SD Mag.";
		descriptionShort="Caliber: 9mm<br/>Rounds: 8<br/>Used in: Mk22 SD";
		model = "\RH_de\mags\mag_mk22.p3d";
		type = 16;
		picture = "\RH_de\inv\m_mk22.paa";
		ammo = "RH_B_9x19_SD";
		initSpeed = 278;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 8Rnd_9x19_P38: CA_Magazine
	{
		scope = 2;
		displayName = "P38 Mag.";
		descriptionShort="Caliber: 9x19mm Parabellum<br/>Rounds: 8<br/>Used in: P38";
		model = "\RH_de\mags\mag_p38.p3d";
		type = 16;
		picture = "\RH_de\inv\m_p38.paa";
		ammo = "RH_B_9x19_Ball";
		count = 8;
		initSpeed = 365;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 15Rnd_9x19_usp: CA_Magazine
	{
		scope = 2;
		displayName = "USP Mag.";
		descriptionShort="Caliber: 9x19mm Parabellum<br/>Rounds: 15<br/>Used in: USP";
		model = "\RH_de\mags\mag_usp.p3d";
		type = 16;
		picture = "\RH_de\inv\m_usp.paa";
		ammo = "RH_B_9x19_Ball";
		count = 15;
		initSpeed = 365;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
		};
	};
	class 15Rnd_9x19_uspsd: 15Rnd_9x19_usp
	{
		displayName = "USP SD Mag.";
		descriptionShort="Caliber: 9x19mm Parabellum<br/>Rounds: 15<br/>Used in: USP SD";
		model = "\RH_de\mags\mag_usp.p3d";
		type = 16;
		picture = "\RH_de\inv\m_usp.paa";
		ammo = "RH_B_9x19_SD";
		initSpeed = 278;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
		};
	};
	class 20Rnd_32cal_vz61: CA_Magazine
	{
		scope = 2;
		displayName = "vz.61 Mag.";
		descriptionShort="Caliber: 7.65x17mm<br/>Rounds: 20<br/>Used in: Skorpion vz.61";
		picture = "\RH_de\inv\m_vz61.paa";
		model = "\RH_de\mags\mag_vz61.p3d";
		type = 16;
		ammo = "RH_32ACP";
		count = 20;
		initSpeed = 300;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class 7Rnd_32cal_ppk: CA_Magazine
	{
		scope = 2;
		displayName = "PPK mag";
		picture = "\RH_de\inv\m_ppk.paa";
		model = "\RH_de\mags\mag_ppk.p3d";
		type = 16;
		ammo = "RH_32ACP";
		count = 7;
		initSpeed = 300;
	};
	class 12Rnd_45cal_usp: CA_Magazine
	{
		scope = 2;
		displayName = "Usp mag (.45)";
		picture = "\RH_de\inv\m_usp.paa";
		model = "\RH_de\mags\mag_uspm.p3d";
		type = 16;
		ammo = "RH_45ACP";
		count = 12;
		initSpeed = 260;
	};
	class 8Rnd_45cal_m1911: CA_Magazine
	{
		scope = 2;
		displayName = "M1911 Mag.";
		descriptionShort="Caliber: .45 ACP<br/>Rounds: 8<br/>Used in: M1911 old";
		picture = "\RH_de\inv\m_colt.paa";
		model = "\RH_de\mags\mag_kim.p3d";
		type = 16;
		ammo = "RH_45ACP";
		count = 8;
		initSpeed = 260;
	};
	class 8Rnd_762_tt33: CA_Magazine
	{
		scope = 2;
		displayName = "TT33 mag";
		picture = "\RH_de\inv\m_tt33.paa";
		model = "\RH_de\mags\mag_tt33.p3d";
		type = 16;
		ammo = "RH_762x25";
		count = 8;
		initSpeed = 420;
	};
	class 10Rnd_22LR_mk2: CA_Magazine
	{
		scope = 2;
		displayName = "Mk II Mag.";
		descriptionShort="Caliber: .22mm<br/>Rounds: 10<br/>Used in: Ruger Mk II";
		picture = "\RH_de\inv\m_mk2.paa";
		model = "\RH_de\mags\mag_mk2.p3d";
		type = 16;
		ammo = "RH_B_22LR_SD";
		count = 10;
		initSpeed = 290;
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class cfgWeapons
{
	class Default;
	class PistolCore;
	class Pistol;
	class RH_deagle: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_deagle.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\de.paa";
		minRange = 2;
		minRangeProbab = 0.1;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		dexterity = 1.85;
		displayName = "Desert Eagle";
		descriptionShort = "Large caliber gas-operated semi-automatic pistol.<br/>Magazine: Desert Eagle Mag.";
		begin1[] = {"\RH_de\sound\desert_eagle_shot.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\de_reload.wss",0.1,1,20};
		magazines[] = {"7Rnd_50_AE"};
		dispersion = 0.0085;
		ffCount = 1;
		recoil = "deagleBase";
		reloadTime = 0.2;
		aiRateOfFire = 2.9;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The Desert Eagle is a large caliber gas-operated semi-automatic pistol manufactured in Israel by IMI (Israel Military Industries) for Magnum Research, Inc.Magnum Research, based in the USA, developed and patented the original Desert Eagle design and this design was further refined by IMI. Manufacturing was moved to Saco Defense in the state of Maine from 1995 to 2000, but shifted back to Israel when Saco was acquired by General Dynamics.";
		};
	};
	/*class RH_Deagleg: RH_deagle
	{
		displayName = "Desert Eagle Gold ";
		model = "\RH_de\RH_deagleg.p3d";
		picture = "\RH_de\inv\deg.paa";
	};
	class RH_Deagles: RH_deagle
	{
		displayName = "Desert Eagle Silver ";
		model = "\RH_de\RH_deagles.p3d";
		picture = "\RH_de\inv\des.paa";
	};
	class RH_anac: Pistol
	{
		scope = 2;
		displayName = "Colt Anaconda ";
		model = "\RH_de\RH_anac.p3d";
		picture = "\RH_de\inv\anac.paa";
		modelOptics = "-";
		minRange = 2;
		minRangeProbab = 0.1;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		dexterity = 1.85;
		begin1[] = {"\RH_de\sound\anac.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\bullreload.wss",0.1,1,20};
		magazines[] = {"6Rnd_44_Mag"};
		dispersion = 0.0085;
		ffCount = 1;
		recoil = "RH_45Base";
		reloadTime = 0.2;
		aiRateOfFire = 2.9;
		aiRateOfFireDistance = 50;
	};
	class RH_anacg: RH_anac
	{
		displayName = "Colt Anaconda Gold ";
		model = "\RH_de\RH_anacg.p3d";
		picture = "\RH_de\inv\anacg.paa";
	};
	class RH_bull: Pistol
	{
		scope = 2;
		displayName = "Taurus Raging Bull";
		model = "\RH_de\RH_bull.p3d";
		picture = "\RH_de\inv\bull.paa";
		modelOptics = "-";
		minRange = 2;
		minRangeProbab = 0.1;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		dexterity = 1.85;
		begin1[] = {"\RH_de\sound\bull.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\bullreload.wss",0.1,1,20};
		magazines[] = {"6Rnd_44_Mag"};
		dispersion = 0.0085;
		ffCount = 1;
		recoil = "RH_45Base";
		reloadTime = 0.2;
		aiRateOfFire = 2.9;
		aiRateOfFireDistance = 50;
	};
	class RH_python: Pistol
	{
		scope = 2;
		displayName = "Colt Python";
		model = "\RH_de\RH_python.p3d";
		picture = "\RH_de\inv\python.paa";
		modelOptics = "-";
		minRange = 2;
		minRangeProbab = 0.1;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		dexterity = 1.85;
		begin1[] = {"\RH_de\sound\python.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\bullreload.wss",0.1,1,20};
		magazines[] = {"6Rnd_357_Mag"};
		dispersion = 0.0085;
		ffCount = 1;
		recoil = "RH_45Base";
		reloadTime = 0.2;
		aiRateOfFire = 2.9;
		aiRateOfFireDistance = 50;
	};
	class RH_p226: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_p226.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\p226.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.2;
		optics = "true";
		displayName = "Sig P226";
		begin1[] = {"\RH_de\sound\p226.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\p226_reload.wss",0.1,1,20};
		magazines[] = {"15Rnd_9x19_usp","15Rnd_9x19_uspsd","15Rnd_9x19_usp","15Rnd_9x19_uspSD"};
		dispersion = 0.011;
		ffCount = 1;
		recoil = "RH_pistolBase";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The SIG Sauer P226 is a full-sized, service-type pistol chambered for the 9x19mm Parabellum, .40 S&W, and .357 SIG. Its design is based on the SIG Sauer P220.The P226 was designed for entry into the XM9 Service Pistol Trials, which were held by the US Army in 1984 on behalf of the US armed forces to find a replacement for the M1911A1. Only the Beretta 92F and the SIG P226 satisfactorily completed the trials. According to a GAO report, Beretta was awarded the M9 contract for the 92F due to better durability during endurance testing and a lower total package price. The P226 cost less per pistol than the 92F, but SIG's package price with magazines and spare parts was higher than Beretta's. The Navy SEALs, however, chose to adopt the P226 later after a repetition of failures with some issued Beretta M9s.";
		};
	};
	class RH_p226s: RH_p226
	{
		displayName = "Sig P226 Silver ";
		model = "\RH_de\RH_p226s.p3d";
		picture = "\RH_de\inv\p226s.paa";
	};*/
	class RH_p38: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_p38.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\p38.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 1;
		minRangeProbab = 0.5;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.2;
		optics = "true";
		displayName = "P38";
		descriptionShort = "Walther P38 is a 9mm semi-automatic pistol.<br/>Magazine: P38 Mag.";
		begin1[] = {"\RH_de\sound\p38.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\p38_reload.wss",0.1,1,20};
		magazines[] = {"8Rnd_9x19_p38"};
		dispersion = 0.011;
		ffCount = 1;
		recoil = "RH_pistolBase";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The P38 was the first locked-breech pistol to use a double-action trigger. The shooter could load a round into the chamber, use the de-cocking lever to safely lower the hammer without firing the round, and carry the weapon loaded with the hammer down. A pull of the trigger, with the hammer down, fired the first shot and the operation of the pistol ejected the fired round and reloaded a fresh round into the chamber, all features found in many modern day handguns.";
		};
	};
	/*class RH_ppk: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_ppk.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\ppk.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		optics = "true";
		displayName = "PPK";
		begin1[] = {"\RH_de\sound\ppk.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\ppk_reload.wss",0.1,1,20};
		magazines[] = {"7Rnd_32cal_ppk"};
		dispersion = 0.011;
		ffCount = 1;
		recoil = "RH_pistolBase";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The PP and the PPK were among the world's first, successful double action semi-automatic pistols that were widely copied, but still made by Walther. The design inspired other pistols, among them the Soviet Makarov, the Hungarian FEG PA-63, and the Czech CZ50. Although it was an excellent semi-automatic pistol, it had competitors in its time. The Mauser HSC pistol and the Sauer 38H pistol (a.k.a. model H), were successful in their own rights. Sauer pistol production ended at war's end, but the refined SIG P230 and the P232, owe much to the Walther PPK.";
		};
	};*/
	class RH_mk22: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_mk22.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\mk22.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		optics = "true";
		displayName = "Mk22";
		descriptionShort="Modified version of the Smith and Wesson Model 39, semi-automatic pistol.<br/>Magazine: Mk22 Mag.";
		begin1[] = {"\RH_de\sound\mk22.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\mk22_reload.wss",0.1,1,20};
		magazines[] = {"8Rnd_9x19_Mk"};
		dispersion = 0.011;
		ffCount = 1;
		recoil = "RH_pistolBase";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The Smith and Wesson Model 39 was developed for the US Army service pistol trials of 1949.It went on the market in 1955 and was the first of Smith Wesson's first generation semi-automatic pistols.The Model 39 was manufactured with an anodized aluminum frame with a curved backstrap and a blued carbon steel slide that carries the manual safety. The grip is of three pieces made of two walnut wood panels joined by a metal backstrap. It has a magazine release located at the rear of the trigger guard, similar to the M1911A1 it was designed to replace.";
		};
	};
	class RH_mk22sd: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_mk22sd.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\mk22sd.paa";
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		optics = "true";
		displayName = "Mk22 SD";
		descriptionShort = "Mk22 suppressed version.<br/>Magazine: Mk22 SD Mag.";
		begin1[] = {"\RH_de\sound\mk22sd.wss",0.316228,1,200};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\mk22_reload.wss",0.031623,1,20};
		magazines[] = {"8Rnd_9x19_Mksd"};
		dispersion = 0.011;
		ffCount = 1;
		recoil = "RH_pistolBase";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The Smith and Wesson Model 39 was developed for the US Army service pistol trials of 1949,It went on the market in 1955 and was the first of Smith Wesson's first generation semi-automatic pistols,The Model 39 was manufactured with an anodized aluminum frame with a curved backstrap and a blued carbon steel slide that carries the manual safety.The grip is of three pieces made of two walnut wood panels joined by a metal backstrap.It has a magazine release located at the rear of the trigger guard, similar to the M1911A1 it was designed to replace.";
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Pistol Suppressor";
				script = "spawn player_removeSuppressorPistol;";
			};
		};
	};
	/*class RH_mk22v: RH_mk22
	{
		displayName = "MK22v ";
		model = "\RH_de\RH_mk22v.p3d";
		picture = "\RH_de\inv\mk22v.paa";
	};
	class RH_mk22vsd: RH_mk22sd
	{
		displayName = "Mk22vsd ";
		model = "\RH_de\RH_mk22vsd.p3d";
		picture = "\RH_de\inv\mk22vsd.paa";
	};*/
	class RH_usp: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_usp.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\usp.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.2;
		optics = "true";
		displayName = "USP";
		descriptionShort = "Semi-automatic pistol designed by the German arms manufacturer Heckler Koch.<br/>Magazine: USP Mag.";
		begin1[] = {"\RH_de\sound\usp.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\usp_reload.wss",0.1,1,20};
		magazines[] = {"15Rnd_9x19_usp"/*,"15Rnd_9x19_uspsd"*/};
		dispersion = 0.011;
		ffCount = 1;
		recoil = "RH_pistolBase";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The Heckler Koch USP (Universale Selbstladepistole, or Universal Self-loading Pistol) is a semi-automatic pistol designed by the German arms manufacturer Heckler Koch.When Heckler Koch introduced the USP in 1993, it marked the first time HK chose to incorporate many traditional handgun design elements, such as elements of John Browning's M1911, in one pistol. Two principles guided its development � the first being the use of molded polymer material, and the second being the creation of a pistol paradigm.";
		};
	};
	class RH_uspsd: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_uspsd.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\uspsd.paa";
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		optics = "true";
		displayName = "USP SD";
		descriptionShort = "USP suppressed version.<br/>Magazine: USP SD Mag.";
		begin1[] = {"\RH_de\sound\uspsd.wss",0.316228,1,200};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\usp_reload.wss",0.031623,1,20};
		magazines[] = {"15Rnd_9x19_uspsd"/*,"15Rnd_9x19_usp"*/};
		dispersion = 0.0081;
		ffCount = 1;
		recoil = "RH_pistolBase";
		aiRateOfFire = 0.5;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The Heckler Koch USP (Universale Selbstladepistole, or Universal Self-loading Pistol) is a semi-automatic pistol designed by the German arms manufacturer Heckler Koch.When Heckler Koch introduced the USP in 1993, it marked the first time HK chose to incorporate many traditional handgun design elements, such as elements of John Browning's M1911, in one pistol. Two principles guided its development � the first being the use of molded polymer material, and the second being the creation of a pistol paradigm.";
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Pistol Suppressor";
				script = "spawn player_removeSuppressorPistol;";
			};
		};
	};
	/*class RH_uspm: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_uspm.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\uspm.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.2;
		optics = "true";
		displayName = "USP Match";
		begin1[] = {"\RH_de\sound\uspm.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\usp_reload.wss",0.1,1,20};
		magazines[] = {"12Rnd_45cal_usp"};
		dispersion = 0.008;
		ffCount = 1;
		recoil = "RH_45Base";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The USP Match (9 mm Parabellum, .40 SW, .45 ACP) is specifically designed to appeal to target shooters. In addition to the features offered on the Expert, the Match distinguishes itself by its barrel weight, or compensator, which replaces the elongated slide found on the Expert. The compensator provides counterbalance to the weapon's recoil, greatly improving follow up target tracking. The USP Match was discontinued in 2001.";
		};
	};
	class RH_m1911: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_m1911.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\colt.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.2;
		optics = "true";
		displayName = "Kimber M1911";
		begin1[] = {"\RH_de\sound\m1911.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\m1911_reload.wss",0.1,1,20};
		magazines[] = {"8Rnd_45cal_m1911"};
		dispersion = 0.0085;
		ffCount = 1;
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		recoil = "RH_45Base";
		class Library
		{
			libTextDesc = "The M1911 is a single-action, semiautomatic handgun chambered for the .45 ACP cartridge. It was designed by John M. Browning, and was the standard-issue side arm for the United States armed forces from 1911 to 1985. It was widely used in World War I, World War II, the Korean War and the Vietnam War. Its formal designation as of 1940 was Automatic Pistol, Caliber .45, M1911 for the original Model of 1911 or Automatic Pistol, Caliber .45, M1911A1 for the M1911A1, adopted in 1924. The designation changed to Pistol, Caliber .45, Automatic, M1911A1 in the Vietnam era. In total, the United States procured around 2.7 million M1911 and M1911A1 pistols during its service life. This is modern version of M1911 by Kimber manufacturer";
		};
	};
	class RH_m1911sd: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_m1911sd.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\coltsd.paa";
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.2;
		optics = "true";
		displayName = "Kimber M1911 SD";
		begin1[] = {"\RH_de\sound\m1911sd.wss",0.316228,1,200};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\m1911_reload.wss",0.031623,1,20};
		magazines[] = {"8Rnd_45cal_m1911"};
		dispersion = 0.0075;
		ffCount = 1;
		recoil = "RH_45Base";
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		class Library
		{
			libTextDesc = "The M1911 is a single-action, semiautomatic handgun chambered for the .45 ACP cartridge. It was designed by John M. Browning, and was the standard-issue side arm for the United States armed forces from 1911 to 1985. It was widely used in World War I, World War II, the Korean War and the Vietnam War. Its formal designation as of 1940 was Automatic Pistol, Caliber .45, M1911 for the original Model of 1911 or Automatic Pistol, Caliber .45, M1911A1 for the M1911A1, adopted in 1924. The designation changed to Pistol, Caliber .45, Automatic, M1911A1 in the Vietnam era. In total, the United States procured around 2.7 million M1911 and M1911A1 pistols during its service life. This is modern version of M1911 by Kimber manufacturer";
		};
	};*/
	class RH_m1911old: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_m1911old.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\coltold.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		optics = "true";
		displayName = "M1911 old";
		descriptionShort = "Single-action, semi-automatic, magazine-fed, recoil-operated pistol.<br/>Magazine: M1911 Mag.";
		begin1[] = {"\RH_de\sound\m1911old.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\m1911_reload.wss",0.1,1,20};
		magazines[] = {"8Rnd_45cal_m1911"};
		dispersion = 0.0085;
		ffCount = 1;
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		recoil = "RH_45Base";
		class Library
		{
			libTextDesc = "The M1911 is a single-action, semiautomatic handgun chambered for the .45 ACP cartridge. It was designed by John M. Browning, and was the standard-issue side arm for the United States armed forces from 1911 to 1985. It was widely used in World War I, World War II, the Korean War and the Vietnam War. Its formal designation as of 1940 was Automatic Pistol, Caliber .45, M1911 for the original Model of 1911 or Automatic Pistol, Caliber .45, M1911A1 for the M1911A1, adopted in 1924. The designation changed to Pistol, Caliber .45, Automatic, M1911A1 in the Vietnam era. In total, the United States procured around 2.7 million M1911 and M1911A1 pistols during its service life.";
		};
	};
	/*class RH_tt33: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_tt33.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\tt33.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.5;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.2;
		optics = "true";
		displayName = "TT33";
		begin1[] = {"\RH_de\sound\TT33.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\tt33_reload.wss",0.1,1,20};
		magazines[] = {"8Rnd_762_tt33"};
		dispersion = 0.0085;
		ffCount = 1;
		aiRateOfFire = 1.5;
		aiRateOfFireDistance = 50;
		recoil = "RH_PistolBase";
		class Library
		{
			libTextDesc = "The TT33 is a semi-automatic pistol developed by Fedor Tokarev as a service pistol for the Soviet military to replace the Nagant M1895 revolvers in use since tsarist times.The TT-33 (Tokarev-Tula) was an improved design of the TT-30 which was the first major-employed automatic pistol in the Soviet forces, but few TT-30's were built before the update in 1933. The TT-33 was widely used by Soviet troops during World War II, but did not completely replace the Nagant until that war.The TT-33 is chambered for the 7.62 x 25 mm Tokarev cartridge, which was itself based on the similar 7.63 mm Mauser cartridge used in the Mauser C96 pistol. Able to withstand tremendous abuse, large numbers of the TT-33 were produced during WWII and well into the 1950s.";
		};
	};*/
	class RH_mk2: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_mk2.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\mk2.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		minRange = 2;
		minRangeProbab = 0.2;
		midRange = 30;
		midRangeProbab = 0.8;
		maxRange = 50;
		maxRangeProbab = 0.04;
		optics = "true";
		displayName = "Ruger Mk II";
		descriptionShort = "Single-action, semi-automatic, magazine-fed, recoil-operated pistol.<br/>Magazine: Mk II Mag.";
		begin1[] = {"\RH_de\sound\Mk2.wss",0.316228,1,200};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\mk2_reload.wss",0.031623,1,20};
		magazines[] = {"10Rnd_22LR_mk2"};
		dispersion = 0.0085;
		ffCount = 1;
		aiRateOfFire = 2.0;
		aiRateOfFireDistance = 50;
		recoil = "RH_LowRecoil";
		class Library
		{
			libTextDesc = "The Ruger rimfire semiautomatic pistols are some of the most popular handguns made, with over 3 million sold. They are manufactured by Sturm, Ruger Company.The most prevalent model is the MK II, pronounced Mark Two, made from 1982 to 2005. Previous models include the Standard, Ruger's first model, made from 1949 to 1982, and the MK I Target, made from 1951 to 1982. Variations include the Target models, which have heavier barrels and adjustable sights, and the 22/45 models, which have a polymer frame with a grip-angle that matches the Colt 1911 rather than the steel frame's Luger P08-like layout. The MK II was removed from production in 2004, when it was replaced by the MK III. All Ruger rimfire pistols are chambered in .22 Long Rifle only.";
		};
	};/*
	class RH_m93r: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_m93r.p3d";
		modelOptics = "-";
		optics = 1;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		dexterity = 1.75;
		displayName = "Beretta M93R";
		picture = "\RH_de\inv\m93.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_de\sound\m93r_reload.wss",0.1,1,20};
		magazines[] = {"20Rnd_9x19_M93","15Rnd_9x19_usp","15Rnd_9x19_uspSD"};
		modes[] = {"Single","Burst"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\Sound\m93r.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.011;
			reloadTime = 0.055;
			recoil = "RH_pistolBase";
			recoilProne = "RH_pistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.05;
		};
		class Burst: Mode_Burst
		{
			begin1[] = {"\RH_de\Sound\m93r.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			autofire = 0;
			soundBurst = 0;
			soundContinuous = 0;
			burst = 3;
			dispersion = 0.011;
			reloadTime = 0.055;
			recoil = "RH_pistolBase_Auto";
			recoilProne = "RH_pistolBase_Auto";
			ffCount = 3;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.055;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 10;
			midRangeProbab = 0.7;
			maxRange = 20;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "Beretta Model 93R is a selective-fire machine pistol. The R stands for Raffica which means burst in Italian. It was designed in the 70s and meant for police and military use, offering extra firepower in a small package. It is perfect for concealed carry purposes such as VIP protection, or for close quarters fighting such as room-to-room searches.";
		};
	};*/
	class RH_m9: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_m9.p3d";
		modelOptics = "-";
		optics = 1;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		dexterity = 1.75;
		displayName = "Beretta M9";
		descriptionShort = "Semi-automatic pistol adopted by the United States Armed Forces in 1985.<br/>Magazine: M9 Mag.";
		picture = "\RH_de\inv\m9.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_de\sound\m93r_reload.wss",0.1,1,20};
		magazines[] = {"15Rnd_9x19_M9"};
		modes[] = {"Single"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\Sound\m93r.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.011;
			reloadTime = 0.05;
			recoil = "RH_PistolBase";
			recoilProne = "RH_PistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 2.0;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The M9 handgun, formally Pistol, Semiautomatic, 9mm, M9, is a 9mm pistol of the U.S. military adopted in the 1980s. It is essentially a mil-spec Beretta 92F, later the 92FS.";
		};
	};
	class RH_m9sd: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_m9sd.p3d";
		modelOptics = "-";
		optics = 1;
		fireLightDuration = 0;
		fireLightIntensity = 0;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		dexterity = 1.75;
		displayName = "Beretta M9SD";
		descriptionShort = "M9 suppressed version.<br/>Magazine: M9SD Mag.";
		picture = "\RH_de\inv\m9sd.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_de\sound\m93r_reload.wss",0.031623,1,20};
		magazines[] = {"15Rnd_9x19_M9SD"};
		modes[] = {"Single"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\Sound\m9sd.wss",0.316228,1,200};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.011;
			reloadTime = 0.05;
			recoil = "RH_PistolBase";
			recoilProne = "RH_PistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The M9 handgun, formally Pistol, Semiautomatic, 9mm, M9, is a 9mm pistol of the U.S. military adopted in the 1980s. It is essentially a mil-spec Beretta 92F, later the 92FS.";
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Pistol Suppressor";
				script = "spawn player_removeSuppressorPistol;";
			};
		};
	};
	/*class RH_m9c: RH_m9
	{
		displayName = "Beretta M9 Camo ";
		model = "\RH_de\RH_m9c.p3d";
		picture = "\RH_de\inv\m9c.paa";
	};
	class RH_m9csd: RH_m9sd
	{
		displayName = "Beretta M9SD Camo ";
		model = "\RH_de\RH_m9csd.p3d";
		picture = "\RH_de\inv\m9csd.paa";
	};
	class RH_g18: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_g18.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\g18.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		displayName = "Glock 18";
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\glock_reload.wss",0.1,1,20};
		magazines[] = {"19Rnd_9x19_g18","33Rnd_9x19_g18","17Rnd_9x19_g17"};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\sound\glock.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.011;
			reloadTime = 0.05;
			recoil = "RH_PistolBase";
			recoilProne = "RH_PistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 1.5;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.4;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_de\sound\glock.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			multiplier = 1;
			burst = 1;
			dispersion = 0.011;
			reloadTime = 0.05;
			recoil = "RH_PistolBase_Auto";
			recoilProne = "RH_PistolBase_Auto";
			ffCount = 3;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.001;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.7;
			midRange = 10;
			midRangeProbab = 0.7;
			maxRange = 20;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The Glock 18 is a handgun manufactured by Glock. It is much like a Glock 17 with a fire selector switch on its slide that enables it to fire in semi-automatic or fully automatic modes. Rotating the switch toward the bottom of the frame allows for fully automatic fire, while rotating it to the top switches the gun to semi-automatic. The Glock 18 appears identical to the Glock 17, with the addition of the fire selector. However, the internal dimensions of the main parts of the Glock 18 are slightly different from the Glock 17, and are not interchangeable. This was done by Glock so that the Glock 17 could not be considered a semi-automatic version of the Glock 18.";
		};
	};*/
	class RH_g17: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_g17.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\g17.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		displayName = "Glock 17";
		descriptionShort = "The most widely used law enforcement pistol worldwide.<br/>Magazine: Glock 17 Mag.";
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\glock_reload.wss",0.1,1,20};
		magazines[] = {"17Rnd_9x19_g17","19Rnd_9x19_g18","33Rnd_9x19_g18"};
		modes[] = {"Single"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\sound\glock.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.011;
			reloadTime = 0.05;
			recoil = "RH_PistolBase";
			recoilProne = "RH_PistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 2.0;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The Glock 17 is a handgun manufactured by Glock. It is much like a Glock 17 with a fire selector switch on its slide that enables it to fire in semi-automatic or fully automatic modes. Rotating the switch toward the bottom of the frame allows for fully automatic fire, while rotating it to the top switches the gun to semi-automatic. The Glock 18 appears identical to the Glock 17, with the addition of the fire selector. However, the internal dimensions of the main parts of the Glock 18 are slightly different from the Glock 17, and are not interchangeable. This was done by Glock so that the Glock 17 could not be considered a semi-automatic version of the Glock 18; rather they are two separate pistols.";
		};
	};
	/*class RH_g19: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_g19.p3d";
		modelOptics = "-";
		picture = "\RH_de\inv\g19.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		displayName = "Glock 19";
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_de\sound\glock_reload.wss",0.1,1,20};
		magazines[] = {"17Rnd_9x19_g17","19Rnd_9x19_g18","33Rnd_9x19_g18"};
		modes[] = {"Single"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\sound\glock.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.011;
			reloadTime = 0.05;
			recoil = "RH_PistolBase";
			recoilProne = "RH_PistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 2.0;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The Glock 17 is a handgun manufactured by Glock. It is much like a Glock 17 with a fire selector switch on its slide that enables it to fire in semi-automatic or fully automatic modes. Rotating the switch toward the bottom of the frame allows for fully automatic fire, while rotating it to the top switches the gun to semi-automatic. The Glock 18 appears identical to the Glock 17, with the addition of the fire selector. However, the internal dimensions of the main parts of the Glock 18 are slightly different from the Glock 17, and are not interchangeable. This was done by Glock so that the Glock 17 could not be considered a semi-automatic version of the Glock 18; rather they are two separate pistols.";
		};
	};
	class RH_g19t: RH_g19
	{
		model = "\RH_de\RH_g19t.p3d";
		picture = "\RH_de\inv\g19t.paa";
		displayName = "Glock 19 Tan";
	};*/
	class RH_vz61: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_vz61.p3d";
		modelOptics = "-";
		optics = 1;
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		dexterity = 1.75;
		displayName = "Skorpion vz.61";
		descriptionShort = "Skorpion vz.61 is a Czechoslovak 7.65mm submachine gun.<br/>Magazine: vz.61 Mag.";
		picture = "\RH_de\inv\vz61.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_de\sound\vz61_reload.wss",0.1,1,20};
		magazines[] = {"20Rnd_32cal_vz61"};
		modes[] = {"Single","Fullauto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\Sound\vz61.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.015;
			reloadTime = 0.07;
			recoil = "RH_LowRecoil";
			recoilProne = "RH_LowRecoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 1.5;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.4;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.4;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_de\Sound\vz61.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			burst = 1;
			dispersion = 0.015;
			reloadTime = 0.07;
			recoil = "RH_LowRecoil";
			recoilProne = "RH_LowRecoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.7;
			midRange = 20;
			midRangeProbab = 0.8;
			maxRange = 40;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The vz 61 (correctly Sa vz 61 short for samopal vzor 61 submachine gun model 1961), or �korpion (scorpion), is a Czechoslovak submachine gun. It is designed to fire 7.65 x 17 mm ammunition, also known as .32 ACP. Variants in 9 x 17 mm (380 ACP), 9 x 18 mm Makarov, and 9 x 19 mm have also been reported.";
		};
	};
	class RH_tec9: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_tec9.p3d";
		modelOptics = "-";
		optics = 1;
		distanceZoomMin = 50;
		distanceZoomMax = 50;
		dexterity = 1.75;
		displayName = "Intratec TEC-9";
		descriptionShort = "Blowback-operated semi-automatic handgun.<br/>Magazine: TEC-9 Mag.";
		picture = "\RH_de\inv\tec9.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_de\sound\tec9_reload.wss",0.056234,1,25};
		magazines[] = {"30Rnd_9x19_tec"};
		modes[] = {"Single","Fullauto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\Sound\tec9.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.015;
			reloadTime = 0.07;
			recoil = "RH_pistolBase";
			recoilProne = "RH_pistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.5;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_de\Sound\tec9.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			burst = 1;
			dispersion = 0.015;
			reloadTime = 0.07;
			recoil = "RH_pistolBase_Auto";
			recoilProne = "RH_pistolBase_Auto";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The Intratec TEC-9 is a blowback-operated, semi-automatic 9 mm Para caliber firearm, classified by the Bureau of Alcohol, Tobacco, and Firearms as a handgun. It is made of inexpensive molded polymer and stamped steel parts. Magazines holding 10-, 20-, 32-, 36- and 50-round capacities are available. There are three different models, all of which are commonly referred to as the TEC-9, although only one model was actually sold under that name.";
		};
	};
	/*class RH_muzi: Pistol
	{
		scope = 2;
		model = "\RH_de\RH_muzi.p3d";
		modelOptics = "-";
		optics = 1;
		distanceZoomMin = 57;
		distanceZoomMax = 57;
		dexterity = 1.75;
		displayName = "Micro Uzi";
		picture = "\RH_de\inv\muzi.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_de\sound\muzi_reload.wss",0.056234,1,25};
		magazines[] = {"32Rnd_9x19_Muzi"};
		modes[] = {"Single","Fullauto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_de\Sound\muzi.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.011;
			reloadTime = 0.048;
			recoil = "RH_pistolBase";
			recoilProne = "RH_pistolBase";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 2.0;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.2;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_de\Sound\muzi.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			multiplier = 1;
			burst = 1;
			dispersion = 0.011;
			reloadTime = 0.048;
			recoil = "RH_pistolBase_Auto";
			recoilProne = "RH_pistolBase_Auto";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.05;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.7;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "Micro Uzi, At only 250 mm (9.84 inches) in length, it is slightly larger than a standard pistol and is about as small as the original Uzi design could be made. It fires from a closed bolt position and has a side-folding stock similar to the one on the Mini Uzi. The forward handgrip is completely eliminated. First introduced in 1986, the Micro Uzi weighs 2.2 kilograms less than the Uzi when unloaded and fires at a rate of 1250 rpm, which can unload the 20 round magazine in 0.96 seconds.";
		};
	};*/
};
/*class CfgVehicles
{
	class All;
	class AllVehicles;
	class ThingEffect;
	class Land;
	class Man;
	class CAManBase;
	class ReammoBox;
	class SoldierWB;
	class USBasicAmmunitionBox: ReammoBox{};
	class RHpistammobox: USBasicAmmunitionBox
	{
		displayName = "RH Pistol Ammo Box";
		class TransportMagazines
		{
			class xx7Rnd_50_AE
			{
				magazine = "7Rnd_50_AE";
				count = 25;
			};
			class xx6Rnd_44_Mag
			{
				magazine = "6Rnd_44_Mag";
				count = 25;
			};
			class xx6Rnd_357_Mag
			{
				magazine = "6Rnd_357_Mag";
				count = 25;
			};
			class xx8Rnd_9x19_Mk
			{
				magazine = "8Rnd_9x19_Mk";
				count = 25;
			};
			class xx8Rnd_9x19_Mksd
			{
				magazine = "8Rnd_9x19_Mksd";
				count = 25;
			};
			class xx8Rnd_762_tt33
			{
				magazine = "8Rnd_762_tt33";
				count = 25;
			};
			class xx10Rnd_22LR_mk2
			{
				magazine = "10Rnd_22LR_mk2";
				count = 25;
			};
			class xx7Rnd_32cal_ppk
			{
				magazine = "7Rnd_32cal_ppk";
				count = 25;
			};
			class xx8Rnd_9x19_p38
			{
				magazine = "8Rnd_9x19_p38";
				count = 25;
			};
			class xx15Rnd_9x19_M9
			{
				magazine = "7Rnd_32cal_ppk";
				count = 25;
			};
			class xx15Rnd_9x19_M9SD
			{
				magazine = "15Rnd_9x19_M9SD";
				count = 25;
			};
			class xx17Rnd_9x19_g17
			{
				magazine = "17Rnd_9x19_g17";
				count = 25;
			};
			class xx19Rnd_9x19_g18
			{
				magazine = "19Rnd_9x19_g18";
				count = 25;
			};
			class xx33Rnd_9x19_g18
			{
				magazine = "33Rnd_9x19_g18";
				count = 25;
			};
			class xx20Rnd_9x19_M93
			{
				magazine = "20Rnd_9x19_M93";
				count = 25;
			};
			class xx20Rnd_32cal_vz61
			{
				magazine = "20Rnd_32cal_vz61";
				count = 25;
			};
			class xx12Rnd_45cal_usp
			{
				magazine = "12Rnd_45cal_usp";
				count = 25;
			};
			class xx8Rnd_45cal_m1911
			{
				magazine = "8Rnd_45cal_m1911";
				count = 25;
			};
			class xx15Rnd_9x19_usp
			{
				magazine = "15Rnd_9x19_usp";
				count = 25;
			};
			class xx15Rnd_9x19_uspsd
			{
				magazine = "15Rnd_9x19_uspsd";
				count = 25;
			};
			class xx30Rnd_9x19_tec
			{
				magazine = "30Rnd_9x19_tec";
				count = 25;
			};
			class xx32Rnd_9x19_Muzi
			{
				magazine = "32Rnd_9x19_Muzi";
				count = 25;
			};
		};
		class TransportWeapons
		{
			class xxRH_deagle
			{
				weapon = "RH_deagle";
				count = 3;
			};
			class xxRH_Deagleg
			{
				weapon = "RH_Deagleg";
				count = 3;
			};
			class xxRH_Deagles
			{
				weapon = "RH_Deagles";
				count = 3;
			};
			class xxRH_anac
			{
				weapon = "RH_anac";
				count = 3;
			};
			class xxRH_anacg
			{
				weapon = "RH_anacg";
				count = 3;
			};
			class xxRH_bull
			{
				weapon = "RH_bull";
				count = 3;
			};
			class xxRH_python
			{
				weapon = "RH_python";
				count = 3;
			};
			class xxRH_mk22
			{
				weapon = "RH_mk22";
				count = 3;
			};
			class xxRH_mk22sd
			{
				weapon = "RH_mk22sd";
				count = 3;
			};
			class xxRH_mk22v
			{
				weapon = "RH_mk22v";
				count = 3;
			};
			class xxRH_mk22vsd
			{
				weapon = "RH_mk22vsd";
				count = 3;
			};
			class xxRH_usp
			{
				weapon = "RH_usp";
				count = 3;
			};
			class xxRH_uspsd
			{
				weapon = "RH_uspsd";
				count = 3;
			};
			class xxRH_uspm
			{
				weapon = "RH_uspm";
				count = 3;
			};
			class xxRH_m1911
			{
				weapon = "RH_m1911";
				count = 3;
			};
			class xxRH_m1911sd
			{
				weapon = "RH_m1911sd";
				count = 3;
			};
			class xxRH_m1911old
			{
				weapon = "RH_m1911old";
				count = 3;
			};
			class xxRH_m9
			{
				weapon = "RH_m9";
				count = 3;
			};
			class xxRH_m9sd
			{
				weapon = "RH_m9sd";
				count = 3;
			};
			class xxRH_m9c
			{
				weapon = "RH_m9c";
				count = 3;
			};
			class xxRH_m9csd
			{
				weapon = "RH_m9csd";
				count = 3;
			};
			class xxRH_m93r
			{
				weapon = "RH_m93r";
				count = 3;
			};
			class xxRH_g17
			{
				weapon = "RH_g17";
				count = 3;
			};
			class xxRH_g18
			{
				weapon = "RH_g18";
				count = 3;
			};
			class xxRH_g19
			{
				weapon = "RH_g19";
				count = 3;
			};
			class xxRH_g19t
			{
				weapon = "RH_g19t";
				count = 3;
			};
			class xxRH_tt33
			{
				weapon = "RH_tt33";
				count = 3;
			};
			class xxRH_mk2
			{
				weapon = "RH_mk2";
				count = 3;
			};
			class xxRH_p226
			{
				weapon = "RH_p226";
				count = 3;
			};
			class xxRH_p226s
			{
				weapon = "RH_p226s";
				count = 3;
			};
			class xxRH_p38
			{
				weapon = "RH_p38";
				count = 3;
			};
			class xxRH_ppk
			{
				weapon = "RH_ppk";
				count = 3;
			};
			class xxRH_vz61
			{
				weapon = "RH_vz61";
				count = 3;
			};
			class xxRH_tec9
			{
				weapon = "RH_tec9";
				count = 3;
			};
			class xxRH_muzi
			{
				weapon = "RH_muzi";
				count = 3;
			};
		};
	};
};*/
//};
