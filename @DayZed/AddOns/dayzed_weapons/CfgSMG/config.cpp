////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 4.13
//Fri Oct 11 23:50:44 2013 : Source 'file' date Fri Oct 11 23:50:44 2013
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

class CfgPatches
{
	class RH_smg
	{
		//units[] = {"RHsmgbox"};
		weapons[] = {"RH_mp5a4","RH_mp5a5","RH_mp5a5aim","RH_mp5a5eot","RH_mp5sd6","RH_mp5sd6aim","RH_mp5sd6eot","RH_ump","RH_umpaim","RH_umpeot","RH_umpsd","RH_umpsdaim","RH_umpsdeot","RH_p90","RH_p90aim","RH_P90eot","RH_P90sd","RH_p90sdaim","RH_p90sdeot"/*,"RH_HK53","RH_HK53aim","RH_HK53eot","RH_HK53RFX","RH_mp5a4aim","RH_mp5a4eot","RH_mp5a4rfx","RH_mp5a5rfx","RH_mp5a5ris","RH_mp5a5risaim","RH_mp5a5riseot","RH_mp5a5risRFX","RH_mp5a5eod","RH_mp5a5eodaim","RH_mp5a5eodeot","RH_mp5a5eodRFX","RH_mp5sd6RFX","RH_mp5sd6g","RH_mp5k","RH_mp5kp","RH_mp5kpdw","RH_mp5kpdwaim","RH_mp5kpdweot","RH_mp5kpdwRFX","RH_p90i","RH_P90isd","RH_umpRFX","RH_umpsdRFX","RH_kriss","RH_krissaim","RH_krisseot","RH_krissRFX","RH_krisssd","RH_krisssdaim","RH_krisssdeot","RH_krisssdRFX","RH_uzi","RH_uzim","RH_uzig","RH_uzisd","RH_mac10","RH_mac10p","RH_tmp","RH_tmpaim","RH_tmpeot","RH_tmpsd","RH_tmpsdaim","RH_tmpsdeot","RH_pp2000","RH_pp2000p","RH_pp2000aim","RH_pp2000eot","RH_mp7","RH_mp7p","RH_mp7aim","RH_mp7eot","RH_mp7sd","RH_mp7sdaim","RH_mp7sdeot","RH_fmg9","RH_pdr","RH_pdraim","RH_pdreot"*/};
		requiredVersion = 1.04;
		requiredAddons[] = {"CAweapons"};
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class RH_smg
		{
			list[] = {"RH_smg"};
		};
	};
};
class cfgRecoils
{
	RH_subMachineGunBase[] = {0,0.005,0.005,0.005,0.007,0.006,0.09,0.002,-0.0015,0.1,0,0};
	RH_smg_Recoil[] = {0,0.005,-0.002,0.01,0.005,0.007,0.03,0.003,0.007,0.08,0,0};
	RH_SMG_M203GLBase[] = {0,0.03,0.05,0.13,0.01,-0.001,0.26,0,0};
};
class CfgAmmo
{
	class Default;
	class BulletCore;
	class BulletBase;
	class RH_smg_45ACP_Round: BulletBase
	{
		//hit = 10;
		hit = 5.5; //DayZed
		indirectHit = 0;
		indirectHitRange = 0;
		cartridge = "FxCartridge_9mm";
		caliber = 0.33;
		visibleFire = 16;
		//audibleFire = 18;
		audibleFire = 16; //DayZed
		cost = 5;
		typicalSpeed = 260;
		airFriction = -0.001;
	};
	class RH_smg_45ACP_SD_Round: BulletBase
	{
		//hit = 9;
		hit = 5.5; //DayZed
		indirectHit = 0;
		indirectHitRange = 0;
		cartridge = "FxCartridge_9mm";
		caliber = 0.33;
		visibleFire = 0.035;
		audibleFire = 0.035;
		visibleFireTime = 2;
		cost = 5;
		typicalSpeed = 315;
		airFriction = -0.001312;
	};
	class RH_smg_57x28mm_Round: BulletBase
	{
		//hit = 10;
		hit = 6; //DayZed
		cartridge = "FxCartridge_9mm";
		visibleFire = 16; //DayZed
		audibleFire = 16; //DayZed
		indirectHit = 0;
		indirectHitRange = 0;
		cost = 1;
		airFriction = -0.001425;
		caliber = 0.5;
		model = "\ca\Weapons\Data\bullettracer\tracer_red";
		tracerScale = 1;
		tracerStartTime = 0.05;
		tracerEndTime = 1;
		nvgOnly = 1;
	};
	class RH_smg_57x28mm_SD_Round: BulletBase
	{
		//hit = 9;
		hit = 6; //DayZed
		cartridge = "FxCartridge_9mm";
		indirectHit = 0;
		indirectHitRange = 0;
		visibleFire=0.035;
		audibleFire=0.035;
		visibleFireTime = 2;
		cost = 1;
		typicalSpeed = 320;
		airFriction = -0.0006;
		caliber = 0.5;
	};
};
class CfgMagazines
{
	class Default;
	class CA_Magazine;
	class 30Rnd_9x19_MP5;
	class 30Rnd_9x19_MP5SD;
	class 30Rnd_9x19_MP5p: 30Rnd_9x19_MP5
	{
		displayName = "Mp5 pistol Magazine";
		type = 16;
	};
	class RH_9mm_32RND_pMag: 30Rnd_9x19_MP5
	{
		displayName = "Uzi pistol Magazine";
		type = 16;
		model = "\RH_smg\mags\mag_uzi.p3d";
		picture = "\RH_smg\inv\m_uzi.paa";
		count = 32;
	};
	class RH_9mm_32RND_Mag: 30Rnd_9x19_MP5
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		displayName = "Uzi Magazine";
		model = "\RH_smg\mags\mag_uzi.p3d";
		picture = "\RH_smg\inv\m_uzi.paa";
		count = 32;
	};
	class RH_9mm_32RND_SD_Mag: 30Rnd_9x19_MP5SD
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		displayName = "Uzi SD Magazine";
		model = "\RH_smg\mags\mag_uzi.p3d";
		picture = "\RH_smg\inv\m_uzi.paa";
		count = 32;
	};
	class RH_45ACP_25RND_Mag: 30Rnd_9x19_MP5
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_45ACP_Round";
		displayName = "HK UMP45 Mag.";
		descriptionShort="Caliber: .45 ACP<br/>Rounds: 25<br/>Used in: HK UMP45";
		model = "\RH_smg\mags\mag_ump.p3d";
		picture = "\RH_smg\inv\m_ump.paa";
		count = 25;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class RH_45ACP_25RND_SD_Mag: 30Rnd_9x19_MP5SD
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_45ACP_SD_Round";
		displayName = "HK UMP45SD Mag.";
		descriptionShort="Caliber: .45 ACP<br/>Rounds: 25<br/>Used in suppressed: HK UMP45";
		model = "\RH_smg\mags\mag_ump.p3d";
		picture = "\RH_smg\inv\m_ump.paa";
		count = 25;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class RH_45ACP_13RND_Mag: 30Rnd_9x19_MP5
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_45ACP_Round";
		displayName = "Kriss Magazine";
		model = "\RH_smg\mags\mag_kriss.p3d";
		picture = "\RH_smg\inv\m_kriss.paa";
		count = 13;
	};
	class RH_45ACP_30RND_Mag: 30Rnd_9x19_MP5
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_45ACP_Round";
		displayName = "Kriss 30Rnd Magazine";
		model = "\RH_smg\mags\mag_kriss30.p3d";
		picture = "\RH_smg\inv\m_kriss30.paa";
		count = 30;
	};
	class RH_45ACP_30RND_SD_Mag: 30Rnd_9x19_MP5SD
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_45ACP_SD_Round";
		displayName = "Kriss 30Rnd SD Magazine";
		model = "\RH_smg\mags\mag_kriss30.p3d";
		picture = "\RH_smg\inv\m_kriss30.paa";
		count = 30;
	};
	class RH_57x28mm_50RND_Mag: 30Rnd_9x19_MP5
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_57x28mm_Round";
		displayName = "FN P90 Mag.";
		descriptionShort="Caliber: 5.7x28mm<br/>Rounds: 50<br/>Used in: FN P90";
		model = "\RH_smg\mags\mag_p90.p3d";
		picture = "\RH_smg\inv\m_p90.paa";
		count = 50;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class RH_57x28mm_50RND_SD_Mag: 30Rnd_9x19_MP5SD
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_57x28mm_SD_Round";
		displayName = "FN P90SD Mag.";
		descriptionShort="Caliber: 5.7x28mm<br/>Rounds: 50<br/>Used in suppressed: FN P90";
		model = "\RH_smg\mags\mag_p90.p3d";
		picture = "\RH_smg\inv\m_p90.paa";
		count = 50;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class RH_46x30mm_40RND_Mag: 30Rnd_9x19_MP5
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_57x28mm_Round";
		displayName = "HK MP7 Magazine";
		model = "\RH_smg\mags\mag_mp7.p3d";
		picture = "\RH_smg\inv\m_mp7.paa";
		count = 40;
	};
	class RH_46x30mm_40RND_SD_Mag: 30Rnd_9x19_MP5SD
	{
		scopeWeapon = 0;
		scopeMagazine = 2;
		ammo = "RH_smg_57x28mm_SD_Round";
		displayName = "HK MP7 SD Magazine";
		model = "\RH_smg\mags\mag_mp7.p3d";
		picture = "\RH_smg\inv\m_mp7.paa";
		count = 40;
	};
	class RH_46x30mm_40RND_pMag: RH_46x30mm_40RND_Mag
	{
		displayName = "HK MP7 pistol Magazine";
		type = 16;
	};
};
class Mode_SemiAuto{};
class Mode_Burst: Mode_SemiAuto{};
class Mode_FullAuto: Mode_SemiAuto{};
class cfgWeapons
{
	class Default;
	class PistolCore;
	class Pistol;
	class RifleCore;
	class MGunCore;
	class LauncherCore;
	class GrenadeCore;
	class CannonCore;
	class Launcher;
	class GrenadeLauncher;
	class M203Muzzle: GrenadeLauncher{};
	class RocketPods;
	class MissileLauncher;
	class MGun;
	class Rifle;
	class M4A1;
	class MP5SD;
	class MP5A5;
	/*class RH_HK53: M4A1
	{
		scope = 2;
		model = "\RH_smg\RH_HK53.p3d";
		displayName = "HK 53";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		picture = "\RH_smg\inv\hk53.paa";
		reloadMagazineSound[] = {"\RH_smg\sound\HK53_reload.wss",0.056234,1,25};
		handAnim[] = {"OFP2_ManSkeleton"};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\HK53.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.09;
			recoil = "recoil_single_primary_3outof10";
			recoilProne = "recoil_single_primary_prone_3outof10";
			dispersion = 0.0025;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\HK53.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.09;
			recoil = "recoil_auto_primary_3outof10";
			recoilProne = "recoil_auto_primary_prone_3outof10";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 60;
			maxRangeProbab = 0.05;
		};
		class Library
		{
			libTextDesc = "The HK 53 is an assault carbine variant of the HK33 rifle, based on the proven delayed roller lock bolt system introduced with the MG30 machine gun scaled down to 5.56 mm NATO. The gun is useful because it combines the power of an assault rifle, including the ability to penetrate body armor, with the handling characteristics of a submachine gun.";
		};
	};
	class RH_HK53aim: RH_HK53
	{
		displayName = "HK 53 CCO";
		model = "\RH_smg\RH_HK53aim.p3d";
		picture = "\RH_smg\inv\HK53aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_HK53eot: RH_HK53
	{
		displayName = "HK 53 Holo";
		model = "\RH_smg\RH_HK53eot.p3d";
		picture = "\RH_smg\inv\HK53eot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_HK53RFX: RH_HK53
	{
		displayName = "HK 53 RFX";
		model = "\RH_smg\RH_HK53RFX.p3d";
		picture = "\RH_smg\inv\HK53RFX.paa";
	};
	class RH_PDR: M4A1
	{
		displayName = "Magpul PDR";
		model = "\RH_smg\RH_pdr.p3d";
		picture = "\RH_smg\inv\pdr.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\RH_pdr.rtm"};
		reloadMagazineSound[] = {"\RH_smg\sound\MP5_Reload.wss",0.056234,1,25};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\pdr.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.09;
			recoil = "recoil_single_primary_3outof10";
			recoilProne = "recoil_single_primary_prone_3outof10";
			dispersion = 0.0025;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 400;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\pdr.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.09;
			recoil = "recoil_auto_primary_3outof10";
			recoilProne = "recoil_auto_primary_prone_3outof10";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 60;
			maxRangeProbab = 0.05;
		};
	};
	class RH_pdraim: RH_PDR
	{
		displayName = "Magpul PDR CCO";
		model = "\RH_smg\RH_pdraim.p3d";
		picture = "\RH_smg\inv\pdraim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_pdreot: RH_PDR
	{
		displayName = "Magpul PDR Holo";
		model = "\RH_smg\RH_pdreot.p3d";
		picture = "\RH_smg\inv\pdreot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};*/
	class RH_mp5a4: MP5A5
	{
		scope = 2;
		model = "\RH_smg\RH_mp5a4.p3d";
		picture = "\RH_smg\inv\mp5a4.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		magazines[] = {"30Rnd_9x19_MP5"/*,"30Rnd_9x19_MP5SD"*/};
		displayName = "HK MP5A4";
		handAnim[] = {"OFP2_ManSkeleton"};
		drySound[] = {"Ca\sounds\Weapons\rifles\dry",0.003162,1,10};
		reloadMagazineSound[] = {"\RH_smg\sound\MP5_Reload.wss",0.056234,1,25};
		fireLightDuration = 0.05;
		fireLightIntensity = 0.012;
		distanceZoomMin = 107;
		distanceZoomMax = 107;
		value = 10;
		modes[] = {"Single","Burst","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\mp5.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class Burst: Mode_Burst
		{
			begin1[] = {"\RH_smg\sound\mp5.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			soundBurst = 0;
			reloadtime = 0.071;
			ffCount = 1;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 60;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\mp5.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			reloadtime = 0.071;
			ffCount = 1;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			aiRateOfFire = 0.001;
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class FlashLight
		{
			color[] = {0.9,0.9,0.7,0.9};
			ambient[] = {0.1,0.1,0.1,1.0};
			position = "flash dir";
			direction = "flash";
			angle = 30;
			scale[] = {1,1,0.5};
			brightness = 0.1;
		};
	};
	/*class RH_mp5a4aim: RH_mp5a4
	{
		displayname = "HK MP5A4 CCO";
		model = "\RH_smg\RH_mp5a4aim.p3d";
		picture = "\RH_smg\inv\mp5a4aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a4eot: RH_mp5a4
	{
		displayname = "HK MP5A4 Holo";
		model = "\RH_smg\RH_mp5a4eot.p3d";
		picture = "\RH_smg\inv\mp5a4eot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a4RFX: RH_mp5a4
	{
		displayname = "HK MP5A4 RFX";
		model = "\RH_smg\RH_mp5a4RFX.p3d";
		picture = "\RH_smg\inv\mp5a4RFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};*/
	class RH_mp5a5: RH_mp5a4
	{
		displayName = "HK MP5A5";
		descriptionShort = "Submachine gun developed and manufactured by Heckler and Koch.<br/>Magazine: MP5 Mag.";
		model = "\RH_smg\RH_mp5a5.p3d";
		picture = "\RH_smg\inv\mp5a5.paa";
		magazines[] = {"30Rnd_9x19_MP5"};
		class FlashLight{};
	};
	class RH_mp5a5aim: RH_mp5a5
	{
		displayName = "HK MP5A5 CCO";
		model = "\RH_smg\RH_mp5a5aim.p3d";
		picture = "\RH_smg\inv\mp5a5aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	class RH_mp5a5eot: RH_mp5a5
	{
		displayName = "HK MP5A5 Holo";
		model = "\RH_smg\RH_mp5a5eot.p3d";
		picture = "\RH_smg\inv\mp5a5eot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
		};
	};
	/*class RH_mp5a5RFX: RH_mp5a5
	{
		displayname = "HK MP5A5 RFX";
		model = "\RH_smg\RH_mp5a5RFX.p3d";
		picture = "\RH_smg\inv\mp5a5RFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a5ris: RH_mp5a5
	{
		displayName = "HK MP5A5 RIS";
		model = "\RH_smg\RH_mp5a5ris.p3d";
		picture = "\RH_smg\inv\mp5a5ris.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_mp5a5ris.rtm"};
		irDistance = 150;
	};
	class RH_mp5a5risaim: RH_mp5a5ris
	{
		displayname = "HK MP5A5 RIS CCO";
		model = "\RH_smg\RH_mp5a5risaim.p3d";
		picture = "\RH_smg\inv\mp5a5risaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a5riseot: RH_mp5a5ris
	{
		displayname = "HK MP5A5 RIS Holo";
		model = "\RH_smg\RH_mp5a5riseot.p3d";
		picture = "\RH_smg\inv\mp5a5riseot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a5risRFX: RH_mp5a5ris
	{
		displayname = "HK MP5A5 RIS RFX";
		model = "\RH_smg\RH_mp5a5risRFX.p3d";
		picture = "\RH_smg\inv\mp5a5risRFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a5eod: RH_mp5a5
	{
		displayName = "HK MP5A5 M203";
		model = "\RH_smg\RH_mp5a5eod.p3d";
		picture = "\RH_smg\inv\mp5a5eod.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_MPMuzzle","RH_M203Muzzle"};
		dexterity = 1.59;
		class RH_MPMuzzle: RH_mp5a5{};
		class RH_M203Muzzle: M203Muzzle
		{
			displayName = "$STR_DN_M203";
			sound[] = {"\RH_smg\sound\M203.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_smg\sound\M203_Reload.wss",0.056234,1,20};
			magazineReloadTime = 0;
			reloadTime = 0.1;
			recoil = "RH_SMG_M203GLBase";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
		};
	};
	class RH_mp5a5eodaim: RH_mp5a5eod
	{
		displayname = "HK MP5A5 EOD CCO";
		model = "\RH_smg\RH_mp5a5eodaim.p3d";
		picture = "\RH_smg\inv\mp5a5eodaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a5eodeot: RH_mp5a5eod
	{
		displayname = "HK MP5A5 EOD Holo";
		model = "\RH_smg\RH_mp5a5eodeot.p3d";
		picture = "\RH_smg\inv\mp5a5eodeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5a5eodRFX: RH_mp5a5eod
	{
		displayname = "HK MP5A5 EOD RFX";
		model = "\RH_smg\RH_mp5a5eodRFX.p3d";
		picture = "\RH_smg\inv\mp5a5eodRFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};*/
	class RH_mp5sd6: RH_mp5a5
	{
		displayName = "HK MP5SD6";
		descriptionShort="HK MP5 suppressed submachine gun.<br/>Magazine: MP5SD Mag.";
		model = "\RH_smg\RH_mp5sd6.p3d";
		picture = "\RH_smg\inv\mp5sd6.paa";
		magazines[] = {"30Rnd_9x19_MP5SD"/*,"30Rnd_9x19_MP5"*/};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\mp5sd.wss",1.778279,1,300};
			soundBegin[] = {"begin1",1};
		};
		class Burst: Burst
		{
			begin1[] = {"\RH_smg\sound\mp5sd.wss",1.778279,1,300};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\mp5sd.wss",1.778279,1,300};
			soundBegin[] = {"begin1",1};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_mp5sd6aim: RH_mp5sd6
	{
		displayName = "HK MP5SD6 CCO";
		model = "\RH_smg\RH_mp5sd6aim.p3d";
		picture = "\RH_smg\inv\mp5sd6aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
			class Use2
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	class RH_mp5sd6eot: RH_mp5sd6
	{
		displayName = "HK MP5SD6 Holo";
		model = "\RH_smg\RH_mp5sd6eot.p3d";
		picture = "\RH_smg\inv\mp5sd6eot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
			class Use2
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
		};
	};
	/*class RH_mp5sd6RFX: RH_mp5sd6
	{
		displayName = "HK MP5SD6 RFX";
		model = "\RH_smg\RH_mp5sd6RFX.p3d";
		picture = "\RH_smg\inv\mp5sd6rfx.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5sd6g: RH_mp5sd6
	{
		displayName = "HK MP5SD6 Gold";
		model = "\RH_smg\RH_mp5sd6g.p3d";
		picture = "\RH_smg\inv\mp5sd6g.paa";
	};
	class RH_mp5k: RH_mp5a5
	{
		displayName = "HK MP5K";
		model = "\RH_smg\RH_mp5k.p3d";
		picture = "\RH_smg\inv\mp5k.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_mp5a5kpdw.rtm"};
	};
	class RH_mp5kpdw: RH_mp5k
	{
		displayName = "HK Mp5k PDW";
		model = "\RH_smg\RH_mp5kpdw.p3d";
		picture = "\RH_smg\inv\mp5kpdw.paa";
	};
	class RH_mp5kpdwaim: RH_mp5k
	{
		displayName = "HK Mp5k PDW CCO";
		model = "\RH_smg\RH_mp5kpdwaim.p3d";
		picture = "\RH_smg\inv\mp5kpdwaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5kpdweot: RH_mp5k
	{
		displayName = "HK Mp5k PDW Holo";
		model = "\RH_smg\RH_mp5kpdweot.p3d";
		picture = "\RH_smg\inv\mp5kpdweot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5kpdwRFX: RH_mp5k
	{
		displayName = "HK Mp5k PDW RFX";
		model = "\RH_smg\RH_mp5kpdwRFX.p3d";
		picture = "\RH_smg\inv\mp5kpdwRFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp5kp: Pistol
	{
		scope = 2;
		modelOptics = "-";
		optics = 1;
		model = "\RH_smg\RH_mp5kp.p3d";
		picture = "\RH_smg\inv\mp5k.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		magazines[] = {"30Rnd_9x19_MP5p"};
		displayName = "HK MP5K pistol";
		drySound[] = {"Ca\sounds\Weapons\rifles\dry",0.003162,1,10};
		reloadMagazineSound[] = {"\RH_smg\sound\MP5_Reload.wss",0.056234,1,25};
		fireLightDuration = 0.05;
		fireLightIntensity = 0.012;
		distanceZoomMin = 107;
		distanceZoomMax = 107;
		value = 10;
		modes[] = {"Single","Burst","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\mp5.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class Burst: Mode_Burst
		{
			begin1[] = {"\RH_smg\sound\mp5.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			soundBurst = 0;
			reloadtime = 0.071;
			ffCount = 1;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 60;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\mp5.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			reloadtime = 0.071;
			ffCount = 1;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			aiRateOfFire = 0.001;
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class Library
		{
			libTextDesc = "$STR_LIB_MP5A5";
		};
		descriptionShort = "$STR_DSS_MP5A5";
	};
	class RH_kriss: RH_mp5a5
	{
		displayName = "TDI Kriss";
		model = "\RH_smg\RH_kriss.p3d";
		picture = "\RH_smg\inv\kriss.paa";
		magazines[] = {"RH_45ACP_30RND_Mag","RH_45ACP_13RND_Mag","RH_45ACP_30RND_SD_Mag"};
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_Kris1.rtm"};
		reloadMagazineSound[] = {"\RH_smg\Sound\kriss_reload.wss",0.056234,1,25};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\kriss.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_SEMIAUTO";
			multiplier = 1;
			burst = 1;
			dispersion = 0.006;
			reloadTime = 0.06315789;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 150;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 50;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\kriss.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_FULLAUTO";
			multiplier = 1;
			burst = 1;
			dispersion = 0.007;
			reloadTime = 0.06315789;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.001;
			aiRateOfFireDistance = 100;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class FlashLight
		{
			color[] = {0.9,0.9,0.7,0.9};
			ambient[] = {0.1,0.1,0.1,1.0};
			position = "flash dir";
			direction = "flash";
			angle = 30;
			scale[] = {1,1,0.5};
			brightness = 0.1;
		};
	};
	class RH_krissaim: RH_kriss
	{
		displayname = "TDI Kriss CCO";
		model = "\RH_smg\RH_krissaim.p3d";
		picture = "\RH_smg\inv\krissaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_krisseot: RH_kriss
	{
		displayname = "TDI Kriss Holo";
		model = "\RH_smg\RH_krisseot.p3d";
		picture = "\RH_smg\inv\krisseot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_krissRFX: RH_kriss
	{
		displayname = "TDI Kriss RFX";
		model = "\RH_smg\RH_krissRFX.p3d";
		picture = "\RH_smg\inv\krissRFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_krisssd: RH_kriss
	{
		displayName = "TDI Kriss SD";
		model = "\RH_smg\RH_krisssd.p3d";
		picture = "\RH_smg\inv\krisssd.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_Kris.rtm"};
		magazines[] = {"RH_45ACP_30RND_SD_Mag","RH_45ACP_30RND_Mag","RH_45ACP_13RND_Mag"};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\umpsd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\umpsd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_krisssdaim: RH_krisssd
	{
		displayname = "TDI Kriss SD CCO";
		model = "\RH_smg\RH_krisssdaim.p3d";
		picture = "\RH_smg\inv\krisssdaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_krisssdeot: RH_krisssd
	{
		displayname = "TDI Kriss SD Holo";
		model = "\RH_smg\RH_krisssdeot.p3d";
		picture = "\RH_smg\inv\krisssdeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_krisssdRFX: RH_krisssd
	{
		displayname = "TDI Kriss SD RFX";
		model = "\RH_smg\RH_krisssdRFX.p3d";
		picture = "\RH_smg\inv\krisssdRFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_uzi: RH_mp5a5
	{
		displayName = "UZI";
		model = "\RH_smg\RH_uzi.p3d";
		picture = "\RH_smg\inv\uzi.paa";
		magazines[] = {"RH_9mm_32RND_Mag","RH_9mm_32RND_SD_Mag"};
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_uzi.rtm"};
		reloadMagazineSound[] = {"\RH_smg\sound\uzi_Reload.wss",0.056234,1,25};
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\uzi.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class Burst: Burst
		{
			begin1[] = {"\RH_smg\sound\uzi.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\uzi.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_uzim: RH_uzi
	{
		displayName = "UZI Mini";
		model = "\RH_smg\RH_uzim.p3d";
		picture = "\RH_smg\inv\uzim.paa";
		magazines[] = {"RH_9mm_32RND_Mag","RH_9mm_32RND_SD_Mag"};
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\RH_uzim.rtm"};
	};
	class RH_uzig: RH_uzi
	{
		displayName = "UZI Gold";
		model = "\RH_smg\RH_uzig.p3d";
		picture = "\RH_smg\inv\uzig.paa";
	};
	class RH_uzisd: RH_uzi
	{
		displayName = "Uzi SD";
		model = "\RH_smg\RH_uzisd.p3d";
		picture = "\RH_smg\inv\uzisd.paa";
		magazines[] = {"RH_9mm_32RND_SD_Mag","RH_9mm_32RND_Mag"};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\uzi_sd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class Burst: Burst
		{
			begin1[] = {"\RH_smg\sound\uzi_sd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\uzi_sd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_mac10: RH_mp5a5
	{
		scope = 2;
		modelOptics = "-";
		optics = 1;
		dexterity = 1.75;
		displayName = "Ingram Mac10";
		model = "\RH_smg\RH_mac10.p3d";
		picture = "\RH_smg\inv\mac10.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_mac10.rtm"};
		reloadMagazineSound[] = {"\RH_smg\Sound\Mac10_reload.wss",0.056234,1,25};
		magazines[] = {"RH_9mm_32RND_Mag"};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\mac10.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_SEMIAUTO";
			multiplier = 1;
			burst = 1;
			dispersion = 0.006;
			reloadTime = 0.06315789;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 150;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 50;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\mac10.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_FULLAUTO";
			multiplier = 1;
			burst = 1;
			dispersion = 0.007;
			reloadTime = 0.06315789;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.001;
			aiRateOfFireDistance = 100;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
	};
	class RH_mac10p: Pistol
	{
		scope = 2;
		modelOptics = "-";
		optics = 1;
		model = "\RH_smg\RH_mac10p.p3d";
		picture = "\RH_smg\inv\mac10.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		magazines[] = {"RH_9mm_32RND_pMag"};
		displayName = "Ingram Mac10 pistol";
		drySound[] = {"Ca\sounds\Weapons\rifles\dry",0.003162,1,10};
		reloadMagazineSound[] = {"\RH_smg\Sound\Mac10_reload.wss",0.056234,1,25};
		distanceZoomMin = 107;
		distanceZoomMax = 107;
		value = 10;
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\mac10.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			dispersion = 0.006;
			reloadTime = 0.06315789;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\mac10.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.007;
			reloadTime = 0.06315789;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			ffCount = 1;
			aiRateOfFire = 0.001;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class Library
		{
			libTextDesc = "$STR_LIB_MP5A5";
		};
		descriptionShort = "$STR_DSS_MP5A5";
	};
	class RH_fmg9: RH_mac10
	{
		displayName = "Magpul FMG9";
		model = "\RH_smg\RH_fmg9.p3d";
		picture = "\RH_smg\inv\fmg9.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\RH_fmg9.rtm"};
		reloadMagazineSound[] = {"\RH_smg\Sound\kriss_reload.wss",0.056234,1,25};
		magazines[] = {"RH_9mm_32RND_Mag"};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\fmg9.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_SEMIAUTO";
			multiplier = 1;
			burst = 1;
			dispersion = 0.006;
			reloadtime = 0.071;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 150;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 50;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\fmg9.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_FULLAUTO";
			multiplier = 1;
			burst = 1;
			dispersion = 0.007;
			reloadtime = 0.071;
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.001;
			aiRateOfFireDistance = 100;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class FlashLight
		{
			color[] = {0.9,0.9,0.7,0.9};
			ambient[] = {0.1,0.1,0.1,1.0};
			position = "flash dir";
			direction = "flash";
			angle = 30;
			scale[] = {1,1,0.5};
			brightness = 0.1;
		};
	};
	class RH_tmp: RH_mp5a5
	{
		displayName = "Steyr TMP";
		model = "\RH_smg\RH_tmp.p3d";
		picture = "\RH_smg\inv\tmp.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_tmp.rtm"};
		reloadMagazineSound[] = {"\RH_smg\sound\tmp_Reload.wss",0.056234,1,25};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\tmp.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\tmp.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_tmpaim: RH_tmp
	{
		displayName = "TMP CCO";
		model = "\RH_smg\RH_tmpaim.p3d";
		picture = "\RH_smg\inv\tmpaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_tmpeot: RH_tmp
	{
		displayName = "TMP Holo";
		model = "\RH_smg\RH_tmpeot.p3d";
		picture = "\RH_smg\inv\tmpeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_tmpsd: RH_tmp
	{
		displayName = "TMP SD";
		model = "\RH_smg\RH_tmpsd.p3d";
		picture = "\RH_smg\inv\tmpsd.paa";
		magazines[] = {"30Rnd_9x19_MP5SD","30Rnd_9x19_MP5"};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\tmpsd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\tmpsd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_tmpsdaim: RH_tmpsd
	{
		displayName = "TMP SD CCO";
		model = "\RH_smg\RH_tmpsdaim.p3d";
		picture = "\RH_smg\inv\tmpsdaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_tmpsdeot: RH_tmpsd
	{
		displayName = "TMP SD Holo";
		model = "\RH_smg\RH_tmpsdeot.p3d";
		picture = "\RH_smg\inv\tmpsdeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp7p: Pistol
	{
		scope = 2;
		modelOptics = "-";
		optics = 1;
		displayName = "HK Mp7 pistol";
		model = "\RH_smg\RH_mp7p.p3d";
		picture = "\RH_smg\inv\mp7.paa";
		reloadMagazineSound[] = {"\RH_smg\sound\tmp_Reload.wss",0.056234,1,25};
		magazines[] = {"RH_46x30mm_40RND_pMag"};
		fireLightDuration = 0.05;
		fireLightIntensity = 0.012;
		distanceZoomMin = 107;
		distanceZoomMax = 107;
		value = 10;
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\mp7.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\mp7.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			reloadtime = 0.071;
			ffCount = 1;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			aiRateOfFire = 0.001;
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class Library
		{
			libTextDesc = "$STR_LIB_MP5A5";
		};
		descriptionShort = "$STR_DSS_MP5A5";
	};
	class RH_mp7: RH_mp5a5
	{
		displayName = "HK Mp7";
		model = "\RH_smg\RH_mp7.p3d";
		picture = "\RH_smg\inv\mp7.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_tmp.rtm"};
		reloadMagazineSound[] = {"\RH_smg\sound\tmp_Reload.wss",0.056234,1,25};
		magazines[] = {"RH_46x30mm_40RND_Mag","RH_46x30mm_40RND_SD_Mag"};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\mp7.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\mp7.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_mp7aim: RH_mp7
	{
		displayName = "HK MP7 CCO";
		model = "\RH_smg\RH_mp7aim.p3d";
		picture = "\RH_smg\inv\mp7aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp7eot: RH_mp7
	{
		displayName = "HK MP7 Holo";
		model = "\RH_smg\RH_mp7eot.p3d";
		picture = "\RH_smg\inv\mp7eot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp7sd: RH_mp7
	{
		displayName = "HK MP7 SD";
		model = "\RH_smg\RH_mp7sd.p3d";
		picture = "\RH_smg\inv\mp7sd.paa";
		magazines[] = {"RH_46x30mm_40RND_SD_Mag","RH_46x30mm_40RND_Mag"};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\mp7sd.wss",1.778279,1,300};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\mp7sd.wss",1.778279,1,300};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_mp7sdaim: RH_mp7sd
	{
		displayName = "HK MP7 SD CCO";
		model = "\RH_smg\RH_mp7sdaim.p3d";
		picture = "\RH_smg\inv\mp7sdaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_mp7sdeot: RH_mp7sd
	{
		displayName = "HK MP7 SD Holo";
		model = "\RH_smg\RH_mp7sdeot.p3d";
		picture = "\RH_smg\inv\mp7sdeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_pp2000p: Pistol
	{
		scope = 2;
		modelOptics = "-";
		optics = 1;
		displayName = "PP-2000 pistol";
		model = "\RH_smg\RH_pp2000p.p3d";
		picture = "\RH_smg\inv\pp2000.paa";
		reloadMagazineSound[] = {"\RH_smg\sound\tmp_Reload.wss",0.056234,1,25};
		magazines[] = {"30Rnd_9x19_MP5p"};
		fireLightDuration = 0.05;
		fireLightIntensity = 0.012;
		distanceZoomMin = 107;
		distanceZoomMax = 107;
		value = 10;
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_smg\sound\tmp.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.05;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_smg\sound\tmp.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			reloadtime = 0.071;
			ffCount = 1;
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
			aiRateOfFire = 0.001;
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.2;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 40;
			maxRangeProbab = 0.05;
		};
		class Library
		{
			libTextDesc = "$STR_LIB_MP5A5";
		};
		descriptionShort = "$STR_DSS_MP5A5";
	};
	class RH_pp2000: RH_mp5a5
	{
		displayName = "PP-2000";
		model = "\RH_smg\RH_pp2000.p3d";
		picture = "\RH_smg\inv\pp2000.paa";
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_p90.rtm"};
		reloadMagazineSound[] = {"\RH_smg\sound\tmp_Reload.wss",0.056234,1,25};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\tmp.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\tmp.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_pp2000aim: RH_pp2000
	{
		displayName = "PP-2000 CCO";
		model = "\RH_smg\RH_pp2000aim.p3d";
		picture = "\RH_smg\inv\pp2000aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};
	class RH_pp2000eot: RH_pp2000
	{
		displayName = "PP-2000 Holo";
		model = "\RH_smg\RH_pp2000eot.p3d";
		picture = "\RH_smg\inv\pp2000eot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};*/
	class RH_UMP: MP5A5
	{
		scope = 2;
		displayName = "HK UMP45";
		descriptionShort="Submachine gun developed and manufactured by Heckler and Koch.<br/>Magazine: HK UMP45 Mag.";
		model = "\RH_smg\RH_UMP.p3d";
		picture = "\RH_smg\inv\ump.paa";
		optics = 1;
		modelOptics = "-";
		dexterity = 1.8;
		maxLeadSpeed = 23;
		handAnim[] = {"OFP2_ManSkeleton"};
		reloadMagazineSound[] = {"\RH_smg\sound\ump_Reload.wss",0.056234,1,25};
		magazines[] = {"RH_45ACP_25RND_Mag"/*,"RH_45ACP_25RND_SD_Mag"*/};
		count = 25;
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			ammo = "RH_smg_45ACP_Round";
			multiplier = 1;
			burst = 1;
			soundContinuous = 0;
			begin1[] = {"\RH_smg\sound\ump.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			ffCount = 1;
			dispersion = 0.0002;
			reloadTime = 0.1;
			autofire = 0;
			aiRateOfFire = 1.0;
			aiRateOfFireDistance = 500;
			UseAction = 0;
			useActionTitle = "";
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
		};
		class FullAuto: Mode_FullAuto
		{
			ammo = "RH_smg_45ACP_Round";
			multiplier = 1;
			burst = 1;
			soundContinuous = 0;
			begin1[] = {"\RH_smg\sound\ump.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			ffCount = 1;
			dispersion = 0.0004;
			reloadTime = 0.1;
			autofire = 1;
			aiRateOfFire = 1.0;
			aiRateOfFireDistance = 500;
			UseAction = 0;
			useActionTitle = "";
			recoil = "RH_smg_Recoil";
			recoilProne = "RH_smg_Recoil";
		};
	};
	class RH_umpaim: RH_UMP
	{
		displayName = "HK UMP45 CCO";
		model = "\RH_smg\RH_umpaim.p3d";
		picture = "\RH_smg\inv\umpaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	class RH_umpeot: RH_UMP
	{
		displayName = "HK UMP45 Holo";
		model = "\RH_smg\RH_umpeot.p3d";
		picture = "\RH_smg\inv\umpeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
		};
	};
	/*class RH_umpRFX: RH_UMP
	{
		displayName = "HK UMP45 RFX";
		model = "\RH_smg\RH_umpRFX.p3d";
		picture = "\RH_smg\inv\umpRFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};*/
	class RH_umpsd: RH_UMP
	{
		displayName = "HK UMP45 SD";
		descriptionShort="HK UMP45 suppressed submachine gun.<br/>Magazine: HK UMP45SD Mag.";
		model = "\RH_smg\RH_umpsd.p3d";
		picture = "\RH_smg\inv\umpsd.paa";
		magazines[] = {"RH_45ACP_25RND_SD_Mag"/*,"RH_45ACP_25RND_Mag"*/};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\umpsd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\umpsd.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_umpsdaim: RH_umpsd
	{
		displayName = "HK UMP45 SD CCO";
		model = "\RH_smg\RH_umpsdaim.p3d";
		picture = "\RH_smg\inv\umpsdaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_umpsdeot: RH_umpsd
	{
		displayName = "HK UMP45 SD Holo";
		model = "\RH_smg\RH_umpsdeot.p3d";
		picture = "\RH_smg\inv\umpsdeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	/*class RH_umpsdRFX: RH_umpsd
	{
		displayName = "HK UMP45 SD RFX";
		model = "\RH_smg\RH_umpsdRFX.p3d";
		picture = "\RH_smg\inv\umpsdRFX.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
	};*/
	class RH_P90: MP5A5
	{
		scope = 2;
		displayName = "FN P90";
		descriptionShort="Selective fire personal defense weapon.<br/>Magazine: FN P90 Mag.";
		model = "\RH_smg\RH_p90.p3d";
		picture = "\RH_smg\inv\p90.paa";
		optics = 1;
		modelOptics = "-";
		dexterity = 1.8;
		maxLeadSpeed = 23;
		handAnim[] = {"OFP2_ManSkeleton","\RH_smg\Anim\NORRN_RH_p90.rtm"};
		reloadMagazineSound[] = {"\RH_smg\sound\p90_Reload.wss",0.056234,1,25};
		magazines[] = {"RH_57x28mm_50RND_Mag"/*,"RH_57x28mm_50RND_SD_Mag"*/};
		count = 25;
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			ammo = "RH_57x28mm_Round";
			multiplier = 1;
			burst = 1;
			soundContinuous = 0;
			begin1[] = {"\RH_smg\sound\p90s.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			ffCount = 1;
			dispersion = 0.0045;
			reloadTime = 0.1;
			autofire = 0;
			aiRateOfFire = 1.0;
			aiRateOfFireDistance = 500;
			UseAction = 0;
			useActionTitle = "";
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
		};
		class FullAuto: Mode_FullAuto
		{
			ammo = "RH_57x28mm_Round";
			multiplier = 1;
			burst = 1;
			soundContinuous = 0;
			begin1[] = {"\RH_smg\sound\p90f.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
			ffCount = 1;
			dispersion = 0.005;
			reloadTime = 0.1;
			autofire = 1;
			aiRateOfFire = 1.0;
			aiRateOfFireDistance = 500;
			UseAction = 0;
			useActionTitle = "";
			recoil = "RH_subMachineGunBase";
			recoilProne = "RH_subMachineGunBase";
		};
		class FlashLight
		{
			color[] = {0.9,0.9,0.7,0.9};
			ambient[] = {0.1,0.1,0.1,1.0};
			position = "flash dir";
			direction = "flash";
			angle = 30;
			scale[] = {1,1,0.5};
			brightness = 0.1;
		};
	};
	class RH_P90aim: RH_P90
	{
		displayname = "FN P90 CCO";
		model = "\RH_smg\RH_p90aim.p3d";
		picture = "\RH_smg\inv\p90aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	class RH_P90eot: RH_P90
	{
		displayname = "FN P90 Holo";
		model = "\RH_smg\RH_p90eot.p3d";
		picture = "\RH_smg\inv\p90eot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
		};
	};
	class RH_P90sd: RH_P90
	{
		displayname = "FN P90SD";
		descriptionShort="FN P90 suppressed version.<br/>Magazine: FN P90SD Mag.";
		model = "\RH_smg\RH_p90sd.p3d";
		picture = "\RH_smg\inv\p90sd.paa";
		magazines[] = {"RH_57x28mm_50RND_SD_Mag"/*,"RH_57x28mm_50RND_Mag"*/};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		class Single: Single
		{
			begin1[] = {"\RH_smg\sound\p90sds.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_smg\sound\p90sdf.wss",1.778279,1,900};
			soundBegin[] = {"begin1",1};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_p90sdaim: RH_P90sd
	{
		displayName = "FN P90SD CCO";
		model = "\RH_smg\RH_p90sdaim.p3d";
		picture = "\RH_smg\inv\p90sdaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_p90sdeot: RH_P90sd
	{
		displayName = "FN P90SD Holo";
		model = "\RH_smg\RH_p90sdeot.p3d";
		picture = "\RH_smg\inv\p90sdeot.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		class ItemActions
		{
			class Use
			{
				text = "Remove Holo Sight";
				script = "spawn player_removeHOLO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	/*class RH_P90i: RH_P90
	{
		displayName = "FN P90 Iron Sights";
		model = "\RH_smg\RH_p90i.p3d";
		picture = "\RH_smg\inv\p90i.paa";
	};
	class RH_p90isd: RH_P90sd
	{
		displayName = "FN P90 SD Iron Sights";
		model = "\RH_smg\RH_p90isd.p3d";
		picture = "\RH_smg\inv\p90isd.paa";
	};*/
};
/*class cfgVehicles
{
	class ReammoBox;
	class RHsmgbox: ReammoBox
	{
		scope = 2;
		accuracy = 1000;
		model = "\ca\weapons\AmmoBoxes\USBasicWeapons.p3d";
		displayName = "RH SMG weapons box";
		class TransportMagazines
		{
			class _xx_RH_45ACP_30RND_Mag
			{
				magazine = "RH_45ACP_30RND_Mag";
				count = 200;
			};
			class _xx_RH_45ACP_30RND_SD_Mag
			{
				magazine = "RH_45ACP_30RND_SD_Mag";
				count = 200;
			};
			class _xx_RH_45ACP_13RND_Mag
			{
				magazine = "RH_45ACP_13RND_Mag";
				count = 200;
			};
			class _xx_RH_45ACP_25RND_Mag
			{
				magazine = "RH_45ACP_25RND_Mag";
				count = 200;
			};
			class _xxRH_45ACP_25RND_SD_Mag
			{
				magazine = "RH_45ACP_25RND_SD_Mag";
				count = 200;
			};
			class _xx_RH_9mm_32RND_Mag
			{
				magazine = "RH_9mm_32RND_Mag";
				count = 200;
			};
			class _xx_RH_9mm_32RND_SD_Mag
			{
				magazine = "RH_9mm_32RND_SD_Mag";
				count = 200;
			};
			class _xx_RH_9mm_32RND_pMag
			{
				magazine = "RH_9mm_32RND_pMag";
				count = 200;
			};
			class _xx_RH_57x28mm_50RND_Mag
			{
				magazine = "RH_57x28mm_50RND_Mag";
				count = 200;
			};
			class _xx_RH_57x28mm_50RND_SD_Mag
			{
				magazine = "RH_57x28mm_50RND_SD_Mag";
				count = 200;
			};
			class _xx_RH_46x30mm_40RND_Mag
			{
				magazine = "RH_46x30mm_40RND_Mag";
				count = 200;
			};
			class _xx_RH_46x30mm_40RND_SD_Mag
			{
				magazine = "RH_46x30mm_40RND_SD_Mag";
				count = 200;
			};
			class _xx_RH_46x30mm_40RND_pMag
			{
				magazine = "RH_46x30mm_40RND_pMag";
				count = 200;
			};
			class _xx_30Rnd_556x45_Stanag
			{
				magazine = "30Rnd_556x45_Stanag";
				count = 200;
			};
			class _xx_20Rnd_556x45_Stanag
			{
				magazine = "20Rnd_556x45_Stanag";
				count = 200;
			};
			class _xx_30Rnd_9x19_MP5
			{
				magazine = "30Rnd_9x19_MP5";
				count = 200;
			};
			class _xx_30Rnd_9x19_MP5SD
			{
				magazine = "30Rnd_9x19_MP5SD";
				count = 200;
			};
			class _xx_30Rnd_9x19_MP5p
			{
				magazine = "30Rnd_9x19_MP5p";
				count = 200;
			};
			class _xx_1rnd_HE_M203
			{
				magazine = "1rnd_HE_M203";
				count = 30;
			};
			class _xx_HandGrenade_West
			{
				magazine = "HandGrenade_West";
				count = 50;
			};
			class _xx_FlareWhite_M203
			{
				magazine = "FlareWhite_M203";
				count = 6;
			};
			class _xx_FlareRed_M203
			{
				magazine = "FlareRed_M203";
				count = 6;
			};
			class _xx_FlareGreen_M203
			{
				magazine = "FlareGreen_M203";
				count = 6;
			};
			class _xx_FlareYellow_M203
			{
				magazine = "FlareYellow_M203";
				count = 6;
			};
		};
		class TransportWeapons
		{
			class _xx_RH_HK53
			{
				weapon = "RH_HK53";
				count = 6;
			};
			class _xx_RH_HK53aim
			{
				weapon = "RH_HK53aim";
				count = 6;
			};
			class _xx_RH_HK53eot
			{
				weapon = "RH_HK53eot";
				count = 6;
			};
			class _xx_RH_HK53RFX
			{
				weapon = "RH_HK53RFX";
				count = 6;
			};
			class _xx_RH_pdr
			{
				weapon = "RH_pdr";
				count = 6;
			};
			class _xx_RH_pdraim
			{
				weapon = "RH_pdraim";
				count = 6;
			};
			class _xx_RH_pdreot
			{
				weapon = "RH_pdreot";
				count = 6;
			};
			class _xx_RH_fmg9
			{
				weapon = "RH_fmg9";
				count = 6;
			};
			class _xx_RH_mp5a4
			{
				weapon = "RH_mp5a4";
				count = 6;
			};
			class _xx_RH_mp5a4aim
			{
				weapon = "RH_mp5a4aim";
				count = 6;
			};
			class _xx_RH_mp5a4eot
			{
				weapon = "RH_mp5a4eot";
				count = 6;
			};
			class _xx_RH_mp5a4RFX
			{
				weapon = "RH_mp5a4RFX";
				count = 6;
			};
			class _xx_RH_mp5a5
			{
				weapon = "RH_mp5a5";
				count = 6;
			};
			class _xx_RH_mp5a5aim
			{
				weapon = "RH_mp5a5aim";
				count = 6;
			};
			class _xx_RH_mp5a5eot
			{
				weapon = "RH_mp5a5eot";
				count = 6;
			};
			class _xx_RH_mp5a5RFX
			{
				weapon = "RH_mp5a5RFX";
				count = 6;
			};
			class _xx_RH_mp5a5ris
			{
				weapon = "RH_mp5a5ris";
				count = 6;
			};
			class _xx_RH_mp5a5risaim
			{
				weapon = "RH_mp5a5risaim";
				count = 6;
			};
			class _xx_RH_mp5a5riseot
			{
				weapon = "RH_mp5a5riseot";
				count = 6;
			};
			class _xx_RH_mp5a5risRFX
			{
				weapon = "RH_mp5a5risRFX";
				count = 6;
			};
			class _xx_RH_mp5a5eod
			{
				weapon = "RH_mp5a5eod";
				count = 6;
			};
			class _xx_RH_mp5a5eodaim
			{
				weapon = "RH_mp5a5eodaim";
				count = 6;
			};
			class _xx_RH_mp5a5eodeot
			{
				weapon = "RH_mp5a5eodeot";
				count = 6;
			};
			class _xx_RH_mp5a5eodRFX
			{
				weapon = "RH_mp5a5eodRFX";
				count = 6;
			};
			class _xx_RH_mp5sd6
			{
				weapon = "RH_mp5sd6";
				count = 6;
			};
			class _xx_RH_mp5sd6aim
			{
				weapon = "RH_mp5sd6aim";
				count = 6;
			};
			class _xx_RH_mp5sd6eot
			{
				weapon = "RH_mp5sd6eot";
				count = 6;
			};
			class _xx_RH_mp5sd6RFX
			{
				weapon = "RH_mp5sd6RFX";
				count = 6;
			};
			class _xx_RH_mp5sd6g
			{
				weapon = "RH_mp5sd6g";
				count = 6;
			};
			class _xx_RH_mp5k
			{
				weapon = "RH_mp5k";
				count = 6;
			};
			class _xx_RH_mp5kp
			{
				weapon = "RH_mp5kp";
				count = 6;
			};
			class _xx_RH_mp5kpdw
			{
				weapon = "RH_mp5kpdw";
				count = 6;
			};
			class _xx_RH_mp5kpdwaim
			{
				weapon = "RH_mp5kpdwaim";
				count = 6;
			};
			class _xx_RH_mp5kpdweot
			{
				weapon = "RH_mp5kpdweot";
				count = 6;
			};
			class _xx_RH_mp5kpdwRFX
			{
				weapon = "RH_mp5kpdwRFX";
				count = 6;
			};
			class _xx_RH_p90
			{
				weapon = "RH_p90";
				count = 6;
			};
			class _xx_RH_p90i
			{
				weapon = "RH_p90i";
				count = 6;
			};
			class _xx_RH_p90aim
			{
				weapon = "RH_p90aim";
				count = 6;
			};
			class _xx_RH_p90eot
			{
				weapon = "RH_p90eot";
				count = 6;
			};
			class _xx_RH_p90sd
			{
				weapon = "RH_p90sd";
				count = 6;
			};
			class _xx_RH_p90isd
			{
				weapon = "RH_p90isd";
				count = 6;
			};
			class _xx_RH_p90sdaim
			{
				weapon = "RH_p90sdaim";
				count = 6;
			};
			class _xx_RH_p90sdeot
			{
				weapon = "RH_p90sdeot";
				count = 6;
			};
			class _xx_RH_ump
			{
				weapon = "RH_ump";
				count = 6;
			};
			class _xx_RH_umpaim
			{
				weapon = "RH_umpaim";
				count = 6;
			};
			class _xx_RH_umpeot
			{
				weapon = "RH_umpeot";
				count = 6;
			};
			class _xx_RH_umpRFX
			{
				weapon = "RH_umpRFX";
				count = 6;
			};
			class _xx_RH_umpsd
			{
				weapon = "RH_umpsd";
				count = 6;
			};
			class _xx_RH_umpsdaim
			{
				weapon = "RH_umpsdaim";
				count = 6;
			};
			class _xx_RH_umpsdeot
			{
				weapon = "RH_umpsdeot";
				count = 6;
			};
			class _xx_RH_umpsdRFX
			{
				weapon = "RH_umpsdRFX";
				count = 6;
			};
			class _xx_RH_kriss
			{
				weapon = "RH_kriss";
				count = 6;
			};
			class _xx_RH_krissaim
			{
				weapon = "RH_krissaim";
				count = 6;
			};
			class _xx_RH_krisseot
			{
				weapon = "RH_krisseot";
				count = 6;
			};
			class _xx_RH_krissRFX
			{
				weapon = "RH_krissRFX";
				count = 6;
			};
			class _xx_RH_krisssd
			{
				weapon = "RH_krisssd";
				count = 6;
			};
			class _xx_RH_krisssdaim
			{
				weapon = "RH_krisssdaim";
				count = 6;
			};
			class _xx_RH_krissssdeot
			{
				weapon = "RH_krisssdeot";
				count = 6;
			};
			class _xx_RH_krisssdRFX
			{
				weapon = "RH_krisssdRFX";
				count = 6;
			};
			class _xx_RH_uzi
			{
				weapon = "RH_uzi";
				count = 6;
			};
			class _xx_RH_uzim
			{
				weapon = "RH_uzim";
				count = 6;
			};
			class _xx_RH_uzig
			{
				weapon = "RH_uzig";
				count = 6;
			};
			class _xx_RH_uzisd
			{
				weapon = "RH_uzisd";
				count = 6;
			};
			class _xx_RH_mac10
			{
				weapon = "RH_mac10";
				count = 6;
			};
			class _xx_RH_mac10p
			{
				weapon = "RH_mac10p";
				count = 6;
			};
			class _xx_RH_tmp
			{
				weapon = "RH_tmp";
				count = 6;
			};
			class _xx_RH_tmpaim
			{
				weapon = "RH_tmpaim";
				count = 6;
			};
			class _xx_RH_tmpeot
			{
				weapon = "RH_tmpeot";
				count = 6;
			};
			class _xx_RH_tmpsd
			{
				weapon = "RH_tmpsd";
				count = 6;
			};
			class _xx_RH_tmpsdaim
			{
				weapon = "RH_tmpsdaim";
				count = 6;
			};
			class _xx_RH_tmpsdeot
			{
				weapon = "RH_tmpsdeot";
				count = 6;
			};
			class _xx_RH_pp2000
			{
				weapon = "RH_pp2000";
				count = 6;
			};
			class _xx_RH_pp2000p
			{
				weapon = "RH_pp2000p";
				count = 6;
			};
			class _xx_RH_pp2000aim
			{
				weapon = "RH_pp2000aim";
				count = 6;
			};
			class _xx_RH_pp2000eot
			{
				weapon = "RH_pp2000eot";
				count = 6;
			};
			class _xx_RH_mp7
			{
				weapon = "RH_mp7";
				count = 6;
			};
			class _xx_RH_mp7p
			{
				weapon = "RH_mp7p";
				count = 6;
			};
			class _xx_RH_mp7aim
			{
				weapon = "RH_mp7aim";
				count = 6;
			};
			class _xx_RH_mp7eot
			{
				weapon = "RH_mp7eot";
				count = 6;
			};
			class _xx_RH_mp7sd
			{
				weapon = "RH_mp7sd";
				count = 6;
			};
			class _xx_RH_mp7sdaim
			{
				weapon = "RH_mp7sdaim";
				count = 6;
			};
			class _xx_RH_mp7sdeot
			{
				weapon = "RH_mp7sdeot";
				count = 6;
			};
		};
	};
};*/
//};
