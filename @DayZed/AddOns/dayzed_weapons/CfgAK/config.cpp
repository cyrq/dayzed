////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 4.13
//Fri Oct 11 14:37:50 2013 : Source 'file' date Fri Oct 11 14:37:50 2013
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

class cfgPatches
{
	class RH_aks_cfg
	{
		// units[] = {"RHAKbox"};
		weapons[] = {"RH_ak47","RH_svd","RH_oc14","RH_ak74m","RH_ak74","RH_ak74gl","RH_ak74k","RH_ak74kgl","RH_ak74sp","RH_aks74","RH_aks74gl","RH_aks74k","RH_aks74kgl","RH_aks74u","RH_aks74uk","RH_aks74usd","RH_aks74usdk","RH_aks74sp","RH_akm","RH_akmssd","RH_akmsp","RH_akms","RH_akmgl","RH_rk95","RH_rk95aim","RH_rk95sd","RH_rk95sdaim","RH_bizon","RH_bizonk","RH_bizonsd","RH_bizonsdk","RH_an94","RH_an94gl","RH_svd_CAMO","RH_gr1","RH_gr1sd","RH_rpk74","RH_ak107","RH_ak107gl","RH_ak107k","RH_ak107kgl","RH_ak107sp","RH_ak107glsp","RH_pm","RH_pmsd","RH_ak105","RH_ak105gl","RH_ak105k","RH_ak105kgl","RH_ak105sp","RH_ak105glsp","RH_aps","RH_apssd","RH_ak105_1p29","RH_ak105gl1p29","RH_ak107_1p29","RH_ak107gl1p29","RH_svdb"/*,"RH_aks47b","RH_aks74g","RH_aks47s","RH_ak47gl","RH_aks74upt","RH_aks74uptk","RH_aks74uptsp","RH_svdg","RH_svds","RH_svu","RH_oc14gl","RH_oc14sd","RH_oc14sp","RH_oc14glsp","RH_gr1sp","RH_oc14sdsp","RH_gr1sdsp","RH_rpk47","RH_rpk74m","RH_rpk74m1p29","RH_ak74mgl","RH_ak74mk","RH_ak74mkgl","RH_ak74msp","RH_ak74mglsp","RH_ak74m1p29","RH_ak74mgl1p29","RH_aks74m","RH_aks74mgl","RH_aks74mk","RH_aks74mkgl","RH_aks74msp","RH_aks74mglsp","RH_aks74m1p29","RH_aks74mgl1p29","RH_ak103","RH_ak103gl","RH_ak103k","RH_ak103kgl","RH_ak103sp","RH_ak103glsp","RH_ak103_1p29","RH_ak103gl1p29","RH_aks74p","RH_aks74pgl","RH_aks74pk ","RH_aks74pkgl","RH_aks74psp","RH_aks74pglsp","RH_aks74p1p29","RH_aks74pgl1p29","RH_ak104","RH_ak104gl","RH_ak104k","RH_ak104kgl","RH_ak104sp","RH_ak104glsp","RH_ak104_1p29","RH_ak104gl1p29","RH_asval","RH_asvalk","RH_asvalsp","RH_rk95sdag","RH_rk95ag","RH_aks47"*/};
		requiredVersion = 1.04;
		requiredAddons[] = {"CAweapons"};
	};
};
class CfgAddons
{
	class PreloadAddons
	{
		class RH_aks_cfg
		{
			list[] = {"RH_aks_cfg"};
		};
	};
};
class cfgRecoils
{
	RH_AK47Recoil[] = {0,0.009,0.01,0.01,0.009,0.01,0.07,0.004,0.018,0.17,0,0};
	RH_AK47RecoilProne[] = {0,0.007,0.005,0.01,0.007,0.005,0.07,0.007,0.001,0.16,0,0};
	RH_AKMRecoil[] = {0,0.009,0.01,0.01,0.009,0.01,0.07,0.004,0.017,0.17,0,0};
	RH_AKMRecoilProne[] = {0,0.007,0.006,0.01,0.007,0.006,0.07,0.007,0.001,0.16,0,0};
	RH_AKSRecoil[] = {0,0.01,0.02,0.017,0.01,0.02,0.06,0.005,0.03,0.2,0,0};
	RH_AKSRecoilProne[] = {0,0.01,0.005,0.012,0.01,0.005,0.17,0.005,0,0.25,0,0};
	RH_AN94Recoil[] = {0,0.008,0.006,0.01,0.008,0.006,0.04,0.004,0.012,0.12,0,0};
	RH_AN94RecoilProne[] = {0,0.007,0.003,0.01,0.007,0.003,0.17,0.003,0,0.22,0,0};
	RH_AN94BurstRecoil[] = {0,0,0,0.05,0.005,0,0.07,0.01,0.006,0.12,0,0};
	RH_AN94BurstRecoilProne[] = {0,0.002,0,0.01,0.003,0.005,0.03,0.007,-0.0003,0.1,0,0};
	RH_AKS74URecoil[] = {0,0.008,0.006,0.01,0.008,0.006,0.04,0.004,0.012,0.12,0,0};
	RH_AKS74URecoilProne[] = {0,0.007,0.003,0.01,0.007,0.003,0.17,0.003,0,0.22,0,0};
	RH_SVDRecoil[] = {0,0.011,0.015,0.02,0.011,0.015,0.1,0.009,0.015,0.12,0,0};
	RH_SVDRecoilProne[] = {0,0.01,0.005,0.013,0.01,0.005,0.1,0.007,0,0.12,0,0};
	RH_BizonRecoil[] = {0,0.004,0.005,0.01,0.004,0.005,0.04,0.003,0.006,0.1,0,0};
	RH_MGRecoil[] = {0,0.008,0.02,0.14,0.006,0,0.2,0,0};
	RH_MGRecoilProne[] = {0,0.008,0.002,0.1,0.008,0,0.15,0,0};
};
class CfgAmmo
{
	class Default;
	class BulletBase;
	class B_9x18_Ball;
	class B_9x18_SD;
	class B_556x45_Ball;
	class B_762x39_Ball;
	class RH_B_762x39SD_Ball: BulletBase
	{
		hit = 7;
		typicalSpeed = 290;
		airFriction = -0.00055;
		supersonicCrackNear[] = {"",1,1};
		supersonicCrackFar[] = {"",1,1};
		visibleFire = 0.2;
		audibleFire = 0.2;
		visibleFireTime = 0.5;
		tracerColor[] = {0,0,0,0};
		tracerColorR[] = {0,0,0,0};
		cartridge = "FxCartridge_762";
	};
	class RH_B_9x39_Ball: BulletBase
	{
		hit = 10;
		typicalSpeed = 300;
		airFriction = -0.000325;
		visibleFire = 10;
		audibleFire = 10;
		visibleFireTime = 3;
		cartridge = "FxCartridge_9mm";
		supersonicCrackNear[] = {"",1,1};
		supersonicCrackFar[] = {"",1,1};
		tracerColor[] = {0,0,0,0};
		tracerColorR[] = {0,0,0,0};
		cost = 1.2;
	};
	class RH_B_9x39_SD: RH_B_9x39_Ball
	{
		visibleFire = 0.05;
		audibleFire = 0.05;
		visibleFireTime = 2;
	};
	class GrenadeCore;
	class GrenadeBase: GrenadeCore{};
	class G_40mm_HE: GrenadeBase{};
	class RH_30mm_AP: G_40mm_HE
	{
		typicalSpeed = 100;
		hit = 40;
		indirectHit = 5;
		indirectHitRange = 2.5;
		visibleFire = 0.8;
		audibleFire = 0.8;
		visibleFireTime = 0.5;
		explosive = 1;
		cost = 10;
		deflecting = 3;
		CraterEffects = "ExploAmmoCrater";
		explosionEffects = "ExploAmmoExplosion";
	};
};
class CfgMagazines
{
	class Default;
	class CA_Magazine;
	class 8Rnd_9x18_Makarov;
	class 8Rnd_9x18_MakarovSD;
	class 30Rnd_545x39_AK;
	class 30Rnd_545x39_AKSD;
	class 30Rnd_762x39_AK47;
	class RH_20Rnd_9x18_aps: CA_Magazine
	{
		scope = 2;
		type = 16;
		displayName = "Stechkin Mag.";
		model="\ca\CommunityConfigurationProject_E\Gameplay_ActualModelsOfWeaponMagazinesVisibleOnTheGround\p3d\8Rnd_9x18_Makarov.p3d";
		picture = "\Ca\weapons\Data\Equip\m_makarov_CA.paa";
		ammo = "B_9x18_Ball";
		count = 20;
		initSpeed = 320;
		descriptionShort="Caliber: 9x18mm Makarov<br/>Rounds: 20<br/>Used in: Stechkin APS";
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
		};
	};
	class RH_20Rnd_9x18_apsSD: RH_20Rnd_9x18_aps
	{
		scope = 2;
		type = 16;
		displayName = "StechkinSD Mag.";
		picture = "\Ca\weapons\Data\Equip\m_makarov_CA.paa";
		ammo = "B_9x18_SD";
		initSpeed = 310;
		descriptionShort="Caliber: 9x18mm Makarov<br/>Rounds: 20<br/>Used in: Stechkin APS SD";
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
		};
	};
	class RH_20Rnd_9x39_SP6_mag: 30Rnd_545x39_AK
	{
		displayName = "OC14 mag";
		picture = "\RH_aks\inv\m_oc.paa";
		model = "\RH_aks\mags\mag_oc14.p3d";
		initspeed = 295;
		count = 20;
		ammo = "RH_B_9x39_Ball";
	};
	class RH_20Rnd_9x39_SP6SD_mag: RH_20Rnd_9x39_SP6_mag
	{
		displayName = "OC14SD mag";
		ammo = "RH_B_9x39_SD";
		selectionFireAnim = "zasleh";
	};
	class RH_20Rnd_9x39_val_mag: 30Rnd_545x39_AK
	{
		displayName = "Val mag";
		picture = "\CA\weapons\data\equip\M_VSSx20_CA.paa";
		ammo = "RH_B_9x39_SD";
		count = 20;
		selectionFireAnim = "zasleh";
	};
	class RH_30Rnd_545x39_AKSU_mag: 30Rnd_545x39_AK
	{
		displayName = "AKSU Mag.";
		descriptionShort="Caliber: 5.45x39mm<br/>Rounds: 30<br/>Used in: AKS-74U";
		initspeed = 735;
		model = "\RH_aks\mags\mag_aksu.p3d";
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class RH_30Rnd_762x39_SDmag: 30Rnd_762x39_AK47
	{
		displayName = "AKMSD Mag.";
		descriptionShort="Caliber: 7.62x39mm<br/>Rounds: 30<br/>Used in suppressed: AKMS, Groza, RK-95";
		picture = "\RH_aks\inv\m_ak.paa";
		initSpeed=710;
		ammo = "RH_B_762x39SD_Ball";
		model = "\RH_aks\mags\mag_ak47.p3d";
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class RH_75Rnd_762x39_mag: 30Rnd_762x39_AK47
	{
		displayName = "RPK47 drum mag";
		picture = "\RH_aks\inv\m_rpk.paa";
		model = "\RH_aks\mags\mag_rpk.p3d";
		count = 75;
		initspeed = 745;
		type = "2*256";
	};
	class RH_45Rnd_545x39_mag: 30Rnd_545x39_AK
	{
		displayName = "RPK-74 Mag.";
		descriptionShort="Caliber: 5.45x39mm<br/>Rounds: 45<br/>Used in: RPK-74";
		picture = "\RH_aks\inv\m_rpk74.paa";
		model = "\RH_aks\mags\mag_rpk74.p3d";
		count = 45;
		initspeed = 960;
		class ItemActions {
				class CombineMag {
				text = $STR_MAG_COMBINE;
				script = "spawn player_combineMag; r_action_count = r_action_count + 1;";
		};
	};
	};
	class RH_1Rnd_30mm: CA_Magazine
	{
		scope = 2;
		type = 16;
		displayName = "30mm Grenade";
		picture = "\Ca\weapons\Data\Equip\m_30mmHP_CA.paa";
		ammo = "RH_30mm_AP";
		initSpeed = 100;
		count = 1;
	};
};
class Mode_SemiAuto{};
class Mode_Burst: Mode_SemiAuto{};
class Mode_FullAuto: Mode_SemiAuto{};
class cfgWeapons
{
	class Default;
	class PistolCore;
	class Pistol;
	class RifleCore;
	class MGunCore;
	class LauncherCore;
	class GrenadeCore;
	class CannonCore;
	class Launcher;
	class GrenadeLauncher;
	class rifle;
	class AK_BASE;
	class AKS_BASE;
	class AK_74;
	class AKS_74_U;
	class VSS_vintorez;
	class Makarov;
	class MakarovSD;
	class RH_pm: Makarov
	{
		displayName = "Makarov PM";
		model = "\RH_aks\RH_pm.p3d";
		picture = "\RH_aks\inv\pm.paa";
		begin1[] = {"\RH_aks\sound\pm.wss",0.794328,1,700};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.0001,1,20};
		reloadMagazineSound[] = {"\RH_aks\sound\pm_reload.wss",0.031623,1,20};
		magazines[] = {"8Rnd_9x18_Makarov"};
	};
	class RH_pmsd: MakarovSD
	{
		displayName = "Makarov PM SD";
		model = "\RH_aks\RH_pmsd.p3d";
		picture = "\RH_aks\inv\pmsd.paa";
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		begin1[] = {"\RH_aks\sound\pmsd.wss",0.316228,1,200};
		soundBegin[] = {"begin1",1};
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.0001,1,20};
		reloadMagazineSound[] = {"\RH_aks\sound\pm_reload.wss",0.031623,1,20};
		magazines[] = {"8Rnd_9x18_MakarovSD"};
		class ItemActions
		{
			class Use
			{
				text = "Remove Pistol Suppressor";
				script = "spawn player_removeSuppressorPistol;";
			};
		};
	};
	class RH_aps: Pistol
	{
		scope = 2;
		model = "\RH_aks\RH_aps.p3d";
		modelOptics = "-";
		picture = "\RH_aks\inv\aps.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		displayName = "Stechkin APS";
		descriptionShort = "The Stechkin automatic pistol is a Russian selective fire machine pistol.<br/>Magazine: Stechkin Mag.";
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_aks\sound\aps_reload.wss",0.1,1,20};
		magazines[] = {"RH_20Rnd_9x18_aps"/*,"RH_20Rnd_9x18_apssd"*/};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\aps.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.006;
			reloadTime = 0.10909091;
			recoil = "recoil_single_pistol_2outof3";
			recoilProne = "recoil_single_pistol_prone_2outof3";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 1.5;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.4;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\aps.wss",0.794328,1,700};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			multiplier = 1;
			burst = 1;
			dispersion = 0.007;
			reloadTime = 0.10909091;
			recoil = "recoil_single_pistol_2outof3";
			recoilProne = "recoil_single_pistol_prone_2outof3";
			ffCount = 3;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.001;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.7;
			midRange = 10;
			midRangeProbab = 0.7;
			maxRange = 20;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The APS is a simple blowback pistol, of al steel construction, with external hammer and a double action trigger. A three-position safety lever, located on the slide, also acts as fire mode selector, allowing for single-shot or full automatic fire. To slow the rate of fire down to controllable 600 rounds per minute, APS featured a plunger-type fire rate reducer, located in the grip. return spring is located around the barrel. Front sight is fixed to the slide, rear sight is adjustable for range, with settings for 25, 50 and optimistic 100 and 200 meters. To further improve the long range and full automatic mode accuracy, a shoulder stock / holster can be attached to the grip. early holsters / stocks were made from wood, latter from brow plastic.";
		};
	};
	class RH_apssd: Pistol
	{
		scope = 2;
		model = "\RH_aks\RH_apssd.p3d";
		modelOptics = "-";
		picture = "\RH_aks\inv\apssd.paa";
		distanceZoomMin = 56;
		distanceZoomMax = 56;
		optics = "true";
		displayName = "Stechkin APS SD";
		descriptionShort = "Stechkin APS suppressed version.<br/>Magazine: Stechkin APS SD";
		drySound[] = {"\ca\Weapons\Data\Sound\T33_dry_v1",0.01,1,20};
		reloadMagazineSound[] = {"\RH_aks\sound\aps_reload.wss",0.1,1,20};
		magazines[] = {"RH_20Rnd_9x18_apssd"/*,"RH_20Rnd_9x18_aps"*/};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\apssd.wss",0.316228,1,200};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			dispersion = 0.006;
			reloadTime = 0.10909091;
			recoil = "recoil_single_pistol_2outof3";
			recoilProne = "recoil_single_pistol_prone_2outof3";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 1.5;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 30;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.4;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\apssd.wss",0.316228,1,200};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			multiplier = 1;
			burst = 1;
			dispersion = 0.007;
			reloadTime = 0.10909091;
			recoil = "recoil_single_pistol_2outof3";
			recoilProne = "recoil_single_pistol_prone_2outof3";
			ffCount = 3;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.001;
			aiRateOfFireDistance = 50;
			minRange = 2;
			minRangeProbab = 0.7;
			midRange = 10;
			midRangeProbab = 0.7;
			maxRange = 20;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The APS is a simple blowback pistol, of al steel construction, with external hammer and a double action trigger. A three-position safety lever, located on the slide, also acts as fire mode selector, allowing for single-shot or full automatic fire. To slow the rate of fire down to controllable 600 rounds per minute, APS featured a plunger-type fire rate reducer, located in the grip. return spring is located around the barrel. Front sight is fixed to the slide, rear sight is adjustable for range, with settings for 25, 50 and optimistic 100 and 200 meters. To further improve the long range and full automatic mode accuracy, a shoulder stock / holster can be attached to the grip. early holsters / stocks were made from wood, latter from brow plastic.";
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Pistol Suppressor";
				script = "spawn player_removeSuppressorPistol;";
			};
		};
	};
	class RH_ak47: rifle
	{
		scope = 2;
		opticsZoomInit = 0.375;
		model = "\RH_aks\RH_ak47.p3d";
		picture = "\RH_aks\inv\ak47.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\Data\Anim\AK.rtm"};
		modelOptics = "-";
		optics = 1;
		dexterity = 1.7;
		displayName = "AK-47";
		distanceZoomMin = 300;
		distanceZoomMax = 300;
		reloadMagazineSound[] = {"\RH_aks\sound\Ak47_reload.wss",0.056234,1,20};
		magazines[] = {"30Rnd_762x39_AK47"};
		modes[] = {"Single","Fullauto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\ak47.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			multiplier = 1;
			burst = 1;
			dispersion = 0.0028;
			reloadTime = 0.092;
			recoil = "RH_AK47Recoil";
			recoilProne = "RH_AK47RecoilProne";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 4.5;
			aiRateOfFireDistance = 400;
			minRange = 2;
			minRangeProbab = 0.7;
			midRange = 250;
			midRangeProbab = 0.8;
			maxRange = 400;
			maxRangeProbab = 0.5;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\ak47.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			multiplier = 1;
			burst = 1;
			dispersion = 0.0028;
			reloadTime = 0.092;
			recoil = "RH_AK47Recoil";
			recoilProne = "RH_AK47RecoilProne";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDistance = 100;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 70;
			midRangeProbab = 0.8;
			maxRange = 100;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The AK-47 Avtomat Kalashnikova 1947 is a gas-operated assault rifle used in most Eastern bloc countries during the Cold War. Adopted and standardized in 1947, it was designed by Mikhail Kalashnikov and originally produced by Russian manufacturer Izhevsk Mechanical Works. Compared with most auto-loading rifles of World War II, the AK-47 is compact, of comparative range, moderate power, and capable of selective fire. It was one of the first, true assault rifles and remains the most widely-used, known as the Best Automatic Assault Rifle. More AK-type rifles have been produced than of any other assault rifle type. Once manufacturing difficulties had been overcome, a redesigned version designated the AKM M for modernized or upgraded�in was introduced in 1959. This new model used a stamped sheet metal receiver and featured a slanted muzzle brake on the end of the barrel to compensate for muzzle rise under recoil.";
		};
	};
	class RH_akm: RH_ak47
	{
		model = "\RH_aks\RH_akm.p3d";
		picture = "\RH_aks\inv\akm.paa";
		displayName = "AKM";
		descriptionShort = "Assault rifle designed by Mikhail Kalashnikov.<br/>Magazine: AKM Mag.";
		class Single: Single
		{
			reloadtime = 0.1;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
		};
		class FullAuto: FullAuto
		{
			reloadtime = 0.1;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
		};
	};
	/*class RH_aks47: RH_ak47
	{
		model = "\RH_aks\RH_aks47.p3d";
		picture = "\RH_aks\inv\aks47.paa";
		displayName = "AKs-47 w/o butt";
		class Single: Single
		{
			recoil = "RH_AKSRecoil";
			recoilProne = "RH_AKSRecoilProne";
		};
		class FullAuto: FullAuto
		{
			recoil = "RH_AKSRecoil";
			recoilProne = "RH_AKSRecoilProne";
		};
	};
	class RH_aks47b: RH_ak47
	{
		model = "\RH_aks\RH_aks47b.p3d";
		picture = "\RH_aks\inv\aks47b.paa";
		displayName = "AKs-47";
	};
	class RH_aks47g: RH_ak47
	{
		model = "\RH_aks\RH_aks47g.p3d";
		picture = "\RH_aks\inv\aks47g.paa";
		displayName = "AKs-47 Gold";
	};
	class RH_aks47s: RH_ak47
	{
		model = "\RH_aks\RH_aks47s.p3d";
		picture = "\RH_aks\inv\aks47s.paa";
		displayName = "AKs-47 Silver";
	};*/
	class RH_akms: RH_akm
	{
		model = "\RH_aks\RH_akms.p3d";
		picture = "\RH_aks\inv\akms.paa";
		displayName = "AKMS";
		descriptionShort = "7.62mm Kalashnikov AKMS modernized assault rifle with folding stock.<br/>Magazine: AKM Mag.";
	};
	class RH_akmsp: RH_akm
	{
		model = "\RH_aks\RH_akmsp.p3d";
		picture = "\RH_aks\inv\akmsp.paa";
		displayName = "AKM PSO";
		modelOptics="\ca\weapons\AK\pso_optics";
		opticsPPEffects[]={"OpticsCHAbera2","OpticsBlur3"};
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		opticsZoomInit=0.0623;
		opticsZoomMin=0.0623;
		opticsZoomMax=0.0623;
		distanceZoomMin=200;
		distanceZoomMax=200;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.04;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
		};
	};
	class RH_akmssd: RH_akm
	{
		model = "\RH_aks\RH_akmssd.p3d";
		fireLightDuration = 0;
		fireLightIntensity = 0;
		picture = "\RH_aks\inv\akmssd.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		displayName = "AKMS SD";
		descriptionShort = "AKMS suppressed assault rifle.<br/>Magazine: AKMSD Mag.";
		magazines[] = {"RH_30Rnd_762x39_SDmag"};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_aks\sound\akmsd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_aks\sound\akmsd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	/*class RH_ak47gl: RH_ak47
	{
		scope = 2;
		model = "\RH_aks\RH_ak47gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-47w/GP30";
		picture = "\RH_aks\inv\ak47gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\Ak47_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_762x39_AK47"};
		muzzles[] = {"RH_AK47Muzzle","RH_gp30Muzzle"};
		class RH_AK47Muzzle: RH_ak47{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};*/
	class RH_akmgl: RH_akm
	{
		scope = 2;
		model = "\RH_aks\RH_akmgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKMw/GP30";
		picture = "\RH_aks\inv\akmgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\Ak47_reload.wss",0.056234,1,20};
		magazines[] = {"30Rnd_762x39_AK47"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_AKMMuzzle","RH_gp30Muzzle"};
		class RH_AKMMuzzle: RH_akm{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_an94: rifle
	{
		scope = 2;
		opticsZoomInit = 0.375;
		model = "\RH_aks\RH_an94.p3d";
		picture = "\RH_aks\inv\an94.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\Data\Anim\AK.rtm"};
		modelOptics = "-";
		optics = 1;
		dexterity = 1.7;
		displayName = "AN-94";
		descriptionShort = "Modern Russian assault rifle.<br/>Magazine: AK Mag.";
		reloadMagazineSound[] = {"\RH_aks\sound\an94_reload.wss",0.056234,1,20};
		magazines[] = {"30Rnd_545x39_AK"};
		modes[] = {"Single","Burst","Fullauto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\an94.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			recoil = "RH_AN94Recoil";
			recoilProne = "RH_AN94RecoilProne";
			multiplier = 1;
			burst = 1;
			dispersion = 0.002;
			reloadTime = 0.087;
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 500;
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 250;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.2;
		};
		class Burst: Mode_Burst
		{
			begin1[] = {"\RH_aks\sound\an942.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			recoil = "RH_AN94BurstRecoil";
			recoilProne = "RH_AN94BurstRecoilProne";
			soundBurst = 1;
			multiplier = 1;
			burst = 2;
			dispersion = 0.002;
			reloadTime = 0.03;
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 2;
			aiRateOfFireDistance = 200;
			minRange = 1;
			minRangeProbab = 0.6;
			midRange = 150;
			midRangeProbab = 0.9;
			maxRange = 230;
			maxRangeProbab = 0.6;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\an94.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			multiplier = 1;
			burst = 1;
			dispersion = 0.002;
			reloadTime = 0.087;
			recoil = "RH_AN94Recoil";
			recoilProne = "RH_AN94RecoilProne";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 0.1;
			aiRateOfFireDistance = 100;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 80;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.3;
		};
		class Library
		{
			libTextDesc = "The AN-94 Abakan is a modern Russian assault rifle. The acronym stands for Avtomat Nikonova (after Gennadiy Nikonov) Model 1994, and it was chosen over many other competing firearms in an extensive trial, including the AEK-971 design by a team led by the son of AK-47 designer Mikhail Kalashnikov, Sergey Koksharov. The AN-94 was designed at the Izhmash state factories, as a replacement for the AK-74 assault rifle currently in use in the Russian military. Currently, the Russian military is retaining the AK-74M as its standard shoulder arm and the AN-94 is only being issued to the elite forces of the Russian military, some Russian police forces, and the MVD.";
		};
	};
	class RH_an94gl: RH_an94
	{
		scope = 2;
		model = "\RH_aks\RH_an94gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AN-94 w/GP30";
		picture = "\RH_aks\inv\an94gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\an94_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"};
		muzzles[] = {"RH_ANMuzzle","RH_gp30Muzzle"};
		class RH_ANMuzzle: RH_an94{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_SVD: rifle
	{
		scope = 2;
		model="\ca\weapons_E\SVD\SVD";
		dexterity = 1.57;
		displayName = "SVD PSO";
		descriptionShort = "Semi-automatic sniper rifle with built-in rangefinder.<br/>Magazine: SVD Dragunov Mag.";
		modelOptics="\ca\weapons\optika_snpiere";
		opticsPPEffects[]={"OpticsCHAbera3","OpticsBlur3"};
		picture="\CA\weapons\data\equip\w_SVD_ca.paa";
		UiPicture = "\CA\weapons\data\Ico\i_sniper_CA.paa";
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		opticsZoomInit=0.0623;
		opticsZoomMin=0.0623;
		opticsZoomMax=0.0623;
		distanceZoomMin=200;
		distanceZoomMax=200;
		begin1[] = {"\RH_aks\Sound\SVD.wss",1.778279,1,1200};
		soundBegin[] = {"begin1",1};
		reloadMagazineSound[] = {"\RH_aks\sound\svd_reload.wss",0.056234,1,20};
		dispersion=0.00025;
		recoil = "RH_SVDRecoil";
		recoilProne = "RH_SVDRecoilProne";
		minRange=0;
		minRangeProbab=0.1;
		midRange=400;
		midRangeProbab=0.7;
		maxRange=800;
		maxRangeProbab=0.05;
		reloadTime = 0.1;
		autoFire = 0;
		value = 1000;
		aiRateOfFire=9;
		aiRateOfFireDistance=1300;
		magazines[] = {"10Rnd_762x54_SVD"};
		htMin=1;
		htMax=420;
		afMax=0;
		mfMax=0;
		mFact=1;
		tBody=100;
		visionMode[]={"Normal"};
		class Library
		{
			libTextDesc = "SVD was designed not as a standart sniper rifle. In fact, main role of the SVD ir Soviet and Russian Army is to extend effective range of fire of every infantry squad up to 600 meters and to provide special fire support. SVD is a lightweight and quite accurate for it's class rifle, cabable of semi-auto fire. First request for new sniper rifle was issued in 1958. In 1963 SVD Snaiperskaya Vintovka Dragunova, or Dragunov Sniper Rifle was accepted by Soviet Military. SVD can use any kind of standart 7.62x54R ammo, but primary round is specially developed for SVD sniper-grade cartridge with steel-core bullet. ";
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
		};
	};
	class RH_SVDb: RH_SVD
	{
		model = "\RH_aks\RH_svdb.p3d";
		picture = "\RH_aks\inv\svdb.paa";
		displayName = "SVD Black";
		descriptionShort = "Semi-automatic sniper rifle with built-in rangefinder.<br/>Magazine: SVD Dragunov Mag.";
		class ItemActions {};
	};
	/*class RH_SVDg: RH_SVD
	{
		model = "\RH_aks\RH_svdg.p3d";
		picture = "\RH_aks\inv\svdg.paa";
		displayName = "SVD Gold";
	};
	class RH_SVDs: RH_SVD
	{
		model = "\RH_aks\RH_svds.p3d";
		picture = "\RH_aks\inv\svds.paa";
		displayName = "SVDS";
	};
	class RH_SVU: RH_SVD
	{
		model = "\RH_aks\RH_svu.p3d";
		picture = "\RH_aks\inv\svu.paa";
		displayName = "SVU";
		handAnim[] = {"OFP2_ManSkeleton","\CA\weapons\Data\Anim\Bizon.rtm"};
		begin1[] = {"\RH_aks\Sound\SVU.wss",1.778279,1,1200};
		soundBegin[] = {"begin1",1};
	};*/
	class RH_SVD_CAMO: RH_SVD
	{
		model = "\ca\weapons\SVD_CAMO";
		picture = "\CA\weapons\data\equip\W_SVD_camo_CA.paa";
		displayName = "SVD Ghillie";
		descriptionShort = "Semi-automatic sniper rifle with built-in rangefinder and equipped with a ghillie webbing.<br/>Magazine: SVD Dragunov Mag.";
		class ItemActions {};
	};
	class RH_aks74u: AKS_74_U
	{
		scope = 2;
		opticsZoomInit = 0.375;
		model = "\RH_aks\RH_aks74u.p3d";
		picture = "\RH_aks\inv\aks74u.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		displayName = "AKS-74U";
		descriptionShort = "Shortened version of AK-74, commonly used by law enforcement.<br/>Magazine: AK Mag.";
		reloadMagazineSound[] = {"\RH_aks\sound\aks74u_reload.wss",0.056234,1,20};
		modes[] = {"Single","FullAuto"};
		magazines[] = {"30Rnd_545x39_AK"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\Sound\aks74u.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.085;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0035;
			minRange = 1;
			minRangeProbab = 0.6;
			midRange = 200;
			midRangeProbab = 0.8;
			maxRange = 300;
			maxRangeProbab = 0.1;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\aks74u.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.085;
			ffCount = 30;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0035;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.5;
		};
		class Library
		{
			libTextDesc = "The AKS-74U is a assault rifle designed in the Soviet Union that was in production from the early 1980s until circa 1991. The AKS-74U Ukorochennyj shortened was developed in the late 1970s from the AKS-74, a shortened variant of the AK-74. The AKS-74U is the size of, and has the effective range of, a submachine gun. AKS-74U is capable of being fitted with a detachable PBS suppressor, as well as a suppressed 30 mm BS-1 Tishina model grenade launcher which fires HEDP grenades. The grenades from the BS-1 are launched by blank cartridges stored in a pistol grip box magazine.";
		};
	};
	class RH_aks74uk: RH_aks74u
	{
		model = "\RH_aks\RH_aks74uk.p3d";
		picture = "\RH_aks\inv\aks74uk.paa";
		displayName = "AKS-74U Kobra";
		descriptionShort = "Shortened version of AK-74, commonly used by law enforcement.<br/>Magazine: AK Mag.";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 222;
		distanceZoomMax = 222;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
		};		
	};
	class RH_aks74usd: RH_aks74u
	{
		scope = 2;
		model = "\RH_aks\RH_aks74usd.p3d";
		fireLightDuration = 0;
		fireLightIntensity = 0;
		distanceZoomMin = 100;
		distanceZoomMax = 100;
		picture = "\RH_aks\inv\aks74usd.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		displayName = "AKS-74U SD";
		descriptionShort = "Suppressed version of AKS-74U.<br/>Magazine: AKSD Mag.";
		reloadMagazineSound[] = {"\RH_aks\sound\aks74u_reload.wss",0.056234,1,20};
		modes[] = {"Single","FullAuto"};
		magazines[] = {"30Rnd_545x39_AKSD"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\Sound\aks74usd.wss",1.778279,1,50};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.085;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0035;
			minRange = 1;
			minRangeProbab = 0.6;
			midRange = 100;
			midRangeProbab = 0.8;
			maxRange = 200;
			maxRangeProbab = 0.5;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\aks74usd.wss",1.778279,1,50};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.085;
			ffCount = 30;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0035;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.5;
		};
		class Library
		{
			libTextDesc = "The AKS-74U is a assault rifle designed in the Soviet Union that was in production from the early 1980s until circa 1991. The AKS-74U Ukorochennyj shortened was developed in the late 1970s from the AKS-74, a shortened variant of the AK-74. The AKS-74U is the size of, and has the effective range of, a submachine gun. AKS-74U is capable of being fitted with a detachable PBS suppressor, as well as a suppressed 30 mm BS-1 Tishina model grenade launcher which fires HEDP grenades. The grenades from the BS-1 are launched by blank cartridges stored in a pistol grip box magazine.";
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};	
	};
	class RH_aks74usdk: RH_aks74usd
	{
		model = "\RH_aks\RH_aks74usdk.p3d";
		picture = "\RH_aks\inv\aks74usdk.paa";
		displayName = "AKS-74U Kobra SD";
		descriptionShort = "Suppressed version of AKS-74U Kobra.<br/>Magazine: AKSD Mag.";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 120;
		distanceZoomMax = 120;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	/*class RH_aks74upt: RH_aks74usd
	{
		scope = 2;
		model = "\RH_aks\RH_aks74upt.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKs-74u sd w/Bs-1";
		picture = "\RH_aks\inv\aks74upt.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\aks74u_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AKSD","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AK"};
		muzzles[] = {"RH_aks74uMuzzle","RH_bs1Muzzle"};
		class RH_aks74uMuzzle: RH_aks74usd{};
		class RH_bs1Muzzle: GrenadeLauncher
		{
			displayName = "BS-1 Tishina Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\BS-1.wss",0.2,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.000316,1,25};
			magazines[] = {"RH_1Rnd_30mm"};
		};
	};
	class RH_aks74uptk: RH_aks74usd
	{
		scope = 2;
		model = "\RH_aks\RH_aks74uptk.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKs-74u sd Kobra w/Bs-1";
		picture = "\RH_aks\inv\aks74uptk.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\aks74u_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AKSD","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AK"};
		muzzles[] = {"RH_aks74uMuzzle","RH_bs1Muzzle"};
		class RH_aks74uMuzzle: RH_aks74usdk{};
		class RH_bs1Muzzle: GrenadeLauncher
		{
			displayName = "BS-1 Tishina Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\BS-1.wss",0.2,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.000316,1,25};
			magazines[] = {"RH_1Rnd_30mm"};
		};
	};
	class RH_aks74uptsp: RH_aks74usd
	{
		scope = 2;
		model = "\RH_aks\RH_aks74uptsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKs-74u PSO sd w/Bs-1";
		picture = "\RH_aks\inv\aks74uptsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\aks74u_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AKSD","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AK"};
		muzzles[] = {"RH_aks74uMuzzle","RH_bs1Muzzle"};
		class RH_aks74uMuzzle: RH_aks74usd
		{
			modelOptics = "\RH_aks\NWD_PSO_1_1_AK74";
			opticsZoomMin = 0.071945;
			opticsZoomMax = 0.071945;
			opticsFlare = "true";
			opticsDisablePeripherialVision = "true";
			distanceZoomMin = 228;
			distanceZoomMax = 228;
			class Single: Single
			{
				minRange = 2;
				minRangeProbab = 0.5;
				midRange = 200;
				midRangeProbab = 0.7;
				maxRange = 300;
				maxRangeProbab = 0.1;
			};
		};
		class RH_bs1Muzzle: GrenadeLauncher
		{
			displayName = "BS-1 Tishina Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\BS-1.wss",0.2,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.000316,1,25};
			magazines[] = {"RH_1Rnd_30mm"};
		};
	};*/
	class RH_rk95: rifle
	{
		scope = 2;
		opticsZoomInit = 0.375;
		model = "\RH_aks\RH_rk95.p3d";
		picture = "\RH_aks\inv\rk95.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\Data\Anim\AK.rtm"};
		modelOptics = "-";
		optics = 1;
		dexterity = 1.6;
		displayName = "RK-95";
		descriptionShort = "7.62x39mm Finnish assault rifle.<br/>Magazine: AKM Mag.";
		distanceZoomMin = 400;
		distanceZoomMax = 400;
		reloadMagazineSound[] = {"\RH_aks\sound\rk95_reload.wss",0.056234,1,20};
		magazines[] = {"30Rnd_762x39_AK47"};
		modes[] = {"Single","Fullauto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\rk95.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			multiplier = 1;
			burst = 1;
			dispersion = 0.0025;
			reloadTime = 0.092;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 0;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 5.5;
			aiRateOfFireDistance = 400;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 250;
			midRangeProbab = 0.8;
			maxRange = 400;
			maxRangeProbab = 0.2;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\rk95.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			multiplier = 1;
			burst = 1;
			dispersion = 0.0025;
			reloadTime = 0.092;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			ffCount = 1;
			ffMagnitude = 0.5;
			ffFrequency = 11;
			autofire = 2;
			flash = "gunfire";
			flashSize = 0.1;
			useAction = 0;
			useActionTitle = "";
			showToPlayer = 1;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 100;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 70;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.2;
		};
		class Library
		{
			libTextDesc = "The 7.62 RK 95 TP (Rynn�kk�kiv��ri 95 Taittoper�, Assault rifle 95, Folding stock) is an assault rifle that was produced by the Sako company based in Finland. A relatively small quantity was purchased by the Finnish Defence Forces. The weapon is a further development of the Rk 62 which was based on the Soviet AK-47 assault rifle. It fires the same 7.62 x 39 mm ammunition as the AK-47, but a few samples were made in 5.56 x 45 mm NATO. It is considered to be the best AK-47 variant in the world by many experts, due to the superior quality of materials and workmanship. A civilian semi-automatic version with a fixed buttstock was manufactured under the name Sako M92S.";
		};
	};
	class RH_Rk95sd: RH_rk95
	{
		fireLightDuration = 0;
		fireLightIntensity = 0;
		displayName = "RK-95 SD";
		descriptionShort = "RK-95 suppressed assault rifle.<br/>Magazine: AKMSD Mag.";
		model = "\RH_aks\RH_rk95sd.p3d";
		picture = "\RH_aks\inv\rk95sd.paa";
		opticsDisablePeripherialVision = 1;
		magazines[] = {"RH_30Rnd_762x39_SDmag"};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_aks\sound\rk95_sd.wss",1.778279,1,50};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_aks\sound\rk95_sd.wss",1.778279,1,50};
			soundBegin[] = {"begin1",1};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_Rk95sdaim: RH_Rk95sd
	{
		displayName = "RK-95 SD Aimpoint";
		model = "\RH_aks\RH_rk95sdaim.p3d";
		picture = "\RH_aks\inv\rk95sdaim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 110;
		distanceZoomMax = 110;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	/*class RH_Rk95sdag: RH_Rk95sd
	{
		displayName = "RK-95 SD Acog";
		model = "\RH_aks\RH_rk95sdag.p3d";
		picture = "\RH_aks\inv\rk95sdag.paa";
		modelOptics = "\RH_aks\fnc_acog_ta11fin";
		opticsZoomMin = 0.08222;
		opticsZoomMax = 0.08222;
		distanceZoomMin = 164;
		distanceZoomMax = 164;
		opticsPPEffects[] = {"OpticsCHAbera2","OpticsBlur2"};
		opticsFlare = 1;
		opticsDisablePeripherialVision = 1;
	};*/
	class RH_Rk95aim: RH_rk95
	{
		displayName = "RK-95 Aimpoint";
		model = "\RH_aks\RH_rk95aim.p3d";
		picture = "\RH_aks\inv\rk95aim.paa";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		class ItemActions
		{
			class Use
			{
				text = "Remove Aimpoint Sight";
				script = "spawn player_removeCCO;";
			};
		};
	};
	/*class RH_Rk95ag: RH_rk95
	{
		displayName = "RK-95 Acog";
		model = "\RH_aks\RH_rk95ag.p3d";
		picture = "\RH_aks\inv\rk95ag.paa";
		modelOptics = "\RH_aks\fnc_acog_ta11fin";
		opticsZoomMin = 0.08222;
		opticsZoomMax = 0.08222;
		distanceZoomMin = 164;
		distanceZoomMax = 164;
		opticsPPEffects[] = {"OpticsCHAbera2","OpticsBlur2"};
		opticsFlare = 1;
		opticsDisablePeripherialVision = 1;
	};*/
	class RH_bizon: RH_aks74u
	{
		scope = 2;
		model = "\rh_aks\RH_bizon.p3d";
		picture = "\rh_aks\inv\bizon.paa";
		magazines[] = {"64Rnd_9x19_Bizon"};
		displayName = "Bizon";
		descriptionShort = "9mm submachine gun developed in the early 1990s.<br/>Magazine: Bizon Mag.";
		drySound[] = {"\ca\Weapons\Data\sound\M16_cock_v1",0.000316,1};
		reloadMagazineSound[] = {"\rh_aks\sound\bizon_Reload.wss",0.056234,1,20};
		fireLightDuration = 0.05;
		fireLightIntensity = 0.012;
		distanceZoomMin = 107;
		distanceZoomMax = 107;
		value = 10;
		handAnim[] = {"OFP2_ManSkeleton","\CA\weapons\Data\Anim\Bizon.rtm"};
		modes[] = {"Single","FullAuto"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\bizon.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadtime = 0.071;
			recoil = "RH_BizonRecoil";
			recoilProne = "RH_BizonRecoil";
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 40;
			midRangeProbab = 0.7;
			maxRange = 150;
			maxRangeProbab = 0.2;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\bizon.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			soundContinuous = 0;
			reloadtime = 0.071;
			ffCount = 1;
			recoil = "RH_BizonRecoil";
			recoilProne = "RH_BizonRecoil";
			aiRateOfFire = 0.001;
			dispersion = 0.004;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 50;
			midRangeProbab = 0.7;
			maxRange = 80;
			maxRangeProbab = 0.2;
		};
	};
	class RH_bizonk: RH_bizon
	{
		model = "\RH_aks\RH_bizonk.p3d";
		picture = "\RH_aks\inv\bizonk.paa";
		displayName = "Bizon Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 120;
		distanceZoomMax = 120;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
		};
	};
	class RH_bizonsd: RH_bizon
	{
		displayName = "BizonSD";
		descriptionShort = "Bizon suppressed submachine gun.<br/>Magazine: BizonSD Mag.";
		model = "\rh_aks\RH_bizonsd.p3d";
		picture = "\rh_aks\inv\bizonsd.paa";
		magazines[] = {"64Rnd_9x19_SD_Bizon"};
		fireLightDuration = 0;
		fireLightIntensity = 0;
		class Single: Single
		{
			begin1[] = {"\RH_aks\sound\bizon_sd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_aks\sound\bizon_sd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_bizonsdk: RH_bizonsd
	{
		model = "\RH_aks\RH_bizonsdk.p3d";
		picture = "\RH_aks\inv\bizonsdk.paa";
		displayName = "Bizon SD Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 120;
		distanceZoomMax = 120;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
			class Use2
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	class RH_ak74: AK_74
	{
		scope = 2;
		opticsZoomInit = 0.375;
		model = "\RH_aks\RH_ak74.p3d";
		picture = "\RH_aks\inv\ak74.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		displayName = "AK-74";
		descriptionShort = "Assault rifle developed in the Soviet Union by Mikhail Kalashnikov.<br/>Magazine: AK Mag.";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss",0.056234,1,20};
		modes[] = {"Single","FullAuto"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\Sound\ak74.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.1;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 250;
			midRangeProbab = 0.9;
			maxRange = 400;
			maxRangeProbab = 0.1;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\ak74.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.1;
			ffCount = 30;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.4;
		};
		class Library
		{
			libTextDesc = "The AK-74 is a 5.45 mm assault rifle developed in the early 1970s in the Soviet Union by Mikhail Kalashnikov. It was the first Soviet rifle to be chambered in an intermediate rifle caliber. It was introduced into service in 1974 (used to equip, among others, Soviet forces engaged in the Afghanistan conflict). The weapon�s name is an abbreviation for Avtomat Kalashnikova model 1974.";
		};
	};
	class RH_ak74m: RH_ak74
	{
		model = "\RH_aks\RH_ak74m.p3d";
		picture = "\RH_aks\inv\ak74m.paa";
		displayName = "AK-74M";
	};
	/*class RH_ak74mgl: RH_ak74m
	{
		scope = 2;
		model = "\RH_aks\RH_ak74mgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-74Mw/GP30";
		picture = "\RH_aks\inv\ak74mgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		class RH_ak74mMuzzle: RH_ak74m{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak74mk: RH_ak74m
	{
		model = "\RH_aks\RH_ak74mk.p3d";
		picture = "\RH_aks\inv\ak74mk.paa";
		displayName = "AK-74M Kobra";
		opticsDisablePeripherialVision = 0;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
	};
	class RH_ak74mkgl: RH_ak74mk
	{
		scope = 2;
		model = "\RH_aks\RH_ak74mkgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-74Mw/GP30 Kobra";
		picture = "\RH_aks\inv\ak74mkgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_ak74mk{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak74msp: RH_ak74m
	{
		model = "\RH_aks\RH_ak74msp.p3d";
		picture = "\RH_aks\inv\ak74msp.paa";
		displayName = "AK-74M Sniper";
		modelOptics = "\RH_aks\NWD_PSO_1_1_ak74";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_ak74mglsp: RH_ak74m
	{
		scope = 2;
		model = "\RH_aks\RH_ak74mglsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-74Mw/GP30 Sniper";
		picture = "\RH_aks\inv\ak74mglsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_ak74msp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak74m1p29: RH_ak74m
	{
		model = "\RH_aks\RH_ak74m1p29.p3d";
		picture = "\RH_aks\inv\ak74m1p29.paa";
		displayName = "AK-74M 1p29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_ak74mgl1p29: RH_ak74m
	{
		scope = 2;
		model = "\RH_aks\RH_ak74mgl1p29.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-74Mw/GP30 1p29";
		picture = "\RH_aks\inv\ak74mgl1p29.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_ak74m1p29{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74m: RH_ak74m
	{
		model = "\RH_aks\RH_aks74m.p3d";
		picture = "\RH_aks\inv\aks74m.paa";
		displayName = "AKS-74M";
	};
	class RH_aks74mgl: RH_aks74m
	{
		scope = 2;
		model = "\RH_aks\RH_aks74mgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Mw/GP30";
		picture = "\RH_aks\inv\aks74mgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_aks74mMuzzle","RH_gp30Muzzle"};
		class RH_aks74mMuzzle: RH_aks74m{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74mk: RH_aks74m
	{
		model = "\RH_aks\RH_aks74mk.p3d";
		picture = "\RH_aks\inv\aks74mk.paa";
		displayName = "AKS-74M Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
	};
	class RH_aks74mkgl: RH_aks74mk
	{
		scope = 2;
		model = "\RH_aks\RH_aks74mkgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Mw/GP30 Kobra";
		picture = "\RH_aks\inv\aks74mkgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_aks74mk{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74msp: RH_aks74m
	{
		model = "\RH_aks\RH_aks74msp.p3d";
		picture = "\RH_aks\inv\aks74msp.paa";
		displayName = "AKS-74M Sniper";
		modelOptics = "\RH_aks\NWD_PSO_1_1_ak74";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_aks74mglsp: RH_aks74m
	{
		scope = 2;
		model = "\RH_aks\RH_aks74mglsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Mw/GP30 Sniper";
		picture = "\RH_aks\inv\aks74mglsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_aks74msp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74m1p29: RH_aks74m
	{
		model = "\RH_aks\RH_aks74m1p29.p3d";
		picture = "\RH_aks\inv\aks74m1p29.paa";
		displayName = "AKS-74M 1p29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_aks74mgl1p29: RH_aks74m
	{
		scope = 2;
		model = "\RH_aks\RH_aks74mgl1p29.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Mw/GP30 1p29";
		picture = "\RH_aks\inv\aks74mgl1p29.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\aks74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_aks74mMuzzle","RH_gp30Muzzle"};
		class RH_aks74mMuzzle: RH_aks74m1p29{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};*/
	class RH_ak74gl: RH_ak74
	{
		scope = 2;
		model = "\RH_aks\RH_ak74gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-74w/GP30";
		picture = "\RH_aks\inv\ak74gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\Ak74_reload.wss",0.056234,1,20};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_AK74Muzzle","RH_gp30Muzzle"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		class RH_AK74Muzzle: RH_ak74{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_ak74k: RH_ak74
	{
		model = "\RH_aks\RH_ak74k.p3d";
		picture = "\RH_aks\inv\ak74k.paa";
		displayName = "AK-74 Kobra";
		opticsDisablePeripherialVision = 0;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
		};
	};
	class RH_ak74kgl: RH_ak74k
	{
		scope = 2;
		model = "\RH_aks\RH_ak74kgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-74w/GP30 Kobra";
		picture = "\RH_aks\inv\ak74kgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\Ak74_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_AK74Muzzle","RH_gp30Muzzle"};
		class RH_AK74Muzzle: RH_ak74k{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_ak74sp: RH_ak74
	{
		model = "\RH_aks\RH_ak74sp.p3d";
		picture = "\RH_aks\inv\ak74sp.paa";
		displayName = "AK-74 PSO";
		modelOptics="\ca\weapons\AK\pso_optics";
		opticsPPEffects[]={"OpticsCHAbera2","OpticsBlur3"};
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		opticsZoomInit=0.0623;
		opticsZoomMin=0.0623;
		opticsZoomMax=0.0623;
		distanceZoomMin=200;
		distanceZoomMax=200;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.04;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
		};
	};
	class RH_aks74: RH_ak74
	{
		model = "\RH_aks\RH_aks74.p3d";
		picture = "\RH_aks\inv\aks74.paa";
		displayName = "AKS-74";
		descriptionShort = "Variant of the AK-74 equipped with a side-folding metal shoulder stock.<br/>Magazine: AK Mag.";
	};
	class RH_aks74gl: RH_aks74
	{
		scope = 2;
		model = "\RH_aks\RH_aks74gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74w/GP30";
		picture = "\RH_aks\inv\aks74gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\Ak74_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_AKS74Muzzle","RH_gp30Muzzle"};
		class RH_AKS74Muzzle: RH_aks74{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_aks74k: RH_aks74
	{
		model = "\RH_aks\RH_aks74k.p3d";
		picture = "\RH_aks\inv\aks74k.paa";
		displayName = "AKS-74 Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
		};
	};
	class RH_aks74kgl: RH_aks74k
	{
		scope = 2;
		model = "\RH_aks\RH_aks74kgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74w/GP30 Kobra";
		picture = "\RH_aks\inv\aks74kgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\Ak74_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_AK74Muzzle","RH_gp30Muzzle"};
		class RH_AK74Muzzle: RH_aks74k{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_aks74sp: RH_aks74
	{
		model = "\RH_aks\RH_aks74sp.p3d";
		picture = "\RH_aks\inv\aks74sp.paa";
		displayName = "AKS-74 PSO";
		modelOptics="\ca\weapons\AK\pso_optics";
		opticsPPEffects[]={"OpticsCHAbera2","OpticsBlur3"};
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		opticsZoomInit=0.0623;
		opticsZoomMin=0.0623;
		opticsZoomMax=0.0623;
		distanceZoomMin=200;
		distanceZoomMax=200;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.04;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
		};
	};
	class RH_oc14: RH_ak74
	{
		model = "\RH_aks\RH_oc14.p3d";
		picture = "\RH_aks\inv\oc14.paa";
		displayName = "Groza-9";
		reloadMagazineSound[] = {"\RH_aks\sound\oc14_reload.wss",0.056234,1,20};
		modes[] = {"Single","FullAuto"};
		magazines[] = {"RH_20Rnd_9x39_SP6_mag","RH_20Rnd_9x39_SP6SD_mag"};
		distanceZoomMin = 110;
		distanceZoomMax = 110;
		handAnim[] = {"OFP2_ManSkeleton","\RH_aks\RH_oc14.rtm"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\oc14.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.085;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 1;
			minRangeProbab = 0.6;
			midRange = 200;
			midRangeProbab = 0.8;
			maxRange = 300;
			maxRangeProbab = 0.1;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\oc14.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.08;
			ffCount = 30;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.8;
			maxRange = 100;
			maxRangeProbab = 0.3;
		};
		class Library
		{
			libTextDesc = "The OC-14 Groza ( Thunderstorm in Slavic languages), available in either 9 mm or 7.62mm bullpup combined assault rifle and grenade launcher designed by CKIB SOO (Central Design Bureau of Sporting and Hunting Weapons) in Tula, Russia, and manufactured by TOZ (Tula Weapons Plant, Russia). It is nearly identical to AKS-74U in function and can be modified with a suppressor, telescopic sight and or night vision device. The 9 mm uses the family of cartridges developed for suppressed fire during special operations - the subsonic SP-5, SP-6 and PAB-9 rounds. In the rifle grenade launcher version, OC-14 is operated with a single trigger, with a rifle\launcher switch located near the trigger guard. A 7.62x39mm version named Groza-1 was also developed for the Spetsnaz. This version accepts AK-47 standard magazines and may be equipped with a standard issue AK-74 bayonet. The 9 mm version supports 20-round magazines, and the 7.62x39mm allows using AK-type 30-round magazines.";
		};
	};
	/*class RH_oc14sp: RH_oc14
	{
		model = "\RH_aks\RH_oc14sp.p3d";
		picture = "\RH_aks\inv\oc14sp.paa";
		displayName = "Groza-9 Sniper";
		modelOptics = "\RH_aks\NWD_GScope_1";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 200;
			midRangeProbab = 0.8;
			maxRange = 300;
			maxRangeProbab = 0.3;
		};
	};
	class RH_oc14gl: RH_oc14
	{
		scope = 2;
		model = "\RH_aks\RH_oc14gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "Groza-9 w/GP30";
		picture = "\RH_aks\inv\oc14gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\oc14_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\CA\weapons\Data\Anim\Bizon.rtm"};
		magazines[] = {"RH_20Rnd_9x39_SP6_mag","RH_20Rnd_9x39_SP6SD_mag"};
		muzzles[] = {"RH_Oc14Muzzle","RH_gp30Muzzle"};
		class RH_Oc14Muzzle: RH_oc14{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_oc14glsp: RH_oc14
	{
		scope = 2;
		model = "\RH_aks\RH_oc14glsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "Groza-9 Sniper w/GP30";
		picture = "\RH_aks\inv\oc14glsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\oc14_reload.wss",0.056234,1,20};
		magazines[] = {"RH_20Rnd_9x39_SP6_mag","RH_20Rnd_9x39_SP6SD_mag"};
		muzzles[] = {"RH_Oc14Muzzle","RH_gp30Muzzle"};
		class RH_Oc14Muzzle: RH_oc14sp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_oc14sd: RH_oc14
	{
		scope = 2;
		model = "\RH_aks\RH_oc14sd.p3d";
		fireLightDuration = 0;
		fireLightIntensity = 0;
		distanceZoomMin = 110;
		distanceZoomMax = 110;
		picture = "\RH_aks\inv\oc14sd.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		displayName = "Groza-9 SD";
		reloadMagazineSound[] = {"\RH_aks\sound\oc14_reload.wss",0.056234,1,20};
		magazines[] = {"RH_20Rnd_9x39_SP6SD_mag","RH_20Rnd_9x39_SP6_mag"};
		handAnim[] = {"OFP2_ManSkeleton","\CA\weapons\Data\Anim\Bizon.rtm"};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_aks\sound\oc14sd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_aks\sound\oc14sd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
		};
	};
	class RH_oc14sdsp: RH_oc14sd
	{
		model = "\RH_aks\RH_oc14sdsp.p3d";
		picture = "\RH_aks\inv\oc14sdsp.paa";
		displayName = "Groza-9 SD Sniper";
		modelOptics = "\RH_aks\NWD_GScope_1";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 200;
			midRangeProbab = 0.8;
			maxRange = 300;
			maxRangeProbab = 0.3;
		};
	};*/
	class RH_gr1: RH_oc14
	{
		model = "\RH_aks\RH_gr1.p3d";
		picture = "\RH_aks\inv\gr1.paa";
		displayName = "Groza-7.62";
		descriptionShort = "Russian selective fire bullpup assault rifle.<br/>Magazine: AKM Mag.";
		reloadMagazineSound[] = {"\RH_aks\sound\oc14_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\CA\weapons\Data\Anim\Bizon.rtm"};
		modes[] = {"Single","FullAuto"};
		magazines[] = {"30Rnd_762x39_AK47"};
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\Sound\gr1.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.08;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			dispersion = 0.0027;
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 250;
			midRangeProbab = 0.8;
			maxRange = 400;
			maxRangeProbab = 0.1;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\gr1.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.08;
			ffCount = 30;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			dispersion = 0.0027;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.8;
			maxRange = 100;
			maxRangeProbab = 0.5;
		};
		class Library
		{
			libTextDesc = "The OC-14 Groza ( Thunderstorm in Slavic languages), available in either 9 mm or 7.62mm bullpup combined assault rifle and grenade launcher designed by CKIB SOO (Central Design Bureau of Sporting and Hunting Weapons) in Tula, Russia, and manufactured by TOZ (Tula Weapons Plant, Russia). It is nearly identical to AKS-74U in function and can be modified with a suppressor, telescopic sight and or night vision device. The 9 mm uses the family of cartridges developed for suppressed fire during special operations - the subsonic SP-5, SP-6 and PAB-9 rounds. In the rifle grenade launcher version, OC-14 is operated with a single trigger, with a rifle\launcher switch located near the trigger guard. A 7.62x39mm version named Groza-1 was also developed for the Spetsnaz. This version accepts AK-47 standard magazines and may be equipped with a standard issue AK-74 bayonet. The 9 mm version supports 20-round magazines, and the 7.62x39mm allows using AK-type 30-round magazines.";
		};
	};
	/*class RH_gr1sp: RH_gr1
	{
		model = "\RH_aks\RH_gr1sp.p3d";
		picture = "\RH_aks\inv\gr1sp.paa";
		displayName = "Groza-7.62 Sniper";
		modelOptics = "\RH_aks\NWD_GScope_1";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 228;
		distanceZoomMax = 228;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 350;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};*/
	class RH_gr1sd: RH_gr1
	{
		scope = 2;
		model = "\RH_aks\RH_gr1sd.p3d";
		fireLightDuration = 0;
		fireLightIntensity = 0;
		distanceZoomMin = 110;
		distanceZoomMax = 110;
		picture = "\RH_aks\inv\gr1sd.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		displayName = "Groza-7.62 SD";
		descriptionShort = "Groza-7.62 suppressed assault rifle.<br/>Magazine: AKMSD Mag.";
		reloadMagazineSound[] = {"\RH_aks\sound\oc14_reload.wss",0.056234,1,20};
		modes[] = {"Single","FullAuto"};
		magazines[] = {"RH_30Rnd_762x39_SDmag"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\sound\gr1sd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.08;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			dispersion = 0.0027;
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 150;
			midRangeProbab = 0.8;
			maxRange = 200;
			maxRangeProbab = 0.1;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\sound\gr1sd.wss",0.562341,1,50};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.08;
			ffCount = 30;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			dispersion = 0.0027;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.8;
			maxRange = 100;
			maxRangeProbab = 0.5;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Rifle Suppressor";
				script = "spawn player_removeSuppressorRifle;";
			};
		};
	};
	/*class RH_gr1sdsp: RH_gr1sd
	{
		model = "\RH_aks\RH_gr1sdsp.p3d";
		picture = "\RH_aks\inv\gr1sdsp.paa";
		displayName = "Groza-7.62 SD Sniper";
		modelOptics = "\RH_aks\NWD_GScope_1";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
	};
	class RH_ak103: RH_ak74
	{
		model = "\RH_aks\RH_ak103.p3d";
		picture = "\RH_aks\inv\ak103.paa";
		displayName = "AK-103";
		magazines[] = {"30Rnd_762x39_AK47","RH_30Rnd_762x39_SDmag"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\Sound\ak103.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.1;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 250;
			midRangeProbab = 0.9;
			maxRange = 400;
			maxRangeProbab = 0.1;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\ak103.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.1;
			ffCount = 30;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.4;
		};
	};
	class RH_ak103gl: RH_ak103
	{
		scope = 2;
		model = "\RH_aks\RH_ak103gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-103w/GP30";
		picture = "\RH_aks\inv\ak103gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak103_reload.wss",0.056234,1,20};
		muzzles[] = {"RH_ak103Muzzle","RH_gp30Muzzle"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		class RH_ak103Muzzle: RH_ak103{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak103k: RH_ak103
	{
		model = "\RH_aks\RH_ak103k.p3d";
		picture = "\RH_aks\inv\ak103k.paa";
		displayName = "AK-103 Kobra";
		opticsDisablePeripherialVision = 0;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
	};
	class RH_ak103kgl: RH_ak103k
	{
		scope = 2;
		model = "\RH_aks\RH_ak103kgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-103w/GP30 Kobra";
		picture = "\RH_aks\inv\ak103kgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak103_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_ak103Muzzle","RH_gp30Muzzle"};
		class RH_ak103Muzzle: RH_ak103k{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak103sp: RH_ak103
	{
		model = "\RH_aks\RH_ak103sp.p3d";
		picture = "\RH_aks\inv\ak103sp.paa";
		displayName = "AK-103 Sniper";
		modelOptics = "\RH_aks\NWD_PSO_1_1_ak74";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_ak103glsp: RH_ak103
	{
		scope = 2;
		model = "\RH_aks\RH_ak103glsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-103w/GP30 Sniper";
		picture = "\RH_aks\inv\ak103glsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_ak103sp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak103_1p29: RH_ak103
	{
		model = "\RH_aks\RH_ak103_1p29.p3d";
		picture = "\RH_aks\inv\ak103_1p29.paa";
		displayName = "AK-103 1p29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_ak103gl1p29: RH_ak103
	{
		scope = 2;
		model = "\RH_aks\RH_ak103gl1p29.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-103w/GP30 1p29";
		picture = "\RH_aks\inv\ak103gl1p29.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak103_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_ak103Muzzle","RH_gp30Muzzle"};
		class RH_ak103Muzzle: RH_ak103_1p29{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak104: RH_ak74
	{
		model = "\RH_aks\RH_ak104.p3d";
		picture = "\RH_aks\inv\ak104.paa";
		displayName = "AK-104";
		magazines[] = {"30Rnd_762x39_AK47","RH_30Rnd_762x39_SDmag"};
		class Single: Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\Sound\ak103.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.1;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 2;
			minRangeProbab = 0.5;
			midRange = 250;
			midRangeProbab = 0.9;
			maxRange = 400;
			maxRangeProbab = 0.1;
		};
		class FullAuto: Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\ak103.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.1;
			ffCount = 30;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			minRange = 0.1;
			minRangeProbab = 0.9;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.4;
		};
	};
	class RH_ak104gl: RH_ak104
	{
		scope = 2;
		model = "\RH_aks\RH_ak104gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-104w/GP30";
		picture = "\RH_aks\inv\ak104gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		muzzles[] = {"RH_ak104Muzzle","RH_gp30Muzzle"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		class RH_ak104Muzzle: RH_ak104{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak104k: RH_ak104
	{
		model = "\RH_aks\RH_ak104k.p3d";
		picture = "\RH_aks\inv\ak104k.paa";
		displayName = "AK-104 Kobra";
		opticsDisablePeripherialVision = 0;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
	};
	class RH_ak104kgl: RH_ak104k
	{
		scope = 2;
		model = "\RH_aks\RH_ak104kgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-104w/GP30 Kobra";
		picture = "\RH_aks\inv\ak104kgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_ak104Muzzle","RH_gp30Muzzle"};
		class RH_ak104Muzzle: RH_ak104k{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak104sp: RH_ak104
	{
		model = "\RH_aks\RH_ak104sp.p3d";
		picture = "\RH_aks\inv\ak104sp.paa";
		displayName = "AK-104 Sniper";
		modelOptics = "\RH_aks\NWD_PSO_1_1_ak74";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_ak104glsp: RH_ak104
	{
		scope = 2;
		model = "\RH_aks\RH_ak104glsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-104w/GP30 Sniper";
		picture = "\RH_aks\inv\ak104glsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_ak104sp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_ak104_1p29: RH_ak104
	{
		model = "\RH_aks\RH_ak104_1p29.p3d";
		picture = "\RH_aks\inv\ak104_1p29.paa";
		displayName = "AK-104 1p29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_ak104gl1p29: RH_ak104
	{
		scope = 2;
		model = "\RH_aks\RH_ak104gl1p29.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-104w/GP30 1p29";
		picture = "\RH_aks\inv\ak104gl1p29.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		muzzles[] = {"RH_ak104Muzzle","RH_gp30Muzzle"};
		class RH_ak104Muzzle: RH_ak104_1p29{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74p: RH_aks74
	{
		model = "\RH_aks\RH_aks74p.p3d";
		picture = "\RH_aks\inv\aks74p.paa";
		displayName = "AKS-74P";
	};
	class RH_aks74pgl: RH_aks74p
	{
		scope = 2;
		model = "\RH_aks\RH_aks74pgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Pw/GP30";
		picture = "\RH_aks\inv\aks74pgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74pMuzzle","RH_gp30Muzzle"};
		class RH_ak74pMuzzle: RH_aks74p{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74pk: RH_aks74p
	{
		model = "\RH_aks\RH_aks74pk.p3d";
		picture = "\RH_aks\inv\aks74pk.paa";
		displayName = "AKS-74P Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
	};
	class RH_aks74pkgl: RH_aks74pk
	{
		scope = 2;
		model = "\RH_aks\RH_aks74pkgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Pw/GP30 Kobra";
		picture = "\RH_aks\inv\aks74pkgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak104Muzzle","RH_gp30Muzzle"};
		class RH_ak104Muzzle: RH_aks74pk{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74psp: RH_aks74p
	{
		model = "\RH_aks\RH_aks74psp.p3d";
		picture = "\RH_aks\inv\aks74psp.paa";
		displayName = "AKS-74P Sniper";
		modelOptics = "\RH_aks\NWD_PSO_1_1_ak74";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		distanceZoomMin = 182;
		distanceZoomMax = 182;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_aks74pglsp: RH_aks74p
	{
		scope = 2;
		model = "\RH_aks\RH_aks74pglsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Pw/GP30 Sniper";
		picture = "\RH_aks\inv\aks74pglsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_aks74psp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};
	class RH_aks74p1p29: RH_aks74p
	{
		model = "\RH_aks\RH_aks74p1p29.p3d";
		picture = "\RH_aks\inv\aks74p1p29.paa";
		displayName = "AKS-74P 1p29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
	class RH_aks74pgl1p29: RH_aks74p
	{
		scope = 2;
		model = "\RH_aks\RH_aks74pgl1p29.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AKS-74Pw/GP30 1p29";
		picture = "\RH_aks\inv\aks74pgl1p29.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\aks74p_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK","RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"};
		muzzles[] = {"RH_aks74pMuzzle","RH_gp30Muzzle"};
		class RH_aks74pMuzzle: RH_aks74p1p29{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
	};*/
	class RH_ak105: RH_aks74
	{
		model = "\RH_aks\RH_ak105.p3d";
		picture = "\RH_aks\inv\ak105.paa";
		displayName = "AK-105";
		descriptionShort = "Shortened carbine version of the AK-74M rifle.<br/>Magazine: AK Mag.";
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
	};
	class RH_ak105gl: RH_ak105
	{
		scope = 2;
		model = "\RH_aks\RH_ak105gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-105w/GP30";
		picture = "\RH_aks\inv\ak105gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_ak74pMuzzle","RH_gp30Muzzle"};
		class RH_ak74pMuzzle: RH_ak105{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_ak105k: RH_ak105
	{
		model = "\RH_aks\RH_ak105k.p3d";
		picture = "\RH_aks\inv\ak105k.paa";
		displayName = "AK-105 Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
		};
	};
	class RH_ak105kgl: RH_ak105k
	{
		scope = 2;
		model = "\RH_aks\RH_ak105kgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-105w/GP30 Kobra";
		picture = "\RH_aks\inv\ak105kgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_ak104Muzzle","RH_gp30Muzzle"};
		class RH_ak104Muzzle: RH_ak105k{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_ak105sp: RH_ak105
	{
		model = "\RH_aks\RH_ak105sp.p3d";
		picture = "\RH_aks\inv\ak105sp.paa";
		displayName = "AK-105 PSO";
		modelOptics="\ca\weapons\AK\pso_optics";
		opticsPPEffects[]={"OpticsCHAbera2","OpticsBlur3"};
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		opticsZoomInit=0.0623;
		opticsZoomMin=0.0623;
		opticsZoomMax=0.0623;
		distanceZoomMin=200;
		distanceZoomMax=200;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.04;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
		};
	};
	class RH_ak105glsp: RH_ak105
	{
		scope = 2;
		model = "\RH_aks\RH_ak105glsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-105w/GP30 PSO";
		picture = "\RH_aks\inv\ak105glsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_ak105sp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_ak105_1p29: RH_ak105
	{
		model = "\RH_aks\RH_ak105_1p29.p3d";
		picture = "\RH_aks\inv\ak105_1p29.paa";
		displayName = "AK-105 1P29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove 1P29 Scope";
				script = "spawn player_remove1P29;";
			};
		};
	};
	class RH_ak105gl1p29: RH_ak105
	{
		scope = 2;
		model = "\RH_aks\RH_ak105gl1p29.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-105w/GP30 1P29";
		picture = "\RH_aks\inv\ak105gl1p29.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak105_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_ak105Muzzle","RH_gp30Muzzle"};
		class RH_ak105Muzzle: RH_ak105_1p29{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove 1P29 Scope";
				script = "spawn player_remove1P29;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_AK107: RH_ak74m
	{
		model = "\RH_aks\RH_AK107.p3d";
		picture = "\RH_aks\inv\AK107.paa";
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag"*/};
		displayName = "AK-107";
		descriptionShort = "Russian 5.45mm assault rifle developed from the AK-100 series.<br/>Magazine: AK Mag.";
	};
	class RH_AK107gl: RH_AK107
	{
		scope = 2;
		model = "\RH_aks\RH_AK107gl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-107w/GP30";
		picture = "\RH_aks\inv\AK107gl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_ak74pMuzzle","RH_gp30Muzzle"};
		class RH_ak74pMuzzle: RH_AK107{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_AK107k: RH_AK107
	{
		model = "\RH_aks\RH_AK107k.p3d";
		picture = "\RH_aks\inv\AK107k.paa";
		displayName = "AK-107 Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
		};
	};
	class RH_AK107kgl: RH_AK107k
	{
		scope = 2;
		model = "\RH_aks\RH_AK107kgl.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-107w/GP30 Kobra";
		picture = "\RH_aks\inv\AK107kgl.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak104_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_ak104Muzzle","RH_gp30Muzzle"};
		class RH_ak104Muzzle: RH_AK107k{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove Kobra Sight";
				script = "spawn player_removeKOBRA;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_AK107sp: RH_AK107
	{
		model = "\RH_aks\RH_AK107sp.p3d";
		picture = "\RH_aks\inv\AK107sp.paa";
		displayName = "AK-107 PSO";
		modelOptics="\ca\weapons\AK\pso_optics";
		opticsPPEffects[]={"OpticsCHAbera2","OpticsBlur3"};
		opticsFlare=1;
		opticsDisablePeripherialVision=1;
		opticsZoomInit=0.0623;
		opticsZoomMin=0.0623;
		opticsZoomMax=0.0623;
		distanceZoomMin=200;
		distanceZoomMax=200;
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.04;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
		};
	};
	class RH_AK107glsp: RH_AK107
	{
		scope = 2;
		model = "\RH_aks\RH_AK107glsp.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-107w/GP30 PSO";
		picture = "\RH_aks\inv\AK107glsp.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\ak74m_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_ak74mMuzzle","RH_gp30Muzzle"};
		class RH_ak74mMuzzle: RH_AK107sp{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove PSO Scope";
				script = "spawn player_removePSO;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	class RH_AK107_1p29: RH_AK107
	{
		model = "\RH_aks\RH_AK107_1p29.p3d";
		picture = "\RH_aks\inv\AK107_1p29.paa";
		displayName = "AK-107 1P29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		class Single: Single
		{
			minRange = 2;
			minRangeProbab = 0.6;
			midRange = 400;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove 1P29 Scope";
				script = "spawn player_remove1P29;";
			};
		};
	};
	class RH_AK107gl1p29: RH_AK107
	{
		scope = 2;
		model = "\RH_aks\RH_AK107gl1p29.p3d";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "AK-107w/GP30 1P29";
		picture = "\RH_aks\inv\AK107gl1p29.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		reloadMagazineSound[] = {"\RH_aks\sound\AK107_reload.wss",0.056234,1,20};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\data\Anim\M16GL.rtm"};
		magazines[] = {"30Rnd_545x39_AK"/*,"RH_30Rnd_545x39_AKSU_mag","30Rnd_545x39_AKSD"*/};
		muzzles[] = {"RH_AK107Muzzle","RH_gp30Muzzle"};
		class RH_AK107Muzzle: RH_AK107_1p29{};
		class RH_gp30Muzzle: GrenadeLauncher
		{
			displayName = "GP30 Grenadelauncher";
			optics = 1;
			modelOptics = "-";
			cameraDir = "GL look";
			memoryPointCamera = "GL eye";
			opticsZoomMin = 0.22;
			opticsZoomMax = 0.95;
			opticsZoomInit = 0.42;
			magazineReloadTime = 0;
			reloadTime = 0.1;
			sound[] = {"\RH_aks\Sound\GP30.wss",0.562341,1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss",0.056234,1,20};
			magazines[] = {"1Rnd_HE_GP25","FlareWhite_GP25","FlareGreen_GP25","FlareRed_GP25","FlareYellow_GP25","1Rnd_SMOKE_GP25","1Rnd_SMOKERED_GP25","1Rnd_SMOKEGREEN_GP25","1Rnd_SMOKEYELLOW_GP25"};
		};
		class ItemActions
		{
			class Use
			{
				text = "Remove 1P29 Scope";
				script = "spawn player_remove1P29;";
			};
			class Use2
			{
				text = "Remove GP Launcher";
				script = "spawn player_removeGP;";
			};
		};
	};
	/*class RH_asval: VSS_vintorez
	{
		model = "\RH_aks\RH_asval.p3d";
		picture = "\RH_aks\inv\asval.paa";
		magazines[] = {"RH_20Rnd_9x39_val_mag","20Rnd_9x39_SP5_VSS","10Rnd_9x39_SP5_VSS"};
		handAnim[] = {"OFP2_ManSkeleton","\Ca\weapons\Data\Anim\AK.rtm"};
		fireLightDuration = 0.0;
		fireLightIntensity = 0.0;
		modelOptics = "-";
		opticsZoomMin = 0.25;
		opticsZoomMax = 1.1;
		opticsZoomInit = 0.5;
		distanceZoomMin = 300;
		distanceZoomMax = 300;
		displayname = "AS Val";
		reloadMagazineSound[] = {"\RH_aks\sound\val_reload.wss",0.056234,1,20};
		modes[] = {"Single","Full"};
		class Single: Mode_SemiAuto
		{
			dispersion = 0.0018;
			soundContinuous = 0;
			reloadTime = 0.1;
			recoil = "recoil_single_primary_4outof10";
			recoilProne = "recoil_single_primary_prone_4outof10";
			begin1[] = {"\RH_aks\Sound\val.wss",1.0,1,50};
			soundBegin[] = {"begin1",1};
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 250;
			midRangeProbab = 0.7;
			maxRange = 500;
			maxRangeProbab = 0.05;
		};
		class Full: Mode_FullAuto
		{
			dispersion = 0.0018;
			soundContinuous = 0;
			reloadTime = 0.08;
			recoil = "recoil_auto_primary_3outof10";
			recoilProne = "recoil_auto_primary_prone_3outof10";
			begin1[] = {"\RH_aks\Sound\val.wss",1.0,1,50};
			soundBegin[] = {"begin1",1};
			minRange = 0;
			minRangeProbab = 0.1;
			midRange = 60;
			midRangeProbab = 0.7;
			maxRange = 100;
			maxRangeProbab = 0.05;
		};
	};
	class RH_asvalk: RH_asval
	{
		model = "\RH_aks\RH_asvalk.p3d";
		picture = "\RH_aks\inv\asvalk.paa";
		displayName = "AS Val Kobra";
		opticsDisablePeripherialVision = 1;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
	};
	class RH_asvalsp: RH_asval
	{
		model = "\RH_aks\RH_asvalsp.p3d";
		picture = "\RH_aks\inv\asvalsp.paa";
		displayName = "AS Val Sniper";
		modelOptics = "\RH_aks\fnc_PSO_1_1_vss";
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 110;
		distanceZoomMax = 110;
	};
	class RH_rpk47: RH_akm
	{
		scope = 2;
		model = "\RH_aks\RH_rpk47.p3d";
		picture = "\RH_aks\inv\rpk47.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.4;
		displayName = "RPK";
		distanceZoomMin = 300;
		distanceZoomMax = 300;
		reloadMagazineSound[] = {"\RH_aks\sound\rpk47_reload.wss",0.056234,1,20};
		magazines[] = {"RH_75Rnd_762x39_mag","30Rnd_762x39_AK47"};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_aks\Sound\rpk47.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			dispersion = 0.0028;
			reloadTime = 0.1;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			aiRateOfFire = 0.5;
			aiRateOfFireDistance = 500;
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.1;
			showToPlayer = 1;
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_aks\Sound\rpk47.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_FULLAUTO";
			dispersion = 0.0028;
			reloadTime = 0.1;
			recoil = "RH_AKMRecoil";
			recoilProne = "RH_AKMRecoilProne";
			aiRateOfFire = 0.1;
			aiRateOfFireDistance = 500;
			minRange = 2;
			minRangeProbab = 0.4;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.2;
			showToPlayer = 1;
		};
		class Library
		{
			libTextDesc = "The RPK light machine gun, compared to the AKM rifle, has a new, heavier and extended barrel with an increased heat capacity. The barrel is permanently fixed to the receiver and cannot be replaced in field conditions; the barrel�s bore is chrome-lined. The barrel is equipped with a newly designed front sight base, gas block (without a bayonet lug) and an under-barrel cleaning rod guide.";
		};
	};*/
	class RH_rpk74: RH_ak74
	{
		scope = 2;
		model = "\RH_aks\RH_rpk74.p3d";
		picture = "\RH_aks\inv\rpk74.paa";
		UiPicture = "\CA\weapons\data\Ico\i_regular_CA.paa";
		modelOptics = "-";
		optics = 1;
		dexterity = 1.7;
		displayName = "RPK-74";
		descriptionShort = "Light machine gun of Soviet design.<br/>Magazine: RPK-74 Mag.";
		distanceZoomMin = 300;
		distanceZoomMax = 300;
		reloadMagazineSound[] = {"\RH_aks\sound\rpk74_reload.wss",0.056234,1,20};
		magazines[] = {"RH_45Rnd_545x39_mag"/*,"30Rnd_545x39_AK"*/};
		modes[] = {"Single","FullAuto"};
		class Single: Single
		{
			begin1[] = {"\RH_aks\Sound\rpk74.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			reloadTime = 0.1;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
		};
		class FullAuto: FullAuto
		{
			begin1[] = {"\RH_aks\Sound\rpk74.wss",1.778279,1,1000};
			soundBegin[] = {"begin1",1};
			soundBurst = 1;
			soundContinuous = 0;
			displayName = "$STR_DN_MODE_FULLAUTO";
			reloadTime = 0.1;
			recoil = "RH_AKS74URecoil";
			recoilProne = "RH_AKS74URecoilProne";
			dispersion = 0.0025;
			aiRateOfFire = 0.1;
			aiRateOfFireDistance = 500;
			minRange = 2;
			minRangeProbab = 0.1;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 600;
			maxRangeProbab = 0.05;
			showToPlayer = 1;
		};
		class Library
		{
			libTextDesc = "The RPK-74 is a modernized variant of the AK-74 rifle, and the modifications implemented mirror those made to the AKM, while working on the RPK light machine gun. The RPK-74 also uses a longer and heavier chrome-lined barrel. Fixed to the barrel were: a newly designed front sight base, gas block (with the gas tube placed at a right angle to the bore axis) and a ring mount for the cleaning rod. The RPK-74 was also equipped with a folding bipod (modified from the RPK) and a changed front sight tower.";
		};
	};
	/*class RH_rpk74m: RH_rpk74
	{
		model = "\RH_aks\RH_rpk74m.p3d";
		picture = "\RH_aks\inv\rpk74m.paa";
		displayName = "RPK-74M";
	};
	class RH_rpk74m1p29: RH_rpk74m
	{
		model = "\RH_aks\RH_rpk74m1p29.p3d";
		picture = "\RH_aks\inv\rpk74m1p29.paa";
		displayName = "RPK-74M 1p29";
		modelOptics = "\RH_aks\fnc_1P29";
		opticsZoomMin = 0.071945;
		opticsZoomMax = 0.071945;
		distanceZoomMin = 322;
		distanceZoomMax = 322;
		opticsFlare = "true";
		opticsDisablePeripherialVision = "true";
	};*/
};
/*class cfgVehicles
{
	class ReammoBox;
	class RHAKbox: ReammoBox
	{
		scope = 2;
		model = "\ca\weapons\AmmoBoxes\RUBasicAmmo.p3d";
		displayName = "RH AK box";
		class TransportMagazines
		{
			class _xx_8Rnd_9x18_Makarov
			{
				magazine = "8Rnd_9x18_Makarov";
				count = 200;
			};
			class _xx_8Rnd_9x18_MakarovSD
			{
				magazine = "8Rnd_9x18_MakarovSD";
				count = 200;
			};
			class _xx_RH_20Rnd_9x18_aps
			{
				magazine = "RH_20Rnd_9x18_aps";
				count = 200;
			};
			class _xx_RH_20Rnd_9x18_apssd
			{
				magazine = "RH_20Rnd_9x18_apssd";
				count = 200;
			};
			class _xx_RH_20Rnd_9x39_val_mag
			{
				magazine = "RH_20Rnd_9x39_val_mag";
				count = 200;
			};
			class _xx_30Rnd_545x39_AK
			{
				magazine = "30Rnd_545x39_AK";
				count = 200;
			};
			class _xx_30Rnd_545x39_AKSD
			{
				magazine = "30Rnd_545x39_AKSD";
				count = 200;
			};
			class _xx_RH_30Rnd_545x39_AKSU_mag
			{
				magazine = "RH_30Rnd_545x39_AKSU_mag";
				count = 200;
			};
			class _xx_30Rnd_762x39_AK47
			{
				magazine = "30Rnd_762x39_AK47";
				count = 200;
			};
			class _xx_RH_30Rnd_762x39_SDmag
			{
				magazine = "RH_30Rnd_762x39_SDmag";
				count = 200;
			};
			class _xx_RH_20Rnd_9x39_SP6_mag
			{
				magazine = "RH_20Rnd_9x39_SP6_mag";
				count = 200;
			};
			class _xx_20Rnd_9x39_SP5_VSS
			{
				magazine = "20Rnd_9x39_SP5_VSS";
				count = 200;
			};
			class _xx_RH_20Rnd_9x39_SP6SD_mag
			{
				magazine = "RH_20Rnd_9x39_SP6SD_mag";
				count = 200;
			};
			class _xx_RH_10Rnd_762x54_SVD
			{
				magazine = "10Rnd_762x54_SVD";
				count = 200;
			};
			class _xx_RH_1Rnd_30mm
			{
				magazine = "RH_1Rnd_30mm";
				count = 200;
			};
			class _xx_1Rnd_HE_GP25
			{
				magazine = "1Rnd_HE_GP25";
				count = 200;
			};
			class _xx_RH_75Rnd_762x39_mag
			{
				magazine = "RH_75Rnd_762x39_mag";
				count = 200;
			};
			class _xx_RH_45Rnd_545x39_mag
			{
				magazine = "RH_45Rnd_545x39_mag";
				count = 200;
			};
			class _xx_64Rnd_9x19_Bizon
			{
				magazine = "64Rnd_9x19_Bizon";
				count = 200;
			};
			class _xx_64Rnd_9x19_SD_Bizon
			{
				magazine = "64Rnd_9x19_SD_Bizon";
				count = 200;
			};
		};
		class TransportWeapons
		{
			class _xx_RH_pm
			{
				weapon = "RH_pm";
				count = 10;
			};
			class _xx_RH_pmsd
			{
				weapon = "RH_pmsd";
				count = 10;
			};
			class _xx_RH_aps
			{
				weapon = "RH_aps";
				count = 10;
			};
			class _xx_RH_apssd
			{
				weapon = "RH_apssd";
				count = 10;
			};
			class _xx_RH_ak47
			{
				weapon = "RH_ak47";
				count = 10;
			};
			class _xx_RH_aks47
			{
				weapon = "RH_aks47";
				count = 10;
			};
			class _xx_RH_aks47b
			{
				weapon = "RH_aks47b";
				count = 10;
			};
			class _xx_RH_aks47g
			{
				weapon = "RH_aks47g";
				count = 10;
			};
			class _xx_RH_aks47s
			{
				weapon = "RH_aks47s";
				count = 10;
			};
			class _xx_RH_ak47gl
			{
				weapon = "RH_ak47gl";
				count = 10;
			};
			class _xx_RH_akm
			{
				weapon = "RH_akm";
				count = 10;
			};
			class _xx_RH_akms
			{
				weapon = "RH_akms";
				count = 10;
			};
			class _xx_RH_akmssd
			{
				weapon = "RH_akmssd";
				count = 10;
			};
			class _xx_RH_akmsp
			{
				weapon = "RH_akmsp";
				count = 10;
			};
			class _xx_RH_akmgl
			{
				weapon = "RH_akmgl";
				count = 10;
			};
			class _xx_RH_rk95
			{
				weapon = "RH_rk95";
				count = 10;
			};
			class _xx_RH_rk95ag
			{
				weapon = "RH_rk95ag";
				count = 10;
			};
			class _xx_RH_rk95aim
			{
				weapon = "RH_rk95aim";
				count = 10;
			};
			class _xx_RH_rk95sd
			{
				weapon = "RH_rk95sd";
				count = 10;
			};
			class _xx_RH_rk95sdaim
			{
				weapon = "RH_rk95sdaim";
				count = 10;
			};
			class _xx_RH_rk95sdag
			{
				weapon = "RH_rk95sdag";
				count = 10;
			};
			class _xx_RH_an94
			{
				weapon = "RH_an94";
				count = 10;
			};
			class _xx_RH_an94gl
			{
				weapon = "RH_an94gl";
				count = 10;
			};
			class _xx_RH_ak74
			{
				weapon = "RH_ak74";
				count = 10;
			};
			class _xx_RH_ak74gl
			{
				weapon = "RH_ak74gl";
				count = 10;
			};
			class _xx_RH_ak74k
			{
				weapon = "RH_ak74k";
				count = 10;
			};
			class _xx_RH_ak74kgl
			{
				weapon = "RH_ak74kgl";
				count = 10;
			};
			class _xx_RH_ak74sp
			{
				weapon = "RH_ak74sp";
				count = 10;
			};
			class _xx_RH_aks74
			{
				weapon = "RH_aks74";
				count = 10;
			};
			class _xx_RH_aks74gl
			{
				weapon = "RH_aks74gl";
				count = 10;
			};
			class _xx_RH_aks74k
			{
				weapon = "RH_aks74k";
				count = 10;
			};
			class _xx_RH_aks74kgl
			{
				weapon = "RH_aks74kgl";
				count = 10;
			};
			class _xx_RH_aks74sp
			{
				weapon = "RH_aks74sp";
				count = 10;
			};
			class _xx_rh_ak74m
			{
				weapon = "rh_ak74m";
				count = 10;
			};
			class _xx_rh_ak74mgl
			{
				weapon = "rh_ak74mgl";
				count = 10;
			};
			class _xx_rh_ak74mk
			{
				weapon = "rh_ak74mk";
				count = 10;
			};
			class _xx_rh_ak74mkgl
			{
				weapon = "rh_ak74mkgl";
				count = 10;
			};
			class _xx_rh_ak74msp
			{
				weapon = "rh_ak74msp";
				count = 10;
			};
			class _xx_rh_ak74mglsp
			{
				weapon = "rh_ak74mglsp";
				count = 10;
			};
			class _xx_rh_ak74m1p29
			{
				weapon = "rh_ak74m1p29";
				count = 10;
			};
			class _xx_rh_ak74mgl1p29
			{
				weapon = "rh_ak74mgl1p29";
				count = 10;
			};
			class _xx_rh_aks74m
			{
				weapon = "rh_aks74m";
				count = 10;
			};
			class _xx_rh_aks74mgl
			{
				weapon = "rh_aks74mgl";
				count = 10;
			};
			class _xx_rh_aks74mk
			{
				weapon = "rh_aks74mk";
				count = 10;
			};
			class _xx_rh_aks74mkgl
			{
				weapon = "rh_aks74mkgl";
				count = 10;
			};
			class _xx_rh_aks74msp
			{
				weapon = "rh_aks74msp";
				count = 10;
			};
			class _xx_rh_aks74mglsp
			{
				weapon = "rh_aks74mglsp";
				count = 10;
			};
			class _xx_rh_aks74m1p29
			{
				weapon = "rh_aks74m1p29";
				count = 10;
			};
			class _xx_rh_aks74mgl1p29
			{
				weapon = "rh_aks74mgl1p29";
				count = 10;
			};
			class _xx_rh_ak103
			{
				weapon = "rh_ak103";
				count = 10;
			};
			class _xx_rh_ak103gl
			{
				weapon = "rh_ak103gl";
				count = 10;
			};
			class _xx_rh_ak103k
			{
				weapon = "rh_ak103k";
				count = 10;
			};
			class _xx_rh_ak103kgl
			{
				weapon = "rh_ak103kgl";
				count = 10;
			};
			class _xx_rh_ak103sp
			{
				weapon = "rh_ak103sp";
				count = 10;
			};
			class _xx_rh_ak103glsp
			{
				weapon = "rh_ak103glsp";
				count = 10;
			};
			class _xx_rh_ak103_1p29
			{
				weapon = "rh_ak103_1p29";
				count = 10;
			};
			class _xx_rh_ak103gl1p29
			{
				weapon = "rh_ak103gl1p29";
				count = 10;
			};
			class _xx_rh_ak104
			{
				weapon = "rh_ak104";
				count = 10;
			};
			class _xx_rh_ak104gl
			{
				weapon = "rh_ak104gl";
				count = 10;
			};
			class _xx_rh_ak104k
			{
				weapon = "rh_ak104k";
				count = 10;
			};
			class _xx_rh_ak104kgl
			{
				weapon = "rh_ak104kgl";
				count = 10;
			};
			class _xx_rh_ak104sp
			{
				weapon = "rh_ak104sp";
				count = 10;
			};
			class _xx_rh_ak104glsp
			{
				weapon = "rh_ak104glsp";
				count = 10;
			};
			class _xx_rh_ak104_1p29
			{
				weapon = "rh_ak104_1p29";
				count = 10;
			};
			class _xx_rh_ak104gl1p29
			{
				weapon = "rh_ak104gl1p29";
				count = 10;
			};
			class _xx_rh_ak107
			{
				weapon = "rh_ak107";
				count = 10;
			};
			class _xx_rh_ak107gl
			{
				weapon = "rh_ak107gl";
				count = 10;
			};
			class _xx_rh_ak107k
			{
				weapon = "rh_ak107k";
				count = 10;
			};
			class _xx_rh_ak107kgl
			{
				weapon = "rh_ak107kgl";
				count = 10;
			};
			class _xx_rh_ak107sp
			{
				weapon = "rh_ak107sp";
				count = 10;
			};
			class _xx_rh_ak107glsp
			{
				weapon = "rh_ak107glsp";
				count = 10;
			};
			class _xx_rh_ak107_1p29
			{
				weapon = "rh_ak107_1p29";
				count = 10;
			};
			class _xx_rh_ak107gl1p29
			{
				weapon = "rh_ak107gl1p29";
				count = 10;
			};
			class _xx_rh_ak105
			{
				weapon = "rh_ak105";
				count = 10;
			};
			class _xx_rh_ak105gl
			{
				weapon = "rh_ak105gl";
				count = 10;
			};
			class _xx_rh_ak105k
			{
				weapon = "rh_ak105k";
				count = 10;
			};
			class _xx_rh_ak105kgl
			{
				weapon = "rh_ak105kgl";
				count = 10;
			};
			class _xx_rh_ak105sp
			{
				weapon = "rh_ak105sp";
				count = 10;
			};
			class _xx_rh_ak105glsp
			{
				weapon = "rh_ak105glsp";
				count = 10;
			};
			class _xx_rh_ak105_1p29
			{
				weapon = "rh_ak105_1p29";
				count = 10;
			};
			class _xx_rh_ak105gl1p29
			{
				weapon = "rh_ak105gl1p29";
				count = 10;
			};
			class _xx_rh_aks74p
			{
				weapon = "rh_aks74p";
				count = 10;
			};
			class _xx_rh_aks74pgl
			{
				weapon = "rh_aks74pgl";
				count = 10;
			};
			class _xx_rh_aks74pk
			{
				weapon = "rh_aks74pk";
				count = 10;
			};
			class _xx_rh_aks74pkgl
			{
				weapon = "rh_aks74pkgl";
				count = 10;
			};
			class _xx_rh_aks74psp
			{
				weapon = "rh_aks74psp";
				count = 10;
			};
			class _xx_rh_aks74pglsp
			{
				weapon = "rh_aks74pglsp";
				count = 10;
			};
			class _xx_rh_aks74p1p29
			{
				weapon = "rh_aks74p1p29";
				count = 10;
			};
			class _xx_rh_aks74pgl1p29
			{
				weapon = "rh_aks74pgl1p29";
				count = 10;
			};
			class _xx_RH_aks74u
			{
				weapon = "RH_aks74u";
				count = 10;
			};
			class _xx_RH_aks74uk
			{
				weapon = "RH_aks74uk";
				count = 10;
			};
			class _xx_RH_aks74usd
			{
				weapon = "RH_aks74usd";
				count = 10;
			};
			class _xx_RH_aks74upt
			{
				weapon = "RH_aks74upt";
				count = 10;
			};
			class _xx_RH_aks74usdk
			{
				weapon = "RH_aks74usdk";
				count = 10;
			};
			class _xx_RH_aks74uptk
			{
				weapon = "RH_aks74uptk";
				count = 10;
			};
			class _xx_RH_aks74uptsp
			{
				weapon = "RH_aks74uptsp";
				count = 10;
			};
			class _xx_RH_svd
			{
				weapon = "RH_svd";
				count = 10;
			};
			class _xx_RH_svdb
			{
				weapon = "RH_svdb";
				count = 10;
			};
			class _xx_RH_svdg
			{
				weapon = "RH_svdg";
				count = 10;
			};
			class _xx_RH_svds
			{
				weapon = "RH_svds";
				count = 10;
			};
			class _xx_RH_svu
			{
				weapon = "RH_svu";
				count = 10;
			};
			class _xx_RH_svd_CAMO
			{
				weapon = "RH_svd_CAMO";
				count = 10;
			};			
			class _xx_RH_asval
			{
				weapon = "RH_asval";
				count = 10;
			};
			class _xx_RH_asvalk
			{
				weapon = "RH_asvalk";
				count = 10;
			};
			class _xx_RH_asvalsp
			{
				weapon = "RH_asvalsp";
				count = 10;
			};
			class _xx_RH_bizon
			{
				weapon = "RH_bizon";
				count = 10;
			};
			class _xx_RH_bizonk
			{
				weapon = "RH_bizonk";
				count = 10;
			};
			class _xx_RH_bizonsd
			{
				weapon = "RH_bizonsd";
				count = 10;
			};
			class _xx_RH_bizonsdk
			{
				weapon = "RH_bizonsdk";
				count = 10;
			};
			class _xx_RH_oc14
			{
				weapon = "RH_oc14";
				count = 10;
			};
			class _xx_RH_oc14gl
			{
				weapon = "RH_oc14gl";
				count = 10;
			};
			class _xx_RH_gr1
			{
				weapon = "RH_gr1";
				count = 10;
			};
			class _xx_RH_oc14sd
			{
				weapon = "RH_oc14sd";
				count = 10;
			};
			class _xx_RH_gr1sd
			{
				weapon = "RH_gr1sd";
				count = 10;
			};
			class _xx_RH_oc14sp
			{
				weapon = "RH_oc14sp";
				count = 10;
			};
			class _xx_RH_oc14glsp
			{
				weapon = "RH_oc14glsp";
				count = 10;
			};
			class _xx_RH_gr1sp
			{
				weapon = "RH_gr1sp";
				count = 10;
			};
			class _xx_RH_oc14sdsp
			{
				weapon = "RH_oc14sdsp";
				count = 10;
			};
			class _xx_RH_gr1sdsp
			{
				weapon = "RH_gr1sdsp";
				count = 10;
			};
			class _xx_RH_rpk47
			{
				weapon = "RH_rpk47";
				count = 10;
			};
			class _xx_RH_rpk74
			{
				weapon = "RH_rpk74";
				count = 10;
			};
			class _xx_RH_rpk74m
			{
				weapon = "RH_rpk74m";
				count = 10;
			};
			class _xx_RH_rpk74m1p29
			{
				weapon = "RH_rpk74m1p29";
				count = 10;
			};
		};
	};
};*/
//};
