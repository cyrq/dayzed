gdtmod_grass 1.02
by HeinBloed
http://www.gdt-server.net/
===========================

This Addon trims the grass.
This Addon is client side only.

Required:
Armed Assault 2 v1.07.
Operation Arrowhead v1.54.

===========================

Changelog 1.02
- Update for Operation Arrowhead v1.54.

Changelog 1.01
- Update for Operation Arrowhead.
