class CfgPatches
{
	class dayzed_mapadd
	{
		units[]={};
		weapons[]={};
		requiredVersion = 0.1;
		requiredAddons[] = {"CAData","CAMisc3"};
		DayZedV = "0.2.9.8";
	};
};
class CfgVehicleClasses
{
	class dayzed_mapadd
	{
		displayName="dayzed_addons";
	};
};
class cfgVehicles
{
	class House;
	class Billboard_NE: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\billboard_ne_airfield.p3d";
		mapSize = 0.7;
		accuracy = 0.2;
		displayName="NE"; 
		vehicleClass="dayzed_mapadd"; 
		destrType = "DestructNo";
	};
	
	class Billboard_GM: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\billboard_gm_military_base.p3d";
		mapSize = 0.7;
		accuracy = 0.2;
		displayName="GM"; 
		vehicleClass="dayzed_mapadd"; 
		destrType = "DestructNo";
	};
	
	class Billboard_NW: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\billboard_nw_airfield.p3d";
		mapSize = 0.7;
		accuracy = 0.2;
		displayName="NW"; 
		vehicleClass="dayzed_mapadd"; 
		destrType = "DestructNo";
	};
	
	class Billboard_SS: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\billboard_ss_military_camp.p3d";
		mapSize = 0.7;
		accuracy = 0.2;
		displayName="SS"; 
		vehicleClass="dayzed_mapadd"; 
		destrType = "DestructNo";
	};
		class Billboard_BH: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\billboard_ba_field_hospital.p3d";
		mapSize = 0.7;
		accuracy = 0.2;
		displayName="BH"; 
		vehicleClass="dayzed_mapadd"; 
		destrType = "DestructNo";
	};
		class Billboard_BA: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\billboard_ba_airfield.p3d";
		mapSize = 0.7;
		accuracy = 0.2;
		displayName="BA"; 
		vehicleClass="dayzed_mapadd"; 
		destrType = "DestructNo";
	};
	class Map_South: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\map_south.p3d";
		mapSize=0.69999999;
		accuracy=0.2;
		displayName="MapSouth";
		vehicleClass="dayzed_mapadd";
		destrType="DestructBuilding";
	};
	class Map_North: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\map_north.p3d";
		mapSize=0.69999999;
		accuracy=0.2;
		displayName="MapNorth";
		vehicleClass="dayzed_mapadd";
		destrType="DestructBuilding";
	};
		class VendingMachine: House
	{
		scope=2;
		icon="\ca\data\data\Unknown_object.paa";
		model="dayzed_mapadd\models\vending_machine.p3d";
		mapSize = 0.7;
		accuracy=0.2;
		displayName="Vending Machine";
		transportMaxWeapons=0;
		transportMaxMagazines=5;
		transportMaxBackpacks=0;
		vehicleClass="dayzed_mapadd"; 
		destrType = "DestructBuilding";
		//armor=1000;
		placement="vertical";
	};
};