class CfgVehicleClasses
{
	class CSJ_Air
	{
		displayName="CSJ Air";
	};
};
class CfgPatches
{
	class CSJ_GyroAC
	{
		units[]=
		{
			"CSJ_GyroP",
			"CSJ_GyroCover"
		};
		weapons[]={};
	};
};
class CfgVehicles
{
	class All
	{
	};
	class AllVehicles: All
	{
	};
	class Air: AllVehicles
	{
		class ViewPilot;
	};
	class Plane: Air
	{
		class AnimationSources;
	};
	class CSJ_GyroP: Plane
	{
		scope=2;
		faction="CIV";
		crew="";
		cabinOpening=0;
		driverAction="UH60_Pilot";
		vehicleClass="CSJ_Air";
		model="\CSJ_GyroAC\CSJ_GyroP";
		displayName="GyroCopter";
		getInAction="GetInLow";
		transportSoldier=0;
		picture="\CSJ_GyroAC\data\gyropic.paa";
		Icon="\CSJ_GyroAC\data\gyroIcon.paa";
		gearRetracting=0;
		nameSound="plane";
		mapSize=8;
		fov=0.5;
		side=3;
		soundEngine[]=
		{
			"\CSJ_GyroAC\camel1.wss",
			5.6234102,
			1.8
		};
		insideSoundCoef=1;
		airBrake=0;
		flaps=0;
		wheelSteeringSensitivity=0.25;
		nightVision=0;
		preferRoads=0;
		showWeaponCargo=0;
		camouflage=8;
		audible=5;
		driverCanSee="2+8";
		landingAoa="rad 2";
		armor=20;
		ejectSpeed[]={0,0,0};
		ejectDamageLimit=0.80000001;
		cost=1000;
		formationX=8;
		formationZ=8;
		castCargoShadow=0;
		castCommanderShadow=0;
		castDriverShadow=1;
		castGunnerShadow=0;
		hideWeaponsDriver=0;
		hideWeaponsCargo=0;
		threat[]={0,0,0};
		aileronSensitivity=0.1;
		elevatorSensitivity=0.12;
		noseDownCoef=0;
		brakeDistance=10;
		dammageHalf[]={};
		dammageFull[]={};		
		transportMaxMagazines = 10;
		transportMaxWeapons = 1;
		transportMaxBackpacks = 0;
		fuelCapacity=500;
		damageEffect="DamageSmokePlane";
		soundDammage[]={"ca\sounds\air\noises\alarm_loop1",0.0001,1};
		getOutAction="GetOutLow";
		maxSpeed=125;
		landingSpeed=70;
		unitInfoType="RscUnitInfoAir";	
		extCameraPosition[]={0,0,-5};
		class Library
		{
			libTextDesc="Auto_Gyro (CSJ)";
		};
		class ViewPilot: ViewPilot
		{
			initFov=1;
			minFov=0.30000001;
			maxFov=1.2;
			initAngleX=25;
			minAngleX=-65;
			maxAngleX=80;
			initAngleY=0;
			minAngleY=-155;
			maxAngleY=155;
		};
		class AnimationSources: AnimationSources
		{
		};
		class Reflectors{};
		weapons[]={};
		magazines[]={};
		class UserActions
		{
			class rotateLeft
			{
				displayName="Rotate Left";
				position="osa leve smerovky";
				onlyforplayer=0;
				radius=2;
				condition="(Count (Crew this)==0) and ((getpos this select 2) <1) and (!isengineon this)";
				statement="this exec ""\CSJ_GyroAC\scripts\CSJ_rotateGyroLeft.sqs"" ";
			};
			class rotateRight
			{
				displayName="Rotate Right";
				position="osa leve smerovky";
				onlyforplayer=0;
				radius=2;
				condition="(Count (Crew this)==0) and ((getpos this select 2) <1) and (!isengineon this)";
				statement="this exec ""\CSJ_GyroAC\scripts\CSJ_rotateGyroRight.sqs"" ";
			};
			class push
			{
				displayName="Push GyroCopter";
				position="osa leve smerovky";
				onlyforplayer=0;
				radius=2;
				condition="(Count (Crew this)==0) and ((getpos this select 2) <1) and (!isengineon this)";
				statement="this exec ""\CSJ_GyroAC\scripts\CSJ_moveGyro.sqs"" ";
			};
		};
		class EventHandlers
		{
		};
	};
	class CSJ_GyroCover: CSJ_GyroP
	{
		model="\CSJ_GyroAC\CSJ_GyroCover";
		displayName="GyroCopter";
		maxSpeed=150;
		transportMaxMagazines = 20;
		transportMaxWeapons = 2;
		transportMaxBackpacks = 1;
		class Library
		{
			libTextDesc="Auto_Gyro Enclosed(CSJ)";
		};
	};
};
