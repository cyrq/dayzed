class CfgPatches
{
	class dayzed_buildings
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.10;
		requiredAddons[] = {CAStructuresHouse_HouseV};
		DayZedV = "0.2.9.8";
	};
};

class CfgAddons
{
	class PreloadAddons
	{
		class dayzed_buildings
		{
			list[] = {"dayzed_buildings"};
		};
	};
};

class CfgVehicles
{
	class House;
	class Land_HouseV_1I1: House
	{
		model = "\dayzed_buildings\HouseV_1I1.p3d";
	};
	class Land_HouseV_1I2: House {};
	
	class Land_HouseV_1I3: Land_HouseV_1I2
	{
		model = "\dayzed_buildings\HouseV_1I3.p3d";
	};	
	class Land_HouseV_2I: Land_HouseV_1I2
	{
		model = "\dayzed_buildings\HouseV_2I.p3d";
	};	
	class Land_a_stationhouse: House
	{
		model = "\dayzed_buildings\a_stationhouse.p3d";
	};
	class Land_HouseB_Tenement: House
	{
		model = "\dayzed_buildings\HouseB_Tenement.p3d";
		ladders[]={{"start1","end1"},{"start2","end2"},{"start3","end3"}};
	};
	class Land_Mil_House: House
	{
		model = "\dayzed_buildings\Mil_House.p3d";
		class AnimationSources
		{
			class drzwi {animPeriod=1; initPhase=0;};
			class drzwi1 {animPeriod=1; initPhase=0;};
			class drzwi2 {animPeriod=1; initPhase=0;};
		};
		class UserActions
		{
			class OpenDoors1
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=drzwi_akcja;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""drzwi"" < 0.5";
				statement="this animate [""drzwi"", 1]";
			};
			class CloseDoors1
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=drzwi_akcja;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""drzwi"" >= 0.5";
				statement="this animate [""drzwi"", 0]";
			};
			class OpenDoors2
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=drzwi_akcja1;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""drzwi1"" < 0.5";
				statement="this animate [""drzwi1"", 1];this animate [""drzwi2"", 1]";
			};
			class CloseDoors2
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=drzwi_akcja1;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""drzwi1"" >= 0.5";
				statement="this animate [""drzwi1"", 0];this animate [""drzwi2"", 0]";
			};
		};
	};
	class Land_Shed_W4: House
	{
		model = "\dayzed_buildings\Shed_W4.p3d";
	};
	class Land_Mil_Guardhouse: House
	{
		model = "\dayzed_buildings\Mil_Guardhouse.p3d";
	};
	class Land_Ind_SawMill : House {
		model = "\dayzed_buildings\Ind_SawMill.p3d";
		class AnimationSources
		{
			class DoorLeftHandedInside_01 {animPeriod=1; initPhase=0;source = user; };
			class DoorRightHandedInside_01 {animPeriod=1; initPhase=0;source = user; };
			class Gate_01 {animPeriod=2; initPhase=2.5;source = user; };
			class Gate_02 {animPeriod=2; initPhase=0;source = user; };
		};
		class UserActions
		{
			class OpenDoors1
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorLeftHandedInside_01_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorLeftHandedInside_01"" < 0.5";
				statement="this animate [""DoorLeftHandedInside_01"", 1]";
			};
			class CloseDoors1
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorLeftHandedInside_01_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorLeftHandedInside_01"" >= 0.5";
				statement="this animate [""DoorLeftHandedInside_01"", 0]";
			};
			class OpenDoors2
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorRightHandedInside_01_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_01"" < 0.5";
				statement="this animate [""DoorRightHandedInside_01"", 1]";
			};
			class CloseDoors2
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorRightHandedInside_01_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_01"" >= 0.5";
				statement="this animate [""DoorRightHandedInside_01"", 0]";
			};
			class OpenDoors3
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=Gate_01_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""Gate_01"" < 0.5";
				statement="this animate [""Gate_01"", 2.5]";
			};
			class CloseDoors3
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=Gate_01_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""Gate_01"" >= 0.5";
				statement="this animate [""Gate_01"", 0]";
			};
			class OpenDoors4
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=Gate_02_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""Gate_02"" < 0.5";
				statement="this animate [""Gate_02"", 2.5]";
			};
			class CloseDoors4
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=Gate_02_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""Gate_02"" >= 0.5";
				statement="this animate [""Gate_02"", 0]";
			};
		};
	};
	class Land_Mil_Barracks: House {
		model = "\dayzed_buildings\Mil_Barracks.p3d";
		class AnimationSources
		{
			class DoorLeftHandedOutside_01 {animPeriod=1; initPhase=0.15;source = user; };
			class DoorRightHandedOutside_01 {animPeriod=1; initPhase=0.35;source = user; };
		};
		class UserActions
		{
			class OpenDoors1
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorLeftHandedOutside_01_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorLeftHandedOutside_01"" < 0.5";
				statement="this animate [""DoorLeftHandedOutside_01"", 1]";
			};
			class CloseDoors1
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorLeftHandedOutside_01_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorLeftHandedOutside_01"" >= 0.5";
				statement="this animate [""DoorLeftHandedOutside_01"", 0]";
			};
			class OpenDoors2
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorRightHandedOutside_01_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedOutside_01"" < 0.5";
				statement="this animate [""DoorRightHandedOutside_01"", 1]";
			};
			class CloseDoors2
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorRightHandedOutside_01_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedOutside_01"" >= 0.5";
				statement="this animate [""DoorRightHandedOutside_01"", 0]";
			};
		};
	};
	class Land_Mil_Barracks_L: House {
		model = "\dayzed_buildings\Mil_Barracks_L.p3d";
		class AnimationSources
		{
			class DoorRightHandedInside_01 {animPeriod=1; initPhase=0.15;source = user; };
			class DoorRightHandedInside_02 {animPeriod=1; initPhase=0;source = user; };
			class DoorRightHandedInside_03 {animPeriod=1; initPhase=0.25;source = user; };
			class DoorRightHandedInside_04 {animPeriod=1; initPhase=0.20;source = user; };
			class MainDoorDoubleOutside_01 {animPeriod=1; initPhase=0;source = user; };
			class MainDoorDoubleOutside_02 {animPeriod=1; initPhase=0;source = user; };
		};
		class UserActions
		{
			class OpenDoors1
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorRightHandedInside_01_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_01"" < 0.5";
				statement="this animate [""DoorRightHandedInside_01"", 1]";
			};
			class CloseDoors1
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorRightHandedInside_01_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_01"" >= 0.5";
				statement="this animate [""DoorRightHandedInside_01"", 0]";
			};
			class OpenDoors2
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorRightHandedInside_02_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_02"" < 0.5";
				statement="this animate [""DoorRightHandedInside_02"", 1]";
			};
			class CloseDoors2
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorRightHandedInside_02_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_02"" >= 0.5";
				statement="this animate [""DoorRightHandedInside_02"", 0]";
			};
			class OpenDoors3
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorRightHandedInside_03_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_03"" < 0.5";
				statement="this animate [""DoorRightHandedInside_03"", 1]";
			};
			class CloseDoors3
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorRightHandedInside_03_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_03"" >= 0.5";
				statement="this animate [""DoorRightHandedInside_03"", 0]";
			};
			class OpenDoors4
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=DoorRightHandedInside_04_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_04"" < 0.5";
				statement="this animate [""DoorRightHandedInside_04"", 1]";
			};
			class CloseDoors4
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=DoorRightHandedInside_04_Handle;
				radius=1.5;
				onlyForPlayer=false;
				condition="this animationPhase ""DoorRightHandedInside_04"" >= 0.5";
				statement="this animate [""DoorRightHandedInside_04"", 0]";
			};
			class OpenDoors5
			{
				displayNameDefault=$STR_DN_OUT_O_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_O_DOOR;
				position=MainDoorDoubleOutside_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""MainDoorDoubleOutside_01"" < 0.5";
				statement="this animate [""MainDoorDoubleOutside_01"", 1];this animate [""MainDoorDoubleOutside_02"", 1]";
			};
			class CloseDoors5
			{
				displayNameDefault=$STR_DN_OUT_C_DOOR_DEFAULT;
				displayName=$STR_DN_OUT_C_DOOR;
				position=MainDoorDoubleOutside_Handle;
				radius=3;
				onlyForPlayer=false;
				condition="this animationPhase ""MainDoorDoubleOutside_01"" >= 0.5";
				statement="this animate [""MainDoorDoubleOutside_01"", 0];this animate [""MainDoorDoubleOutside_02"", 0]";
			};
		};
	};
};