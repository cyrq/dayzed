// VopSound v2.1.0 // 10/07/2009//

#define true	1
#define false	0

#define VSoft		0
#define VArmor		1
#define VAir		2

#define private		0
#define protected		1
#define public		2

#define TEast		0
#define TWest		1
#define TGuerrila		2
#define TCivilian		3
#define TSideUnknown		4
#define TEnemy		5
#define TFriendly		6
#define TLogic		7

#define ReadAndWrite		0
#define ReadAndCreate		1
#define ReadOnly		2
#define ReadOnlyVerified		3

#define LockNo		0
#define LockCadet		1
#define LockYes		2

class CfgPatches
{
	class Vops_c_Impact
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"CAData","CAAir","CAAir2","CAAir3","CAA10","CACharacters","CASounds","CAWeapons","CAWeapons2","CAWheeled","CAWheeled2","CAWheeled3","CATracked","CATracked2","dayz_weapons"};
	};

};

class DefaultEventhandlers;	// External class reference

class CfgAmmo {
	class Default;	// External class reference
	class TimeBombCore;	// External class reference
	class MineCore;	// External class reference
	class BulletCore;	// External class reference
	class BombCore;	// External class reference
	
	class BulletBase : BulletCore {
		soundDefault1[] = {"\vops_s_impact\hit1", 0.316228, 1, 60};
		soundDefault2[] = {"\vops_s_impact\hit2", 0.316228, 1, 60};
		soundDefault3[] = {"\vops_s_impact\hit3", 0.316228, 1, 60};
		soundDefault4[] = {"\vops_s_impact\hit4", 0.316228, 1, 60};
		soundDefault5[] = {"\vops_s_impact\whizz1", 0.316228, 1, 60};
		soundDefault6[] = {"\vops_s_impact\whizz2", 0.316228, 1, 60};
		soundDefault7[] = {"\vops_s_impact\whizz3", 0.316228, 1, 60};
		soundDefault8[] = {"\vops_s_impact\whizz4", 0.316228, 1, 60};
		soundGroundSoft1[] = {"\vops_s_impact\earth1", 0.0562341, 1, 60};
		soundGroundSoft2[] = {"\vops_s_impact\earth2", 0.0562341, 1, 60};
		soundGroundSoft3[] = {"\vops_s_impact\earth3", 0.0562341, 1, 60};
		soundGroundSoft4[] = {"\vops_s_impact\earth4", 0.0562341, 1, 60};
		soundGroundSoft5[] = {"\vops_s_impact\ricoearth1", 0.0562341, 1, 60};
		soundGroundSoft6[] = {"\vops_s_impact\ricoearth2", 0.0562341, 1, 60};
		soundGroundSoft7[] = {"\vops_s_impact\ricoearth3", 0.0562341, 1, 60};
		soundGroundSoft8[] = {"\vops_s_impact\ricoearth4", 0.0562341, 1, 60};
		soundGroundHard1[] = {"\vops_s_impact\hit1", 0.125893, 1, 80};
		soundGroundHard2[] = {"\vops_s_impact\hit2", 0.125893, 1, 80};
		soundGroundHard3[] = {"\vops_s_impact\hit3", 0.125893, 1, 80};
		soundGroundHard4[] = {"\vops_s_impact\hit4", 0.125893, 1, 80};
		soundGroundHard5[] = {"\vops_s_impact\whizz1", 0.125893, 1, 80};
		soundGroundHard6[] = {"\vops_s_impact\whizz2", 0.125893, 1, 80};
		soundGroundHard7[] = {"\vops_s_impact\whizz3", 0.125893, 1, 80};
		soundGroundHard8[] = {"\vops_s_impact\whizz4", 0.125893, 1, 80};
		soundMetal1[] = {"\vops_s_impact\hitmetal1", 0.316228, 1, 90};
		soundMetal2[] = {"\vops_s_impact\hitmetal5", 0.316228, 1, 90};
		soundMetal3[] = {"\vops_s_impact\hitmetal3", 0.316228, 1, 90};
		soundMetal4[] = {"\vops_s_impact\hitmetal4", 0.316228, 1, 90};
		soundMetal5[] = {"\vops_s_impact\hitmetal5", 0.316228, 1, 90};
		soundMetal6[] = {"\vops_s_impact\hitmetal6", 0.316228, 1, 90};
		soundMetal7[] = {"\vops_s_impact\hitmetal7", 0.316228, 1, 90};
		soundMetal8[] = {"\vops_s_impact\hitmetal8", 0.316228, 1, 90};
		soundMetal9[] = {"\vops_s_impact\ricometalplate1", 0.316228, 1, 90};
		soundMetal10[] = {"\vops_s_impact\ricometalplate2", 0.316228, 1, 90};
		soundMetal11[] = {"\vops_s_impact\ricometalplate3", 0.316228, 1, 90};
		soundMetal12[] = {"\vops_s_impact\ricometalplate4", 0.316228, 1, 90};
		soundVehiclePlate1[] = {"\vops_s_impact\hitmetal1", 0.562341, 1, 90};
		soundVehiclePlate2[] = {"\vops_s_impact\hitmetal5", 0.562341, 1, 90};
		soundVehiclePlate3[] = {"\vops_s_impact\hitmetal3", 0.562341, 1, 90};
		soundVehiclePlate4[] = {"\vops_s_impact\hitmetal4", 0.562341, 1, 90};
		soundVehiclePlate5[] = {"\vops_s_impact\hitmetal5", 0.562341, 1, 90};
		soundVehiclePlate6[] = {"\vops_s_impact\hitmetal6", 0.562341, 1, 90};
		soundVehiclePlate7[] = {"\vops_s_impact\hitmetal7", 0.562341, 1, 90};
		soundVehiclePlate8[] = {"\vops_s_impact\hitmetal8", 0.562341, 1, 90};
		soundVehiclePlate9[] = {"\vops_s_impact\ricometalplate1", 0.562341, 1, 90};
		soundVehiclePlate10[] = {"\vops_s_impact\ricometalplate2", 0.562341, 1, 90};
		soundVehiclePlate11[] = {"\vops_s_impact\ricometalplate3", 0.562341, 1, 90};
		soundVehiclePlate12[] = {"\vops_s_impact\ricometalplate4", 0.562341, 1, 90};
		soundWood1[] = {"\vops_s_impact\hitwood1", 0.316228, 1, 60};
		soundWood2[] = {"\vops_s_impact\hitwood2", 0.316228, 1, 60};
		soundWood3[] = {"\vops_s_impact\hitwood3", 0.316228, 1, 60};
		soundWood4[] = {"\vops_s_impact\hitwood4", 0.316228, 1, 60};
		soundWood5[] = {"\vops_s_impact\hitwood5", 0.316228, 1, 60};
		soundWood6[] = {"\vops_s_impact\hitwood6", 0.316228, 1, 60};
		soundWood7[] = {"\vops_s_impact\hitwood7", 0.316228, 1, 60};
		soundWood8[] = {"\vops_s_impact\hitwood8", 0.316228, 1, 60};
		soundWood9[] = {"\vops_s_impact\ricowood1", 0.316228, 1, 60};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.316228, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.316228, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.316228, 1, 60};
		soundHitBody1[] = {"\vops_s_impact\uff1", 0.0177828, 1, 20};
		soundHitBody2[] = {"\vops_s_impact\uff2", 0.0177828, 1, 20};
		soundHitBody3[] = {"\vops_s_impact\uff3", 0.0177828, 1, 20};
		soundHitBody4[] = {"\vops_s_impact\uff4", 0.0177828, 1, 20};
		soundHitBody5[] = {"\vops_s_impact\uff5", 0.0177828, 1, 20};
		soundHitBody6[] = {"\vops_s_impact\uff6", 0.0177828, 1, 20};
		soundHitBody7[] = {"\vops_s_impact\uff7", 0.0177828, 1, 20};
		soundHitBody8[] = {"\vops_s_impact\uff8", 0.0177828, 1, 20};
		soundHitBody9[] = {"\vops_s_impact\uff9", 0.0177828, 1, 20};
		soundHitBody10[] = {"\vops_s_impact\uff10", 0.0177828, 1, 20};
		soundHitBody11[] = {"\vops_s_impact\uff11", 0.0177828, 1, 20};
		soundHitBody12[] = {"\vops_s_impact\uff12", 0.0177828, 1, 20};
		soundHitBody13[] = {"\vops_s_impact\uff13", 0.0177828, 1, 20};
		soundMetalPlate1[] = {"\vops_s_impact\hitmetal1", 0.562341, 1, 90};
		soundMetalPlate2[] = {"\vops_s_impact\hitmetal5", 0.562341, 1, 90};
		soundMetalPlate3[] = {"\vops_s_impact\hitmetal3", 0.562341, 1, 90};
		soundMetalPlate4[] = {"\vops_s_impact\hitmetal4", 0.562341, 1, 90};
		soundMetalPlate5[] = {"\vops_s_impact\hitmetal5", 0.562341, 1, 90};
		soundMetalPlate6[] = {"\vops_s_impact\hitmetal6", 0.562341, 1, 90};
		soundMetalPlate7[] = {"\vops_s_impact\hitmetal7", 0.562341, 1, 90};
		soundMetalPlate8[] = {"\vops_s_impact\hitmetal8", 0.562341, 1, 90};
		soundMetalPlate9[] = {"\vops_s_impact\ricometalplate1", 0.562341, 1, 90};
		soundMetalPlate10[] = {"\vops_s_impact\ricometalplate2", 0.562341, 1, 90};
		soundMetalPlate11[] = {"\vops_s_impact\ricometalplate3", 0.562341, 1, 90};
		soundMetalPlate12[] = {"\vops_s_impact\ricometalplate4", 0.562341, 1, 90};
		soundHitBuilding1[] = {"\vops_s_impact\hitwall1", db-12, 1, 60};
		soundHitBuilding2[] = {"\vops_s_impact\hitwall2", db-12, 1, 60};
		soundHitBuilding3[] = {"\vops_s_impact\hitwall3", 0.251189, 1, 60};
		soundHitBuilding4[] = {"\vops_s_impact\hitwall4", 0.251189, 1, 60};
		soundHitBuilding5[] = {"\vops_s_impact\hitwall5", 0.251189, 1, 60};
		soundHitBuilding6[] = {"\vops_s_impact\rico_hit_wall_01", 0.251189, 1, 60};
		soundHitBuilding7[] = {"\vops_s_impact\rico_hit_wall_02", 0.251189, 1, 60};
		soundHitBuilding8[] = {"\vops_s_impact\rico_hit_wall_03", 0.251189, 1, 60};
		soundHitBuilding9[] = {"\vops_s_impact\rico_hit_wall_04", 0.251189, 1, 60};
		soundHitBuilding10[] = {"\vops_s_impact\rico_hit_wall_05", 0.251189, 1, 60};
		soundHitFoliage1[] = {"\vops_s_impact\hitfoliage1", 0.177828, 1, 50};
		soundHitFoliage2[] = {"\vops_s_impact\hitfoliage2", 0.177828, 1, 50};
		soundHitFoliage3[] = {"\vops_s_impact\hitfoliage3", 0.177828, 1, 50};
		soundHitFoliage4[] = {"\vops_s_impact\hitfoliage4", 0.177828, 1, 50};
		soundConcrete1[] = {"\vops_s_impact\hit1", 0.177828, 1, 70};
		soundConcrete2[] = {"\vops_s_impact\hit2", 0.177828, 1, 70};
		soundConcrete3[] = {"\vops_s_impact\hit3", 0.177828, 1, 70};
		soundConcrete4[] = {"\vops_s_impact\hit4", 0.177828, 1, 70};
		soundConcrete5[] = {"\vops_s_impact\rico_hit_concrete_01", 0.177828, 1, 70};
		soundConcrete6[] = {"\vops_s_impact\rico_hit_concrete_02", 0.177828, 1, 70};
		soundConcrete7[] = {"\vops_s_impact\rico_hit_concrete_03", 0.177828, 1, 70};
		soundConcrete8[] = {"\vops_s_impact\rico_hit_concrete_04", 0.177828, 1, 70};
		hitGroundSoft[] = {"soundGroundSoft1", 0.2, "soundGroundSoft2", 0.2, "soundGroundSoft3", 0.1, "soundGroundSoft4", 0.1, "soundGroundSoft5", 0.1, "soundGroundSoft6", 0.1, "soundGroundSoft7", 0.1, "soundGroundSoft8", 0.1};
		hitGroundHard[] = {"soundGroundHard1", 0.2, "soundGroundHard2", 0.2, "soundGroundHard3", 0.1, "soundGroundHard4", 0.1, "soundGroundHard5", 0.1, "soundGroundHard6", 0.1, "soundGroundHard7", 0.1, "soundGroundHard8", 0.1};
		hitMan[] = {"soundHitBody1", 0.077, "soundHitBody2", 0.077, "soundHitBody3", 0.077, "soundHitBody4", 0.077, "soundHitBody5", 0.077, "soundHitBody6", 0.077, "soundHitBody7", 0.077, "soundHitBody8", 0.077, "soundHitBody9", 0.077, "soundHitBody10", 0.077, "soundHitBody11", 0.077, "soundHitBody12", 0.077, "soundHitBody13", 0.077};
		hitArmor[] = {"soundVehiclePlate1", 0.1, "soundVehiclePlate2", 0.1, "soundVehiclePlate3", 0.05, "soundVehiclePlate4", 0.05, "soundVehiclePlate5", 0.1, "soundVehiclePlate6", 0.05, "soundVehiclePlate7", 0.1, "soundVehiclePlate8", 0.1, "soundVehiclePlate9", 0.05, "soundVehiclePlate10", 0.1, "soundVehiclePlate11", 0.1, "soundVehiclePlate12", 0.1};
		hitBuilding[] = {"soundHitBuilding1", 0.1, "soundHitBuilding2", 0.1, "soundHitBuilding3", 0.1, "soundHitBuilding4", 0.1, "soundHitBuilding5", 0.1, "soundHitBuilding6", 0.1, "soundHitBuilding7", 0.1, "soundHitBuilding8", 0.1, "soundHitBuilding9", 0.1, "soundHitBuilding10", 0.1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.05, "soundWood5", 0.05, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.05, "soundWood12", 0.05};
		hitConcrete[] = {"soundConcrete1", 0.2, "soundConcrete2", 0.2, "soundConcrete3", 0.2, "soundConcrete4", 0.05, "soundConcrete5", 0.05, "soundConcrete6", 0.1, "soundConcrete7", 0.1, "soundConcrete8", 0.1};		hitDefault[] = {"soundDefault1", 0.2, "soundDefault2", 0.2, "soundDefault3", 0.1, "soundDefault4", 0.1, "soundDefault5", 0.1, "soundDefault6", 0.1, "soundDefault7", 0.1, "soundDefault8", 0.1};
		hitMetal[] = {"soundMetal1", 0.1, "soundMetal2", 0.1, "soundMetal3", 0.1, "soundMetal4", 0.05, "soundMetal5", 0.1, "soundMetal6", 0.1, "soundMetal7", 0.05, "soundMetal8", 0.1, "soundMetal9", 0.05, "soundMetal10", 0.05, "soundMetal11", 0.1, "soundMetal12", 0.1};
		hitMetalplate[] = {"soundMetalPlate1", 0.1, "soundMetalPlate2", 0.1, "soundMetalPlate3", 0.1, "soundMetalPlate4", 0.05, "soundMetalPlate5", 0.05, "soundMetalPlate6", 0.05, "soundMetalPlate7", 0.1, "soundMetalPlate8", 0.1, "soundMetalPlate9", 0.1, "soundMetalPlate10", 0.1, "soundMetalPlate11", 0.1, "soundMetalPlate12", 0.05};
		bulletFly1[] = {"\vops_s_impact\bullet1", 1.0, 1, 50};
		bulletFly2[] = {"\vops_s_impact\bullet2", 1.0, 1, 50};
		bulletFly3[] = {"\vops_s_impact\bullet3", 1.0, 1, 50};
		bulletFly4[] = {"\vops_s_impact\bullet4", 1.0, 1, 50};
		bulletFly5[] = {"\vops_s_impact\bullet5", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.2, "bulletFly2", 0.2, "bulletFly3", 0.2, "bulletFly4", 0.2, "bulletFly5", 0.2};
		supersonicCrackNear[] = {"\vops_s_impact\snapnear.wss", 1, 1, 150};
		supersonicCrackFar[] = {"\vops_s_impact\snapfarsingle.wss", 2.618, 1, 600};
	};
	class FlareCore;	// External class reference
	
	class FlareBase : FlareCore {
	};
	
	class B_9x18_Ball : BulletBase {
	};
	
	class B_9x18_SD : B_9x18_Ball {
	};
	
	class B_9x19_Ball : B_9x18_Ball {
	};
	
	class B_9x19_SD : B_9x19_Ball {
	};
	
	class B_45ACP_Ball : BulletBase {
	};
	
	class B_545x39_Ball : BulletBase {
	supersonicCrackNear[] = {"\vops_s_impact\snapnear4.wss", 1, 1, 150};
	supersonicCrackFar[] = {"\vops_s_impact\snapfar545", 1, 1, 600};
};
	
	class B_545x39_SD : BulletBase {
	};
	
	class B_556x45_Ball : BulletBase {
	supersonicCrackNear[] = {"\vops_s_impact\snapnear2.wss", 1, 1, 150};
	supersonicCrackFar[] = {"\vops_s_impact\snapfar556", 1, 1, 600};
};
	
	class B_556x45_SD : BulletBase {
	};
	
	class B_9x39_SP5 : BulletBase {
	};
	
	class B_12Gauge_74Slug : BulletBase {
	};
	
	class B_762x39_Ball : BulletBase {
		supersonicCrackNear[] = {"\vops_s_impact\snapnear1.wss", 1, 1, 150};
		supersonicCrackFar[] = {"\vops_s_impact\snapfar762", 1, 1, 800};
};
	
	class B_762x51_Ball : BulletBase {
		supersonicCrackNear[] = {"\vops_s_impact\snapnear2.wss", 1, 1, 150};
		supersonicCrackFar[] = {"\vops_s_impact\snapfarsingle7624", 1, 1, 800};
};
	
	class B_762x51_3RndBurst : B_762x51_Ball {
		supersonicCrackNear[] = {"\vops_s_impact\snapnear3.wss", 1, 1, 150};
		supersonicCrackFar[] = {"", 3.16228, 1, 800};
};
	
	class B_762x51_noTracer : B_762x51_Ball {
		supersonicCrackNear[] = {"\vops_s_impact\snapnear1.wss", 1, 1, 150};
		supersonicCrackFar[] = {"\vops_s_impact\snapfar7622", 1, 1, 800};
};
	
	class B_762x54_Ball : BulletBase {
	supersonicCrackNear[] = {"\vops_s_impact\snapnear2.wss", 1, 1, 150};
		supersonicCrackFar[] = {"\vops_s_impact\snapfar7623", 1, 1, 800};
};
	
	class B_762x54_noTracer : B_762x54_Ball {
		supersonicCrackNear[] = {"\vops_s_impact\snapnear4.wss", 1, 1, 150};
		supersonicCrackFar[] = {"\vops_s_impact\snapfar762", 1, 1, 800};
};
	
	class B_77x56_Ball : BulletBase {
	};
	
	class B_127x99_Ball : BulletBase {
		supersonicCrackNear[] = {"\vops_s_impact\snap_near_50cal_s.wss", 1.0, 1, 150};
		supersonicCrackFar[] = {"\vops_s_impact\snapfardshk", 1, 1, 1000};
	};
	
	class B_127x99_Ball_noTracer : B_127x99_Ball {
		supersonicCrackNear[] = {"\vops_s_impact\snap_near_50cal_s.wss", 1.0, 1, 150};
		supersonicCrackFar[] = {"", 3.16228, 1, 1000};
	};
	
	class B_127x107_Ball : BulletBase {
	};
	
	class B_127x108_Ball : BulletBase {
	};
	
	class B_127x108_APHE : BulletBase {
	};
	
	class B_145x115_AP : BulletBase {
	};
	
	class B_20mm_AP : BulletBase {
		supersonicCrackNear[] = {"", 1, 1, 50};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_20mm_AA : BulletBase {
		soundDefault1[] = {"\vops_s_impact\gau8_imp1.wss", db0, 1, 300};
		soundDefault2[] = {"\vops_s_impact\gau8_imp3.wss", db0, 1, 300};
		soundDefault3[] = {"\vops_s_impact\gau8_imp4.wss", db0, 1, 300};
		soundDefault4[] = {"\vops_s_impact\gau8_imp5.wss", db0, 1, 300};
		soundDefault5[] = {"\vops_s_impact\gau8_imp6.wss", db0, 1, 300};
		soundGroundSoft1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGroundSoft2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGroundSoft3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGroundSoft4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGroundSoft5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGroundHard1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGroundHard2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGroundHard3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGroundHard4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGroundHard5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundMetal1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundMetal2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundMetal3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundMetal4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundMetal5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlass1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGlass2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGlass3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGlass4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGlass5[] = {"ca\sounds\weapons\hits\hit_glass_05", 0.177828, 1, 300};
		soundGlass6[] = {"ca\sounds\weapons\hits\hit_glass_06", 0.177828, 1, 300};
		soundGlass7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlass8[] = {"ca\sounds\weapons\hits\hit_glass_08", 0.177828, 1, 300};
		soundGlass9[] = {"ca\sounds\weapons\hits\hit_glass_09", 0.177828, 1, 300};
		soundGlass10[] = {"ca\sounds\weapons\hits\hit_glass_10", 0.177828, 1, 300};
		soundIron1[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlassArmored1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGlassArmored2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGlassArmored3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGlassArmored4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGlassArmored5[] = {"\vops_s_impact\gau8_imp6.wss", db0, 1, 300};
		soundGlassArmored6[] = {"ca\sounds\weapons\hits\hit_glass_armored_06", 0.177828, 1, 300};
		soundGlassArmored7[] = {"ca\sounds\weapons\hits\hit_glass_armored_07", 0.177828, 1, 300};
		soundGlassArmored8[] = {"ca\sounds\weapons\hits\hit_glass_armored_08", 0.177828, 1, 300};
		soundVehiclePlate1[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundWood1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundWood2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundWood3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundWood4[] = {"\vops_s_impact\gau8_imp5.wss", 0.005, 1, 200};
		soundWood5[] = {"ca\sounds\weapons\hits\hit_wood_05", 0.005, 1, 60};
		soundWood6[] = {"ca\sounds\weapons\hits\hit_wood_06", 0.005, 1, 60};
		soundWood7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundWood8[] = {"ca\sounds\weapons\hits\hit_wood_08", 0.005, 1, 60};
		soundWood9[] = {"\vops_s_impact\gau8_imp4.wss", 0.005, 1, 200};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.005, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.005, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.005, 1, 60};
		soundHitBody1[] = {"ca\sounds\weapons\hits\hit_body_01", 0.0177828, 1, 50};
		soundMetalPlate1[] = {"\vops_s_impact\gau8_imp5.wss", 0.562341, 1, 200};
		soundHitBuilding1[] = {"\vops_s_impact\gau8_imp3.wss", db-12, 1, 200};
		soundHitFoliage1[] = {"\vops_s_impact\gau8_imp1.wss", 0.177828, 1, 200};
		soundHitFoliage2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundHitFoliage3[] = {"\vops_s_impact\gau8_imp4.wss", 0.177828, 1, 200};
		soundHitFoliage4[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundPlastic1[] = {"\vops_s_impact\gau8_imp1.wss", 0.177828, 1, 200};
		soundConcrete1[] = {"\vops_s_impact\gau8_imp6.wss", 0.177828, 1, 200};
		soundRubber1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundRubber2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundRubber3[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundRubber4[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundRubber5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		hitGroundSoft[] = {"soundGroundSoft1", 0.2,"soundGroundSoft2", 0.2,"soundGroundSoft3", 0.2,"soundGroundSoft4", 0.2,"soundGroundSoft5", 0.2};
		hitGroundHard[] = {"soundGroundHard1", 0.2,"soundGroundHard2", 0.2,"soundGroundHard3", 0.2,"soundGroundHard4", 0.2,"soundGroundHard5", 0.2};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 1};
		hitIron[] = {"soundIron1", 1};
		hitBuilding[] = {"soundHitBuilding1", 1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.005, "soundWood5", 0.005, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.005, "soundWood12", 0.05};
		hitGlass[] = {"soundGlass1", 0.1, "soundGlass2", 0.1, "soundGlass3", 0.1, "soundGlass4", 0.1, "soundGlass5", 0.1, "soundGlass6", 0.1, "soundGlass7", 0.1, "soundGlass8", 0.1, "soundGlass9", 0.1, "soundGlass10", 0.1};
		hitGlassArmored[] = {"soundGlassArmored1", 0.125, "soundGlassArmored2", 0.125, "soundGlassArmored3", 0.125, "soundGlassArmored4", 0.125, "soundGlassArmored5", 0.125, "soundGlassArmored6", 0.125, "soundGlassArmored7", 0.125, "soundGlassArmored8", 0.125};
		hitConcrete[] = {"soundConcrete1", 1};
		hitRubber[] = {"soundRubber1", 0.2, "soundRubber2", 0.2, "soundRubber3", 0.2, "soundRubber4", 0.2, "soundRubber5", 0.2};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.2,"soundDefault2", 0.2,"soundDefault3", 0.2,"soundDefault4", 0.2,"soundDefault5", 0.2};
		hitMetal[] = {"soundMetal1", 1};
		hitMetalplate[] = {"soundMetalPlate1", 1};
		bulletFly1[] = {"ca\sounds\weapons\hits\bullet_by1", 1.0, 1, 50};
		bulletFly2[] = {"ca\sounds\weapons\hits\bullet_by2", 1.0, 1, 50};
		bulletFly3[] = {"ca\sounds\weapons\hits\bullet_by3", 1.0, 1, 50};
		bulletFly4[] = {"ca\sounds\weapons\hits\bullet_by4", 1.0, 1, 50};
		bulletFly5[] = {"ca\sounds\weapons\hits\bullet_by5", 1.0, 1, 50};
		bulletFly6[] = {"ca\sounds\weapons\hits\bullet_by6", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.166, "bulletFly2", 0.166, "bulletFly3", 0.166, "bulletFly4", 0.166, "bulletFly5", 0.166, "bulletFly6", 0.167};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_23mm_AA : BulletBase {
		soundDefault1[] = {"\vops_s_impact\gau8_imp1.wss", db0, 1, 300};
		soundDefault2[] = {"\vops_s_impact\gau8_imp3.wss", db0, 1, 300};
		soundDefault3[] = {"\vops_s_impact\gau8_imp4.wss", db0, 1, 300};
		soundDefault4[] = {"\vops_s_impact\gau8_imp5.wss", db0, 1, 300};
		soundDefault5[] = {"\vops_s_impact\gau8_imp6.wss", db0, 1, 300};
		soundGroundSoft1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGroundSoft2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGroundSoft3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGroundSoft4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGroundSoft5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGroundHard1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGroundHard2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGroundHard3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGroundHard4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGroundHard5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundMetal1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundMetal2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundMetal3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundMetal4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundMetal5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlass1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGlass2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGlass3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGlass4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGlass5[] = {"ca\sounds\weapons\hits\hit_glass_05", 0.177828, 1, 300};
		soundGlass6[] = {"ca\sounds\weapons\hits\hit_glass_06", 0.177828, 1, 300};
		soundGlass7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlass8[] = {"ca\sounds\weapons\hits\hit_glass_08", 0.177828, 1, 300};
		soundGlass9[] = {"ca\sounds\weapons\hits\hit_glass_09", 0.177828, 1, 300};
		soundGlass10[] = {"ca\sounds\weapons\hits\hit_glass_10", 0.177828, 1, 300};
		soundIron1[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlassArmored1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGlassArmored2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGlassArmored3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGlassArmored4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGlassArmored5[] = {"\vops_s_impact\gau8_imp6.wss", db0, 1, 300};
		soundGlassArmored6[] = {"ca\sounds\weapons\hits\hit_glass_armored_06", 0.177828, 1, 300};
		soundGlassArmored7[] = {"ca\sounds\weapons\hits\hit_glass_armored_07", 0.177828, 1, 300};
		soundGlassArmored8[] = {"ca\sounds\weapons\hits\hit_glass_armored_08", 0.177828, 1, 300};
		soundVehiclePlate1[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundWood1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundWood2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundWood3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundWood4[] = {"\vops_s_impact\gau8_imp5.wss", 0.005, 1, 200};
		soundWood5[] = {"ca\sounds\weapons\hits\hit_wood_05", 0.005, 1, 60};
		soundWood6[] = {"ca\sounds\weapons\hits\hit_wood_06", 0.005, 1, 60};
		soundWood7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundWood8[] = {"ca\sounds\weapons\hits\hit_wood_08", 0.005, 1, 60};
		soundWood9[] = {"\vops_s_impact\gau8_imp4.wss", 0.005, 1, 200};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.005, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.005, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.005, 1, 60};
		soundHitBody1[] = {"ca\sounds\weapons\hits\hit_body_01", 0.0177828, 1, 50};
		soundMetalPlate1[] = {"\vops_s_impact\gau8_imp5.wss", 0.562341, 1, 200};
		soundHitBuilding1[] = {"\vops_s_impact\gau8_imp3.wss", db-12, 1, 200};
		soundHitFoliage1[] = {"\vops_s_impact\gau8_imp1.wss", 0.177828, 1, 200};
		soundHitFoliage2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundHitFoliage3[] = {"\vops_s_impact\gau8_imp4.wss", 0.177828, 1, 200};
		soundHitFoliage4[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundPlastic1[] = {"\vops_s_impact\gau8_imp1.wss", 0.177828, 1, 200};
		soundConcrete1[] = {"\vops_s_impact\gau8_imp6.wss", 0.177828, 1, 200};
		soundRubber1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundRubber2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundRubber3[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundRubber4[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundRubber5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		hitGroundSoft[] = {"soundGroundSoft1", 0.2,"soundGroundSoft2", 0.2,"soundGroundSoft3", 0.2,"soundGroundSoft4", 0.2,"soundGroundSoft5", 0.2};
		hitGroundHard[] = {"soundGroundHard1", 0.2,"soundGroundHard2", 0.2,"soundGroundHard3", 0.2,"soundGroundHard4", 0.2,"soundGroundHard5", 0.2};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 1};
		hitIron[] = {"soundIron1", 1};
		hitBuilding[] = {"soundHitBuilding1", 1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.005, "soundWood5", 0.005, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.005, "soundWood12", 0.05};
		hitGlass[] = {"soundGlass1", 0.1, "soundGlass2", 0.1, "soundGlass3", 0.1, "soundGlass4", 0.1, "soundGlass5", 0.1, "soundGlass6", 0.1, "soundGlass7", 0.1, "soundGlass8", 0.1, "soundGlass9", 0.1, "soundGlass10", 0.1};
		hitGlassArmored[] = {"soundGlassArmored1", 0.125, "soundGlassArmored2", 0.125, "soundGlassArmored3", 0.125, "soundGlassArmored4", 0.125, "soundGlassArmored5", 0.125, "soundGlassArmored6", 0.125, "soundGlassArmored7", 0.125, "soundGlassArmored8", 0.125};
		hitConcrete[] = {"soundConcrete1", 1};
		hitRubber[] = {"soundRubber1", 0.2, "soundRubber2", 0.2, "soundRubber3", 0.2, "soundRubber4", 0.2, "soundRubber5", 0.2};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.2,"soundDefault2", 0.2,"soundDefault3", 0.2,"soundDefault4", 0.2,"soundDefault5", 0.2};
		hitMetal[] = {"soundMetal1", 1};
		hitMetalplate[] = {"soundMetalPlate1", 1};
		bulletFly1[] = {"ca\sounds\weapons\hits\bullet_by1", 1.0, 1, 50};
		bulletFly2[] = {"ca\sounds\weapons\hits\bullet_by2", 1.0, 1, 50};
		bulletFly3[] = {"ca\sounds\weapons\hits\bullet_by3", 1.0, 1, 50};
		bulletFly4[] = {"ca\sounds\weapons\hits\bullet_by4", 1.0, 1, 50};
		bulletFly5[] = {"ca\sounds\weapons\hits\bullet_by5", 1.0, 1, 50};
		bulletFly6[] = {"ca\sounds\weapons\hits\bullet_by6", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.166, "bulletFly2", 0.166, "bulletFly3", 0.166, "bulletFly4", 0.166, "bulletFly5", 0.166, "bulletFly6", 0.167};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_23mm_HE : B_23mm_AA {
	};
	
	class B_23mm_AP : B_23mm_AA {
		supersonicCrackNear[] = {"\vops_s_impact\bulletcrack4.wss", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_23mm_APHE : BulletBase {
		soundDefault1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundDefault2[] = {"\vops_s_impact\30mmHE_imp2.wss", db0, 1, 200};
		soundDefault3[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGroundSoft1[] = {"\vops_s_impact\30mmHE_imp1.wss", db+10, 1, 200};
		soundGroundSoft2[] = {"\vops_s_impact\30mmHE_imp2.wss", db+10, 1, 200};
		soundGroundSoft3[] = {"\vops_s_impact\30mmHE_imp3.wss", db+10, 1, 200};
		soundGroundHard1[] = {"\vops_s_impact\30mmHE_imp4.wss", db+10, 1, 200};
		soundGroundHard2[] = {"\vops_s_impact\30mmHE_imp2.wss", db+10, 1, 200};
		soundGroundHard3[] = {"\vops_s_impact\30mmHE_imp3.wss", db+10, 1, 200};
		soundMetal1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundMetal2[] = {"\vops_s_impact\30mmHE_imp2.wss", db0, 1, 200};
		soundMetal3[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGlass1[] = {"ca\sounds\weapons\hits\hit_glass_01", 0.177828, 1, 50};
		soundGlass2[] = {"ca\sounds\weapons\hits\hit_glass_02", 0.177828, 1, 50};
		soundGlass3[] = {"ca\sounds\weapons\hits\hit_glass_03", 0.177828, 1, 50};
		soundGlass4[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundGlass5[] = {"ca\sounds\weapons\hits\hit_glass_05", 0.177828, 1, 50};
		soundGlass6[] = {"ca\sounds\weapons\hits\hit_glass_06", 0.177828, 1, 50};
		soundGlass7[] = {"ca\sounds\weapons\hits\hit_glass_07", 0.177828, 1, 50};
		soundGlass8[] = {"ca\sounds\weapons\hits\hit_glass_08", 0.177828, 1, 50};
		soundGlass9[] = {"ca\sounds\weapons\hits\hit_glass_09", 0.177828, 1, 50};
		soundGlass10[] = {"ca\sounds\weapons\hits\hit_glass_10", 0.177828, 1, 50};
		soundIron1[] = {"\vops_s_impact\30mmHE_imp4.wss", db0, 1, 200};
		soundGlassArmored1[] = {"ca\sounds\weapons\hits\hit_glass_armored_01", 0.177828, 1, 60};
		soundGlassArmored2[] = {"ca\sounds\weapons\hits\hit_glass_armored_02", 0.177828, 1, 60};
		soundGlassArmored3[] = {"ca\sounds\weapons\hits\hit_glass_armored_03", 0.177828, 1, 60};
		soundGlassArmored4[] = {"ca\sounds\weapons\hits\hit_glass_armored_04", 0.177828, 1, 60};
		soundGlassArmored5[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGlassArmored6[] = {"ca\sounds\weapons\hits\hit_glass_armored_06", 0.177828, 1, 60};
		soundGlassArmored7[] = {"ca\sounds\weapons\hits\hit_glass_armored_07", 0.177828, 1, 60};
		soundGlassArmored8[] = {"ca\sounds\weapons\hits\hit_glass_armored_08", 0.177828, 1, 60};
		soundVehiclePlate1[] = {"ca\sounds\weapons\hits\hit_vehicle_plate_01", 0.562341, 1, 90};
		soundWood1[] = {"ca\sounds\weapons\hits\hit_wood_01", 0.005, 1, 60};
		soundWood2[] = {"ca\sounds\weapons\hits\hit_wood_02", 0.005, 1, 60};
		soundWood3[] = {"ca\sounds\weapons\hits\hit_wood_03", 0.005, 1, 60};
		soundWood4[] = {"\vops_s_impact\gau8_imp4.wss", 0.005, 1, 200};
		soundWood5[] = {"ca\sounds\weapons\hits\hit_wood_05", 0.005, 1, 60};
		soundWood6[] = {"ca\sounds\weapons\hits\hit_wood_06", 0.005, 1, 60};
		soundWood7[] = {"ca\sounds\weapons\hits\hit_wood_07", 0.005, 1, 60};
		soundWood8[] = {"ca\sounds\weapons\hits\hit_wood_08", 0.005, 1, 60};
		soundWood9[] = {"\vops_s_impact\30mmHE_imp3.wss", 0.005, 1, 200};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.005, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.005, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.005, 1, 60};
		soundHitBody1[] = {"ca\sounds\weapons\hits\hit_body_01", 0.0177828, 1, 50};
		soundMetalPlate1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.562341, 1, 200};
		soundHitBuilding1[] = {"\vops_s_impact\30mmHE_imp3.wss", db-12, 1, 200};
		soundHitFoliage1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.177828, 1, 200};
		soundHitFoliage2[] = {"ca\sounds\weapons\hits\hit_grass_02", 0.177828, 1, 50};
		soundHitFoliage3[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.177828, 1, 200};
		soundHitFoliage4[] = {"ca\sounds\weapons\hits\hit_grass_04", 0.177828, 1, 50};
		soundPlastic1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.177828, 1, 200};
		soundConcrete1[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.177828, 1, 200};
		soundRubber1[] = {"ca\sounds\weapons\hits\hit_Rubber_01", 0.005, 1, 50};
		soundRubber2[] = {"ca\sounds\weapons\hits\hit_Rubber_02", 0.005, 1, 50};
		soundRubber3[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.005, 1, 200};
		soundRubber4[] = {"ca\sounds\weapons\hits\hit_Rubber_04", 0.005, 1, 50};
		soundRubber5[] = {"ca\sounds\weapons\hits\hit_Rubber_05", 0.005, 1, 50};
		hitGroundSoft[] = {"soundGroundSoft1", 0.33,"soundGroundSoft2", 0.33,"soundGroundSoft3", 0.33};
		hitGroundHard[] = {"soundGroundHard1", 0.33,"soundGroundHard2", 0.33,"soundGroundHard3", 0.33};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 1};
		hitIron[] = {"soundIron1", 1};
		hitBuilding[] = {"soundHitBuilding1", 1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.005, "soundWood5", 0.005, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.005, "soundWood12", 0.05};
		hitGlass[] = {"soundGlass1", 0.1, "soundGlass2", 0.1, "soundGlass3", 0.1, "soundGlass4", 0.1, "soundGlass5", 0.1, "soundGlass6", 0.1, "soundGlass7", 0.1, "soundGlass8", 0.1, "soundGlass9", 0.1, "soundGlass10", 0.1};
		hitGlassArmored[] = {"soundGlassArmored1", 0.125, "soundGlassArmored2", 0.125, "soundGlassArmored3", 0.125, "soundGlassArmored4", 0.125, "soundGlassArmored5", 0.125, "soundGlassArmored6", 0.125, "soundGlassArmored7", 0.125, "soundGlassArmored8", 0.125};
		hitConcrete[] = {"soundConcrete1", 1};
		hitRubber[] = {"soundRubber1", 0.2, "soundRubber2", 0.2, "soundRubber3", 0.2, "soundRubber4", 0.2, "soundRubber5", 0.2};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.33,"soundDefault2", 0.33,"soundDefault3", 0.33};
		hitMetal[] = {"soundMetal1", 1};
		hitMetalplate[] = {"soundMetalPlate1", 1};
		bulletFly1[] = {"ca\sounds\weapons\hits\bullet_by1", 1.0, 1, 50};
		bulletFly2[] = {"ca\sounds\weapons\hits\bullet_by2", 1.0, 1, 50};
		bulletFly3[] = {"ca\sounds\weapons\hits\bullet_by3", 1.0, 1, 50};
		bulletFly4[] = {"ca\sounds\weapons\hits\bullet_by4", 1.0, 1, 50};
		bulletFly5[] = {"ca\sounds\weapons\hits\bullet_by5", 1.0, 1, 50};
		bulletFly6[] = {"ca\sounds\weapons\hits\bullet_by6", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.166, "bulletFly2", 0.166, "bulletFly3", 0.166, "bulletFly4", 0.166, "bulletFly5", 0.166, "bulletFly6", 0.167};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_25mm_HE : BulletBase {
		soundDefault1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundDefault2[] = {"\vops_s_impact\30mmHE_imp2.wss", db0, 1, 200};
		soundDefault3[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGroundSoft1[] = {"\vops_s_impact\30mmHE_imp1.wss", db+10, 1, 200};
		soundGroundSoft2[] = {"\vops_s_impact\30mmHE_imp2.wss", db+10, 1, 200};
		soundGroundSoft3[] = {"\vops_s_impact\30mmHE_imp3.wss", db+10, 1, 200};
		soundGroundHard1[] = {"\vops_s_impact\30mmHE_imp4.wss", db+10, 1, 200};
		soundGroundHard2[] = {"\vops_s_impact\30mmHE_imp2.wss", db+10, 1, 200};
		soundGroundHard3[] = {"\vops_s_impact\30mmHE_imp3.wss", db+10, 1, 200};
		soundMetal1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundMetal2[] = {"\vops_s_impact\30mmHE_imp2.wss", db0, 1, 200};
		soundMetal3[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGlass1[] = {"ca\sounds\weapons\hits\hit_glass_01", 0.177828, 1, 50};
		soundGlass2[] = {"ca\sounds\weapons\hits\hit_glass_02", 0.177828, 1, 50};
		soundGlass3[] = {"ca\sounds\weapons\hits\hit_glass_03", 0.177828, 1, 50};
		soundGlass4[] = {"\vops_s_impact\30mmHE_imp4.wss", db0, 1, 200};
		soundGlass5[] = {"ca\sounds\weapons\hits\hit_glass_05", 0.177828, 1, 50};
		soundGlass6[] = {"ca\sounds\weapons\hits\hit_glass_06", 0.177828, 1, 50};
		soundGlass7[] = {"ca\sounds\weapons\hits\hit_glass_07", 0.177828, 1, 50};
		soundGlass8[] = {"ca\sounds\weapons\hits\hit_glass_08", 0.177828, 1, 50};
		soundGlass9[] = {"ca\sounds\weapons\hits\hit_glass_09", 0.177828, 1, 50};
		soundGlass10[] = {"ca\sounds\weapons\hits\hit_glass_10", 0.177828, 1, 50};
		soundIron1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundGlassArmored1[] = {"ca\sounds\weapons\hits\hit_glass_armored_01", 0.177828, 1, 60};
		soundGlassArmored2[] = {"ca\sounds\weapons\hits\hit_glass_armored_02", 0.177828, 1, 60};
		soundGlassArmored3[] = {"ca\sounds\weapons\hits\hit_glass_armored_03", 0.177828, 1, 60};
		soundGlassArmored4[] = {"ca\sounds\weapons\hits\hit_glass_armored_04", 0.177828, 1, 60};
		soundGlassArmored5[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGlassArmored6[] = {"ca\sounds\weapons\hits\hit_glass_armored_06", 0.177828, 1, 60};
		soundGlassArmored7[] = {"ca\sounds\weapons\hits\hit_glass_armored_07", 0.177828, 1, 60};
		soundGlassArmored8[] = {"ca\sounds\weapons\hits\hit_glass_armored_08", 0.177828, 1, 60};
		soundVehiclePlate1[] = {"ca\sounds\weapons\hits\hit_vehicle_plate_01", 0.562341, 1, 90};
		soundWood1[] = {"ca\sounds\weapons\hits\hit_wood_01", 0.005, 1, 60};
		soundWood2[] = {"ca\sounds\weapons\hits\hit_wood_02", 0.005, 1, 60};
		soundWood3[] = {"ca\sounds\weapons\hits\hit_wood_03", 0.005, 1, 60};
		soundWood4[] = {"\vops_s_impact\gau8_imp1.wss", 0.005, 1, 200};
		soundWood5[] = {"ca\sounds\weapons\hits\hit_wood_05", 0.005, 1, 60};
		soundWood6[] = {"ca\sounds\weapons\hits\hit_wood_06", 0.005, 1, 60};
		soundWood7[] = {"ca\sounds\weapons\hits\hit_wood_07", 0.005, 1, 60};
		soundWood8[] = {"ca\sounds\weapons\hits\hit_wood_08", 0.005, 1, 60};
		soundWood9[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.005, 1, 200};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.005, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.005, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.005, 1, 60};
		soundHitBody1[] = {"ca\sounds\weapons\hits\hit_body_01", 0.0177828, 1, 50};
		soundMetalPlate1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.562341, 1, 200};
		soundHitBuilding1[] = {"\vops_s_impact\30mmHE_imp1.wss", db-12, 1, 200};
		soundHitFoliage1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.177828, 1, 200};
		soundHitFoliage2[] = {"ca\sounds\weapons\hits\hit_grass_02", 0.177828, 1, 50};
		soundHitFoliage3[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.177828, 1, 200};
		soundHitFoliage4[] = {"ca\sounds\weapons\hits\hit_grass_04", 0.177828, 1, 50};
		soundPlastic1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.177828, 1, 200};
		soundConcrete1[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.177828, 1, 200};
		soundRubber1[] = {"ca\sounds\weapons\hits\hit_Rubber_01", 0.005, 1, 50};
		soundRubber2[] = {"ca\sounds\weapons\hits\hit_Rubber_02", 0.005, 1, 50};
		soundRubber3[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.005, 1, 200};
		soundRubber4[] = {"ca\sounds\weapons\hits\hit_Rubber_04", 0.005, 1, 50};
		soundRubber5[] = {"ca\sounds\weapons\hits\hit_Rubber_05", 0.005, 1, 50};
		hitGroundSoft[] = {"soundGroundSoft1", 0.33,"soundGroundSoft2", 0.33,"soundGroundSoft3", 0.33};
		hitGroundHard[] = {"soundGroundHard1", 0.33,"soundGroundHard2", 0.33,"soundGroundHard3", 0.33};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 1};
		hitIron[] = {"soundIron1", 1};
		hitBuilding[] = {"soundHitBuilding1", 1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.005, "soundWood5", 0.005, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.005, "soundWood12", 0.05};
		hitGlass[] = {"soundGlass1", 0.1, "soundGlass2", 0.1, "soundGlass3", 0.1, "soundGlass4", 0.1, "soundGlass5", 0.1, "soundGlass6", 0.1, "soundGlass7", 0.1, "soundGlass8", 0.1, "soundGlass9", 0.1, "soundGlass10", 0.1};
		hitGlassArmored[] = {"soundGlassArmored1", 0.125, "soundGlassArmored2", 0.125, "soundGlassArmored3", 0.125, "soundGlassArmored4", 0.125, "soundGlassArmored5", 0.125, "soundGlassArmored6", 0.125, "soundGlassArmored7", 0.125, "soundGlassArmored8", 0.125};
		hitConcrete[] = {"soundConcrete1", 1};
		hitRubber[] = {"soundRubber1", 0.2, "soundRubber2", 0.2, "soundRubber3", 0.2, "soundRubber4", 0.2, "soundRubber5", 0.2};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.33,"soundDefault2", 0.33,"soundDefault3", 0.33};
		hitMetal[] = {"soundMetal1", 1};
		hitMetalplate[] = {"soundMetalPlate1", 1};
		bulletFly1[] = {"ca\sounds\weapons\hits\bullet_by1", 1.0, 1, 50};
		bulletFly2[] = {"ca\sounds\weapons\hits\bullet_by2", 1.0, 1, 50};
		bulletFly3[] = {"ca\sounds\weapons\hits\bullet_by3", 1.0, 1, 50};
		bulletFly4[] = {"ca\sounds\weapons\hits\bullet_by4", 1.0, 1, 50};
		bulletFly5[] = {"ca\sounds\weapons\hits\bullet_by5", 1.0, 1, 50};
		bulletFly6[] = {"ca\sounds\weapons\hits\bullet_by6", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.166, "bulletFly2", 0.166, "bulletFly3", 0.166, "bulletFly4", 0.166, "bulletFly5", 0.166, "bulletFly6", 0.167};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_25mm_HEI : B_25mm_HE {
	};
	
	class B_25mm_APDS : BulletBase {
		supersonicCrackNear[] = {"\vops_s_impact\bulletcrack4.wss", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_30mm_AP : BulletBase {
		supersonicCrackNear[] = {"\vops_s_impact\bulletcrack4.wss", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_30mmA10_AP : BulletBase {
		soundDefault1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundDefault2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundDefault3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundDefault4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundDefault5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundGroundSoft1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundGroundSoft2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundGroundSoft3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundGroundSoft4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundGroundSoft5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundGroundHard1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundGroundHard2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundGroundHard3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundGroundHard4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundGroundHard5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundMetal1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundMetal2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundMetal3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundMetal4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundMetal5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundGlass1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundGlass2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundGlass3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundGlass4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundGlass5[] = {"ca\sounds\weapons\hits\hit_glass_05", 0.177828, 1, 300};
		soundGlass6[] = {"ca\sounds\weapons\hits\hit_glass_06", 0.177828, 1, 300};
		soundGlass7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundGlass8[] = {"ca\sounds\weapons\hits\hit_glass_08", 0.177828, 1, 300};
		soundGlass9[] = {"ca\sounds\weapons\hits\hit_glass_09", 0.177828, 1, 300};
		soundGlass10[] = {"ca\sounds\weapons\hits\hit_glass_10", 0.177828, 1, 300};
		soundIron1[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundGlassArmored1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundGlassArmored2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundGlassArmored3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundGlassArmored4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundGlassArmored5[] = {"\vops_s_impact\gau8_imp6.wss", db0, 1, 1500};
		soundGlassArmored6[] = {"ca\sounds\weapons\hits\hit_glass_armored_06", 0.177828, 1, 300};
		soundGlassArmored7[] = {"ca\sounds\weapons\hits\hit_glass_armored_07", 0.177828, 1, 300};
		soundGlassArmored8[] = {"ca\sounds\weapons\hits\hit_glass_armored_08", 0.177828, 1, 300};
		soundVehiclePlate1[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundWood1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundWood2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundWood3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundWood4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundWood5[] = {"ca\sounds\weapons\hits\hit_wood_05", 0.005, 1, 60};
		soundWood6[] = {"ca\sounds\weapons\hits\hit_wood_06", 0.005, 1, 60};
		soundWood7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundWood8[] = {"ca\sounds\weapons\hits\hit_wood_08", 0.005, 1, 60};
		soundWood9[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.005, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.005, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.005, 1, 60};
		soundHitBody1[] = {"ca\sounds\weapons\hits\hit_body_01", 0.0177828, 1, 50};
		soundMetalPlate1[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundHitBuilding1[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundHitFoliage1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundHitFoliage2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundHitFoliage3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundHitFoliage4[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundPlastic1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundConcrete1[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		soundRubber1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 1500};
		soundRubber2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 1500};
		soundRubber3[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 1500};
		soundRubber4[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 1500};
		soundRubber5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 1500};
		hitGroundSoft[] = {"soundGroundSoft1", 0.2,"soundGroundSoft2", 0.2,"soundGroundSoft3", 0.2,"soundGroundSoft4", 0.2,"soundGroundSoft5", 0.2};
		hitGroundHard[] = {"soundGroundHard1", 0.2,"soundGroundHard2", 0.2,"soundGroundHard3", 0.2,"soundGroundHard4", 0.2,"soundGroundHard5", 0.2};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 1};
		hitIron[] = {"soundIron1", 1};
		hitBuilding[] = {"soundHitBuilding1", 1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.005, "soundWood5", 0.005, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.005, "soundWood12", 0.05};
		hitGlass[] = {"soundGlass1", 0.1, "soundGlass2", 0.1, "soundGlass3", 0.1, "soundGlass4", 0.1, "soundGlass5", 0.1, "soundGlass6", 0.1, "soundGlass7", 0.1, "soundGlass8", 0.1, "soundGlass9", 0.1, "soundGlass10", 0.1};
		hitGlassArmored[] = {"soundGlassArmored1", 0.125, "soundGlassArmored2", 0.125, "soundGlassArmored3", 0.125, "soundGlassArmored4", 0.125, "soundGlassArmored5", 0.125, "soundGlassArmored6", 0.125, "soundGlassArmored7", 0.125, "soundGlassArmored8", 0.125};
		hitConcrete[] = {"soundConcrete1", 1};
		hitRubber[] = {"soundRubber1", 0.2, "soundRubber2", 0.2, "soundRubber3", 0.2, "soundRubber4", 0.2, "soundRubber5", 0.2};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.2,"soundDefault2", 0.2,"soundDefault3", 0.2,"soundDefault4", 0.2,"soundDefault5", 0.2};
		hitMetal[] = {"soundMetal1", 1};
		hitMetalplate[] = {"soundMetalPlate1", 1};
		bulletFly1[] = {"", 1.0, 1, 50};
		bulletFly2[] = {"", 1.0, 1, 50};
		bulletFly3[] = {"", 1.0, 1, 50};
		bulletFly4[] = {"", 1.0, 1, 50};
		bulletFly5[] = {"", 1.0, 1, 50};
		bulletFly6[] = {"", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.166, "bulletFly2", 0.166, "bulletFly3", 0.166, "bulletFly4", 0.166, "bulletFly5", 0.166, "bulletFly6", 0.167};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_30mm_HE : BulletBase {
		soundDefault1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundDefault2[] = {"\vops_s_impact\30mmHE_imp2.wss", db0, 1, 200};
		soundDefault3[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGroundSoft1[] = {"\vops_s_impact\30mmHE_imp1.wss", db+10, 1, 200};
		soundGroundSoft2[] = {"\vops_s_impact\30mmHE_imp2.wss", db+10, 1, 200};
		soundGroundSoft3[] = {"\vops_s_impact\30mmHE_imp3.wss", db+10, 1, 200};
		soundGroundHard1[] = {"\vops_s_impact\30mmHE_imp1.wss", db+10, 1, 200};
		soundGroundHard2[] = {"\vops_s_impact\30mmHE_imp2.wss", db+10, 1, 200};
		soundGroundHard3[] = {"\vops_s_impact\30mmHE_imp3.wss", db+10, 1, 200};
		soundMetal1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundMetal2[] = {"\vops_s_impact\30mmHE_imp2.wss", db0, 1, 200};
		soundMetal3[] = {"\vops_s_impact\30mmHE_imp3.wss", db0, 1, 200};
		soundGlass1[] = {"ca\sounds\weapons\hits\hit_glass_01", 0.177828, 1, 50};
		soundGlass2[] = {"ca\sounds\weapons\hits\hit_glass_02", 0.177828, 1, 50};
		soundGlass3[] = {"ca\sounds\weapons\hits\hit_glass_03", 0.177828, 1, 50};
		soundGlass4[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundGlass5[] = {"ca\sounds\weapons\hits\hit_glass_05", 0.177828, 1, 50};
		soundGlass6[] = {"ca\sounds\weapons\hits\hit_glass_06", 0.177828, 1, 50};
		soundGlass7[] = {"ca\sounds\weapons\hits\hit_glass_07", 0.177828, 1, 50};
		soundGlass8[] = {"ca\sounds\weapons\hits\hit_glass_08", 0.177828, 1, 50};
		soundGlass9[] = {"ca\sounds\weapons\hits\hit_glass_09", 0.177828, 1, 50};
		soundGlass10[] = {"ca\sounds\weapons\hits\hit_glass_10", 0.177828, 1, 50};
		soundIron1[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundGlassArmored1[] = {"ca\sounds\weapons\hits\hit_glass_armored_01", 0.177828, 1, 60};
		soundGlassArmored2[] = {"ca\sounds\weapons\hits\hit_glass_armored_02", 0.177828, 1, 60};
		soundGlassArmored3[] = {"ca\sounds\weapons\hits\hit_glass_armored_03", 0.177828, 1, 60};
		soundGlassArmored4[] = {"ca\sounds\weapons\hits\hit_glass_armored_04", 0.177828, 1, 60};
		soundGlassArmored5[] = {"\vops_s_impact\30mmHE_imp1.wss", db0, 1, 200};
		soundGlassArmored6[] = {"ca\sounds\weapons\hits\hit_glass_armored_06", 0.177828, 1, 60};
		soundGlassArmored7[] = {"ca\sounds\weapons\hits\hit_glass_armored_07", 0.177828, 1, 60};
		soundGlassArmored8[] = {"ca\sounds\weapons\hits\hit_glass_armored_08", 0.177828, 1, 60};
		soundVehiclePlate1[] = {"ca\sounds\weapons\hits\hit_vehicle_plate_01", 0.562341, 1, 90};
		soundWood1[] = {"ca\sounds\weapons\hits\hit_wood_01", 0.005, 1, 60};
		soundWood2[] = {"ca\sounds\weapons\hits\hit_wood_02", 0.005, 1, 60};
		soundWood3[] = {"ca\sounds\weapons\hits\hit_wood_03", 0.005, 1, 60};
		soundWood4[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.005, 1, 200};
		soundWood5[] = {"ca\sounds\weapons\hits\hit_wood_05", 0.005, 1, 60};
		soundWood6[] = {"ca\sounds\weapons\hits\hit_wood_06", 0.005, 1, 60};
		soundWood7[] = {"ca\sounds\weapons\hits\hit_wood_07", 0.005, 1, 60};
		soundWood8[] = {"ca\sounds\weapons\hits\hit_wood_08", 0.005, 1, 60};
		soundWood9[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.005, 1, 200};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.005, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.005, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.005, 1, 60};
		soundHitBody1[] = {"ca\sounds\weapons\hits\hit_body_01", 0.0177828, 1, 50};
		soundMetalPlate1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.562341, 1, 200};
		soundHitBuilding1[] = {"\vops_s_impact\30mmHE_imp1.wss", db-12, 1, 200};
		soundHitFoliage1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.177828, 1, 200};
		soundHitFoliage2[] = {"ca\sounds\weapons\hits\hit_grass_02", 0.177828, 1, 50};
		soundHitFoliage3[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.177828, 1, 200};
		soundHitFoliage4[] = {"ca\sounds\weapons\hits\hit_grass_04", 0.177828, 1, 50};
		soundPlastic1[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.177828, 1, 200};
		soundConcrete1[] = {"\vops_s_impact\30mmHE_imp2.wss", 0.177828, 1, 200};
		soundRubber1[] = {"ca\sounds\weapons\hits\hit_Rubber_01", 0.005, 1, 50};
		soundRubber2[] = {"ca\sounds\weapons\hits\hit_Rubber_02", 0.005, 1, 50};
		soundRubber3[] = {"\vops_s_impact\30mmHE_imp1.wss", 0.005, 1, 200};
		soundRubber4[] = {"ca\sounds\weapons\hits\hit_Rubber_04", 0.005, 1, 50};
		soundRubber5[] = {"ca\sounds\weapons\hits\hit_Rubber_05", 0.005, 1, 50};
		hitGroundSoft[] = {"soundGroundSoft1", 0.33,"soundGroundSoft2", 0.33,"soundGroundSoft3", 0.33};
		hitGroundHard[] = {"soundGroundHard1", 0.33,"soundGroundHard2", 0.33,"soundGroundHard3", 0.33};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 1};
		hitIron[] = {"soundIron1", 1};
		hitBuilding[] = {"soundHitBuilding1", 1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.005, "soundWood5", 0.005, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.005, "soundWood12", 0.05};
		hitGlass[] = {"soundGlass1", 0.1, "soundGlass2", 0.1, "soundGlass3", 0.1, "soundGlass4", 0.1, "soundGlass5", 0.1, "soundGlass6", 0.1, "soundGlass7", 0.1, "soundGlass8", 0.1, "soundGlass9", 0.1, "soundGlass10", 0.1};
		hitGlassArmored[] = {"soundGlassArmored1", 0.125, "soundGlassArmored2", 0.125, "soundGlassArmored3", 0.125, "soundGlassArmored4", 0.125, "soundGlassArmored5", 0.125, "soundGlassArmored6", 0.125, "soundGlassArmored7", 0.125, "soundGlassArmored8", 0.125};
		hitConcrete[] = {"soundConcrete1", 1};
		hitRubber[] = {"soundRubber1", 0.2, "soundRubber2", 0.2, "soundRubber3", 0.2, "soundRubber4", 0.2, "soundRubber5", 0.2};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.33,"soundDefault2", 0.33,"soundDefault3", 0.33};
		hitMetal[] = {"soundMetal1", 1};
		hitMetalplate[] = {"soundMetalPlate1", 1};
		bulletFly1[] = {"ca\sounds\weapons\hits\bullet_by1", 1.0, 1, 50};
		bulletFly2[] = {"ca\sounds\weapons\hits\bullet_by2", 1.0, 1, 50};
		bulletFly3[] = {"ca\sounds\weapons\hits\bullet_by3", 1.0, 1, 50};
		bulletFly4[] = {"ca\sounds\weapons\hits\bullet_by4", 1.0, 1, 50};
		bulletFly5[] = {"ca\sounds\weapons\hits\bullet_by5", 1.0, 1, 50};
		bulletFly6[] = {"ca\sounds\weapons\hits\bullet_by6", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.166, "bulletFly2", 0.166, "bulletFly3", 0.166, "bulletFly4", 0.166, "bulletFly5", 0.166, "bulletFly6", 0.167};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class B_30mm_AA : BulletBase {
		soundDefault1[] = {"\vops_s_impact\gau8_imp1.wss", db0, 1, 300};
		soundDefault2[] = {"\vops_s_impact\gau8_imp3.wss", db0, 1, 300};
		soundDefault3[] = {"\vops_s_impact\gau8_imp4.wss", db0, 1, 300};
		soundDefault4[] = {"\vops_s_impact\gau8_imp5.wss", db0, 1, 300};
		soundDefault5[] = {"\vops_s_impact\gau8_imp6.wss", db0, 1, 300};
		soundGroundSoft1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGroundSoft2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGroundSoft3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGroundSoft4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGroundSoft5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGroundHard1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGroundHard2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGroundHard3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGroundHard4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGroundHard5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundMetal1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundMetal2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundMetal3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundMetal4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundMetal5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlass1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGlass2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGlass3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGlass4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGlass5[] = {"ca\sounds\weapons\hits\hit_glass_05", 0.177828, 1, 300};
		soundGlass6[] = {"ca\sounds\weapons\hits\hit_glass_06", 0.177828, 1, 300};
		soundGlass7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlass8[] = {"ca\sounds\weapons\hits\hit_glass_08", 0.177828, 1, 300};
		soundGlass9[] = {"ca\sounds\weapons\hits\hit_glass_09", 0.177828, 1, 300};
		soundGlass10[] = {"ca\sounds\weapons\hits\hit_glass_10", 0.177828, 1, 300};
		soundIron1[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundGlassArmored1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundGlassArmored2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundGlassArmored3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundGlassArmored4[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundGlassArmored5[] = {"\vops_s_impact\gau8_imp6.wss", db0, 1, 300};
		soundGlassArmored6[] = {"ca\sounds\weapons\hits\hit_glass_armored_06", 0.177828, 1, 300};
		soundGlassArmored7[] = {"ca\sounds\weapons\hits\hit_glass_armored_07", 0.177828, 1, 300};
		soundGlassArmored8[] = {"ca\sounds\weapons\hits\hit_glass_armored_08", 0.177828, 1, 300};
		soundVehiclePlate1[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundWood1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundWood2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundWood3[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundWood4[] = {"\vops_s_impact\gau8_imp5.wss", 0.005, 1, 200};
		soundWood5[] = {"ca\sounds\weapons\hits\hit_wood_05", 0.005, 1, 60};
		soundWood6[] = {"ca\sounds\weapons\hits\hit_wood_06", 0.005, 1, 60};
		soundWood7[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundWood8[] = {"ca\sounds\weapons\hits\hit_wood_08", 0.005, 1, 60};
		soundWood9[] = {"\vops_s_impact\gau8_imp4.wss", 0.005, 1, 200};
		soundWood10[] = {"ca\sounds\weapons\hits\rico_hit_wood_02", 0.005, 1, 60};
		soundWood11[] = {"ca\sounds\weapons\hits\rico_hit_wood_03", 0.005, 1, 60};
		soundWood12[] = {"ca\sounds\weapons\hits\rico_hit_wood_04", 0.005, 1, 60};
		soundHitBody1[] = {"ca\sounds\weapons\hits\hit_body_01", 0.0177828, 1, 50};
		soundMetalPlate1[] = {"\vops_s_impact\gau8_imp5.wss", 0.562341, 1, 200};
		soundHitBuilding1[] = {"\vops_s_impact\gau8_imp3.wss", db-12, 1, 200};
		soundHitFoliage1[] = {"\vops_s_impact\gau8_imp1.wss", 0.177828, 1, 200};
		soundHitFoliage2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundHitFoliage3[] = {"\vops_s_impact\gau8_imp4.wss", 0.177828, 1, 200};
		soundHitFoliage4[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		soundPlastic1[] = {"\vops_s_impact\gau8_imp1.wss", 0.177828, 1, 200};
		soundConcrete1[] = {"\vops_s_impact\gau8_imp6.wss", 0.177828, 1, 200};
		soundRubber1[] = {"\vops_s_impact\gau8_imp1.wss", db+10, 1, 300};
		soundRubber2[] = {"\vops_s_impact\gau8_imp3.wss", db+10, 1, 300};
		soundRubber3[] = {"\vops_s_impact\gau8_imp5.wss", db+10, 1, 300};
		soundRubber4[] = {"\vops_s_impact\gau8_imp4.wss", db+10, 1, 300};
		soundRubber5[] = {"\vops_s_impact\gau8_imp6.wss", db+10, 1, 300};
		hitGroundSoft[] = {"soundGroundSoft1", 0.2,"soundGroundSoft2", 0.2,"soundGroundSoft3", 0.2,"soundGroundSoft4", 0.2,"soundGroundSoft5", 0.2};
		hitGroundHard[] = {"soundGroundHard1", 0.2,"soundGroundHard2", 0.2,"soundGroundHard3", 0.2,"soundGroundHard4", 0.2,"soundGroundHard5", 0.2};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 1};
		hitIron[] = {"soundIron1", 1};
		hitBuilding[] = {"soundHitBuilding1", 1};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.1, "soundWood2", 0.1, "soundWood3", 0.1, "soundWood4", 0.005, "soundWood5", 0.005, "soundWood6", 0.1, "soundWood7", 0.1, "soundWood8", 0.1, "soundWood9", 0.1, "soundWood10", 0.1, "soundWood11", 0.005, "soundWood12", 0.05};
		hitGlass[] = {"soundGlass1", 0.1, "soundGlass2", 0.1, "soundGlass3", 0.1, "soundGlass4", 0.1, "soundGlass5", 0.1, "soundGlass6", 0.1, "soundGlass7", 0.1, "soundGlass8", 0.1, "soundGlass9", 0.1, "soundGlass10", 0.1};
		hitGlassArmored[] = {"soundGlassArmored1", 0.125, "soundGlassArmored2", 0.125, "soundGlassArmored3", 0.125, "soundGlassArmored4", 0.125, "soundGlassArmored5", 0.125, "soundGlassArmored6", 0.125, "soundGlassArmored7", 0.125, "soundGlassArmored8", 0.125};
		hitConcrete[] = {"soundConcrete1", 1};
		hitRubber[] = {"soundRubber1", 0.2, "soundRubber2", 0.2, "soundRubber3", 0.2, "soundRubber4", 0.2, "soundRubber5", 0.2};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.2,"soundDefault2", 0.2,"soundDefault3", 0.2,"soundDefault4", 0.2,"soundDefault5", 0.2};
		hitMetal[] = {"soundMetal1", 1};
		hitMetalplate[] = {"soundMetalPlate1", 1};
		bulletFly1[] = {"", 1.0, 1, 50};
		bulletFly2[] = {"", 1.0, 1, 50};
		bulletFly3[] = {"", 1.0, 1, 50};
		bulletFly4[] = {"", 1.0, 1, 50};
		bulletFly5[] = {"", 1.0, 1, 50};
		bulletFly6[] = {"", 1.0, 1, 50};
		bulletFly[] = {"bulletFly1", 0.166, "bulletFly2", 0.166, "bulletFly3", 0.166, "bulletFly4", 0.166, "bulletFly5", 0.166, "bulletFly6", 0.167};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	class GrenadeCore;	// External class reference
	
	
	
	class GrenadeBase : GrenadeCore {
		soundDefault1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundDefault2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundDefault3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundDefault4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundGroundSoft1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundGroundSoft2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundGroundSoft3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundGroundSoft4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundGroundHard1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundGroundHard2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundGroundHard3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundGroundHard4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundMetal1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundMetal2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundMetal3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundMetal4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundGlass1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundGlass2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundGlass3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundGlass4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundIron1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundIron2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundIron3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundIron4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundGlassArmored1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundGlassArmored2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundGlassArmored3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundGlassArmored4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundVehiclePlate1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundVehiclePlate2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundVehiclePlate3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundVehiclePlate4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundWood1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundWood2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundWood3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundWood4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundHitBody1[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundMetalPlate1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundMetalPlate2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundMetalPlate3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundMetalPlate4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundHitBuilding1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundHitBuilding2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundHitBuilding3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundHitBuilding4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundHitFoliage1[] = {"\vops_s_impact\grenade1_foliage.wss", db+10, 1, 800};
		soundHitFoliage2[] = {"\vops_s_impact\grenade2_foliage.wss", db+10, 1, 800};
		soundHitFoliage3[] = {"\vops_s_impact\grenade3_foliage.wss", db+10, 1, 800};
		soundHitFoliage4[] = {"\vops_s_impact\grenade4_foliage.wss", db+10, 1, 800};
		soundPlastic1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundConcrete1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundConcrete2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundConcrete3[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		soundConcrete4[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundRubber1[] = {"\vops_s_impact\grenade1.wss", db+10, 1, 800};
		soundRubber2[] = {"\vops_s_impact\grenade2.wss", db+10, 1, 800};
		soundRubber3[] = {"\vops_s_impact\grenade4.wss", db+10, 1, 800};
		soundRubber4[] = {"\vops_s_impact\grenade3.wss", db+10, 1, 800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		soundFly[] = {"Ca\sounds\Weapons\explosions\noise", db-80, 1, 20};
		soundEngine[] = {"", db-80, 4};
		supersonicCrackNear[] = {"", 1, 1, 100};
		supersonicCrackFar[] = {"", 1, 1, 150};
	};
	
	class G_30mm_HE : GrenadeBase {
	};
	
	class G_40mm_HE : GrenadeBase {
	};
	
	class F_40mm_White : FlareBase {
	};
	
	class F_40mm_Green : F_40mm_White {
	};
	
	class F_40mm_Red : F_40mm_White {
	};
	
	class F_40mm_Yellow : F_40mm_White {
	};

	class ShellCore;	// External class reference
	
	class ShellBase : ShellCore {
		soundDefault1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\he2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\he1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\he5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\he4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\he3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", db+10, 1, 500};
	};

	class ARTY_Sh_Base : ShellBase {
	};

	class ARTY_Sh_105_HE : ARTY_Sh_Base {
		soundDefault1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"\vops_s_impact\shellflyarty.wss", 1, 1, 50};
		supersonicCrackFar[] = {"\vops_s_impact\aricracknear.wss", db+10, 1, 500};
	};

	class ARTY_Sh_105_WP : ARTY_Sh_105_HE {
		soundDefault1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_105_SMOKE : ARTY_Sh_105_HE {
		soundDefault1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_105_ILLUM : ARTY_Sh_105_HE {
		soundDefault1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_81_HE : ARTY_Sh_Base {
		soundDefault1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\mortar_imp5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\mortar_imp5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\mortar_imp5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"", 1, 1, 50};
		supersonicCrackFar[] = {"", db+10, 1, 500};
	};
	
	class ARTY_Sh_81_WP : ARTY_Sh_81_HE {
		soundDefault1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_81_ILLUM : ARTY_Sh_81_HE {		soundDefault1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_122_HE : ARTY_Sh_Base {
		soundDefault1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_he1haus.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_he2haus.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_he3haus.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_he4haus.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"\vops_s_impact\shellflyarty.wss", 1, 1, 50};
		supersonicCrackFar[] = {"\vops_s_impact\aricracknear.wss", db+10, 1, 500};
	};
	
	class ARTY_Sh_122_WP : ARTY_Sh_122_HE {
		soundDefault1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_122_SMOKE : ARTY_Sh_122_HE {
		soundDefault1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_122_ILLUM : ARTY_Sh_122_HE {
		soundDefault1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_SmokeShellWhite : ShellBase {
		soundDefault1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_82_HE : ARTY_Sh_Base {
		soundDefault1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\mortar_imp5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\mortar_imp2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\mortar_imp5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\mortar_imp1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\mortar_imp5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\mortar_imp4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\mortar_imp3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"", 1, 1, 50};
		supersonicCrackFar[] = {"", db+10, 1, 500};
	};
	
	class ARTY_Sh_82_WP : ARTY_Sh_82_HE {
		soundDefault1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_smoke1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_smoke2.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_smoke3.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_smoke4.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_Sh_82_ILLUM : ARTY_Sh_82_HE {
		soundDefault1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\illu_imp.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
	};

	class ARTY_R_227mm_HE : ARTY_Sh_Base {
		soundDefault1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_he1haus.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_he2haus.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_he3haus.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_he4haus.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"\vops_s_impact\shellflyarty.wss", 1, 1, 50};
		supersonicCrackFar[] = {"\vops_s_impact\aricracknear.wss", db+10, 1, 500};
	};

	class ARTY_R_120mm_HE : ARTY_Sh_Base {
		soundDefault1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\arty_he1haus.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\arty_he2haus.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\arty_he3haus.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\arty_he4haus.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\arty_he2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_he1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_he5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_he4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_he3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"\vops_s_impact\shellflyarty.wss", 1, 1, 50};
		supersonicCrackFar[] = {"\vops_s_impact\aricracknear.wss", db+10, 1, 500};
	};

	class Sh_105_HE : ShellBase {
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	};
	
	class Sh_100_HE : Sh_105_HE {
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	};
	
	class Sh_120_HE : ShellBase {
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	};
	
	class Sh_120_SABOT : ShellBase {
		soundDefault1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundDefault2[] = {"\vops_s_impact\sabot_imp2.wsss", db+10, 1, 1000};
		soundDefault3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundDefault4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGroundSoft1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGroundSoft2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGroundSoft3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGroundSoft4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGroundHard1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGroundHard2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGroundHard3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGroundHard4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundMetal1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundMetal2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetal3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundMetal4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlass1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGlass2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGlass3[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlass4[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundIron1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundIron2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundIron3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundIron4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlassArmored1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGlassArmored2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGlassArmored3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGlassArmored4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundVehiclePlate1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundVehiclePlate2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundVehiclePlate3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundVehiclePlate4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundWood1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundWood2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundWood3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundWood4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitBody1[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetalPlate1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundMetalPlate2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetalPlate3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundMetalPlate4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitBuilding1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundHitBuilding2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundHitBuilding3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundHitBuilding4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitFoliage1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundHitFoliage2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundHitFoliage3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundHitFoliage4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundPlastic1[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundConcrete1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundConcrete2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundConcrete3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundConcrete4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundRubber1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundRubber2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundRubber3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundRubber4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	};
	
	class Sh_122_HE : ShellBase {
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	
	};
	
	class Sh_125_HE : ShellBase {
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	};
	
	class Sh_125_SABOT : ShellBase {
		soundDefault1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundDefault2[] = {"\vops_s_impact\sabot_imp2.wsss", db+10, 1, 1000};
		soundDefault3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundDefault4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGroundSoft1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGroundSoft2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGroundSoft3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGroundSoft4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGroundHard1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGroundHard2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGroundHard3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGroundHard4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundMetal1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundMetal2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetal3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundMetal4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlass1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGlass2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGlass3[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlass4[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundIron1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundIron2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundIron3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundIron4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlassArmored1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGlassArmored2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGlassArmored3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGlassArmored4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundVehiclePlate1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundVehiclePlate2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundVehiclePlate3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundVehiclePlate4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundWood1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundWood2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundWood3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundWood4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitBody1[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetalPlate1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundMetalPlate2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetalPlate3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundMetalPlate4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitBuilding1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundHitBuilding2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundHitBuilding3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundHitBuilding4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitFoliage1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundHitFoliage2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundHitFoliage3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundHitFoliage4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundPlastic1[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundConcrete1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundConcrete2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundConcrete3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundConcrete4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundRubber1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundRubber2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundRubber3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundRubber4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	};

	class Sh_85_HE : ShellBase {
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	
	};
	
	class Sh_85_AP : ShellBase {
		soundDefault1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundDefault2[] = {"\vops_s_impact\sabot_imp2.wsss", db+10, 1, 1000};
		soundDefault3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundDefault4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGroundSoft1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGroundSoft2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGroundSoft3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGroundSoft4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGroundHard1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGroundHard2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGroundHard3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGroundHard4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundMetal1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundMetal2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetal3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundMetal4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlass1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGlass2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGlass3[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlass4[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundIron1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundIron2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundIron3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundIron4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundGlassArmored1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundGlassArmored2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundGlassArmored3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundGlassArmored4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundVehiclePlate1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundVehiclePlate2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundVehiclePlate3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundVehiclePlate4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundWood1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundWood2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundWood3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundWood4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitBody1[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetalPlate1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundMetalPlate2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundMetalPlate3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundMetalPlate4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitBuilding1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundHitBuilding2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundHitBuilding3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundHitBuilding4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundHitFoliage1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundHitFoliage2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundHitFoliage3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundHitFoliage4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundPlastic1[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundConcrete1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundConcrete2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundConcrete3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundConcrete4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		soundRubber1[] = {"\vops_s_impact\sabot_imp.wss", db+10, 1, 1000};
		soundRubber2[] = {"\vops_s_impact\sabot_imp2.wss", db+10, 1, 1000};
		soundRubber3[] = {"\vops_s_impact\sabot_imp3.wss", db+10, 1, 1000};
		soundRubber4[] = {"\vops_s_impact\sabot_imp4.wss", db+10, 1, 1000};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		supersonicCrackNear[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 300};
		supersonicCrackFar[] = {"\vops_s_impact\sabotfly.wss", 1, 1, 350};
	};

	class RocketCore;	// External class reference
	
	class RocketBase : RocketCore {
		soundDefault1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\shell_ground3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\shell_ground4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\shell_ground3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\shell_ground4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\shell_ground3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\shell_ground4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\shell_glass1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\shell_glass2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\shell_glass1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\shell_glass2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\shell_glass1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\shell_glass2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\shell_glass3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\shell_glass4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\shell_foliage1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\shell_foliage2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\shell_foliage3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\shell_foliage4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\shell_ground4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\shell_house1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\shell_house2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\shell_house3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\shell_house4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\shell_foliage1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\shell_foliage2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\shell_foliage3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\shell_foliage4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\shell_ground3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\shell_ground4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\shell_ground4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\shell_ground3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		soundFly[] = {"\vops_s_impact\rpgfly.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\rpgfly.wss", 0.050, 1, 20};
		supersonicCrackNear[] = {"", 1, 1, 150};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class G_Camel_HE : RocketCore {
	};
	
	class R_Hydra_HE : RocketBase {
		soundFly[] = {"\vops_s_impact\rpgfly.wss", 0.1, 1, 400};
		soundHit[] = {"\vops_s_impact\expl4.wss", db10, 1, 1500};
	};
	
	class R_57mm_HE : RocketBase {
		soundFly[] = {"\vops_s_impact\rpgfly.wss", 0.1, 1, 400};
		soundHit[] = {"\vops_s_impact\expl4.wss", db10, 1, 1500};
	};
	
	class R_80mm_HE : RocketBase {
		soundFly[] = {"\vops_s_impact\rpgfly.wss", 0.1, 1, 400};
		soundHit[] = {"\vops_s_impact\expl2.wss", db10, 1, 1500};
	};
	
	class R_S8T_AT : RocketBase {
		soundFly[] = {"\vops_s_impact\rpgfly.wss", 0.1, 1, 400};
		soundHit[] = {"\vops_s_impact\expl2.wss", db10, 1, 1500};
	};
	
	class R_M136_AT : RocketBase {
		supersonicCrackNear[] = {"\vops_s_impact\at4flyby.wss", 1, 1, 150};
	};
	
	class R_RPG18_AT : RocketBase {
		supersonicCrackNear[] = {"\vops_s_impact\rpgflyby.wss", 1, 1, 150};
	};
	
	class R_PG7V_AT : RocketBase {
		supersonicCrackNear[] = {"\vops_s_impact\rpgflyby.wss", 1, 1, 150};
		};
	
	class R_PG7VL_AT : R_PG7V_AT {
		supersonicCrackNear[] = {"\vops_s_impact\rpgflyby.wss", 1, 1, 150};
	};
	
	class R_PG7VR_AT : R_PG7V_AT {
		supersonicCrackNear[] = {"\vops_s_impact\rpgflyby.wss", 1, 1, 150};
	};
	
	class R_OG7_AT : R_PG7V_AT {
		supersonicCrackNear[] = {"\vops_s_impact\rpgflyby.wss", 1, 1, 150};
	};
	
	class R_PG9_AT : RocketBase {
		supersonicCrackNear[] = {"\vops_s_impact\rpgflyby.wss", 1, 1, 150};
	};
	
	class R_OG9_HE : R_PG9_AT {
		supersonicCrackNear[] = {"\vops_s_impact\rpgflyby.wss", 1, 1, 150};
	};
	
	class R_SMAW_HEDP : RocketBase {
		supersonicCrackNear[] = {"\vops_s_impact\at4flyby.wss", 1, 1, 150};
	};
	
	class R_SMAW_HEAA : R_SMAW_HEDP {
		supersonicCrackNear[] = {"\vops_s_impact\at4flyby.wss", 1, 1, 150};
	};
	
	class R_GRAD : RocketBase {
		soundFly[] = {"\vops_s_impact\rpgfly.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e_low.wss", 0.050, 1, 20};
	};
	
	class R_MLRS : RocketBase {
		soundFly[] = {"\vops_s_impact\rpgfly.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", 0.050, 1, 20};
	};

	class MissileCore;	// External class reference
	
	class MissileBase : MissileCore {
		soundDefault1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\arty_wp3.wss", db+10, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\arty_wp4.wss", db+10, 1, 1800};
		soundGroundSoft1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundGroundSoft2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundGroundSoft3[] = {"\vops_s_impact\arty_wp3.wss", db+10, 1, 1800};
		soundGroundSoft4[] = {"\vops_s_impact\arty_wp4.wss", db+10, 1, 1800};
		soundGroundHard1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundGroundHard2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundGroundHard3[] = {"\vops_s_impact\arty_wp3.wss", db+10, 1, 1800};
		soundGroundHard4[] = {"\vops_s_impact\arty_wp4.wss", db+10, 1, 1800};
		soundMetal1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundMetal2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundMetal3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundMetal4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundGlass1[] = {"\vops_s_impact\shell_glass1.wss", db+10, 1, 1800};
		soundGlass2[] = {"\vops_s_impact\shell_glass2.wss", db+10, 1, 1800};
		soundGlass3[] = {"\vops_s_impact\shell_glass1.wss", db+10, 1, 1800};
		soundGlass4[] = {"\vops_s_impact\shell_glass2.wss", db+10, 1, 1800};
		soundIron1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundIron2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundIron3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundIron4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundGlassArmored1[] = {"\vops_s_impact\shell_glass1.wss", db+10, 1, 1800};
		soundGlassArmored2[] = {"\vops_s_impact\shell_glass2.wss", db+10, 1, 1800};
		soundGlassArmored3[] = {"\vops_s_impact\shell_glass3.wss", db+10, 1, 1800};
		soundGlassArmored4[] = {"\vops_s_impact\shell_glass4.wss", db+10, 1, 1800};
		soundVehiclePlate1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundVehiclePlate2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundVehiclePlate3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundVehiclePlate4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundWood1[] = {"\vops_s_impact\shell_foliage1.wss", db+10, 1, 1800};
		soundWood2[] = {"\vops_s_impact\shell_foliage2.wss", db+10, 1, 1800};
		soundWood3[] = {"\vops_s_impact\shell_foliage3.wss", db+10, 1, 1800};
		soundWood4[] = {"\vops_s_impact\shell_foliage4.wss", db+10, 1, 1800};
		soundHitBody1[] = {"\vops_s_impact\arty_wp4.wss", db+10, 1, 1800};
		soundMetalPlate1[] = {"\vops_s_impact\shell_metal1.wss", db+10, 1, 1800};
		soundMetalPlate2[] = {"\vops_s_impact\shell_metal2.wss", db+10, 1, 1800};
		soundMetalPlate3[] = {"\vops_s_impact\shell_metal3.wss", db+10, 1, 1800};
		soundMetalPlate4[] = {"\vops_s_impact\shell_metal4.wss", db+10, 1, 1800};
		soundHitBuilding1[] = {"\vops_s_impact\shell_house1.wss", db+10, 1, 1800};
		soundHitBuilding2[] = {"\vops_s_impact\shell_house2.wss", db+10, 1, 1800};
		soundHitBuilding3[] = {"\vops_s_impact\shell_house3.wss", db+10, 1, 1800};
		soundHitBuilding4[] = {"\vops_s_impact\shell_house4.wss", db+10, 1, 1800};
		soundHitFoliage1[] = {"\vops_s_impact\shell_foliage1.wss", db+10, 1, 1800};
		soundHitFoliage2[] = {"\vops_s_impact\shell_foliage2.wss", db+10, 1, 1800};
		soundHitFoliage3[] = {"\vops_s_impact\shell_foliage3.wss", db+10, 1, 1800};
		soundHitFoliage4[] = {"\vops_s_impact\shell_foliage4.wss", db+10, 1, 1800};
		soundPlastic1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundConcrete1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundConcrete2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundConcrete3[] = {"\vops_s_impact\arty_wp3.wss", db+10, 1, 1800};
		soundConcrete4[] = {"\vops_s_impact\arty_wp4.wss", db+10, 1, 1800};
		soundRubber1[] = {"\vops_s_impact\arty_wp1.wss", db+10, 1, 1800};
		soundRubber2[] = {"\vops_s_impact\arty_wp5.wss", db+10, 1, 1800};
		soundRubber3[] = {"\vops_s_impact\arty_wp4.wss", db+10, 1, 1800};
		soundRubber4[] = {"\vops_s_impact\arty_wp3.wss", db+10, 1, 1800};
		hitGroundSoft[] = {"soundGroundSoft1", 0.25,"soundGroundSoft2", 0.25,"soundGroundSoft3", 0.25,"soundGroundSoft4", 0.25};
		hitGroundHard[] = {"soundGroundHard1", 0.25,"soundGroundHard2", 0.25,"soundGroundHard3", 0.25,"soundGroundHard4", 0.25};
		hitMan[] = {"soundHitBody1", 1};
		hitArmor[] = {"soundVehiclePlate1", 0.25,"soundVehiclePlate2", 0.25,"soundVehiclePlate3", 0.25,"soundVehiclePlate4", 0.25};
		hitIron[] = {"soundIron1", 0.25,"soundIron2", 0.25,"soundIron3", 0.25,"soundIron4", 0.25};
		hitBuilding[] = {"soundHitBuilding1", 0.25,"soundHitBuilding2", 0.25,"soundHitBuilding3", 0.25,"soundHitBuilding4", 0.25};
		hitFoliage[] = {"soundHitFoliage1", 0.25, "soundHitFoliage2", 0.25, "soundHitFoliage3", 0.25, "soundHitFoliage4", 0.25};
		hitWood[] = {"soundWood1", 0.25, "soundWood2", 0.25, "soundWood3", 0.25, "soundWood4", 0.25};
		hitGlass[] = {"soundGlass1", 0.25, "soundGlass2", 0.25, "soundGlass3", 0.25, "soundGlass4", 0.25};
		hitGlassArmored[] = {"soundGlassArmored1", 0.25, "soundGlassArmored2", 0.25, "soundGlassArmored3", 0.25, "soundGlassArmored4", 0.25};
		hitConcrete[] = {"soundConcrete1", 0.25,"soundConcrete2", 0.25,"soundConcrete3", 0.25,"soundConcrete4", 0.25};
		hitRubber[] = {"soundRubber1", 0.25, "soundRubber2", 0.25, "soundRubber3", 0.25, "soundRubber4", 0.25};
		hitPlastic[] = {"soundPlastic1", 1};
		hitDefault[] = {"soundDefault1", 0.25,"soundDefault2", 0.25,"soundDefault3", 0.25,"soundDefault4", 0.25};
		hitMetal[] = {"soundMetal1", 0.25,"soundMetal2", 0.25,"soundMetal3", 0.25,"soundMetal4", 0.25};
		hitMetalplate[] = {"soundMetalPlate1", 0.25,"soundMetalPlate2", 0.25,"soundMetalPlate3", 0.25,"soundMetalPlate4", 0.25};
		soundFly[] = {"\vops_s_impact\missile_e_low.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e_low.wss", 0.050, 1, 100};
		supersonicCrackNear[] = {"", 1, 1, 100};
		supersonicCrackFar[] = {"", 1, 1, 250};
	};
	
	class M_Javelin_AT : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", 0.050, 1, 400};
		supersonicCrackNear[] = {"\vops_s_impact\at4flyby.wss", 1, 1, 150};
		};
	
	class M_Stinger_AA : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", 0.050, 1, 400};
	};
	
	class M_Sidewinder_AA : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", 0.050, 1, 400};
	};
	
	class M_Sidewinder_AA_F35 : M_Sidewinder_AA {
	};
	
	class M_Strela_AA : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", 0.050, 1, 400};
	};
	
	class M_Igla_AA : M_Strela_AA {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", 0.050, 1, 400};
	};
	
	class M_AT5_AT : MissileBase {
		soundEngine[] = {"\vops_s_impact\missile_e_low.wss", db0, 1, 400};

	};
	
	class M_AT13_AT : M_AT5_AT {
	};
	
	class M_TOW_AT : MissileBase {
		soundEngine[] = {"\vops_s_impact\missile_e_low.wss", db0, 1, 400};
	};
	
	class M_TOW2_AT : M_TOW_AT {
	};
	
	class M_AT10_AT : MissileBase {
		soundEngine[] = {"\vops_s_impact\missile_e_low.wss", db0, 1, 400};

	};
	
	class M_AT11_AT : M_AT10_AT {
	};
	
	class M_Hellfire_AT : MissileBase {
		soundFly[] = {"", 1, 0.2, 400};
		soundEngine[] = {"\vops_s_impact\hellfirefly.wss", db0, 1, 400};
	};
	
	class M_Vikhr_AT : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", db0, 1, 400};
	};
	
	class M_Maverick_AT : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", db0, 1, 400};
	};
	
	class M_R73_AA : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", db0, 1, 400};
	};
	
	class M_Ch29_AT : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", db0, 1, 400};
	};
	
	class M_AT2_AT : MissileBase {
	};
	
	class M_AT6_AT : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundHit[] = {"\vops_s_impact\expl4.wss", db31, 1, 1800};
	};
	
	class M_AT9_AT : M_AT6_AT {
	};
	
	class M_9M311_AA : MissileBase {
		soundFly[] = {"\vops_s_impact\missile_e.wss", 0.1, 1, 400};
		soundEngine[] = {"\vops_s_impact\missile_e.wss", db0, 1, 400};
	};
	class LaserBombCore;	// External class reference
	
	class Bo_GBU12_LGB : LaserBombCore {
		soundHit[] = {"\vops_s_impact\bomb.wss", 56.2341, 1, 2500};
		soundDefault1[] = {"\vops_s_impact\bomb.wss", 56.2341, 1, 2500};
		soundDefault2[] = {"\vops_s_impact\bomb2.wss", 56.2341, 1, 2500};
		soundDefault[] = {"soundDefault1", 0.5, "soundDefault2", 0.5};
	};
	
	class Bo_GBU12_LGB_F35 : Bo_GBU12_LGB {
	};
	
	class Bo_FAB_250 : BombCore {
		soundHit[] = {"\vops_s_impact\satch1.wss", 56.2341, 1, 2000};
		soundDefault1[] = {"\vops_s_impact\satch1.wss", 56.2341, 1, 2000};
		soundDefault2[] = {"\vops_s_impact\satch2.wss", 56.2341, 1, 2000};
		soundDefault[] = {"soundDefault1", 0.5, "soundDefault2", 0.5};
	};
	
	class Bo_Mk82 : Bo_FAB_250 {
	};
	
	class TimeBomb : TimeBombCore {
		soundHit[] = {"\vops_s_impact\satch1.wss", 56.2341, 1, 1800};
		soundDefault1[] = {"\vops_s_impact\satch1.wss", 56.2341, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\satch2.wss", 56.2341, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\satch3.wss", 56.2341, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\satch3.wss", 56.2341, 1, 1800};
		soundDefault[] = {"soundDefault1", 0.25, "soundDefault2", 0.25, "soundDefault3", 0.25, "soundDefault4", 0.25};
	};
	
	class PipeBomb : TimeBomb {
	};
	
	class Mine : MineCore {
		soundHit[] = {"\vops_s_impact\expl4.wss", 56.2341, 1, 1500};
	};
	
	class MineE : Mine {
	};
	
	class FuelExplosion : Default {
		soundHit[] = {"\vops_s_impact\tank5.wss", db16, 1, 1600};
		soundDefault1[] = {"\vops_s_impact\fuel3.wss", 5, 1, 800};
		soundDefault2[] = {"\vops_s_impact\fuel2.wss", 5, 1, 800};
		soundDefault3[] = {"\vops_s_impact\fuel1.wss", 5, 1, 800};
		soundDefault4[] = {"\vops_s_impact\30mmHE_imp4.wss", 5, 1, 800};
		soundDefault[] = {"soundDefault1", 0.25, "soundDefault2", 0.25, "soundDefault3", 0.25, "soundDefault4", 0.25};
	};
	
	class Laserbeam : Default {
		soundHit[] = {"\vops_s_impact\satch1.wss", 56.2341, 1, 1800};
		soundDefault1[] = {"\vops_s_impact\satch1.wss", 56.2341, 1, 1800};
		soundDefault2[] = {"\vops_s_impact\satch2.wss", 56.2341, 1, 1800};
		soundDefault3[] = {"\vops_s_impact\satch3.wss", 56.2341, 1, 1800};
		soundDefault4[] = {"\vops_s_impact\satch3.wss", 56.2341, 1, 1800};
		soundDefault[] = {"soundDefault1", 0.25, "soundDefault2", 0.25, "soundDefault3", 0.25, "soundDefault4", 0.25};
		soundFly[] = {"Ca\sounds\Weapons\explosions\rocket_fly1", db-80, 4};
	};
	
	class HelicopterExploSmall : ShellBase {
		soundHit[] = {"\vops_s_impact\50mm_imp1.wss", 5, 1, 800};
		soundDefault1[] = {"\vops_s_impact\fuel3.wss", 5, 1, 800};
		soundDefault2[] = {"\vops_s_impact\fuel2.wss", 5, 1, 800};
		soundDefault3[] = {"\vops_s_impact\fuel1.wss", 5, 1, 800};
		soundDefault4[] = {"\vops_s_impact\30mmHE_imp4.wss", 5, 1, 800};
		soundDefault[] = {"soundDefault1", 0.25, "soundDefault2", 0.25, "soundDefault3", 0.25, "soundDefault4", 0.25};
	};
	
	class HelicopterExploBig : HelicopterExploSmall {
		soundHit[] = {"\vops_s_impact\50mm_imp1.wss", 5, 1, 800};
		soundDefault1[] = {"\vops_s_impact\fuel3.wss", 5, 1, 800};
		soundDefault2[] = {"\vops_s_impact\fuel2.wss", 5, 1, 800};
		soundDefault3[] = {"\vops_s_impact\fuel1.wss", 5, 1, 800};
		soundDefault4[] = {"\vops_s_impact\satch2.wss", 5, 1, 800};
		soundDefault[] = {"soundDefault1", 0.25, "soundDefault2", 0.25, "soundDefault3", 0.25, "soundDefault4", 0.25};
	};
	
	class SmallSecondary : HelicopterExploSmall {
		soundHit[] = {"\vops_s_impact\50mm_imp1.wss", 5, 1, 800};
		soundDefault1[] = {"\vops_s_impact\fuel3.wss", 5, 1, 800};
		soundDefault2[] = {"\vops_s_impact\fuel2.wss", 5, 1, 800};
		soundDefault3[] = {"\vops_s_impact\fuel1.wss", 5, 1, 800};
		soundDefault4[] = {"\vops_s_impact\30mmHE_imp4.wss", 5, 1, 800};
		soundDefault[] = {"soundDefault1", 0.25, "soundDefault2", 0.25, "soundDefault3", 0.25, "soundDefault4", 0.25};
	};
	
	class SmokeLauncherAmmo : BulletBase {
	};
	
	class FlareLauncherAmmo : SmokeLauncherAmmo {
			};
		};
