﻿/****************************************************************************
Copyright (C) 2010 Team ~R3F~
This program is free software under the terms of the GNU General Public License version 3.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
@authors team-r3f.org
@version 1.00
@date 20101006
Modified/Simplified by cyrq (cyrq1337@gmail.com) for DayZed (http://dayzed.eu)
*****************************************************************************/
//#define R3F_TIRED_DEBUG
//adjustment player weight with helmet jacket Rangers  Light equipment (not weapons)
#define R3F_TIRED_WEIGHT_PLAYER_EMPTY 23
//adjustment weight * speed player
#define R3F_TIRED_WEIGHT_SPEED_RATIO 1	
// adjustment onset threshold blackout effect
#define R3F_TIRED_BLACKOUT_LEVEL 12000	
// adjustment player attitude * weight (stand up, down, crouch)
#define R3F_TIRED_DOWN_LEVEL 7
#define R3F_TIRED_KNEE_LEVEL 1.3
#define R3F_TIRED_UP_LEVEL 1
#define R3F_TIRED_WEIGHT_CLIMB_FACTOR 18
//adjustment threshold weight agravante factor
#define R3F_TIRED_WEIGHT_LEVEL1 25
#define R3F_TIRED_WEIGHT_LEVEL2 30
#define R3F_TIRED_WEIGHT_LEVEL3 40
#define R3F_TIRED_SHORTNESS_THRESHOLD	0.8
#define R3F_TIRED_UNCONSCIOUSNESS_DURATION 30
// ratio of threshold weight
#define R3F_TIRED_WEIGHT_RATIO1 0.6  /* for weight < 10 kg */
#define R3F_TIRED_WEIGHT_RATIO2 0.7  /* for weight 10 à 20 kg */
#define R3F_TIRED_WEIGHT_RATIO3 1.2  /* for weight 20 à 30 kg */
#define R3F_TIRED_WEIGHT_RATIO4 1.4  /* for weight  > 30 kg */
// activation / deactivation unit recup loss over time (true / false)
#define R3F_TIRED_GLOBAL_TIRING true
// adjustment recovery rate
#define R3F_TIRED_TIME_RECOVERING 100
#define R3F_TIRED_RATIO_RECOVERING 40
#define R3F_TIRED_WITH_VANISH true

R3F_TIRED_FNCT_DoBlackVanish = {
	_randomTime = round(random 4);
	cutText ["You're pushing yourself to hard. Rest up!", "PLAIN DOWN"];
	[player,_randomTime] call fnc_usec_damageUnconscious;
};

R3F_Weight = 0;
R3F_TIRED_Ratio_Position = 0;
R3F_TIRED_Accumulator = 0;
R3F_TIRED_Handle_Blur_Effect = [] spawn {}; 
R3F_TIRED_Handle_Blackout_Effect = [] spawn {};
R3F_TIRED_Counter_Time = 0;
R3F_TIRED_Ratio_Recovery = R3F_TIRED_RATIO_RECOVERING;
R3F_TIRED_Ratio_Overweight = 1;

private ["_level", "_n", "_s"];

_n = 0;
_posATL = 0;
R3F_TIRED_Accumulator = 0;
R3F_TIRED_vitesse_de_mon_joueur = 0;
sleep 1;
_level = 1;

while {true} do {
	R3F_TIRED_POIDS_TOTAL_PLAYER= R3F_Weight + R3F_TIRED_WEIGHT_PLAYER_EMPTY;
	if (R3F_weight < R3F_TIRED_WEIGHT_LEVEL3 )then {
		if (r3f_weight < R3F_TIRED_WEIGHT_LEVEL2) then {
			if (r3f_weight < R3F_TIRED_WEIGHT_LEVEL1 ) then {
				R3F_TIRED_Ratio_Overweight = R3F_TIRED_WEIGHT_RATIO1;
			} else {
				R3F_TIRED_Ratio_Overweight = R3F_TIRED_WEIGHT_RATIO2;
			};			
		} else {
			R3F_TIRED_Ratio_Overweight = R3F_TIRED_WEIGHT_RATIO3;
		};		
	} else {	
		R3F_TIRED_Ratio_Overweight = R3F_TIRED_WEIGHT_RATIO4;
	};	
		 
	if (alive player) then {	
		switch (toArray (animationState player) select 5) do {
			case 112: {
				R3F_TIRED_Ratio_Position = R3F_TIRED_DOWN_LEVEL;
			};
			case 107:{
				R3F_TIRED_Ratio_Position=R3F_TIRED_KNEE_LEVEL;
			};
			case 101:{
				R3F_TIRED_Ratio_Position = R3F_TIRED_UP_LEVEL;
			};
		};
	
		R3F_TIRED_vitesse_de_mon_joueur = [0,0,0] distance velocity player;	
		R3F_TIRED_coeff_mon_elevation_en_z = 0 max ((velocity player select 2) / R3F_TIRED_WEIGHT_CLIMB_FACTOR);
		
		_posATL = (getPosATL player) select 2;
		_isInMountedVehicle = player getVariable["inMountedSlot",false];
		
		if((vehicle player == player) && (_posATL < 100) && !(_isInMountedVehicle)) then {
			R3F_TIRED_Accumulator =  R3F_TIRED_Accumulator 	
					+ (R3F_TIRED_POIDS_TOTAL_PLAYER * R3F_TIRED_vitesse_de_mon_joueur * R3F_TIRED_Ratio_Position * R3F_TIRED_WEIGHT_SPEED_RATIO*R3F_TIRED_Ratio_Overweight) 
					+ (R3F_TIRED_POIDS_TOTAL_PLAYER * R3F_TIRED_coeff_mon_elevation_en_z * R3F_TIRED_WEIGHT_LEVEL2);				
			};
		
		R3F_TIRED_Accumulator = 0 max (R3F_TIRED_Accumulator - R3F_TIRED_Ratio_Recovery);
		
		_level = ((R3F_TIRED_Accumulator / R3F_TIRED_BLACKOUT_LEVEL) *  100);
		_level = 0 max (1 - (_level / 100));
		
		#ifdef R3F_TIRED_DEBUG
			hintsilent format[
"Stamina : %1\n
dayz_onBack : %2\n
AnimState : %3\n
Surface : %4\n
canPickup : %5\n
pickupInit : %6\n
dayZ_lastPlayerUpdate : %7\n
dayz_unsaved : %8\n
dayz_lastSave : %9\n
dayz_mylastPos : %10\n
r_player_unconscious : %11\n
r_handlerCount : %12\n
r_player_handler1 : %13\n
r_player_cardiac : %14\n
carryClick : %15",
dayzed_stamina,
dayz_onBack,
animationState player,
surfaceType position player,
canPickup,
pickupInit,
dayZ_lastPlayerUpdate,
dayz_unsaved,
dayz_lastSave,
dayz_mylastPos,
r_player_unconscious,
r_handlerCount,
r_player_handler1,
r_player_cardiac,
carryClick
							];
		#endif
		
			dayzed_stamina = R3F_TIRED_Accumulator;
			
			if (R3F_TIRED_Accumulator >= 6000) then {
				[player] spawn fn_breathfog;
			};
			
			if (R3F_TIRED_Accumulator >= 9000) then {
				[nil,player,rSAY,["heartbeat_1", 3]] call RE;
			};
			
			if (animationState player == "amovpsitmstpsraswrfldnon_smoking" or animationState player == "amovpsitmstpsnonwnondnon_smoking" 
				or animationState player == "Die" or animationState player == "amovppnemstpsnonwnondnon" 
				or animationState player == "amovppnemstpsraswrfldnon" or animationState player == "amovppnemstpsraswpstdnon" 
				or animationState player == "amovpsitmstpsnonwnondnon_ground") then {
					R3F_TIRED_Ratio_Recovery = R3F_TIRED_RATIO_RECOVERING + 50;
				} else {
					R3F_TIRED_Ratio_Recovery = R3F_TIRED_RATIO_RECOVERING;
			};
		
		if (R3F_TIRED_Accumulator  > R3F_TIRED_BLACKOUT_LEVEL and scriptDone R3F_TIRED_Handle_Blackout_Effect and scriptDone R3F_TIRED_Handle_Blur_Effect) then {
			R3F_TIRED_Handle_Blackout_Effect = [] spawn R3F_TIRED_FNCT_DoBlackVanish;
		};
	} else {	
		R3F_TIRED_Accumulator = 0;
	};

	if (R3F_TIRED_GLOBAL_TIRING 
		&& R3F_TIRED_vitesse_de_mon_joueur > 4 
		&& R3F_TIRED_Ratio_Recovery > R3F_TIRED_RATIO_RECOVERING
		) then {
			R3F_TIRED_Counter_Time= R3F_TIRED_Counter_Time + 1;
	};
	
	if ((R3F_TIRED_Counter_Time > R3F_TIRED_TIME_RECOVERING)) then {
		R3F_TIRED_Ratio_Recovery = R3F_TIRED_Ratio_Recovery - 1;
		R3F_TIRED_Counter_Time = 0;
	};

	sleep 1;
	_n = _n + 1;
};