private ["_configBase","_tempList","_id"];

	diag_log ("Creating Chernarus Debris...");
    _tempList = ["Chernogorsk","Elektrozavodsk","Balota","Komarovo","Kamenka","Kamyshovo","Prigorodki","Kabanino","Solnichniy","StarySobor","NovySobor","SouthernAirport","NorthernAirport","Berezino","Lopatino","GreenMountain","Zelenogorsk","Nadezhdino","Kozlovka","Mogilevka","Pusta","Bor","Pulkovo","Vyshnoye","Drozhino","Pogorevka","Rogovo","Guglovo","Staroye","Pavlovo","Shakhovka","Sosnovka","Msta","Pustoshka","Dolina","Myshkino","Tulga","Vybor","Polana","Gorka","Orlovets","Grishino","Dubrovka","Nizhnoye","Gvozdno","Petrovka","Khelm","Krasnostav","Olsha"];
    for "_j" from 0 to ((count _tempList) - 1) do
    {
        _configBase = configFile >> "CfgTownGenerator" >> (_tempList select _j);
 
        for "_i" from 0 to ((count _configBase) - 1) do
        {
            private ["_config","_type","_position","_dir","_onFire","_object"];
       
            _config =    (_configBase select _i);
            if (isClass(_config)) then {
                _type =    getText    (_config >> "type");
                if (((getText (_config >> "type")) != "Body1") and ((getText (_config >> "type")) != "Body2") and ((getText (_config >> "type")) != "Mass_grave")) then {
                    _position = [] + getArray    (_config >> "position");
                    _dir =        getNumber    (_config >> "direction");
                    _onFire =    getNumber    (_config >> "onFire");
               
                    _object = _type createVehicleLocal _position;
                    _object setPos _position;
                    _object setDir _dir;
                    _object allowDamage false;
					_object enableSimulation false;
                };
            };
        };
        //sleep 0.2;
    };