//Gather milk by cyrq

private ["_milkTarget","_type","_name","_hasBottle","_isMilked","_cowSound","_goatSound"];

if(dayz_milkInProgress) exitWith {
	cutText ["Milking already in progress..", "PLAIN DOWN"];
};
dayz_milkInProgress = true;

_milkTarget = cursorTarget;
_type = typeOf _milkTarget;
_name = getText (configFile >> "CfgVehicles" >> _type >> "displayName");
_hasBottle = "ItemMilkbottleUnfilled" in magazines player;
_isMilked = _milkTarget getVariable["cowmilked",false];

if (!_hasBottle) exitwith {
cutText [format["You need an Empty Milk Bottle to milk the %1.", _name], "PLAIN DOWN"];
dayz_milkInProgress = false;
};

if (!_isMilked) then {

		player removeAction s_player_milk;

		_milkTarget forceSpeed 0; //stop the cow/goat from running

		player playActionNow "Medic";
		cutText [format["Milking the %1. Stand still...",_name],"PLAIN DOWN"];
		sleep 1;
		[player,"bandage",0,false] call dayz_zombieSpeak;

			r_interrupt = false;
			_animState = animationState player;
			r_doLoop = true;
			_started = false;
			_finished = false;

				while {r_doLoop} do {
				_animState = animationState player;
				_isMedic = ["medic",_animState] call fnc_inString;
				if (_isMedic) then {
				_started = true;
				};
				if (_started and !_isMedic) then {
				r_doLoop = false;
				_finished = true;
				};
				if (r_interrupt) then {
				r_doLoop = false;
				};
				sleep 0.1;
				};
				r_doLoop = false;

//Loot add
	if (_finished) then {
	
	_cowSound = ["cow_01","cow_02","cow_03"] call BIS_fnc_selectRandom;
	_goatSound = ["goat_01","goat_02","goat_03"] call BIS_fnc_selectRandom;
	
	Switch true do {
	
	case (_milkTarget isKindOf "Cow") : {
		[nil,_milkTarget,rSAY,[_cowSound, 10]] call RE;
		};
	case (_milkTarget isKindOf "Goat") : {
		[nil,_milkTarget,rSAY,[_goatSound, 10]] call RE;
		};
	default {};
	};
		
	cutText [format["You've successfully milked the %1",_name],"PLAIN DOWN"];
	
	_milkTarget forceSpeed -1; //-1 should give default speed. run cow (or goat :P), run!!!
	_milkTarget setVariable ["cowmilked",true,true];
	
	player removeMagazine  "ItemMilkbottleUnfilled";
	player addMagazine "ItemMilkbottle";
	
	} else {

		r_interrupt = false;
		[objNull, player, rSwitchMove,""] call RE;
		player playActionNow "stop";
		cutText [format["You failed milking the %1. Stand still!",_name],"PLAIN DOWN"];
	};
	
	} else {
	
	cutText [format["The %1 has been already milked.", _name], "PLAIN DOWN"]
	
	};
		
dayz_milkInProgress = false;
dayz_myCursorTarget = objNull;
s_player_milk = -1;