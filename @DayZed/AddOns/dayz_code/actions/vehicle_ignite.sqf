private["_veh","_burnTime","_burnTimeLeft","_configVeh","_vehType"];

_veh = cursorTarget;
_burnTime = 45; //takes one minuite to delete the vehicle and it's content from the DB
_burnTimeLeft = _burnTime;
_configVeh = configFile >> "cfgVehicles" >> TypeOf(_veh);
_vehType = getText(_configVeh >> "displayName");
_alreadyBurning = _veh getVariable["burning",false];

if (_alreadyBurning) exitWith {
	cutText [format["%1 is already burning!",_vehType],"PLAIN DOWN"];
};

player removeAction s_player_igniteVehicle;
player removeAction s_player_inspectVehicle;
{dayz_myCursorTarget removeAction _x} forEach s_player_repairActions;
s_player_repairActions = [];
player removeAction s_player_repair_crtl;

player playActionNow "Medic";

[player,"bandage",0,false] call dayz_zombieSpeak;
cutText [format["Preparing to set %1 ablaze...",_vehType], "PLAIN DOWN"];

sleep 6; //wait for the burn animation

PVDZ_obj_Fire = [_veh,3,time,false,true];
publicVariable "PVDZ_obj_Fire";
_id = PVDZ_obj_Fire spawn BIS_Effects_Burn;
_ent setVariable ["burning",true,true];

switch true do {
	case ("ItemMatchbox5" in items player) : {
		player removeWeapon "ItemMatchbox5";
		player addWeapon "ItemMatchbox4";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox4" in items player) : {
		player removeWeapon "ItemMatchbox4";
		player addWeapon "ItemMatchbox3";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox3" in items player) : {
		player removeWeapon "ItemMatchbox3";
		player addWeapon "ItemMatchbox2";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox2" in items player) : {
		player removeWeapon "ItemMatchbox2";
		player addWeapon "ItemMatchbox1";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox1" in items player) : {
		player removeWeapon "ItemMatchbox1";
		player addWeapon "ItemMatchboxEmpty";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox" in items player) : {
		if (round(random 100) < 30) then {
			player removeWeapon "ItemMatchbox";
			player addWeapon "ItemMatchbox5";
			cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.\nAfter creating the fire you realized that you have 5 matches left.",_text],"PLAIN DOWN"];
		} else {
			cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
		};
	};
};

player removeMagazine "ItemJerrycan";
player addMagazine "ItemJerrycanEmpty";

for "_i" from 0 to _burnTime do {
sleep 1;
_burnTimeLeft = _burnTimeLeft - 1;
};
 
if(_burnTimeLeft == 0 || _burnTimeLeft < 0) then {

_veh setDamage 1;

cutText [format["%1 has been burned and completely destroyed!",_vehType], "PLAIN DOWN"];
	
PVDZ_veh_Save = [_veh,"repair"];
if (isServer) then {
	PVDZ_veh_Save call server_updateObject;
} else {
	publicVariableServer "PVDZ_veh_Save";
};
};

dayz_myCursorTarget = objNull;