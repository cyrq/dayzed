/*
drink_water.sqf
Author: cyrq (cyrq1337@gmail.com).
Made for DayZed: http://dayzed.eu
 
You can use, modify and distribute this without any permissions.
Give credit where credit is due.
 
Description: Gives you the ability to drink water directly from Land_pumpa.
*/

private ["_onLadder","_timeDrink","_staminaGain","_display"];

disableserialization;

_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;

if (_onLadder) exitWith {
	cutText [(localize "str_player_21") , "PLAIN DOWN"];
	s_player_drink = -1;
};

if (((dayz_statusArray select 1) > 0.75)) exitWith {
	cutText ["You may not drink, you're not thirsty.", "PLAIN DOWN"];
};

player removeAction s_player_drink;

if (currentWeapon player != "") then {
	player playActionNow "treated";
} else {
	player playActionNow "PutDown";
};
sleep 2;
[player,"drink",0,false,1] call dayz_zombieSpeak;

if (random 15 < 1) then {
    r_player_infected = true;
    player setVariable["USEC_infected",true,true];
};

player setVariable ["messing",[dayz_hunger,dayz_thirst],true];

_staminaGain = 1500;
R3F_TIRED_Accumulator = R3F_TIRED_Accumulator - _staminaGain;

if (R3F_TIRED_Accumulator < 0) then {
R3F_TIRED_Accumulator = 0;
};

dayz_thirst = 0;

_display = uiNamespace getVariable 'DAYZ_GUI_display';
(_display displayCtrl 1302) ctrlShow true;

cutText ["You have consumed Water and regained some stamina", "PLAIN DOWN"];

s_player_drink = -1;
dayz_myCursorTarget = objNull;