private["_vehicle","_isInspected","_type","_time","_text","_InspectTime"];

_vehicle = cursorTarget;
_isInspected = _vehicle getVariable["vehicleInspected",false];
_swimming = surfaceIsWater (getPosATL player);

_type = typeOf _vehicle;
_text = getText (configFile >> "CfgVehicles" >> _type >> "displayName");

if (_isInspected) exitwith {
	cutText [format["You've already inspected the %1. No need to do it twice.", _text], "PLAIN DOWN"];
};

_time = time;
_InspectTime = 16;

//Engineering
{_vehicle removeAction _x} forEach s_player_repairActions;
s_player_repairActions = [];
s_player_repair_crtl = 1;
player removeAction s_player_inspectVehicle;

if (!_swimming) then {
	cutText [format["Inspecting %1. Stand still...", _text], "PLAIN DOWN"];
	player playMove "AmovPercMstpSnonWnonDnon_carCheckWheel";
	waitUntil { animationState player != "AmovPercMstpSnonWnonDnon_carCheckWheel"};
	player playMove "AmovPercMstpSnonWnonDnon_carCheckPush";

	r_interrupt = false;
	_animState = animationState player;
	r_doLoop = true;
	_started = false;
	_finished = false;
	
	while {r_doLoop} do {
		_animState = animationState player;
		_isInspecting = ["AmovPercMstpSnonWnonDnon_carCheckPush",_animState] call fnc_inString;
		if (_isInspecting) then {
			_started = true;
		};
		if(!_isInspecting && !r_interrupt && (time - _time) < _InspectTime) then {
		_isInspecting = true;
		};
		if (_started && !_isInspecting && (time - _time) > _InspectTime) then {
			r_doLoop = false;
			_finished = true;
		};
		if (r_interrupt) then {
			r_doLoop = false;
		};
		sleep 0.1;
	};
r_doLoop = false;

	if (_finished) then {
		_vehicle setVariable ["vehicleInspected",true,true];
		cutText [format["You've successfully inspected the %1.\nYou may now try to repair or salvage it.", _text], "PLAIN DOWN"];
	} else {
		r_interrupt = false;
		[objNull, player, rSwitchMove,""] call RE;
		player playActionNow "stop";
		cutText ["Inspection failed. Stand still!", "PLAIN DOWN"];
	};
} else {
	cutText [format["Inspecting %1. Stand still...", _text], "PLAIN DOWN"];
	sleep 10;
	_vehicle setVariable ["vehicleInspected",true,true];
	cutText [format["You've successfully inspected the %1.\nYou may now try to repair or salvage it.", _text], "PLAIN DOWN"];
};

dayz_myCursorTarget = objNull;
s_player_repair_crtl = -1;
s_player_inspectVehicle = -1;