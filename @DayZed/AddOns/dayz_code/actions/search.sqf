//Search static objects by cyrq

private ["_hasCrowbar","_searchableObject","_type","_name","_isSearched","_lootAmmount","_chance","_dis","_sfx","_loot"];

if(dayz_searchInProgress) exitWith {
	cutText ["Search already in progress..", "PLAIN DOWN"];
};

dayz_searchInProgress = true;
player removeAction s_player_search;

//Basic defines
	//_hasCrowbar = "ItemCrowbar" in items player;
	_searchableObject = _this select 3;;
	_type = typeOf _searchableObject;
	_name = getText (configFile >> "CfgVehicles" >> _type >> "displayName");
	_isSearched = _searchableObject getVariable["objectsearched",false];
	_lootAmount = floor(random 6);
	_chance = (random 1);

//Loot Tables
	_fridgeLoot = ["FoodCanBakedBeans","FoodSteakRaw","FoodmeatRaw","FoodbeefRaw","FoodmuttonRaw","FoodchickenRaw","FoodrabbitRaw","FoodbaconRaw","FoodgoatRaw","FishRawTrout","FishRawSeaBass","FishRawTuna","FoodCanBakedBeans","FoodCanSardines","FoodCanFrankBeans","FoodCanPasta","FoodCanGriff","FoodCanBadguy","FoodCanBoneboy","FoodCanCorn","FoodCanCurgon","FoodCanDemon","FoodCanFraggleos","FoodCanHerpy","FoodCanDerpy","FoodCanOrlok","FoodCanPowell","FoodCanTylers","FoodCanUnlabeled","FoodCanRusUnlabeled","FoodCanRusPork","FoodCanRusPeas","FoodCanRusMilk","FoodCanRusCorn","FoodCanRusStew","FoodChipsSulahoops","FoodChipsMysticales","ItemSodaEmpty","ItemSodaCoke","ItemSodaPepsi","ItemSodaMdew","ItemSodaMtngreen","ItemSodaR4z0r","ItemSodaClays","ItemSodaSmasht","ItemSodaDrwaste","ItemSodaLemonade","ItemSodaLvg","ItemSodaMzly","ItemSodaRabbit","TrashTinCan","FoodCanGriffEmpty","FoodCanBadguyEmpty","FoodCanBoneboyEmpty","ItemSodaEmpty"];
	
	_closetLoot = ["ItemFishingPole","ItemWaterbottleUnfilled","Winchester1866","ItemCarpenterGuide","DZ_ALICE_Pack_EP1","FoodCanPasta","bwc_SKS","ItemSodaCoke","ItemBandage","ItemHeatPack","ItemPainkiller","ItemDomeTent","TrashTinCan","Skin_Survivor2_DZ","FoodSteakCooked","ItemCards","TrashJackDaniels","ItemTrashRazor","2Rnd_shotgun_74Pellets","2Rnd_shotgun_74Slug","10x_303_DZed","8Rnd_9x18_Makarov","bwc_SKS_mag","15Rnd_W1866_Slug","ItemPainkiller","equip_scrapelectronics"];
	
	_pcLoot = ["ItemSodaCoke","RH_pm","8Rnd_9x18_Makarov","ItemCarpenterGuide","ItemCanOpener","ItemMap","TrashJackDaniels","ItemCards","ItemSodaMtngreen","ItemTrashToiletpaper","ItemSodaEmpty","FoodChipsMysticales","TrashTinCan","ItemTrashRazor","FoodChipsSulahoopsEmpty","ItemWatch","FoodChipsMysticalesEmpty","FoodCanTylersEmpty","equip_scrapelectronics"];
	
	_chimneyLoot = ["TrashTinCan","ItemMatchbox","WoodenArrow","HandRoadFlare","PartWoodPile","PartWoodPile","PartWoodPile","PartWoodPile","PartWoodPile","ItemTrashToiletpaper"];
	
	_medkitLoot = ["ItemBandage","ItemPainkiller","ItemAntibiotic","ItemMorphine","ItemMedKit","ItemBloodbag","ItemEpinephrine","ItemChloroform","ItemHeatPack","ItemSodaMdew"];
	
	_vendingLoot = ["ItemSodaCoke","ItemSodaPepsi","ItemSodaMdew","ItemSodaMtngreen","ItemSodaR4z0r","ItemSodaClays","ItemSodaSmasht","ItemSodaDrwaste","ItemSodaLemonade","ItemSodaLvg","ItemSodaMzly","ItemSodaRabbit","FoodChipsMysticales","FoodChipsSulahoops","FoodCandyLegacys","FoodCandyAnders","FoodNutmix","FoodPistachio"];
	
	_boatLoot = ["ItemFishingPole","TrashJackDaniels","FoodCanUnlabeled","ItemCards","FoodCanRusUnlabeled","ItemToolbox","ItemWaterbottleUnfilled","ItemHeatPack","FoodCanUnlabeledEmpty","DZ_TK_Assault_Pack_EP1","FoodCanRusUnlabeledEmpty","TrashTinCan","ItemSodaEmpty"];
	
	_militarycontainerLoot = ["30Rnd_556x45_G36","ItemKnife","20Rnd_556x45_Stanag","TrashTinCan","Skin_Soldier1_DZ","10x_303_DZed","ItemWaterbottle","ItemMorphine","TrashJackDaniels","ItemGPS","RH_UMP","Binocular","RH_aps","ItemCanOpener","ItemSniperScope","HandGrenade_east","30Rnd_762x39_AK47","Skin_SG_GRU_Assaultman_W_DZed","RH_45ACP_25RND_Mag","equip_scrapelectronics","SmokeShell","ItemSodaEmpty","FoodCanUnlabeledEmpty","ItemBandage","RH_20Rnd_9x18_aps","RH_M4sbr","ItemPainkiller","FoodMRE","ItemFlashlightRed","FoodCanRusUnlabeledEmpty","RH_akms","ItemMatchbox","DZed_MedicPack","ItemWatch","HandRoadFlare","ItemAntibiotic","RH_acrb"];
	
	_militaryammoboxLoot = ["17Rnd_9x19_g17","8Rnd_9x18_Makarov","8Rnd_9x18_MakarovSD","8Rnd_45cal_m1911","10Rnd_22LR_mk2","8Rnd_9x19_Mk","8Rnd_9x19_Mksd","15Rnd_9x19_M9","15Rnd_9x19_M9SD","30Rnd_9x19_tec","20Rnd_32cal_vz61","7Rnd_50_AE","RH_20Rnd_9x18_aps","RH_20Rnd_9x18_apsSD","15Rnd_9x19_usp","15Rnd_9x19_uspsd","8Rnd_B_Beneli_74Slug_DZed","8Rnd_B_Beneli_Pellets_DZed","30Rnd_9x19_MP5","30Rnd_9x19_MP5SD","RH_45ACP_25RND_Mag","RH_45ACP_25RND_SD_Mag","RH_57x28mm_50RND_Mag","RH_57x28mm_50RND_SD_Mag","20Rnd_556x45_Stanag","30Rnd_556x45_Stanag","30Rnd_556x45_StanagSD","30Rnd_556x45_G36","30Rnd_556x45_G36SD","64Rnd_9x19_Bizon","64Rnd_9x19_SD_Bizon","30Rnd_545x39_AK","30Rnd_762x39_AK47","30Rnd_545x39_AKSD","RH_30Rnd_762x39_SDmag","RH_45Rnd_545x39_mag","200Rnd_556x45_M249","50Rnd_127x107_DSHKM","100Rnd_762x54_PK","100Rnd_762x51_M240","5Rnd_762x51_M24","20Rnd_762x51_DMR","7Rnd_M200_DZed","20Rnd_762x51_g3","10Rnd_762x54_SVD","5Rnd_86x70_L115A1_DZed"];

	_metallockerLoot = ["FoodMRE","ItemWaterbottle","HandRoadFlare","Binocular","HandGrenade_east","HandGrenade_west","SmokeShellGreen","SmokeShellRed","SmokeShell","Skin_SG_GRU_Sniper_DZed","ItemGPS_Broken","LeeEnfield_DZed","10x_303_DZed","RH_m9","15Rnd_9x19_M9","M1014_DZed","8Rnd_B_Beneli_Pellets_DZed","8Rnd_B_Beneli_74Slug_DZed","RH_M4a1","30Rnd_556x45_Stanag","RH_pm","8Rnd_9x18_Makarov","Skin_Soldier1_DZ","DZ_CivilBackpack_EP1"];
	
	_washingmachineLoot = ["Skin_SG_GRU_Assaultman_W_DZed","Skin_SG_GRU_TeamLeader_DZed","Skin_SG_GRU_Marksman_W_DZed","Skin_Soldier1_DZed","Skin_Soldier2_DZed","Skin_Soldier3_DZed","Skin_Soldier1_DZ","Skin_Camo1_DZ"];
	
	//The action itself
	if (!_isSearched) then {

		player playActionNow "Medic";
		cutText [format["Searching the %1. Stand still...",_name],"PLAIN DOWN"];
		sleep 1;
		_dis = 3;
		_sfx = "search";
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;

			r_interrupt = false;
			_animState = animationState player;
			r_doLoop = true;
			_started = false;
			_finished = false;

				while {r_doLoop} do {
				_animState = animationState player;
				_isMedic = ["medic",_animState] call fnc_inString;
				if (_isMedic) then {
				_started = true;
				};
				if (_started and !_isMedic) then {
				r_doLoop = false;
				_finished = true;
				};
				if (r_interrupt) then {
				r_doLoop = false;
				};
				sleep 0.1;
				};
				r_doLoop = false;

//Loot add
	if (_finished) then {
		_searchableObject setVariable ["objectsearched",true,true];
			
			if (_chance < 0.4) then {
			cutText ["You've found some trash...","PLAIN DOWN"];
			_searchableObject addMagazineCargoGlobal [(["ItemSodaEmpty","TrashTinCan"] call BIS_fnc_selectRandom),1];
			
			} else {
		
		switch true do {
		
			case (_searchableObject isKindOf "MAP_fridge") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _fridgeLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};
			
			case (_searchableObject isKindOf "MAP_Skrin_bar") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _closetLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};

			case (_searchableObject isKindOf "MAP_Dhangar_psacistul") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _pcLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};

			case (_searchableObject isKindOf "MAP_Dkamna_bila") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _chimneyLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};	

			case (_searchableObject isKindOf "MAP_lekarnicka") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _medkitLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};

			case (_searchableObject isKindOf "VendingMachine") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _vendingLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};	

			case (_searchableObject isKindOf "MAP_BoatSmall_1") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _boatLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};

			case (_searchableObject isKindOf "MAP_bedna_ammo2X") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _militarycontainerLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};
			
			case (_searchableObject isKindOf "MAP_Proxy_UsBasicAmmoBoxSmall") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _militaryammoboxLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};
			
			case (_searchableObject isKindOf "MAP_metalcase_a") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _metallockerLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgWeapons" >> _loot)) : {
					_searchableObject addWeaponCargoGlobal [_loot, 1];
					};
				case (isClass (configFile >> "CfgVehicles" >> _loot)) : {
					_searchableObject addBackpackCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};
			
			case (_searchableObject isKindOf "MAP_washing_machine") : {		
			for "_i" from 0 to _lootAmount do {		
			_loot = _washingmachineLoot call BIS_fnc_selectRandom;
			switch true do {
				case (isClass (configFile >> "CfgMagazines" >> _loot)) : {
					_searchableObject addMagazineCargoGlobal [_loot, 1];
					};
				};
			};
			cutText [format["You've found some goods in the %1!",_name],"PLAIN DOWN"];
			};
			
			//add more cases here here^^
		default {};
		};
	};

	} else {

		r_interrupt = false;
		[objNull, player, rSwitchMove,""] call RE;
		player playActionNow "stop";
		cutText [format["Search failed. Stand still!",_name],"PLAIN DOWN"];
	};
	
	} else {
	cutText [format["It seems that the %1 has been already searched...", _name], "PLAIN DOWN"]
};

dayz_searchInProgress = false;
dayz_myCursorTarget = objNull;
s_player_search -1;