/*
player_checkToolbox.sqf
Author: cyrq (cyrq1337@gmail.com).
Made for DayZed: http://dayzed.eu
 
You can use, modify and distribute this without any permissions.
Give credit where credit is due.
 
Description: Checks items stored in ItemToolBox (via dayz_toolBox) which have been added by player_addToolBox.
*/

_toolboxItems = dayz_toolBox;
_hasToolbox = "ItemToolbox" in items player;
_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;
_text = [];

call gear_ui_init;

if (!_hasToolbox) exitWith {
	closeDialog 0;
	cutText ["You don't have a ToolBox.", "PLAIN DOWN"];
};

if (_onLadder) exitWith {
	closeDialog 0;
	cutText [(localize "str_player_21") , "PLAIN DOWN"];
};

if (count _toolboxItems > 0) then {
	closeDialog 0;
	{ 
	_name = getText (configFile >> "CfgMagazines" >> _x >> "displayName");
		if (!(_name in _text)) then {
			_text set [count _text,_name];			
		};
	} forEach _toolboxItems;
	
	_str = "";
	{
	_str = format ["%1%2, ", _str, _x]
	} foreach _text;

	cutText [format["Toolbox Contents: %1",_str], "PLAIN DOWN"];
};
	