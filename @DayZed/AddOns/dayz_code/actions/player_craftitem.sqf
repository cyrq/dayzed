/*
	Created by Alby exclusively for DayZMod.
	Please request permission to use/alter from Alby.
	Modified for DayZed by cyrq.
*/

private["_craftOption","_bookType","_input","_output","_required","_failChance","_complexTime","_hasInput","_availabeSpace"];

call gear_ui_init;
closeDialog 0;

_craftOption = _this select 0;
_bookType = _this select 1;
_input = getArray (configFile >> "CfgWeapons" >> _bookType >> "ItemActions" >> _craftOption >> "input");
_output = getArray (configFile >> "CfgWeapons" >> _bookType >> "ItemActions" >> _craftOption >> "output");
_required = getArray (configFile >> "CfgWeapons" >> _bookType >> "ItemActions" >> _craftOption >> "required");
_failChance = getNumber (configFile >> "CfgWeapons" >> _bookType >> "ItemActions" >> _craftOption >> "failChance");
_complexTime = getNumber (configFile >> "CfgWeapons" >> _bookType >> "ItemActions" >> _craftOption >> "complexTime");

_hasInput = true;

{
	private ["_avail"];
	_item = _x select 0;
	_selection = _x select 1;
	_amount = _x select 2;

	switch (_selection) do {
		case "CfgWeapons":
		{
			_avail = {_x == _item} count weapons player;
		};
		case "CfgMagazines":
		{
			_avail = {_x == _item} count magazines player;
		};
	};

	if (_avail < _amount) exitWith {
		_hasInput = false;
		_itemName = getText(configFile >> _selection >> _item >> "displayName");
		cutText[format["You don't have the required items.\nYou're missing: %1 %2(s)",(_amount - _avail),_itemName], "PLAIN DOWN"];
	};
}forEach (_input + _required);

if (_hasInput) then {
	_freeSlots = [player] call BIS_fnc_invSlotsEmpty;
	{
		_item = _x select 0;
		_amount = _x select 2;
		_slotType = [_item] call BIS_fnc_invSlotType;
		for "_i" from 1 to _amount do {
			for "_j" from 1 to (count _slotType) do {
				if ((_slotType select _j) > 0) then {
					_freeSlots set[_j, ((_freeSlots select _j) + (_slotType select _j))];
				};
			};
		};
	}forEach _input;

	_availabeSpace = true;
	{

		_item = _x select 0;
		_amount = _x select 2;
		_slotType = [_item] call BIS_fnc_invSlotType;
		for "_i" from 1 to _amount do {
			for "_j" from 1 to (count _slotType) do {
				if ((_slotType select _j) > 0) then {
					_freeSlots set[_j, ((_freeSlots select _j) - (_slotType select _j))];
					if (_freeSlots select _j < 0) exitWith {
						_availabeSpace = false;
						cutText[(localize "str_crafting_space"), "PLAIN DOWN"];
					};
				};
			};
		};
	}forEach _output;
	//sleep 1;

	if (_availabeSpace) then {
		_item = _x select 0;
		_selection = _x select 1;
		_itemName = getText(configFile >> _selection >> _item >> "displayName");
		player playActionNow "Medic";
		cutText [format["Crafting. Stand still!"], "PLAIN DOWN"];
		_sfx = getText (configFile >> "CfgWeapons" >> _bookType >> "ItemActions" >> _craftOption >> "sfx");
		if (_sfx == "") then {_sfx = "repair"};
		_dis = 6;
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;
		
		r_interrupt = false;
		_animState = animationState player;
		r_doLoop = true;
		_started = false;
		_finished = false;
		_time = time;
		
		while {r_doLoop} do {
			_animState = animationState player;
			_isMedic = ["medic",_animState] call fnc_inString;
				if (_isMedic) then {
					_started = true;
				};
				if(!_isMedic && !r_interrupt && (time - _time) < _complexTime) then {
					player playActionNow "Medic";
					_isMedic = true;
				};
				if (_started && !_isMedic && (time - _time) > _complexTime) then {
					r_doLoop = false;
					_finished = true;
				};
				if (r_interrupt) then {
					r_doLoop = false;
				};
				sleep 0.1;
			};
			r_doLoop = false;
		
		if (_finished) then {
		{
			_item = _x select 0;
			_amount = _x select 2;
			for "_i" from 1 to _amount do {
				_selection = _x select 1;
				switch (_selection) do {
					case "CfgWeapons":
					{
						player removeWeapon _item;
					};
					case "CfgMagazines":
					{
						player removeMagazine _item;
					};
				};
				//sleep 0.1;
			};
		}forEach _input;

			{
				_item = _x select 0;
				_selection = _x select 1;
				_amount = _x select 2;
				_itemName = getText(configFile >> _selection >> _item >> "displayName");
				for "_i" from 1 to _amount do {
					if (random 1 > _failChance) then {
						switch (_selection) do {
							case "CfgWeapons":
							{
								player addWeapon _item;
							};
							case "CfgMagazines":
							{
								player addMagazine _item;
							};
						};
						cutText[format[(localize "str_crafting_success"),_itemName], "PLAIN DOWN"];
						//sleep 2;
					} else {
						cutText[format[(localize "str_crafting_failed"),_itemName], "PLAIN DOWN"];
						//sleep 2;
					};
				};

			}forEach _output;
			
		} else {
			_item = _x select 0;
			_selection = _x select 1;
			_itemName = getText(configFile >> _selection >> _item >> "displayName");
			r_interrupt = false;
			player switchMove "";
			player playActionNow "stop";
			{
			_item = _x select 0;
			_amount = _x select 2;
			for "_i" from 1 to _amount do {
				_selection = _x select 1;
				switch (_selection) do {
					case "CfgWeapons":
					{
						player removeWeapon _item;
					};
					case "CfgMagazines":
					{
						player removeMagazine _item;
					};
				};
				//sleep 0.1;
			};
		}forEach _input;
		cutText [format["You moved! Crafting failed!\nAll crafting items are lost!"], "PLAIN DOWN"];
		};
	};
};