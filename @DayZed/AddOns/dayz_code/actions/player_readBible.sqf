/*
Created by cyrq for DayZed.
Returns the player humanity value + a known quote from the bible.
*/

private ["_humanity","_quotes","_randomQuote"];

closeDialog 0;

_humanity = player getVariable["humanity",0];
_quotes = [
		"Philippians 4:13: ""I can do all things through him who strengthens me.""",
		"John 3:16: ""For God so loved the world, that he gave his only Son,\nthat whoever believes in him should not perish but have eternal life.""",
		"Isaiah 41:10: ""Fear not, for I am with you; be not dismayed, for I am your God;\nI will strengthen you, I will help you, I will uphold you with my righteous right hand.""",
		"Proverbs 3:5-5: ""Trust in the LORD with all your heart, and do not lean on your own understanding. In all your ways acknowledge him, and he will make straight your paths.""",
		"Proverbs 18:10: ""The name of the LORD is a strong tower;\nthe righteous man runs into it and is safe.""",
		"Deuteronomy 31:6: ""Be strong and courageous. Do not fear or be in dread of them, for it is the LORD your God who goes with you.He will not leave you or forsake you.""",
		"Galatians 5:22-23: ""But the fruit of the Spirit is love, joy, peace, patience,\nkindness, goodness, faithfulness, gentleness, self-control; against such things there is no law.""",
		"1 Chronicles 22:13: ""Then you will prosper if you are careful to observe the statutes and the rules that the LORD commanded Moses for Israel.\nBe strong and courageous. Fear not; do not be dismayed.""",
		"Ezra 10:4: ""Arise, for it is your task, and we are with you;\nbe strong and do it.""",
		"Hebrews 10:23: Let us hold fast the confession of our hope without wavering,\nfor he who promised is faithful.""",
		"Hebrews 12:3: ""Consider him who endured from sinners such hostility against himself,\nso that you may not grow weary or fainthearted.""",
		"1 Peter 3:14: ""But even if you should suffer for righteousness’ sake, you will be blessed.\nHave no fear of them, nor be troubled."""
		];
_randomQuote = _quotes call BIS_fnc_selectRandom;

cutText [format["Your Humanity: %1.\n%2", _humanity,_randomQuote], "PLAIN DOWN"];
		
		
