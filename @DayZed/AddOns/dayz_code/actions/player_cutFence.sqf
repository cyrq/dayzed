//cut fences by cyrq

private["_fences","_tree","_dis","_sfx","_findNearestFence","_objName","_distance2d","_distance3d"];

_fences = Fences;

call gear_ui_init;
closeDialog 1;

_findNearestFence = [];
{
	if("" == typeOf _x) then {	
		if (alive _x) then {	
			_objName = _x call dayz_getModelName;
			if (_objName in _fences) exitWith { 
				_findNearestFence set [(count _findNearestFence),_x];
			};
		};
	};

} foreach nearestObjects [getPos player, [], 20];

if (count(_findNearestFence) >= 1) then {
	_fence = _findNearestFence select 0;

	_distance2d = [player, _fence] call BIS_fnc_distance2D;
	_distance3d = player distance _fence;

	if(_distance2d <= 3) then {
	
	cutText ["Cutting down fence...", "PLAIN DOWN"];	
	player playActionNow "Medic";
	
	sleep 5;	
	
	if("" == typeOf _fence) then {
		_fence setDamage 1;
		cutText ["You've successfully cut down the fence.", "PLAIN DOWN"];
		_dis = 10;
		_sfx = "fence";
		sleep 1;
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;
	};

	} else {
		cutText ["You must be close to a fence to cut it down.", "PLAIN DOWN"];
	};
} else {
	cutText ["You must be close to a fence to cut it down.", "PLAIN DOWN"];
};