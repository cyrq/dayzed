private ["_create","_config","_create_magsize","_type","_slotstart","_slotend","_dialog","_qty_total_ammo","_control","_mag","_qtynew_create_mags_full","_qtynew_create_ammo_rest"];

disableSerialization;
call gear_ui_init;

closeDialog 0;

if (r_action_count != 1) exitWith { cutText [localize "str_player_actionslimit", "PLAIN DOWN"]; };

_create = _this;
_create_magsize = getNumber(configFile >> "CfgMagazines" >> _create >> "count");
_type = getNumber(configFile >> "CfgMagazines" >> _create >> "type");
_name = getText(configFile >> "CfgMagazines" >> _create >> "displayName");
_magCount = {_x == _create} count magazines player;

if (!(_create in magazines player)) exitWith {r_action_count = 0;};
if (_magCount == 1) exitWith {
	cutText [format["You have only one %1\nThere's nothing to combine.", _name], "PLAIN DOWN"];
	sleep 1;
	r_action_count = 0;
};

_config = configFile >> "CfgMagazines" >> _create;

player playActionNow "PutDown";

//primary/secondary mags?
_slotstart = 0;
_slotend = 0;
if ((_type == 256) or (_type == 256*2)) then {
	_slotstart = 109;
	_slotend = 120;
};
if (_type == 16) then {
	_slotstart = 122;
	_slotend = 129;
};

_dialog = findDisplay 106;

_qty_total_ammo = 0;
for "_i" from _slotstart to _slotend do {
	_control = _dialog displayCtrl _i;
	_mag = gearSlotData _control;
	if (_mag == _create) then {
		_qty_total_ammo = _qty_total_ammo + gearSlotAmmoCount _control;
	};
};

_qtynew_create_mags_full = 0;
_qtynew_create_ammo_rest = 0;
_qtynew_create_mags_full = floor(_qty_total_ammo/_create_magsize);
_qtynew_create_ammo_rest = _qty_total_ammo - (_qtynew_create_mags_full*_create_magsize);

player removeMagazines _create;
for "_i" from 1 to _qtynew_create_mags_full do {
	player addMagazine _create;
};
if (_qtynew_create_ammo_rest != 0) then {
	player addMagazine [_create,_qtynew_create_ammo_rest];
};

switch true do {
	case (_qtynew_create_ammo_rest == 0) : {
		cutText [format["You've combined %1 non-full %2(s) into %3 full magazines.",_magCount, _name, _qtynew_create_mags_full], "PLAIN DOWN"];
		};
	case (_qtynew_create_mags_full == 0) : {
		cutText [format["You've combined %1 non-full %2(s) into one.\nYou have %3 bullets left",_magCount, _name, _qtynew_create_ammo_rest], "PLAIN DOWN"];
		};
	default {
		cutText [format["You've combined %1 non-full %2(s) into %3 full magazines.\nLast magazine still has %4 bullets inside.",_magCount, _name, _qtynew_create_mags_full, _qtynew_create_ammo_rest], "PLAIN DOWN"];
	};
};
sleep 1;
r_action_count = 0;