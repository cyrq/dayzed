/*
player_addToolBox.sqf
Author: cyrq (cyrq1337@gmail.com).
Made for DayZed: http://dayzed.eu
 
You can use, modify and distribute this without any permissions.
Give credit where credit is due.
 
Description: Adds items to ItemToolBox or ItemToolBoxEmpty which are stored in a sub-array in in players inventory.
			 The array is synced with the Hive, similar to dayz_onBack.
*/

_item = _this;
_hasItem = _this in magazines player;
_hasToolbox = "ItemToolbox" in items player;
_hasToolboxEmpty = "ItemToolboxEmpty" in items player;
_toolboxItems = dayz_toolBox;
_name = getText (configFile >> "CfgMagazines" >> _item >> "displayName");
_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;

call gear_ui_init;

if ((!_hasToolbox) AND (!_hasToolboxEmpty)) exitWith {
	closeDialog 0;
	cutText ["You don't have a ToolBox.", "PLAIN DOWN"];
};

if (_onLadder) exitWith {
	closeDialog 0;
	cutText [(localize "str_player_21") , "PLAIN DOWN"];
};

if (_item in _toolboxItems) exitWith {
	closeDialog 0;
	cutText [format["You already have a %1 in your ToolBox.",_name], "PLAIN DOWN"];
};

if (_hasItem) then {
	closeDialog 0;
	
	player playActionNow "PutDown";
	sleep 2;
	
	player removeMagazine _item;
	
	if ((_hasToolboxEmpty) AND (!_hasToolbox)) then {
		player removeWeapon "ItemToolboxEmpty";
		player addWeapon "ItemToolbox";
	};
	
	_toolboxItems set [count _toolboxItems,_item];
	dayz_toolBox = _toolboxItems;
	
	cutText [format["You've added a %1 to your ToolBox.",_name], "PLAIN DOWN"];
};