/*
player_fixEQ.sqf
Author: cyrq (cyrq1337@gmail.com).
Made for DayZed: http://dayzed.eu
 
You can use, modify and distribute this without any permissions.
Give credit where credit is due.
 
Description: Swaps broken Equipment classes with proper ones.
*/

private ["_part","_output","_hasItem","_hasPart","_name","_animState","_started","_finished","_isMedic"];

closeDialog 0;

_part = "equip_scrapelectronics";
_hasItem = _this in weapons player;
_hasPart = _part in magazines player;

switch (_this) do {
	case "NVGoggles_Broken" : {
		_output = "NVGoggles";
	};
	case "ItemGPS_Broken" : {
		_output = "ItemGPS";
	};
	case "Binocular_Vector_Broken" : {
		_output = "Binocular_Vector";
	};
	case "ItemRadio_Broken" : {
		_output = "ItemRadio";
	};
};

_name = getText (configFile >> "CfgWeapons" >> _output >> "displayName");

if (_hasItem AND _hasPart) then {
	if (vehicle player == player) then {
	player playActionNow "Medic";
};

r_interrupt = false;
_animState = animationState player;
r_doLoop = true;
_started = false;
_finished = false;
[player,"attachment",0,false] call dayz_zombieSpeak;

while {r_doLoop} do {
	_animState = animationState player;
	_isMedic = ["medic",_animState] call fnc_inString;
	if (_isMedic) then {
		_started = true;
	};
	if (_started and !_isMedic) then {
		r_doLoop = false;
		_finished = true;
	};
	if (r_interrupt) then {
		r_doLoop = false;
	};
	if (vehicle player != player) then {
		sleep 3;
		r_doLoop = false;
		_finished = true;
	};
	sleep 0.1;
};
r_doLoop = false;

if (_finished) then {
	player removeWeapon _this;
	player removeMagazine _part;
	player addWeapon _output;
	cutText [format["You've successfully repaired the %1!", _name], "PLAIN DOWN"];
} else {
	r_interrupt = false;
	[objNull, player, rSwitchMove,""] call RE;
	player playActionNow "stop";
	cutText [format["You moved! Stand Still!"], "PLAIN DOWN"];
};
} else {
	cutText [format["You think that some kind of electronic part\nmay be able to fix the %1", _name], "PLAIN DOWN"];
};