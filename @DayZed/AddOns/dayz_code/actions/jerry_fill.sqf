private ["_qty","_dis","_dis2","_sfx","_sfx2","_started","_finished","_animState","_isRefuel","_fuelcans","_qty","_fillCounter","_abort","_fuelAmount","_hasFuelAlready","_posPlayer","_Barell","_newfuelAmount"];

player removeAction s_player_fillfuel;
//s_player_fillfuel = -1;

_fuelcans = [];

{
if (_x == "ItemJerrycanEmpty" or _x == "ItemFuelcanEmpty") then {
_fuelCans set [(count _fuelCans),_x];
};
} forEach magazines player;

_qty = count _fuelCans;
_fillCounter = 0;
_abort = false;

_fuelAmount = 0;

//Thanks to Allen Kaganovsky
{ _hasFuelAlready = count ((position player) nearObjects ["Land_Fire_barrel", 30]) > 0;

if (!_hasFuelAlready) then {
    _posPlayer = [((getPos player) select 0), ((getPos player) select 1),-20];
    _Barell = createVehicle ["Land_Fire_barrel", _posPlayer, [], 0, "CAN_COLLIDE"];
    _Barell setVariable ["Sarge",1,true];
    _fuelAmount = round(random 10);	
    _Barell setVariable ["fuelAmount",_fuelAmount,true];
} else {
    _Barell = (position player) nearObjects ["Land_Fire_barrel", 30];
	_Barell = _Barell select 0;
    _fuelAmount = _Barell getVariable["fuelAmount",0];
};

_isFillingATM = _Barell getVariable ["isFilling",false];

if !(("ItemJerrycanEmpty" in magazines player) or ("ItemFuelcanEmpty" in magazines player)) exitWith {
	cutText [(localize "str_player_10") , "PLAIN DOWN"];
};

if (("ItemJerrycanEmpty" in magazines player) or ("ItemFuelcanEmpty" in magazines player)) then {

if (_isFillingATM) exitwith {
	cutText [format["Someone is already filling his Jerry/Fuel Can using this tank...", _text], "PLAIN DOWN"]
};

if (_fuelAmount == 0) exitWith {
	cutText [format["There's no more fuel in this tank."], "PLAIN DOWN"];
	_dis2=5;
	_sfx2 = "no_fuel";
	[player,_sfx2,0,false,_dis2] call dayz_zombieSpeak;
	[player,_dis2,true,(getPosATL player)] call player_alertZombies;
	};
	
	_fillCounter = _fillCounter + 1;

	player playActionNow "Medic";
	_Barell setVariable ["isFilling",true,true];
	
	if(_qty == 1) then {
		cutText ["Filling can. Stand stil.. ", "PLAIN DOWN"];
	} else {
		cutText [format[("Filling cans (%1 of %2). Stand still..."),_fillCounter,_qty] , "PLAIN DOWN"];
	};

	_dis=5;
	_sfx = "refuel";
	[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
	[player,_dis,true,(getPosATL player)] call player_alertZombies;

	r_interrupt = false;
	_animState = animationState player;
	r_doLoop = true;
	_started = false;
	_finished = false;
	while {r_doLoop} do {
		_animState = animationState player;
		_isRefuel = ["medic",_animState] call fnc_inString;
		if (_isRefuel) then {
			_started = true;
		};
		if (_started and !_isRefuel) then {
			r_doLoop = false;
			_finished = true;
		};
		if (r_interrupt) then {
			r_doLoop = false;
		};
		sleep 0.1;
	};

	r_doLoop = false;

	if (!_finished) then {
		r_interrupt = false;
		[objNull, player, rSwitchMove,""] call RE;
		player playActionNow "stop";
		_Barell setVariable ["isFilling",false,true];
		if(_fillCounter == 1) then {
			cutText ["Filling cancelled. You didn't fill any cans.", "PLAIN DOWN"];
		} else {
			cutText [format[("Filling cancelled. Filled only %1 of %2 cans."),(_fillCounter - 1),_qty] , "PLAIN DOWN"];
		};
		_abort = true;
		};

	if (_finished) then {
	
		_Barell = (position player) nearObjects ["Land_Fire_barrel", 30];
		_Barell = _Barell select 0;
		_newfuelAmount = _fuelAmount - 1;
		_Barell setVariable ["fuelAmount",_newfuelAmount,true];
		_Barell setVariable ["isFilling",false,true];
	
		if (_x == "ItemJerrycanEmpty") then {
			player removeMagazine "ItemJerrycanEmpty";
			player addMagazine "ItemJerrycan";
		};
		if (_x == "ItemFuelcanEmpty") then {
			player removeMagazine "ItemFuelcanEmpty";
			player addMagazine "ItemFuelcan";
		};
		if(_qty == 1) then {
			cutText ["You successfully filled the Can", "PLAIN DOWN"];
		} else {		
			cutText [format[("Successfully filled can (%1 of %2)."),_fillCounter,_qty] , "PLAIN DOWN"];
		};
	};
	sleep 1;
};
		sleep 1;
		if(_abort) exitWith {};

} forEach _fuelCans;