private["_id","_array","_vehicle","_part","_hitpoint","_type","_hasToolbox","_section","_nameType","_namePart","_damage","_selection","_dis","_sfx","_hitpoints","_allFixed"];

_id = _this select 2;
_array = _this select 3;
_vehicle = _array select 0;
_part = _array select 1;
_hitpoint = _array select 2;
_type = typeOf _vehicle;
_swimming = surfaceIsWater (getPosATL player);

_nameType = getText(configFile >> "cfgVehicles" >> _type >> "displayName");
_namePart = getText(configFile >> "cfgMagazines" >> _part >> "displayName");

_hasToolbox = "ItemToolbox" in items player;
_section = _part in magazines player;

{_vehicle removeAction _x} forEach s_player_repairActions;
s_player_repairActions = [];
s_player_repair_crtl = 1;

if (_section and _hasToolbox) then {

	player removeMagazine _part;

	if (_part == "PartFueltank" OR _part == "PartEngine" OR _part == "PartVRotor") then {
			
		_anim = "ActsPercSnonWnonDnon_carFixing";
		
		if (!_swimming) then {
			player playMove _anim;
			sleep 15;
		};

		_dis=20;
		_sfx = "repair";
			
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;
		
		sleep 28; //once more
		
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;
		
		waitUntil {animationState player != _anim};
		[objNull, player, rSwitchMove,""] call RE;
		player playActionNow "stop";
		
	} else {
	
		_anim = "ActsPercSnonWnonDnon_carFixing2";
		
		if (!_swimming) then {
			player playMove _anim;
			sleep 8;
		};

		_dis=20;
		_sfx = "repair";
			
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;
		
		sleep 20; //once more
		
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;
		
		waitUntil {animationState player != _anim};
		[objNull, player, rSwitchMove,""] call RE;
		player playActionNow "stop";		
	};

	_damage = [_vehicle,_hitpoint] call object_getHit;
	_vehicle removeAction _id;
	//dont waste loot on undamaged parts
	if (_damage > 0) then {

		//Fix the part
		_selection = getText(configFile >> "cfgVehicles" >> _type >> "HitPoints" >> _hitpoint >> "name");

		[_vehicle, _selection, 0, true] call fnc_veh_handleRepair;
		_vehicle setvelocity [0,0,1];

		//Success!
		cutText [format[localize "str_player_04",_namePart,_nameType], "PLAIN DOWN"];
	} else { player addMagazine _part; };

} else {
	cutText [format[localize "str_player_03",_namePart], "PLAIN DOWN"];
};


//check if repaired fully
_hitpoints = _vehicle call vehicle_getHitpoints;
_allFixed = true;
{
	_damage = [_vehicle,_x] call object_getHit;
	if (_damage > 0) exitWith {
		_allFixed = false;
	};
} forEach _hitpoints;

//update if repaired
if (_allFixed) then {
	_vehicle setDamage 0;
	PVDZ_veh_Save = [_vehicle,"repair"];
	if (isServer) then {
		PVDZ_veh_Save call server_updateObject;
	} else {
		publicVariableServer "PVDZ_veh_Save";
	};
};

dayz_myCursorTarget = objNull;
s_player_repair_crtl = -1;