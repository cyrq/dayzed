call gear_ui_init;
player removeWeapon "ItemFlashlightRed";
player addweapon "ItemFlashlightReduse";
pzn_light = "pzn_NoSearchLight_t" createVehicle getpos player; 
pzn_light attachto  [player, [.35,-.5,-1.2],"RightShoulder"];
pzn_light setvectorup [-.3,.3,1];
player action ["lightOn", pzn_light];
pzn_fstate = true;
dayz_hasLight = true;
pzn_keyDown = { 
	private["_dikCode", "_ctrlKey"];
	_dikCode = _this select 1;
	if (_dikCode in (actionkeys "Headlights")) then {
		if (pzn_fstate) then {
			player action ["lightOff", pzn_light];
			pzn_fstate = false;
			dayz_hasLight = false;
		} else {
			player action ["lightOn", pzn_light];
			pzn_fstate = true;
			dayz_hasLight = true;
		};
	};
	false;
};
waitUntil {!isNull(findDisplay 46)};
(findDisplay 46) displayAddEventHandler ["keyDown", "_this call pzn_keyDown"];
player spawn {
	waituntil {isnull _this};
	detach pzn_light;
	deletevehicle pzn_light;
	dayz_hasLight = false;
};

while {(alive pzn_light) and dayz_hasLight} do {
		if (vehicle player != player) then {
		player action ["lightOff", pzn_light];
		detach pzn_light;
		deletevehicle pzn_light; 
		pzn_fstate = false;
		dayz_hasLight = false;
		player removeWeapon "ItemFlashlightReduse";
		player addweapon "ItemFlashlightRed";
	};
	sleep 0.1;
};