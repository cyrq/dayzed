private ["_timeDrink","_onLadder","_itemorignal","_hasdrinkitem","_hasoutput","_config","_text","_invehicle","_sfx","_itemtodrop","_nearByPile","_item","_display","_sfxdis","_staminaGain"];

disableserialization;
call gear_ui_init;

_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;
if (_onLadder) exitWith {cutText [(localize "str_player_21") , "PLAIN DOWN"]};

//if (vehicle player != player) exitWith {cutText [localize "str_player_fail_drink", "PLAIN DOWN"]};

//Force players to wait 5 mins to drink again
if (((dayz_statusArray select 1) > 0.75)) exitWith {
closeDialog 0;
cutText ["You may not drink, you're not thirsty.", "PLAIN DOWN"];
};

_itemorignal = _this;
_hasdrinkitem = _itemorignal in magazines player;
_hasoutput = _itemorignal in drink_with_output;
_invehicle = false;

_config = configFile >> "CfgMagazines" >> _itemorignal;
_text = getText (_config >> "displayName");

//getting type of sfx (now just drink od soda open and drink)
_sfx = getText (_config >> "sfx");
//Get distance of sfx sound
_sfxdis = getNumber (_config >> "sfxdis");

if (!_hasdrinkitem) exitWith {cutText [format[(localize "str_player_31"),_text,(localize "str_player_31_drink")] , "PLAIN DOWN"]};

if (vehicle player != player) then {
	_display = findDisplay 106;
	_display closeDisplay 0;
	_invehicle = true;
} else {
player playActionNow "PutDown";
closeDialog 1;
};
player removeMagazine _itemorignal;

sleep 1;

if (_itemorignal in no_output_drink) then {
	Switch true do {
		case (_itemorignal == "ItemMilkbottle") : {
		player addMagazine "ItemMilkbottleUnfilled";
		};
		case (_itemorignal == "ItemWaterbottle") : {
		player addMagazine "ItemWaterbottleUnfilled";
		};
		case (_itemorignal == "ItemWaterbottleBoiled") : {
		player addMagazine "ItemWaterbottleUnfilled";	
		};
	default {};
	};
};

[player,_sfx,0,false,_sfxdis] call dayz_zombieSpeak;
[player,_sfxdis,true,(getPosATL player)] call player_alertZombies;

if (_hasoutput and !_invehicle) then {
    // Selecting output
    _itemtodrop = drink_output select (drink_with_output find _itemorignal);

    sleep 3;
    _nearByPile= nearestObjects [(getPosATL player), ["WeaponHolder","WeaponHolderBase"],2];
    if (count _nearByPile ==0) then {
        _item = createVehicle ["WeaponHolder", (getPosATL player), [], 0.0, "CAN_COLLIDE"];
    } else {
        _item = _nearByPile select 0;
    };
    _item addMagazineCargoGlobal [_itemtodrop,1];
	_item setvelocity [0,0,1];
};

if (_hasoutput && _invehicle) then {
	_itemtodrop = drink_output select (drink_with_output find _itemorignal);
	_vehicle = (vehicle player);
	sleep 3;
	_vehicle addMagazineCargoGlobal [_itemtodrop,1];
};

//add infection chance for "ItemWaterbottle",
if ((random 15 < 1) and (_itemorignal == "ItemWaterbottle")) then {
    r_player_infected = true;
    player setVariable["USEC_infected",true,true];
};

player setVariable ["messing",[dayz_hunger,dayz_thirst],true];

if (_itemorignal == "ItemMilkbottle") then {
_staminaGain = 3000;
	} else {
_staminaGain = 1500;
};

R3F_TIRED_Accumulator = R3F_TIRED_Accumulator - _staminaGain;

if (R3F_TIRED_Accumulator < 0) then {
R3F_TIRED_Accumulator = 0;
};

dayz_thirst = 0;

//Ensure Control is visible
_display = uiNamespace getVariable 'DAYZ_GUI_display';
(_display displayCtrl 1302) ctrlShow true;

cutText [format[(localize "str_player_consumed"),_text], "PLAIN DOWN"];