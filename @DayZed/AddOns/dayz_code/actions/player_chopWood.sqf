private["_trees","_tree","_dis","_sfx","_dis2","_sfx2","_findNearestTree","_objName","_distance2d","_distance3d","_item","_itemOut"];

_trees = Trees;

call gear_ui_init;
closeDialog 1;

if ((currentWeapon player) != "MeleeHatchet") exitWith {
cutText ["You need to have a Hatchet in your hands to chop down a tree!", "PLAIN DOWN"];
};

_findNearestTree = [];
{
	if("" == typeOf _x) then {	
		if (alive _x) then {	
			_objName = _x call dayz_getModelName;
			if (_objName in _trees) exitWith { 
				_findNearestTree set [(count _findNearestTree),_x];
			};
		};
	};

} foreach nearestObjects [getPos player, [], 20];

if (count(_findNearestTree) >= 1) then {
	_tree = _findNearestTree select 0;

	// get 2d distance
	_distance2d = [player, _tree] call BIS_fnc_distance2D;
	_distance3d = player distance _tree;

	if(_distance2d <= 3) then {
	
	//Remove melee magazines (BIS_fnc_invAdd fix) (add new melee ammo to array if needed)
	{player removeMagazines _x} forEach ["hatchet_swing","crowbar_swing","Machete_swing","Fishing_Swing"];
		
		cutText ["Chopping down tree...", "PLAIN DOWN"];
	
		_dis = 20;
		_sfx = "chopwood";
		
		_dis2 = 35;
		_sfx2 = "tree_fall";
		
		[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
		[player,_dis,true,(getPosATL player)] call player_alertZombies;
		
		player playActionNow "GestureSwing";
		sleep 1;
		player playActionNow "GestureSwing";
		sleep 1;
		player playActionNow "GestureSwing";
		sleep 1;
		player playActionNow "GestureSwing";
		sleep 1;
		
		_itemOut = "PartWoodPile";
                        
		_item = createVehicle ["WeaponHolder", getPosATL player, [], 1, "CAN_COLLIDE"];
		_item addMagazineCargoGlobal [_itemOut, 1];

		player reveal _item;
		
		// destroy tree
		if("" == typeOf _tree) then {
		[player,_sfx2,0,false,_dis2] call dayz_zombieSpeak;
		[player,_dis2,true,(getPosATL player)] call player_alertZombies;
		_tree setDamage 1;
		};
		
		sleep 2.5;

		player action ["Gear", _item];
		
	//adding melee mags back if needed
	switch (primaryWeapon player) do {
		case "MeleeHatchet": {player addMagazine 'hatchet_swing';};
		case "MeleeCrowbar": {player addMagazine 'crowbar_swing';};
		case "MeleeMachete": {player addMagazine 'Machete_swing';};
		case "MeleeFishingPole": {player addMagazine 'Fishing_Swing';};
		};
	} else {
		cutText [localize "str_player_23", "PLAIN DOWN"];
	};
} else {
	cutText [localize "str_player_23", "PLAIN DOWN"];
};