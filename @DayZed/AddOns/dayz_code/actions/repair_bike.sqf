private["_bike","_hitpoints","_damage","_selection","_type","_text","_hasToolbox","_allFixed","_damage"];

_bike = cursorTarget;
_hitpoints = _bike call vehicle_getHitpoints;
_type = typeOf _bike; 
_text = getText (configFile >> "CfgVehicles" >> _type >> "displayName");
_hasToolbox = "ItemToolbox" in items player;

{dayz_myCursorTarget removeAction _x} forEach s_player_repairActions;s_player_repairActions = [];
dayz_myCursorTarget = objNull;

	_allFixed = true;
	{
	_damage = [_bike,_x] call object_getHit;
	if (_damage > 0) exitWith {
	_allFixed = false;
        };
	} forEach _hitpoints;

if (_allFixed) exitWith {cutText [format["The %1 is already fully repaired.", _text], "PLAIN DOWN"]};

if (_hasToolbox) then {

	player playActionNow "Medic";
	sleep 1;

	_dis=20;
	_sfx = "repair";
	[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
	[player,_dis,true,(getPosATL player)] call player_alertZombies;
	
	sleep 3;

	{
    _selection = getText(configFile >> "cfgVehicles" >> _type >> "HitPoints" >> _x >> "name");
    [_bike, _selection, 0, true] call fnc_veh_handleRepair;
    PVDZ_veh_Save = [_bike,"repair"];
    if (isServer) then {
        PVDZ_veh_Save call server_updateObject;
    } else {
        publicVariableServer "PVDZ_veh_Save";
    };
    sleep 0.5;
	} forEach _hitpoints;
	
	_bike setvelocity [0,0,1];
	
	cutText [format["You've successfully repaird the %1.", _text], "PLAIN DOWN"];
};

