private ["_item","_onLadder","_hasclothesitem","_config","_text","_myModel","_humanity","_isBandit","_isHero","_itemNew","_skinToArray","_finalArray","_skinToModel","_model"];

closeDialog 0;

_item = _this;
call gear_ui_init;
r_action_count = 0; //reset for strange glitch
_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;
if (_onLadder) exitWith {cutText [(localize "str_player_21") , "PLAIN DOWN"]};

_hasclothesitem = _this in magazines player;
_config = configFile >> "CfgMagazines";
_text = getText (_config >> _item >> "displayName");

if (!_hasclothesitem) exitWith {cutText [format[(localize "str_player_31"),_text,(localize "str_player_31_wear")] , "PLAIN DOWN"]};

if (vehicle player != player) exitWith {cutText [localize "str_player_fail_wear1", "PLAIN DOWN"]};

_myModel = (typeOf player);
_humanity = player getVariable ["humanity",0];
_isBandit = _humanity < -2000;
_isHero = _humanity > 5000;
_itemNew = "Skin_" + _myModel;

if ( !(isClass(_config >> _itemNew)) ) then {
	_itemNew = "Skin_Survivor2_DZ";
};

_skinToArray = toArray (_item);
_skinToArray set [0,"del"];
_skinToArray set [1,"del"];
_skinToArray set [2,"del"];
_skinToArray set [3,"del"];
_skinToArray set [4,"del"];
_finalArray = _skinToArray - ["del"];
_skinToModel = toString (_finalArray);

_model = _skinToModel;
	
if (_item == "Skin_Survivor2_DZ") then {
	if (_isBandit) then {
		_model = "Bandit1_DZ";
	};
	if (_isHero) then {
		_model = "Survivor3_DZ";
	};
};

if (_model != _myModel) then {
	player removeMagazine _item;
	player addMagazine _itemNew;
	player setVariable ["model_CHK",_model,true];
	cutText ["Since we don't allow nudity in-game, Your clothes will be changed after re-login.", "PLAIN DOWN"];
	PVDZ_sec_atp = format ["MODEL SWITCH: PLAYER [%1] SWITCHED CLOTHES FROM [%2] TO [%3]", name player, _myModel, _model];
	publicVariableServer "PVDZ_sec_atp";
} else {
	cutText ["You've already switched your clothes once/nRelog to wear them.", "PLAIN DOWN"];
};