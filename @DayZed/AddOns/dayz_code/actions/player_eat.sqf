private ["_timeEat","_onLadder","_itemorignal","_hasfooditem","_rawfood","_cookedfood","_invehicle","_hasoutput","_config","_text","_regen","_dis","_sfx","_itemtodrop","_nearByPile","_item","_display","_pos","_hasCanOpener","_hasKnife","_sfx2","_dis2","_noChips","_staminaGain"];

disableserialization;
call gear_ui_init;
_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;
if (_onLadder) exitWith {cutText [(localize "str_player_21") , "PLAIN DOWN"]};

//Force players to wait 5 mins to eat again
if (((dayz_statusArray select 0) > 0.75)) exitWith {
	closeDialog 0;
	cutText ["You may not eat, you're not hungry.", "PLAIN DOWN"];
};

_hasCanOpener = "ItemCanOpener" in items player;
_hasKnife = ("ItemKnife" in items player) || ("ItemKnife5" in items player) || ("ItemKnife4" in items player) || ("ItemKnife3" in items player) || ("ItemKnife2" in items player) || ("ItemKnife1" in items player) || ("ItemKnifeBlunt" in items player) ;

_itemorignal = _this;
_hasfooditem = _itemorignal in magazines player;

_rawfood = _itemorignal in meatraw;
_cookedfood = _itemorignal in meatcooked;
_hasoutput = _itemorignal in food_with_output;
_noChips = (_itemorignal != "FoodChipsSulahoops") and (_itemorignal != "FoodChipsMysticales");
_invehicle = false;

_config = configFile >> "CfgMagazines" >> _itemorignal;
_text = getText (_config >> "displayName");
_regen = getNumber (_config >> "bloodRegen");

if (!_hasCanOpener and !_hasKnife and _hasoutput and _noChips) exitWith {
	closeDialog 0;
	cutText [format["You need something to open up the %1.", _text], "PLAIN DOWN"];
};
	

if (!_hasfooditem) exitWith {cutText [format[(localize "str_player_31"),_text,(localize "str_player_31_consume")] , "PLAIN DOWN"]};

if (vehicle player != player) then {
	_display = findDisplay 106;
	_display closeDisplay 0;
	_invehicle = true;
} else {
	if (currentWeapon player != "") then {
		player playActionNow "treated";
	} else {
		player playActionNow "PutDown";
	};
	closeDialog 0;
};

player removeMagazine _itemorignal;
sleep 1;

if (_hasoutput and _noChips) then {
_dis2 = 8;
_sfx2 = "can_open";

[player,_sfx2,0,false,_dis2] call dayz_zombieSpeak;
[player,_dis2,true,(getPosATL player)] call player_alertZombies;

sleep 3;
};

_dis=6;
_sfx = getText (_config >> "sfx");

//set _sfx to eat if nothing is defined
if (_sfx == "") then {_sfx = "eat"};

[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
[player,_dis,true,(getPosATL player)] call player_alertZombies;

if (_hasoutput and !_invehicle) then{
    // Selecting output
    _itemtodrop = food_output select (food_with_output find _itemorignal);

    sleep 3;
    _nearByPile= nearestObjects [getPosATL player, ["WeaponHolder"], 2];
    _item = if (count _nearByPile > 0) then {_nearByPile select 0} else {nil};
    if ((isNil "_item") OR {(player distance _item > 2)}) then {
    	_pos = player modeltoWorld [0,1,0];
    	//diag_log format [ "%1 itempos:%2 _nearByPile:%3", __FILE__, _pos, _nearByPile];
        _item = createVehicle ["WeaponHolder", _pos, [], 0.0, "CAN_COLLIDE"];
        _item setPosATL _pos;
	};
    _item addMagazineCargoGlobal [_itemtodrop,1];
};

if (_hasoutput && _invehicle) then {
	_itemtodrop = food_output select (food_with_output find _itemorignal);
	_vehicle = (vehicle player);
	sleep 3;
	_vehicle addMagazineCargoGlobal [_itemtodrop,1];
};

if ( _rawfood and (random 15 < 1)) then {
    r_player_infected = true;
    player setVariable["USEC_infected",true,true];
};

r_player_blood = r_player_blood + _regen;
if (r_player_blood > r_player_bloodTotal) then {
    r_player_blood = r_player_bloodTotal;
};

player setVariable ["messing",[dayz_hunger,dayz_thirst],true];
player setVariable["USEC_BloodQty",r_player_blood,true];
player setVariable["medForceUpdate",true];

if (_cookedfood) then {
_staminaGain = 3000;
	} else {
_staminaGain = 1500;
};

R3F_TIRED_Accumulator = R3F_TIRED_Accumulator - _staminaGain;
if (R3F_TIRED_Accumulator < 0) then {
R3F_TIRED_Accumulator = 0;
};

//PVDZ_plr_Save = [player,nil,true];
//publicVariableServer "PVDZ_plr_Save";

dayz_hunger = 0;

//Ensure Control is visible
_display = uiNamespace getVariable 'DAYZ_GUI_display';
(_display displayCtrl 1301) ctrlShow true;

if (r_player_blood / r_player_bloodTotal >= 0.2) then {
    (_display displayCtrl 1300) ctrlShow true;
};

cutText [format[(localize "str_player_consumed_food"),_text], "PLAIN DOWN"];