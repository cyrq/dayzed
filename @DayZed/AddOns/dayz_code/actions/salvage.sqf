private["_vehicle","_part","_hitpoint","_type","_selection","_array"];

if (dayz_salvageInProgress) exitWith { cutText [localize "str_salvage_inprogress", "PLAIN DOWN"]; };
dayz_salvageInProgress = true;

_id = _this select 2;
_array = _this select 3;
_vehicle = _array select 0;
_part = _array select 1;
_hitpoint = _array select 2;
_type = typeOf _vehicle;
_swimming = surfaceIsWater (getPosATL player);
 
_hasToolbox = "ItemToolbox" in items player;
_hasCrowbar = ("ItemCrowbar" in items player) or ("MeleeCrowbar" in weapons player) or (dayz_onBack == "MeleeCrowbar");

_selection = getText(configFile >> "cfgVehicles" >> _type >> "HitPoints" >> _hitpoint >> "name");
_nameType = getText(configFile >> "cfgVehicles" >> _type >> "displayName");
_namePart = getText(configFile >> "cfgMagazines" >> _part >> "displayName");

{_vehicle removeAction _x} forEach s_player_repairActions;
s_player_repairActions = [];
s_player_repair_crtl = 1;
 
if (_hasToolbox and _hasCrowbar) then {

	_damage = [_vehicle,_hitpoint] call object_getHit;
					
		if (_damage < 0.16) then {
					
			if (_part == "PartFueltank" OR _part == "PartEngine" OR _part == "PartVRotor") then {
						
					_anim = "ActsPercSnonWnonDnon_carFixing";
					
					if (!_swimming) then {
					player playMove _anim;
					sleep 15;
					};

					_dis=20;
					_sfx = "repair";
							
					[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
					[player,_dis,true,(getPosATL player)] call player_alertZombies;
						
					sleep 28; //once more
						
					[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
					[player,_dis,true,(getPosATL player)] call player_alertZombies;
					
					waitUntil {animationState player != _anim};
					[objNull, player, rSwitchMove,""] call RE;
					player playActionNow "stop";
						
				} else {
					
					_anim = "ActsPercSnonWnonDnon_carFixing2";
					
					if (!_swimming) then {
					player playMove _anim;
					sleep 8;
					};

					_dis=20;
					_sfx = "repair";
							
					[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
					[player,_dis,true,(getPosATL player)] call player_alertZombies;
						
					sleep 20; //once more
						
					[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
					[player,_dis,true,(getPosATL player)] call player_alertZombies;
					
					waitUntil {animationState player != _anim};
					[objNull, player, rSwitchMove,""] call RE;
					player playActionNow "stop";
				};
			
				_isOK = [player,_part] call BIS_fnc_invAdd;
			
				if (_isOK) then {
					if( _hitpoint == "HitEngine" or _hitpoint == "HitFuel" ) then {
						PVCDZ_veh_SH = [_vehicle,_selection,0.89];
							publicVariable "PVCDZ_veh_SH";
                                if (local _vehicle) then {
									PVCDZ_veh_SH call fnc_veh_handleDam;
							};
						} else {
							PVCDZ_veh_SH = [_vehicle,_selection,1];
								publicVariable "PVCDZ_veh_SH";
									if (local _vehicle) then {
										PVCDZ_veh_SH call fnc_veh_handleDam;
									};
								};
							_vehicle setvelocity [0,0,1];
						cutText [format["You have successfully taken %1 from the %2",_namePart,_nameType], "PLAIN DOWN"];					
					} else {
						cutText [localize "str_player_24", "PLAIN DOWN"];
					};
				} else {	
					cutText [format["Cannot remove %1 from %2, the part is to badly damaged.",_namePart,_nameType], "PLAIN DOWN"];
			};
		} else {
			cutText [format["You need both a ToolBox and a Crowbar to remove %1 from the %2.",_namePart,_nameType], "PLAIN DOWN"];
	};
 
dayz_myCursorTarget = objNull;
s_player_repair_crtl = -1;

dayz_salvageInProgress = false;