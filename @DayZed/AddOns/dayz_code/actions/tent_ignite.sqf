private["_ent","_burnTime","_burnTimeLeft"];

_ent = _this select 3;
_burnTime = 45; //takes one minuite to delete tent and it's content from the DB
_burnTimeLeft = _burnTime;
_cursorTarget = cursorTarget;
_text = getText (configFile >> "CfgVehicles" >> typeOf _cursorTarget >> "displayName");
_alreadyBurning = _ent getVariable["burning",false];

if (_alreadyBurning) exitWith {
	cutText [format["%1 is already burning!",_text],"PLAIN DOWN"];
};

player removeAction s_player_igniteTent;
player removeAction s_player_packtent;
player removeAction s_player_sleep;

player playActionNow "Medic";

[player,"bandage",0,false] call dayz_zombieSpeak;
cutText [format["Preparing to ignite %1...",_text], "PLAIN DOWN"];

_objectID 	= _ent getVariable["ObjectID","0"];
_objectUID	= _ent getVariable["ObjectUID","0"];

sleep 6; //wait for the burn animation

PVDZ_obj_Fire = [_ent,3,time,false,true];
publicVariable "PVDZ_obj_Fire";
_id = PVDZ_obj_Fire spawn BIS_Effects_Burn;
_ent setVariable ["burning",true,true];

switch true do {
	case ("ItemMatchbox5" in items player) : {
		player removeWeapon "ItemMatchbox5";
		player addWeapon "ItemMatchbox4";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox4" in items player) : {
		player removeWeapon "ItemMatchbox4";
		player addWeapon "ItemMatchbox3";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox3" in items player) : {
		player removeWeapon "ItemMatchbox3";
		player addWeapon "ItemMatchbox2";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox2" in items player) : {
		player removeWeapon "ItemMatchbox2";
		player addWeapon "ItemMatchbox1";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox1" in items player) : {
		player removeWeapon "ItemMatchbox1";
		player addWeapon "ItemMatchboxEmpty";
		cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
	};
	case ("ItemMatchbox" in items player) : {
		if (round(random 100) < 30) then {
			player removeWeapon "ItemMatchbox";
			player addWeapon "ItemMatchbox5";
			cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.\nAfter creating the fire you realized that you have 5 matches left.",_text],"PLAIN DOWN"];
		} else {
			cutText [format["%1 is burning!\nYou can still save it's contents before the fire spreads.",_text],"PLAIN DOWN"];
		};
	};
};

for "_i" from 0 to _burnTime do {
sleep 1;
_burnTimeLeft = _burnTimeLeft - 1;
};
 
if(_burnTimeLeft == 0 || _burnTimeLeft < 0) then {

_ent setDamage 2;

cutText [format["%1 has been burned to the ground!",_text],"PLAIN DOWN"];

PVDZ_obj_Delete = [_objectID,_objectUID];
publicVariableServer "PVDZ_obj_Delete";
};

dayz_myCursorTarget = objNull;