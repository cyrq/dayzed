private["_item","_hasKnife","_hasKnifeBlunt","_hasItemKnife5","_hasItemKnife4","_hasItemKnife3","_hasItemKnife2","_hasItemKnife1","_type","_hasHarvested","_config","_isListed","_text","_dis","_sfx","_qty","_array","_string","_stringB", "_stringC","_stringD"];

_item = _this select 3;
_hasKnife = ("ItemKnife" in items player) || ("ItemKnife5" in items player) || ("ItemKnife4" in items player) || ("ItemKnife3" in items player) || ("ItemKnife2" in items player) || ("ItemKnife1" in items player);
_hasKnifeBlunt = "ItemKnifeBlunt" in items player;
_type = typeOf _item;
_hasHarvested = _item getVariable["meatHarvested",false];
_config = configFile >> "CfgSurvival" >> "Meat" >> _type;

player removeAction s_player_butcher;

if ((_hasKnife or _hasKnifeBlunt) and !_hasHarvested) then {
	private ["_qty"];
	//Get Animal Type
	_isListed = isClass (_config);
	_text = getText (configFile >> "CfgVehicles" >> _type >> "displayName");

	player playActionNow "Medic";
	
	_dis=10;
	_sfx = "gut";
	[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
	[player,_dis,true,(getPosATL player)] call player_alertZombies;

	_item setVariable ["meatHarvested",true,true];

	_qty = 2;
	if (_isListed) then {
		_qty = getNumber (_config >> "yield");
	};

	if (_hasKnifeBlunt) then { _qty = round(_qty / 2); };

	if (local _item) then {
		[_item,_qty] spawn local_gutObject; //leave as spawn (sleeping in loops will work but can freeze the script)
	} else {
		PVCDZ_obj_GutBody =[_item,_qty];
		publicVariable "PVCDZ_obj_GutBody";
	};
	
	_string = format[localize "str_success_gutted_animal",_text,_qty];
	_stringB = format["%1 has been gutted, %2 meat steaks now on the carcass\nWhile gutting you realize the knife is becoming blunt.",_text,_qty];
	_stringC = format["%1 has been gutted, %2 meat steaks now on the carcass\nYour knife is completely blunt.",_text,_qty];
	_stringD = format["%1 has been gutted, Only %2 meat steaks are now on the carcass\n, since your knife is blunt.",_text,_qty];
	
	sleep 6;
	
	if (_hasItemKnife5) exitWith {
		player removeWeapon "ItemKnife5";
		player addWeapon "ItemKnife4";
		cutText [_string, "PLAIN DOWN"];
	};
	if (_hasItemKnife4) exitWith {
		player removeWeapon "ItemKnife4";
		player addWeapon "ItemKnife3";
		cutText [_string, "PLAIN DOWN"];
	};
	if (_hasItemKnife3) exitWith {
		player removeWeapon "ItemKnife3";
		player addWeapon "ItemKnife2";
		cutText [_string, "PLAIN DOWN"];
	};
	if (_hasItemKnife2) exitWith {
		player removeWeapon "ItemKnife2";
		player addWeapon "ItemKnife1";
		cutText [_string, "PLAIN DOWN"];
	};
	if (_hasItemKnife1) exitWith {
		player removeWeapon "ItemKnife1";
		player addWeapon "ItemKnifeBlunt";
		cutText [_stringC, "PLAIN DOWN"];
	};
	if (_hasKnifeBlunt) exitWith {
		cutText [_stringD, "PLAIN DOWN"];
	};
	if (_hasKnife) exitWith {
		if (round(random 100) < 30) then {
			player removeWeapon "ItemKnife";
			player addWeapon "ItemKnife5";
			cutText [_stringB, "PLAIN DOWN"];
		} else {
			cutText [_string, "PLAIN DOWN"];
		};
	};
};

s_player_butcher = -1;
dayz_myCursorTarget = objNull;