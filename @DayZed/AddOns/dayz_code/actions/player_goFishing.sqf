/*
	DayZ Fishing
	Usage: spawn player_goFishing;
	Made for DayZ Mod please ask permission to use/edit/distrubute email vbawol@veteranbastards.com.
	modified by cyrq for DayZed.
*/

private ["_linecastmax","_linecastmin","_num","_playerPos","_ispond","_objectsPond","_isPondNearBy","_isOk","_counter","_vehicle","_inVehicle","_rnd","_itemOut","_text","_item","_itemtodrop","_result"];

_vehicle = vehicle player;
_primaryWeapon = primaryWeapon player;
_currentWeapon = currentWeapon player;

call gear_ui_init;
closeDialog 1;

if (!(_primaryWeapon in Dayz_fishingItems)) exitWith { cutText ["You need to have a Fishing Pole in your hands to start fishing!", "PLAIN DOWN"];};
if (dayz_isSwimming) exitWith { cutText [format["You cannot fish while you're swimming!"], "PLAIN DOWN"];};
if ((_vehicle != player) and !(_vehicle isKindOf "Ship")) exitWith { cutText ["You cannot fish while inside land or air vehicle!", "PLAIN DOWN"];};
if ((_vehicle != player) and (_vehicle isKindOf "Ship") and (driver _vehicle == player)) exitWith { cutText ["You cannot fish from the helmsman seat!", "PLAIN DOWN"];};
if (dayz_fishingInprogress) exitWith { cutText [localize "str_fishing_inprogress", "PLAIN DOWN"];};

dayz_fishingInprogress = true;

//line distance
_linecastmax = 67;
_linecastmin = 37;

_num = (round(random _linecastmax)) max _linecastmin;

//Sounds
// Bait Imnpact Sound
_dis_bait = 5;
_sfx_bait = "fishing_bait_impact";
// Swing Sound
_dis_swing = 5;
_sfx_swing = "fishing_swing";
// Cought Sound
_dis_cought = 5;
_sfx_cought = "fishing_fish_cought";

//find player position
_playerPos = getPosATL player;
_isWater = (surfaceIsWater (getPosATL player));
_isInPond = false;
_pondPos = [];
_objectsPond = nearestObjects [_playerPos, [], 25];
{
	_isPond = ["pond",str(_x),false] call fnc_inString;
	if (_isPond) then {
		_pondPos = (_x worldToModel _playerPos) select 2;
	if (_pondPos < 0) then {
		_isInPond = true;
		};
	};
} forEach _objectsPond;

//diag_log (str(_ispond));

if (!_isWater and !_isInPond) exitWith {
	dayz_fishingInprogress = false;
	cutText [localize "str_fishing_watercheck" , "PLAIN DOWN"];
};

_isOk = true;
_counter = 0;

// swing fishingpole
player playActionNow "GestureSwing";
[player,_sfx_swing,0,false,_dis_swing] call dayz_zombieSpeak;
sleep 1.5;
[player,_sfx_bait,0,false,_dis_bait] call dayz_zombieSpeak;
// Alert zeds
[player,3,true,(getPosATL player)] call player_alertZombies;

r_interrupt = false;

while {_isOk} do {
	if(dayz_isSwimming) exitWith {cutText [localize "str_player_26", "PLAIN DOWN"];_isOk = false;};
	if !((currentWeapon player) in Dayz_fishingItems) exitwith {cutText [localize "str_fishing_canceled", "PLAIN DOWN"];_isOk = false;};

	if (r_interrupt) then {
		_isOk = false;
		cutText [localize "str_fishing_canceled", "PLAIN DOWN"];
	} else {
		//make sure the player isnt swimming


		// wait for animation
		sleep 2;

		// check if player is in boat
		_vehicle = vehicle player;
		if ((_vehicle != player) and (_vehicle isKindOf "Ship")) then {
			_inVehicle = true;
			_rnd = 75;
		} else {
			_inVehicle = false;
			_rnd = 100;
		};
		//Check for rain fish are more active during the rain.
		if (rain > 0) then {_rnd = _rnd / 2;};
		
		//Check if night
		if (daytime < 5 OR daytime > 22) then {_rnd = _rnd - 10;};

		// 5% chance to catch anything
		if((random _rnd) <= 5) then {
			// Just the one fish for now
			_itemOut = [];
			if (!_isInPond) then {
				_itemOut = switch (true) do {
					case ((_num > 30) and (_num <= 45)) : { ["FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout"]; };
					case ((_num > 45) and (_num <= 60)) : { ["FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawSeaBass","FishRawSeaBass"]; };
					case ((_num > 60)) : { ["FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawSeaBass","FishRawSeaBass","FishRawTuna"]; }; 				
					default { ["FishRawTrout"]; };
				};
			} else {
				_itemOut = switch (true) do {
					case ((_num > 30) and (_num <= 45)) : { ["FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout"]; };
					case ((_num > 45) and (_num <= 60)) : { ["FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout"]; };
					case ((_num > 60)) : { ["FishRawTrout","FishRawTrout","FishRawTrout","FishRawTrout"]; }; 				
					default { ["FishRawTrout"]; };
				};
			};
			_itemOut = _itemOut call BIS_fnc_selectRandom;
			_text = getText (configFile >> "CfgMagazines" >> _itemOut >> "displayName");
			if(_inVehicle) then { 
				_item = _vehicle;
				_itemtodrop = _itemOut;
				_item addMagazineCargoGlobal [_itemtodrop,1];
				[player,_sfx_cought,0,false,_dis_cought] call dayz_zombieSpeak;
				//Let the player know what he caught
				cutText [format[localize "str_fishing_success",_text], "PLAIN DOWN"];
			} else {
				call gear_ui_init;
				//Remove melee magazines (BIS_fnc_invAdd fix) 
				{player removeMagazines _x} forEach MeleeMagazines;
				_result = [player,_itemOut] call BIS_fnc_invAdd;
				if (_result) then {
					[player,_sfx_cought,0,false,_dis_cought] call dayz_zombieSpeak;
					//Let the player know what he caught
					cutText [format[localize "str_fishing_success",_text], "PLAIN DOWN"];
				} else {
					cutText [format[localize "str_fishing_noroom",_text], "PLAIN DOWN"];
				};
				//adding melee mags back if needed
				call dayz_meleeMagazineCheck;
			};
			_isOk = false;
		} else {

			switch (true) do {
				case (_counter == 0) : { cutText [format[localize "str_fishing_cast",_num], "PLAIN DOWN"]; }; 
				case (_counter == 4) : { cutText [localize "str_fishing_pull", "PLAIN DOWN"]; player playActionNow "GesturePoint"; }; 
				case (_counter == 8) : { cutText [localize "str_fishing_pull", "PLAIN DOWN"]; player playActionNow "GesturePoint"; };
				default { cutText [localize "str_fishing_nibble", "PLAIN DOWN"]; };
			}; 
			_counter = _counter + 1;

			if(_counter == 12) then {
				_isOk = false;
				sleep 1;
				cutText [localize "str_fishing_failed", "PLAIN DOWN"];
			};
		};
	};
};
dayz_fishingInprogress = false;