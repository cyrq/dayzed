disableSerialization;

//Model Variables
Bandit1_DZ = "Bandit1_DZ";
BanditW1_DZ = "BanditW1_DZ";
Survivor1_DZ = "Survivor2_DZ";
Survivor2_DZ = "Survivor2_DZ";
SurvivorW2_DZ = "SurvivorW2_DZ";
Sniper1_DZ = "Sniper1_DZ";
Camo1_DZ = "Camo1_DZ";
Soldier1_DZ = "Soldier1_DZ";
Rocket_DZ = "Rocket_DZ";
Soldier1_DZed = "Soldier1_DZed";
Soldier2_DZed = "Soldier2_DZed";
Soldier3_DZed = "Soldier3_Dzed";
Soldier4_DZed = "Soldier4_Dzed";
Sniper1_DZed = "Sniper1_DZed";

//Models
AllPlayers = [
	"SurvivorW2_DZ",
	"Survivor2_DZ",
	"Sniper1_DZ",
	"Soldier1_DZ",
	"Camo1_DZ",
	"BanditW1_DZ",
	"Bandit1_DZ",
	"Survivor3_DZ",
	"Soldier1_DZed",
	"Soldier2_DZed",
	"Soldier3_DZed",
	"Soldier4_DZed",
	"Sniper1_DZed",
	"SG_GRU_TeamLeader_DZed",
	"SG_GRU_Sniper_DZed",
	"SG_GRU_Marksman_W_DZed",
	"SG_GRU_Assaultman_W_DZed",
	"Policeman_DZed",
	"Policeman_RU_DZed",
	"Civilian_DZed",
	"Civilian1_DZed",
	"Civilian2_DZed",
	"Civilian3_DZed",
	"Civilian_RU_DZed",
	"Civilian1_RU_DZed",
	"Civilian2_RU_DZed",
	"Civilian3_RU_DZed",
	"Worker_DZed",
	"Worker1_DZed",
	"Worker2_DZed",
	"Worker3_DZed",
	"Worker_RU_DZed",
	"Worker1_RU_DZed",
	"Worker2_RU_DZed",
	"Worker3_RU_DZed",
	"Profiteer_DZed",
	"Profiteer1_DZed",
	"Profiteer2_DZed",
	"Profiteer3_DZed",
	"Profiteer_RU_DZed",
	"Profiteer1_RU_DZed",
	"Profiteer2_RU_DZed",
	"Profiteer3_RU_DZed",
	"Woodlander_DZed",
	"Woodlander1_DZed",
	"Woodlander2_DZed",
	"Woodlander3_DZed",
	"Woodlander_RU_DZed",
	"Woodlander1_RU_DZed",
	"Woodlander2_RU_DZed",
	"Woodlander3_RU_DZed",
	"Functionary_DZed",
	"Functionary1_DZed",
	"Functionary_RU_DZed",
	"Functionary1_RU_DZed",
	"Priest_DZed",
	"Doctor_DZed",
	"SchoolTeacher_DZed",
	"SchoolTeacher_RU_DZed",
	"Assistant_DZed",
	"Assistant_RU_DZed",
	"Tshirt_DZed",
	"Tshirt1_DZed",
	"Tshirt2_DZed",
	"Tshirt3_DZed",
	"Tshirt4_DZed",
	"Tshirt5_DZed",
	"Tshirt6_DZed"
];

//Civilians
CivPlayers = [
	"Policeman_DZed",
	"Policeman_RU_DZed",
	"Civilian_DZed",
	"Civilian1_DZed",
	"Civilian2_DZed",
	"Civilian3_DZed",
	"Civilian_RU_DZed",
	"Civilian1_RU_DZed",
	"Civilian2_RU_DZed",
	"Civilian3_RU_DZed",
	"Worker_DZed",
	"Worker1_DZed",
	"Worker2_DZed",
	"Worker3_DZed",
	"Worker_RU_DZed",
	"Worker1_RU_DZed",
	"Worker2_RU_DZed",
	"Worker3_RU_DZed",
	"Profiteer_DZed",
	"Profiteer1_DZed",
	"Profiteer2_DZed",
	"Profiteer3_DZed",
	"Profiteer_RU_DZed",
	"Profiteer1_RU_DZed",
	"Profiteer2_RU_DZed",
	"Profiteer3_RU_DZed",
	"Woodlander_DZed",
	"Woodlander1_DZed",
	"Woodlander2_DZed",
	"Woodlander3_DZed",
	"Woodlander_RU_DZed",
	"Woodlander1_RU_DZed",
	"Woodlander2_RU_DZed",
	"Woodlander3_RU_DZed",
	"Functionary_DZed",
	"Functionary1_DZed",
	"Functionary_RU_DZed",
	"Functionary1_RU_DZed",
	"Priest_DZed",
	"Doctor_DZed",
	"SchoolTeacher_DZed",
	"SchoolTeacher_RU_DZed",
	"Assistant_DZed",
	"Assistant_RU_DZed",
	"Tshirt_DZed",
	"Tshirt1_DZed",
	"Tshirt2_DZed",
	"Tshirt3_DZed",
	"Tshirt4_DZed",
	"Tshirt5_DZed",
	"Tshirt6_DZed"
];

//Melee Weapons
MeleeWeapons = [
	"MeleeHatchet",
	"MeleeCrowbar",
	"MeleeMachete",
	"MeleeBaseball",
	"MeleeBaseBallBat",
	"MeleeBaseBallBatBarbed",
	"MeleeBaseBallBatNails",
	"MeleeFishingPole"
];

//Melee Magazines
MeleeMagazines = [
	"hatchet_swing",
	"crowbar_swing",
	"Machete_swing",
	"Bat_Swing",
	"BatBarbed_Swing",
	"BatNails_Swing",
	"Fishing_Swing"
];

//Fishing Items
Dayz_fishingItems = [
	"MeleeFishingPole"
];

//Plants
Dayz_plants = [
	"Dayz_Plant1",
	"Dayz_Plant2",
	"Dayz_Plant3"
];

//New Zeds
DayZ_ViralZeds = [
	"z_new_villager2",
	"z_new_villager3",
	"z_new_villager4",
	"z_new_worker2",
	"z_new_worker3",
	"z_new_worker4"
];

//Bags
DayZ_Backpacks = [
	"DZ_Patrol_Pack_EP1",
	"DZ_Assault_Pack_EP1",
	"DZ_Czech_Vest_Puch",
	"DZ_ALICE_Pack_EP1",
	"DZ_TK_Assault_Pack_EP1",
	"DZ_British_ACU",
	"DZ_CivilBackpack_EP1",
	"DZ_Backpack_EP1",
	"DZed_GunBag",
	"DZed_SmallBag",
	"DZed_MedicPack"
];

//Safe Deployables
SafeObjects = [
	"Land_Fire_DZ",
	"TentStorage",
	"Wire_cat1",
	"Sandbag1_DZ",
	"Hedgehog_DZ",
	"StashSmall",
	"StashMedium",
	"BearTrap_DZ",
	"DomeTentStorage",
	"CamoNet_DZ",
	"Trap_Cans",
	"TrapTripwireFlare",
	"TrapBearTrapSmoke",
	"TrapTripwireGrenade",
	"TrapTripwireSmoke",
	"TrapBearTrapFlare",
	"StorageBox"
];

//Searchable objects
Searchables = [
	"MAP_fridge",
	"MAP_Skrin_bar",
	"MAP_Dhangar_psacistul",
	"MAP_Dkamna_bila",
	"MAP_lekarnicka",
	"VendingMachine",
	"MAP_BoatSmall_1",
	"MAP_bedna_ammo2X",
	"MAP_Proxy_UsBasicAmmoBoxSmall",
	"MAP_metalcase_a",
	"MAP_washing_machine"
];

//Trees
Trees = [
	"t_picea2s_snow.p3d",
	"b_corylus.p3d",
	"t_quercus3s.p3d",
	"t_larix3s.p3d",
	"t_pyrus2s.p3d",
	"str_briza_kriva.p3d",
	"dd_borovice.p3d",
	"les_singlestrom_b.p3d",
	"les_singlestrom.p3d",
	"smrk_velky.p3d",
	"smrk_siroky.p3d",
	"smrk_maly.p3d",
	"les_buk.p3d",
	"str krovisko vysoke.p3d",
	"str_fikovnik_ker.p3d",
	"str_fikovnik.p3d",
	"str vrba.p3d",
	"hrusen2.p3d",
	"str dub jiny.p3d",
	"str lipa.p3d",
	"str briza.p3d",
	"p_akat02s.p3d",
	"jablon.p3d",
	"p_buk.p3d",
	"str_topol.p3d",
	"str_topol2.p3d",
	"p_osika.p3d",
	"t_picea3f.p3d",
	"t_picea2s.p3d",
	"t_picea1s.p3d",
	"t_fagus2w.p3d",
	"t_fagus2s.p3d",
	"t_fagus2f.p3d",
	"t_betula1f.p3d",
	"t_betula2f.p3d",
	"t_betula2s.p3d",
	"t_betula2w.p3d",
	"t_alnus2s.p3d",
	"t_acer2s.p3d",
	"t_populus3s.p3d",
	"t_quercus2f.p3d",
	"t_sorbus2s.p3d",
	"t_malus1s.p3d",
	"t_salix2s.p3d",
	"t_picea1s_w.p3d",
	"t_picea2s_w.p3d",
	"t_ficusb2s_ep1.p3d",
	"t_populusb2s_ep1.p3d",
	"t_populusf2s_ep1.p3d",
	"t_amygdalusc2s_ep1.p3d",
	"t_pistacial2s_ep1.p3d",
	"t_pinuse2s_ep1.p3d",
	"t_pinuss3s_ep1.p3d",
	"t_prunuss2s_ep1.p3d",
	"t_pinusn2s.p3d",
	"t_pinusn1s.p3d",
	"t_pinuss2f.p3d",
	"t_poplar2f_dead_pmc.p3d",
	"misc_torzotree_pmc.p3d",
	"misc_burnspruce_pmc.p3d",
	"brg_cocunutpalm8.p3d",
	"brg_umbrella_acacia01b.p3d",
	"brg_jungle_tree_canopy_1.p3d",
	"brg_jungle_tree_canopy_2.p3d",
	"brg_cocunutpalm4.p3d",
	"brg_cocunutpalm3.p3d",
	"palm_01.p3d",
	"palm_02.p3d",
	"palm_03.p3d",
	"palm_04.p3d",
	"palm_09.p3d",
	"palm_10.p3d",
	"brg_cocunutpalm2.p3d",
	"brg_jungle_tree_antiaris.p3d",
	"brg_cocunutpalm1.p3d"
];

//Fences
Fences = [
	"wall_indfnc_9.p3d",
	"wall_indfnc_3.p3d",
	"wall_indfnc_corner.p3d",
	"wall_indfnc_3_hole.p3d",
	"wall_indfnc_3_d.p3d",
	"pletivo.p3d",
	"pletivo_wired.p3d",
	"pletivo_wired_army.p3d",
	"pletivo_wired_hole.p3d"
];

//Weapon Attachments
AvailableWeapons1P29 = [
	"RH_ak105",
	"RH_ak105gl",
	"RH_AK107",
	"RH_AK107gl"
];

AvailableWeaponsACOG = [
	"RH_M4a1",
	"RH_M4sbr",
	"RH_M4a1gl",
	"RH_M16a4",
	"RH_M16a4gl",
	"RH_hk416",
	"RH_hk416s",
	"RH_hk416sgl",
	"RH_acrb",
	"RH_m14"
];

AvailableWeaponsAIMSHOT = [
	"C1987_G3"
];

AvailableWeaponsCCO = [
	"RH_m14",
	"RH_M4a1",
	"RH_M4sbr",
	"RH_M4sd",
	"RH_M4a1gl",
	"RH_M4sdgl",
	"RH_M16a4",
	"RH_M16a4gl",
	"RH_hk416",
	"RH_hk416sd",
	"RH_hk416s",
	"RH_hk416sgl",
	"RH_rk95",
	"RH_Rk95sd",
	"RH_mp5a5",
	"RH_mp5sd6",
	"RH_UMP",
	"RH_umpsd",
	"RH_acrb",
	"RH_P90",
	"RH_P90sd",
	"C1987_Famas_f1",
	"C1987_Famas_f1_sd"
];

AvailableWeaponsELCAN = [
	"RH_M249",
	"RH_Mk48mod1"
];

AvailableWeaponsGHILLIERag = [
	"RH_M40A3",
	"RH_SVDb",
	"RH_SVD"
];

AvailableWeaponsGP = [
	"RH_aks74",
	"RH_aks74k",
	"RH_akm",
	"RH_ak74",
	"RH_ak74k",
	"RH_AK107",
	"RH_AK107k",
	"RH_AK107sp",
	"RH_ak105",
	"RH_ak105k",
	"RH_ak105sp",
	"RH_an94",
	"RH_ak105_1p29",
	"RH_AK107_1p29"
];

AvailableWeaponsHOLO = [
	"RH_m14",
	"RH_M4a1",
	"RH_M4sbr",
	"RH_M4sd",
	"RH_M4a1gl",
	"RH_M4sdgl",
	"RH_M16a4",
	"RH_M16a4gl",
	"RH_hk416",
	"RH_hk416sd",
	"RH_hk416s",
	"RH_hk416sgl",
	"C1987_G3",
	"RH_mp5a5",
	"RH_mp5sd6",
	"RH_UMP",
	"RH_umpsd",
	"RH_acrb",
	"RH_P90",
	"RH_P90sd",
	"C1987_Famas_f1",
	"C1987_Famas_f1_sd"
];

AvailableWeaponsKOBRA = [
	"RH_bizon",
	"RH_bizonsd",
	"RH_aks74",
	"RH_aks74gl",
	"RH_aks74u",
	"RH_aks74usd",
	"RH_ak74",
	"RH_ak74gl",
	"RH_AK107",
	"RH_AK107gl",
	"RH_ak105",
	"RH_ak105gl"
];

AvailableWeaponsM203 = [
	"RH_M4a1",
	"RH_M4a1aim",
	"RH_M4a1eotech",
	"RH_M4a1acog",
	"RH_M4sd",
	"RH_M4sdaim",
	"RH_M4sdeotech",
	"RH_M16a4",
	"RH_M16A4aim",
	"RH_M16A4eotech",
	"RH_M16A4acog",
	"RH_hk416s",
	"RH_hk416saim",
	"RH_hk416seotech",
	"RH_hk416sacog"
];

AvailableWeaponsPSO = [
	"RH_aks74",
	"RH_AK107",
	"RH_AK107gl",
	"RH_ak105",
	"RH_ak105gl",
	"RH_akm",
	"RH_ak74",
	"SVD_Ironsight_DayZed"
];

AvailableWeaponsSniperScope = [
	"RH_m14",
	"C1987_G3"
];

AvailableWeaponsSuppressorPistol = [
	"RH_m9",
	"RH_pm",
	"RH_mk22",
	"RH_aps",
	"RH_usp"
];

AvailableWeaponsSuppressorRifle = [
	"RH_M4a1",
	"RH_M4a1aim",
	"RH_M4a1eotech",
	"RH_M4a1gl",
	"RH_M4a1glaim",
	"RH_M4a1gleotech",
	"RH_hk416",
	"RH_hk416aim",
	"RH_hk416eotech",
	"RH_bizon",
	"RH_bizonk",
	"RH_gr1",
	"RH_akms",
	"RH_aks74u",
	"RH_aks74uk",
	"RH_rk95",
	"RH_Rk95aim",
	"RH_mp5a5",
	"RH_mp5a5aim",
	"RH_mp5a5eot",
	"RH_UMP",
	"RH_umpaim",
	"RH_umpeot",
	"RH_P90",
	"RH_P90aim",
	"RH_P90eot",
	"C1987_Famas_f1",
	"C1987_Famas_f1_aim",
	"C1987_Famas_f1_eot"
];

AvailableWeaponsNSPU = [
	"SVD_Ironsight_DayZed"
];

Weapons1P29 = [
	"RH_ak105_1p29",
	"RH_ak105gl1p29",
	"RH_AK107_1p29",
	"RH_AK107gl1p29"
];

WeaponsACOG = [
	"RH_M4a1acog",
	"RH_M4sbracog",
	"RH_M4a1glacog",
	"RH_M16A4acog",
	"RH_M16A4glacog",
	"RH_hk416acog",
	"RH_hk416sacog",
	"RH_hk416sglacog",
	"RH_acrbacog",
	"RH_m14acog"
];

WeaponsAIMSHOT = [
	"C1987_G3_aimshot"
];

WeaponsCCO = [
	"RH_m14aim",
	"RH_M4a1aim",
	"RH_M4sbraim",
	"RH_M4sdaim",
	"RH_M4a1glaim",
	"RH_M4sdglaim",
	"RH_M16A4aim",
	"RH_M16A4glaim",
	"RH_hk416aim",
	"RH_hk416sdaim",
	"RH_hk416saim",
	"RH_hk416sglaim",
	"RH_Rk95aim",
	"RH_Rk95sdaim",
	"RH_mp5a5aim",
	"RH_mp5sd6aim",
	"RH_umpaim",
	"RH_umpsdaim",
	"RH_acrbaim",
	"RH_P90aim",
	"RH_p90sdaim",
	"C1987_Famas_f1_aim",
	"C1987_Famas_f1_aim_sd"
];

WeaponsELCAN = [
	"RH_M249elcan",
	"RH_Mk48mod1elcan"
];

WeaponsGP = [
	"RH_aks74gl",
	"RH_aks74kgl",
	"RH_akmgl",
	"RH_ak74gl",
	"RH_ak74kgl",
	"RH_AK107gl",
	"RH_AK107kgl",
	"RH_AK107glsp",
	"RH_ak105gl",
	"RH_ak105kgl",
	"RH_ak105glsp",
	"RH_an94gl",
	"RH_ak105gl1p29",
	"RH_AK107gl1p29"
];

WeaponsHOLO = [
	"RH_m14eot",
	"RH_M4a1eotech",
	"RH_M4sbreotech",
	"RH_M4sdeotech",
	"RH_M4a1gleotech",
	"RH_M4sdgleotech",
	"RH_M16A4eotech",
	"RH_M16A4gleotech",
	"RH_hk416eotech",
	"RH_hk416sdeotech",
	"RH_hk416seotech",
	"RH_hk416sgleotech",
	"C1987_G3_eotech",
	"RH_mp5a5eot",
	"RH_mp5sd6eot",
	"RH_umpeot",
	"RH_umpsdeot",
	"RH_acrbeotech",
	"RH_P90eot",
	"RH_p90sdeot",
	"C1987_Famas_f1_eot",
	"C1987_Famas_f1_eot_sd"
];

WeaponsKOBRA = [
	"RH_bizonk",
	"RH_bizonsdk",
	"RH_aks74k",
	"RH_aks74kgl",
	"RH_aks74uk",
	"RH_aks74usdk",
	"RH_ak74k",
	"RH_ak74kgl",
	"RH_AK107k",
	"RH_AK107kgl",
	"RH_ak105k",
	"RH_ak105kgl"
];

WeaponsM203 = [
	"RH_M4a1gl",
	"RH_M4a1glaim",
	"RH_M4a1gleotech",
	"RH_M4a1glacog",
	"RH_M4sdgl",
	"RH_M4sdglaim",
	"RH_M4sdgleotech",
	"RH_M16a4gl",
	"RH_M16A4glaim",
	"RH_M16A4gleotech",
	"RH_hk416sglacog",
	"RH_hk416sgl",
	"RH_hk416sglaim",
	"RH_hk416sgleotech"
];

WeaponsPSO = [
	"RH_aks74sp",
	"RH_AK107sp",
	"RH_AK107glsp",
	"RH_ak105sp",
	"RH_ak105glsp",
	"RH_akmsp",
	"RH_ak74sp",
	"RH_SVD"
];

WeaponsSniperScope = [
	"RH_m21",
	"C1987_G3SG1"
];

WeaponsSuppressorPistol = [
	"RH_m9sd",
	"RH_pmsd",
	"RH_mk22sd",
	"RH_apssd",
	"RH_uspsd"
];

WeaponsSuppressorRifle = [
	"RH_M4sd",
	"RH_M4sdaim",
	"RH_M4sdeotech",
	"RH_M4sdgl",
	"RH_M4sdglaim",
	"RH_M4sdgleotech",
	"RH_hk416sd",
	"RH_hk416sdaim",
	"RH_hk416sdeotech",
	"RH_bizonsd",
	"RH_bizonsdk",
	"RH_gr1sd",
	"RH_akmssd",
	"RH_aks74usd",
	"RH_aks74usdk",
	"RH_Rk95sd",
	"RH_Rk95sdaim",
	"RH_mp5sd6",
	"RH_mp5sd6aim",
	"RH_mp5sd6eot",
	"RH_umpsd",
	"RH_umpsdaim",
	"RH_umpsdeot",
	"RH_P90sd",
	"RH_p90sdaim",
	"RH_p90sdeot",
	"C1987_Famas_f1_sd",
	"C1987_Famas_f1_aim_sd",
	"C1987_Famas_f1_eot_sd"
];

WeaponsNSPU = [
	"SVD_NSPU_DayZed"
];

//NearbyObjects
dayzed_updateObjects = [
	"Car",
	"Helicopter",
	"Motorcycle",
	"Ship",
	"TentStorage",
	"DomeTentStorage",
	"StashSmall",
	"StashMedium",
	"StorageBox"
];

dayzed_revealObjects = [
	"AllVehicles",
	"WeaponHolder",
	"StashSmall",
	"StashMedium",
	"TentStorage",
	"DomeTentStorage",
	"BuiltItems",
	"StorageBox"
];

//Raw Meat
meatraw = [
    "FoodSteakRaw",
    "FoodmeatRaw",
    "FoodbeefRaw",
    "FoodmuttonRaw",
    "FoodchickenRaw",
    "FoodrabbitRaw",
    "FoodbaconRaw",
    "FoodgoatRaw",
	"FishRawTrout",
	"FishRawSeaBass",
	"FishRawTuna"
];

//Cooked Meat
meatcooked = [
    "FoodSteakCooked",
    "FoodmeatCooked",
    "FoodbeefCooked",
    "FoodmuttonCooked",
    "FoodchickenCooked",
    "FoodrabbitCooked",
    "FoodbaconCooked",
	"FoodgoatCooked",
	"FishCookedTrout",
	"FishCookedSeaBass",
	"FishCookedTuna"
];

//Food without output
no_output_food = [
	"FoodMRE",
	"FoodPistachio",
	"FoodNutmix",
	"FoodCandyAnders",
	"FoodCandyLegacys",
	"FoodCandyMintception"
] + meatcooked + meatraw;

//Food with output
food_with_output=[
    "FoodCanBakedBeans",
    "FoodCanSardines",
    "FoodCanFrankBeans",
    "FoodCanPasta",
	"FoodCanGriff",
	"FoodCanBadguy",
	"FoodCanBoneboy",
	"FoodCanCorn",
	"FoodCanCurgon",
	"FoodCanDemon",
	"FoodCanFraggleos",
	"FoodCanHerpy",
	"FoodCanDerpy",
	"FoodCanOrlok",
	"FoodCanPowell",
	"FoodCanTylers",
	"FoodCanUnlabeled",
	"FoodCanRusUnlabeled",
	"FoodCanRusPork",
	"FoodCanRusPeas",
	"FoodCanRusMilk",
	"FoodCanRusCorn",
	"FoodCanRusStew",
	"FoodChipsSulahoops",
	"FoodChipsMysticales"
];

//Food output
food_output = [
    "TrashTinCan",
    "TrashTinCan",
    "TrashTinCan",
    "TrashTinCan",
	"FoodCanGriffEmpty",
	"FoodCanBadguyEmpty",
	"FoodCanBoneboyEmpty",
	"FoodCanCornEmpty",
	"FoodCanCurgonEmpty",
	"FoodCanDemonEmpty",
	"FoodCanFraggleosEmpty",
	"FoodCanHerpyEmpty",
	"FoodCanDerpyEmpty",
	"FoodCanOrlokEmpty",
	"FoodCanPowellEmpty",
	"FoodCanTylersEmpty",
	"FoodCanUnlabeledEmpty",
	"FoodCanRusUnlabeledEmpty",
	"FoodCanRusPorkEmpty",
	"FoodCanRusPeasEmpty",
	"FoodCanRusMilkEmpty",
	"FoodCanRusCornEmpty",
	"FoodCanRusStewEmpty",
	"FoodChipsSulahoopsEmpty",
	"FoodChipsMysticalesEmpty"
];

//Food without output
no_output_drink = [
	"ItemWaterbottle",
	"ItemWaterbottleBoiled",
	"ItemMilkbottle"
];

//Drinks with output
drink_with_output = [
    "ItemSoda",
    "ItemSodaCoke",
    "ItemSodaPepsi",
    "ItemSodaMdew",
    "ItemSodaMtngreen",
    "ItemSodaR4z0r",
    "ItemSodaClays",
    "ItemSodaSmasht",
    "ItemSodaDrwaste",
    "ItemSodaLemonade",
    "ItemSodaLvg",
    "ItemSodaMzly",
    "ItemSodaRabbit"
];

//Drink output
drink_output = [
    "ItemSodaEmpty",
    "ItemSodaCokeEmpty",
    "ItemSodaPepsiEmpty",
    "ItemSodaMdewEmpty",
    "ItemSodaMtngreenEmpty",
    "ItemSodaR4z0rEmpty",
    "ItemSodaClaysEmpty",
    "ItemSodaSmashtEmpty",
    "ItemSodaDrwasteEmpty",
    "ItemSodaLemonadeEmpty",
    "ItemSodaLvgEmpty",
    "ItemSodaMzlyEmpty",
    "ItemSodaRabbitEmpty"
];

//Tin Cans for Boiling
boil_tin_cans = [
    "TrashTinCan",
	"FoodCanGriffEmpty",
	"FoodCanBadguyEmpty",
	"FoodCanBoneboyEmpty",
	"FoodCanCornEmpty",
	"FoodCanCurgonEmpty",
	"FoodCanDemonEmpty",
	"FoodCanFraggleosEmpty",
	"FoodCanHerpyEmpty",
	"FoodCanDerpyEmpty",
	"FoodCanOrlokEmpty",
	"FoodCanPowellEmpty",
	"FoodCanTylersEmpty",
	"FoodCanUnlabeledEmpty",
	"FoodCanRusUnlabeledEmpty",
	"FoodCanRusStewEmpty",
	"FoodCanRusPorkEmpty",
	"FoodCanRusPeasEmpty",
	"FoodCanRusMilkEmpty",
	"FoodCanRusCornEmpty",
    "ItemSodaEmpty",
    "ItemSodaCokeEmpty",
    "ItemSodaPepsiEmpty",
    "ItemSodaMdewEmpty",
    "ItemSodaMtngreenEmpty",
    "ItemSodaR4z0rEmpty",
    "ItemSodaClaysEmpty",
    "ItemSodaSmashtEmpty",
    "ItemSodaDrwasteEmpty",
    "ItemSodaLemonadeEmpty",
    "ItemSodaLvgEmpty",
    "ItemSodaMzlyEmpty",
    "ItemSodaRabbitEmpty"
];

canPickup = false;
pickupInit = false;
dayz_chloroform = [];

//Parts
dayZ_partClasses = [
	"PartFueltank",
	"PartWheel",
	"PartEngine"
];

//Explosive Parts
dayZ_explosiveParts = [
	"palivo",
	"motor"
];

//Survival Variables
SleepFood = 1720;
SleepWater = 1000;
SleepTemperatur = 90 / 100;

//Server Variables
allowConnection = false;
dayz_serverObjectMonitor = [];

//GUI
Dayz_GUI_R = 0.38; // 0.7
Dayz_GUI_G = 0.63; // -0.63
Dayz_GUI_B = 0.26; // -0.26

//actions blockers
a_player_cooking = false;
a_player_boil = false;
a_player_jerryfilling = false;
a_player_repairing = false;

//Player self-action handles
dayz_resetSelfActions = {
	s_player_equip_carry = -1;
	s_player_dragbody = -1;
	s_player_fire = -1;
	s_player_cook = -1;
	s_player_boil = -1;
	s_player_fireout = -1;
	s_player_butcher = -1;
	s_player_packtent = -1;
	s_player_fillwater = -1;
	s_player_fillwater2 = -1;
	s_player_fillfuel = -1;
	s_player_grabflare = -1;
	s_player_removeflare = -1;
	s_player_painkiller = -1;
	s_player_studybody = -1;
	s_build_Sandbag1_DZ = -1;
	s_build_Hedgehog_DZ = -1;
	s_build_Wire_cat1 = -1;
	s_player_deleteBuild = -1;
	//s_player_forceSave = -1;
	s_player_flipveh = -1;
	s_player_stats = -1;
	s_player_sleep = -1;
	s_player_fillfuel20 = -1;
	s_player_fillfuel5 = -1;
	s_player_siphonfuel = -1;
	s_player_repair_crtl = -1;
	s_player_fishing = -1;
	s_player_fishing_veh = -1;
	s_player_gather = -1;
	s_player_chloroform = -1;
	s_player_igniteTent = -1;
	s_player_igniteVehicle = -1;
	s_player_inspectVehicle = -1;
	s_player_search = -1;
	s_player_drink = -1;
	s_player_milk = -1;
};
call dayz_resetSelfActions;

//Engineering variables
s_player_lastTarget = objNull;
s_player_repairActions = [];

//for carry slot since determining mouse pos doesn't work right
mouseOverCarry = false;

//Initialize Medical Variables
force_dropBody = false;
r_interrupt = false;
r_doLoop = false;
r_self = false;
r_self_actions = [];
r_drag_sqf = false;
r_action = false;
r_action_unload = false;
r_player_handler = false;
r_player_handler1 = false;
r_player_dead = false;
r_player_unconscious = false;
r_player_infected = false;
r_player_injured = false;
r_player_inpain = false;
r_player_loaded = false;
r_player_cardiac = false;
r_fracture_legs = false;
r_fracture_arms = false;
r_player_vehicle = player;
r_player_blood = 12000;
r_player_bloodregen = 0;
r_player_bloodgainpersec = 0;
r_player_bloodlosspersec = 0;
r_player_bloodpersec = 0;
r_player_bloodpools = [];
r_player_lowblood = false;
r_player_timeout = 0;
r_player_bloodTotal = r_player_blood;
r_public_blood = r_player_blood;
r_player_bloodDanger = r_player_bloodTotal * 0.2;
r_player_actions = [];
r_handlerCount = 0;
r_action_repair = false;
r_action_targets = [];
r_pitchWhine = false;
r_isBandit = false;
carryClick = false;

//count actions
r_action_count = 0;

//ammo routine
r_player_actions2 = [];
r_action2 = false;
r_player_lastVehicle = objNull;
r_player_lastSeat = [];
r_player_removeActions2 = {
	if (!isNull r_player_lastVehicle) then {
		{
			r_player_lastVehicle removeAction _x;
		} forEach r_player_actions2;
		r_player_actions2 = [];
		r_action2 = false;
	};
};

//Body parts
USEC_woundHit = [
	"",
	"body",
	"hands",
	"legs",
	"head_hit"
];

//Wounds
DAYZ_woundHit = [
	[
		"body",
		"hands",
		"legs",
		"head_hit"
	],
	[ 0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,2,2,3]
];

DAYZ_woundHit_ok = [
	[
		"body",
		"hands",
		"legs"
	],
	[0,0,0,0,0,1,1,1,2,2]
];

USEC_MinorWounds = [
	"hands",
	"legs"
];
USEC_woundPoint = [
	["Pelvis","aimpoint"],
	["aimpoint"],
	["lelbow","relbow"],
	["RightFoot","LeftFoot"],
	["neck","pilot"]
];

USEC_typeOfWounds = [
	"Pelvis",
	"aimpoint",
	"lelbow","relbow",
	"RightFoot","LeftFoot",
	"neck","pilot"
];

//Initialize Zombie Variables
dayz_zombieTargetList = [
	["SoldierWB",50],
	["Air",500],
	["LandVehicle",200]
];
PVDZ_obj_Publish = [];
PVCDZ_obj_HideBody = objNull;

//DayZ settings
dayz_maxAnimals = 3;
dayz_maxPlants = 3;
dayz_animalDistance = 600;
dayz_plantDistance = 600;
dayz_spawnArea = 200;
dayz_cantseeDist = 150;
dayz_cantseefov = 70;
dayz_canDelete = 300;

//init global arrays for Loot Chances
call compile preprocessFileLineNumbers "\z\addons\dayz_code\init\loot_init.sqf";

if(isServer) then {
	dayz_players = [];
	dead_bodyCleanup = [];
	needUpdate_objects = [];
	//Server_InfectedCamps = [];
	dayz_traps = [];
	dayz_traps_active = [];
	dayz_traps_trigger = [];
};

if(!isDedicated) then {
	dayz_buildingMonitor = [];
	dayz_bodyMonitor = [];
	dayz_zedMonitor = [];
	dayz_buildingBubbleMonitor = [];
	
	//stamina
	dayzed_stamina = 12000;
	dayzed_stamina_total = dayzed_stamina;
	
	//temperature variables
	dayz_temperatur = 36;
	dayz_temperaturnormal = 36;
	dayz_temperaturmax = 42;
	dayz_temperaturmin = 27;

	//player special variables
	dayZ_lastPlayerUpdate = 0;
	dayz_hunger = 0;
	dayz_thirst = 0;
	dayz_preloadFinished = true;
	dayz_statusArray = [1,1];
	dayz_disAudial = 0;
	dayz_disVisual = 0;
	dayz_firedCooldown = 0;
	dayz_lastSave = time;
	dayzed_greenMountain = time;
	dayz_isSwimming = true;
	dayz_currentDay = 0;
	dayz_hasLight = false;
	dayz_surfaceNoise = 0;
	dayz_surfaceType = "None";
	dayz_heavenCooldown = 0;
	deathHandled = false;
	dayz_firstGroup = group player;
	dayz_originalPlayer = player;
	dayz_playerName = "Unknown";
	dayz_sourceBleeding = objNull;
	dayz_clientPreload = false;
	dayz_authed = false;
	dayz_panicCooldown = 0;
	dayz_areaAffect = 3.5;
	dayz_monitorPeriod = 0.6;
	dayz_heartBeat = false;
	dayz_spawnZombies = 0;
	dayz_swarmSpawnZombies = 0;
	dayz_maxLocalZombies = 30;
	dayz_CurrentNearByZombies = 0;
	dayz_maxNearByZombies = 60;
	dayz_currentGlobalZombies = 0;
	dayz_maxGlobalZeds = 3000;
	dayz_currentGlobalAnimals =	0;
	dayz_maxGlobalAnimals =		100;
	dayz_currentGlobalPlants = 0;
	dayz_maxGlobalPlants = 100;
	dayz_tickTimeOffset = 0;
	dayz_currentWeaponHolders = 0;
	dayz_maxMaxWeaponHolders = 80;	
	dayz_inVehicle = false;
	dayzGearSave = false;
	dayz_unsaved = false;
	dayz_scaleLight = 0;
	dayzDebug = false;
	dayzState = -1;
	dayz_onBack = "";
	dayz_toolBox = [];
	dayzed_currentCamera = "";
	dayz_onBackActive = false;
	dayz_fishingInprogress = false;
	dayz_siphonFuelInProgress = false;
	dayz_searchInProgress = false;
	dayz_milkInProgress = false;
	dayz_salvageInProgress = false;
	lastSpawned = diag_tickTime;
};