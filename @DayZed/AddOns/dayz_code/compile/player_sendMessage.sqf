/*
player_sendMessage.sqf
Author: cyrq (cyrq1337@gmail.com).
Made for DayZed: http://dayzed.eu
 
You can use, modify and distribute this without any permissions.
Give credit where credit is due.
 
Description: Sends messages to other players that have a radio using  publicVariable.
The messages are then received by player_recieveMessage.sqf
*/

private ["_player","_msg"];

disableSerialization;

closeDialog 0;

waitUntil {!dialog;};

player enableSimulation false;

createDialog "RscDisplayPassword";
ctrlSetText [1001,"Radio Message"];
ctrlSetText [101,""];
ctrlshow [1002,false];
buttonSetAction [1,"dzedRadio = [player, toArray(ctrlText 101)]; _player = dzedRadio select 0; _msg = dzedRadio select 1; if (count _msg != 0) then {publicVariable ""dzedRadio""}; if ((player == _player) and (count _msg != 0)) then {systemChat format [""Radio Transmission: %1"", (toString(_msg))];} else {cutText [""No message sent."", ""PLAIN DOWN""];}; if (!(simulationEnabled player)) then {player enableSimulation true;};"];
buttonSetAction [2,"if (!(simulationEnabled player)) then {player enableSimulation true;};"];

//Safety - if someone's presses ESC during the Dialog
waitUntil {!dialog;};
if (!(simulationEnabled player)) then {
	player enableSimulation true;
};
