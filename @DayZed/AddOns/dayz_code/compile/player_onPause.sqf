private["_display","_btnRespawn","_btnAbort","_timeOut","_timeMax", "_timeATM", "_btnAbortText","_inCombat","_playerCheck","_zedCheck","_playerBleeding", "_isTired"];

disableSerialization;

waitUntil
{
	_display = findDisplay 49;
	!isNull _display;
};

_btnRespawn = _display displayCtrl 1010;
_btnRespawn ctrlEnable false;

_btnAbort = _display displayCtrl 104;
_btnAbort ctrlEnable false;

_btnAbortText = ctrlText _btnAbort;

_timeOut = round(diag_tickTime);
_timeMax = round(_timeout + 30);

dayz_lastCheckSave = time;

if (r_player_dead) exitWith {
	_btnAbort ctrlEnable true;
		if !(_notInCombat) then {
		player setVariable ["NotInCombat",true,true];
		};
	};
	
if (r_fracture_legs) then {
	_btnRespawn ctrlEnable true;
	};
	
if (time - dayz_lastCheckSave > 10) then {
	call player_forceSave;
	};

while {(!isNull _display) and !r_player_dead} do {
	
	_timeATM = diag_tickTime;
	_playerCheck = if ({isPlayer _x} count (player nearEntities ["AllVehicles", 5]) > 1) then { true } else { false };
	_zedCheck = if (count (player nearEntities ["zZombie_Base", 10]) > 0) then { true } else { false };
	_isInjured = player getVariable["USEC_injured",false];
	_isTired = dayzed_stamina > 6000;
	_notInCombat = player getVariable["NotInCombat",false];

	Switch true do {
		case (_playerCheck) : {
			_btnAbort ctrlEnable false;
			_btnAbort ctrlSetText format["%1 (in 30)", _btnAbortText];
			cutText ["Something is nearby.\nCannot log out!", "PLAIN DOWN"];
			if (_notInCombat) then {
			player setVariable ["NotInCombat",false,true];
			};
		};
		case (_zedCheck) : {
			_btnAbort ctrlEnable false;
			_btnAbort ctrlSetText format["%1 (in 10)", _btnAbortText];
			cutText ["Something is nearby.\nCannot log out!", "PLAIN DOWN"];
			if (_notInCombat) then {
			player setVariable ["NotInCombat",false,true];
			};
		};
		case (_isInjured) : {
			_btnAbort ctrlEnable false;
			_btnAbort ctrlSetText format["%1 (in 10)", _btnAbortText];
			cutText ["You're injured and bleeding!\nFix yourself up before logging out!", "PLAIN DOWN"];
			if (_notInCombat) then {
			player setVariable ["NotInCombat",false,true];
			};
		};
		case (_isTired) : {
			_btnAbort ctrlEnable false;
			_btnAbort ctrlSetText format["%1 (in 10)", _btnAbortText];
			cutText ["You're to tired to log out.\nRest first!", "PLAIN DOWN"];
			if (_notInCombat) then {
			player setVariable ["NotInCombat",false,true];
			};
		};		
		case (_timeOut < _timeMax) : {
			_btnAbort ctrlEnable false;
			_btnAbort ctrlSetText format["%1 (in %2)", _btnAbortText, (_timeMax - _timeOut)];
			_timeOut = _timeOut + 1;
			if (_notInCombat) then {
			player setVariable ["NotInCombat",false,true];
			};
		};
		default {
		_btnAbort ctrlEnable true;
		_btnAbort ctrlSetText _btnAbortText;
		if !(_notInCombat) then {
			player setVariable ["NotInCombat",true,true];
			};
		};
	};
	waitUntil {
	sleep 0.2;
	((diag_tickTime - _timeATM) > 1)
	}
};