scriptName "Functions\misc\fn_selfActions.sqf";
/***********************************************************
	ADD ACTIONS FOR SELF
	- Function
	- [] call fnc_usec_selfActions;
************************************************************/
_vehicle = vehicle player;
_inVehicle = (_vehicle != player);
_cursorTarget = cursorTarget;
_primaryWeapon = primaryWeapon player;
_currentWeapon = currentWeapon player;
_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;

_nearLight = nearestObject [player,"LitObject"];
_canPickLight = false;

if (!isNull _nearLight) then {
	if (_nearLight distance player < 4) then {
		_canPickLight = isNull (_nearLight getVariable ["owner",objNull]);
	};
};

_canDo = (!r_drag_sqf and !r_player_unconscious and !_onLadder);

//Grab Flare
if (_canPickLight and !dayz_hasLight) then {
	if (s_player_grabflare < 0) then {
		_text = getText (configFile >> "CfgAmmo" >> (typeOf _nearLight) >> "displayName");
		s_player_grabflare = player addAction [format[localize "str_actions_medical_15",_text], "\z\addons\dayz_code\actions\flare_pickup.sqf",_nearLight, 1, false, true, "", ""];
		s_player_removeflare = player addAction [format[localize "str_actions_medical_17",_text], "\z\addons\dayz_code\actions\flare_remove.sqf",_nearLight, 1, false, true, "", ""];
	};
} else {
	player removeAction s_player_grabflare;
	player removeAction s_player_removeflare;
	s_player_grabflare = -1;
	s_player_removeflare = -1;
};

if (dayz_onBack != "" && !dayz_onBackActive && !_inVehicle && !_onLadder && !r_player_unconscious) then {
	if (s_player_equip_carry < 0) then {
		_text = getText (configFile >> "CfgWeapons" >> dayz_onBack >> "displayName");
		s_player_equip_carry = player addAction [format[localize "STR_ACTIONS_WEAPON", _text], "\z\addons\dayz_code\actions\player_switchWeapon.sqf", "action", 0.5, false, true];
	};
} else {
	player removeAction s_player_equip_carry;
	s_player_equip_carry = -1;
};

if (!isNull _cursorTarget and !_inVehicle and (player distance _cursorTarget < 4)) then { //Has some kind of target
	_isHarvested = _cursorTarget getVariable["meatHarvested",false];
	_isInspected = _cursorTarget getVariable["vehicleInspected",false];
	_isVehicle = _cursorTarget isKindOf "AllVehicles";
	_isVehicletype = typeOf _cursorTarget in ["ATV_US_EP1","ATV_CZ_EP1"];
	_isMan = _cursorTarget isKindOf "Man";
	_ownerID = _cursorTarget getVariable ["characterID","0"];
	_isAnimal = _cursorTarget isKindOf "Animal";
	_isMilkable = (_cursorTarget isKindOf "Cow" || _cursorTarget isKindOf "Goat");
	_isZombie = _cursorTarget isKindOf "zZombie_base";
	_isDestructable = _cursorTarget isKindOf "BuiltItems";
	_isTent = (_cursorTarget isKindOf "TentStorage" || _cursorTarget isKindOf "DomeTentStorage");
	_isStash = _cursorTarget isKindOf "StashSmall";
	_isMediumStash = _cursorTarget isKindOf "StashMedium";
	_isStorageBox = _cursorTarget isKindOf "StorageBox";
	_isFuel = false;
	_hasFuel20 = "ItemJerrycan" in magazines player;
	_hasFuel5 = "ItemFuelcan" in magazines player;
	_hasFuelE20 = "ItemJerrycanEmpty" in magazines player;
	_hasFuelE5 = "ItemFuelcanEmpty" in magazines player;
	_hasToolbox = "ItemToolbox" in items player;
	_hasKnife = ("ItemKnife" in items player) || ("ItemKnife5" in items player) || ("ItemKnife4" in items player) || ("ItemKnife3" in items player) || ("ItemKnife2" in items player) || ("ItemKnife1" in items player) || ("ItemKnifeBlunt" in items player);
	_hasMatches = ("ItemMatchbox" in items player) || ("ItemMatchbox5" in items player) || ("ItemMatchbox4" in items player) || ("ItemMatchbox3" in items player) || ("ItemMatchbox2" in items player) || ("ItemMatchbox1" in items player);
	_hasChloroform = "ItemChloroform" in magazines player;
	_hasbottleitem = "ItemWaterbottle" in magazines player;
	_isAlive = alive _cursorTarget;
	_canmove = canmove _cursorTarget;
	_text = getText (configFile >> "CfgVehicles" >> typeOf _cursorTarget >> "displayName");
	_isPlant = typeOf _cursorTarget in Dayz_plants;
	_isBike = cursorTarget isKindOf "Bicycle";
	_isMotorBike = cursorTarget isKindOf "Motorcycle";
	_isTractor = cursorTarget isKindOf "Tractor";
	_isSearchable = typeOf _cursorTarget in Searchables;
	_isWaterSource = (cursorTarget isKindOf "Land_pumpa") || (cursorTarget isKindOf "Land_water_tank");

	/*//gather
	if(_isPlant and _canDo) then {
		if (s_player_gather < 0) then {
			_text = getText (configFile >> "CfgVehicles" >> typeOf _cursorTarget >> "displayName");
			s_player_gather = player addAction [format[localize "str_actions_gather",_text], "\z\addons\dayz_code\actions\player_gather.sqf",_cursorTarget, 1, true, true, "", ""];
		};
	} else {
		player removeAction s_player_gather;
		s_player_gather = -1;
	};*/
	
	if (_hasFuelE20 or _hasFuelE5) then {
		_isFuel = (_cursorTarget isKindOf "Land_Ind_TankSmall") or (_cursorTarget isKindOf "Land_fuel_tank_big") or (_cursorTarget isKindOf "Land_fuel_tank_stairs") or (_cursorTarget isKindOf "Land_wagon_tanker");
	};

	//Allow player to delete objects
	if(_isDestructable and _hasToolbox and _canDo) then {
		if (s_player_deleteBuild < 0) then {
			s_player_deleteBuild = player addAction [format[localize "str_actions_delete",_text], "\z\addons\dayz_code\actions\remove.sqf",_cursorTarget, 1, true, true, "", ""];
		};
	} else {
		player removeAction s_player_deleteBuild;
		s_player_deleteBuild = -1;
	};
	/*
	//Allow player to force save
	if((_isVehicle or _isTent or _isStash or _isMediumStash or _isStorageBox) and _canDo and !_isMan and (damage _cursorTarget < 1)) then {
		if (s_player_forceSave < 0) then {
			s_player_forceSave = player addAction [format[localize "str_actions_save",_text], "\z\addons\dayz_code\actions\forcesave.sqf",_cursorTarget, 1, true, true, "", ""];
		};
	} else {
		player removeAction s_player_forceSave;
		s_player_forceSave = -1;
	};
	*/
	//flip vehicle
	if ((_isVehicletype) and !_canmove and _isAlive and (player distance _cursorTarget >= 2) and (count (crew _cursorTarget))== 0 and ((vectorUp _cursorTarget) select 2) < 0.5) then {
		if (s_player_flipveh < 0) then {
			s_player_flipveh = player addAction [format[localize "str_actions_flipveh",_text], "\z\addons\dayz_code\actions\player_flipvehicle.sqf",_cursorTarget, 1, true, true, "", ""];
		};
	} else {
		player removeAction s_player_flipveh;
		s_player_flipveh = -1;
	};
	
	//Ignite Tent
	if((_isTent or _isStorageBox) and _hasMatches and _canDo and !_isMan) then {
        if (s_player_igniteTent < 0) then {
            s_player_igniteTent = player addAction [format[localize "str_action_ignitetent", _text], "\z\addons\dayz_code\actions\tent_ignite.sqf",cursorTarget, 1, true, true, "", ""];
        };
    } else {
        player removeAction s_player_igniteTent;
        s_player_igniteTent = -1;
    };
	
	//Ignite Vehicle
	if(_hasFuel20 and _canDo and _isVehicle and _hasMatches and !_isMan and (damage _cursorTarget < 1)) then {
        if (s_player_igniteVehicle < 0) then {
            s_player_igniteVehicle = player addAction [format[localize "str_action_ignitevehicle", _text], "\z\addons\dayz_code\actions\vehicle_ignite.sqf",["ItemJerrycan"], 0, true, true, "", "'ItemJerrycan' in magazines player"];
        };
    } else {
        player removeAction s_player_igniteVehicle;
        s_player_igniteVehicle = -1;
    };
	
	//Inspect Vehicle
	if(_canDo and _isVehicle and _hasToolbox and !_isInspected and !_isMan and !_isBike and (damage _cursorTarget < 1)) then {
		if (s_player_inspectVehicle < 0) then {
			s_player_inspectVehicle = player addAction [format[localize "str_action_inspectvehicle", _text], "\z\addons\dayz_code\actions\inspect_vehicle.sqf", _cursorTarget, 1, true, true, "", ""];
		};
	} else {
		player removeAction s_player_inspectVehicle;
		s_player_inspectVehicle = -1;
	};
	
	// Search
	if (_isSearchable and _canDo and (player distance _cursorTarget <= 3)) then {
		if (s_player_search < 0) then {
			_name = getText (configFile >> "CfgVehicles" >> (typeOf cursorTarget) >> "displayName");
			s_player_search = player addAction [format["Search %1", _name], "\z\addons\dayz_code\actions\search.sqf",_cursorTarget, 1, true, true, "", ""];
		};
	} else {
		player removeAction s_player_search;
		s_player_search = -1;
	};
	
	//Chloroform
	if (_isMan and _isAlive and !_isZombie and !_isAnimal and _hasChloroform and _canDo) then {
		if (s_player_chloroform < 0) then {
			s_player_chloroform = player addAction [localize "str_action_chloroform", "\z\addons\dayz_code\actions\chloroform.sqf",cursorTarget, 0, false, true, "",""];
		};
	} else {
		player removeAction s_player_chloroform;
		s_player_chloroform = -1;
	};
	
	//Drink
	if (_isWaterSource and _canDo) then {
		if (s_player_drink < 0) then {
			s_player_drink = player addAction [format["Drink Water"], "\z\addons\dayz_code\actions\drink_water.sqf",_cursorTarget, 1, true, true, "", ""];
		};
	} else {
		player removeAction s_player_drink;
		s_player_drink = -1;
	};		

	//Allow player to fill Fuel can
	if((_hasFuelE20 or _hasFuelE5) and _isFuel and !_isZombie and !_isAnimal and !_isMan and _canDo and !a_player_jerryfilling) then {
		if (s_player_fillfuel < 0) then {
			s_player_fillfuel = player addAction [localize "str_actions_self_10", "\z\addons\dayz_code\actions\jerry_fill.sqf",[], 1, false, true, "", ""];
		};
	} else {
		player removeAction s_player_fillfuel;
		s_player_fillfuel = -1;
	};

	//Allow player to fill vehilce 20L
	if(_hasFuel20 and _canDo and !_isZombie and !_isAnimal and !_isMan and _isVehicle and !_isBike and (fuel _cursorTarget < 1) and (damage _cursorTarget < 1)) then {
		if (s_player_fillfuel20 < 0) then {
			s_player_fillfuel20 = player addAction [format[localize "str_actions_medical_10",_text,"20"], "\z\addons\dayz_code\actions\refuel.sqf",["ItemJerrycan"], 0, true, true, "", "'ItemJerrycan' in magazines player"];
		};
	} else {
		player removeAction s_player_fillfuel20;
		s_player_fillfuel20 = -1;
	};

	//Allow player to fill vehilce 5L
	if(_hasFuel5 and _canDo and !_isZombie and !_isAnimal and !_isMan and _isVehicle and !_isBike and (fuel _cursorTarget < 1) and (damage _cursorTarget < 1)) then {
		if (s_player_fillfuel5 < 0) then {
			s_player_fillfuel5 = player addAction [format[localize "str_actions_medical_10",_text,"5"], "\z\addons\dayz_code\actions\refuel.sqf",["ItemFuelcan"], 0, true, true, "", "'ItemFuelcan' in magazines player"];
		};
	} else {
		player removeAction s_player_fillfuel5;
		s_player_fillfuel5 = -1;
	};

	//Allow player to spihon vehicles
	if ((_hasFuelE20 or _hasFuelE5) and !_isZombie and !_isAnimal and !_isMan and _isVehicle and !_isBike and _canDo and !a_player_jerryfilling and (fuel _cursorTarget > 0) and (damage _cursorTarget < 1)) then {
		if (s_player_siphonfuel < 0) then {
			s_player_siphonfuel = player addAction [format[localize "str_siphon_start"], "\z\addons\dayz_code\actions\siphonFuel.sqf",_cursorTarget, 0, true, true, "", ""];
		};
	} else {
		player removeAction s_player_siphonfuel;
		s_player_siphonfuel = -1;
	};

	//Harvested
	if (!alive _cursorTarget and _isAnimal and _hasKnife and !_isHarvested and _canDo) then {
		if (s_player_butcher < 0) then {
			s_player_butcher = player addAction [localize "str_actions_self_04", "\z\addons\dayz_code\actions\gather_meat.sqf",_cursorTarget, 3, true, true, "", ""];
		};
	} else {
		player removeAction s_player_butcher;
		s_player_butcher = -1;
	};
	
	//Milk Cows	
	if (alive _cursorTarget and _isMilkable and !_isHarvested and _canDo) then {
		if (s_player_milk < 0) then {
			_name = getText (configFile >> "CfgVehicles" >> (typeOf cursorTarget) >> "displayName");
			s_player_milk = player addAction [format[localize "str_actions_cow",_name], "\z\addons\dayz_code\actions\gather_milk.sqf",_cursorTarget, 3, true, true, "", ""];
		};
	} else {
		player removeAction s_player_milk;
		s_player_milk = -1;
	};
	
	//Fireplace Actions check
	if (inflamed _cursorTarget) then {
		_hasRawMeat = {_x in meatraw} count magazines player > 0;
		_hastinitem = {_x in boil_tin_cans} count magazines player > 0;
		
	//Cook Meat	
		if (_hasRawMeat and !a_player_cooking) then {
			if (s_player_cook < 0) then {
				s_player_cook = player addAction [localize "str_actions_self_05", "\z\addons\dayz_code\actions\cook.sqf",_cursorTarget, 3, true, true, "", ""];
			};
		}; 
	//Boil Water
		if (_hastinitem and _hasbottleitem and !a_player_boil) then {
			if (s_player_boil < 0) then {
				s_player_boil = player addAction [localize "str_actions_boilwater", "\z\addons\dayz_code\actions\boil.sqf",_cursorTarget, 3, true, true, "", ""];
			};
		};
	} else {
		if (a_player_cooking) then {
			player removeAction s_player_cook;
			s_player_cook = -1;
		};
		if (a_player_boil) then {
			player removeAction s_player_boil;
			s_player_boil = -1;
		};
	};

	//Fire Pack
	if(_cursorTarget == dayz_hasFire and _canDo) then {
		if ((s_player_fireout < 0) and !(inflamed _cursorTarget) and (player distance _cursorTarget < 3)) then {
			s_player_fireout = player addAction [localize "str_actions_self_06", "\z\addons\dayz_code\actions\fire_pack.sqf",_cursorTarget, 0, false, true, "",""];
		};
	} else {
		player removeAction s_player_fireout;
		s_player_fireout = -1;
	};

	//Packing my tent
	if(_isTent and _canDo and _ownerID == dayz_characterID) then {
		if ((s_player_packtent < 0) and (player distance _cursorTarget < 3)) then {
			s_player_packtent = player addAction [localize "str_actions_self_07", "\z\addons\dayz_code\actions\tent_pack.sqf",_cursorTarget, 0, false, true, "",""];
		};
	} else {
		player removeAction s_player_packtent;
		s_player_packtent = -1;
		};

	//Sleep
	if(_isTent and _canDo and _ownerID == dayz_characterID) then {
		if ((s_player_sleep < 0) and (player distance _cursorTarget < 3)) then {
			s_player_sleep = player addAction [localize "str_actions_self_sleep", "\z\addons\dayz_code\actions\player_sleep.sqf",_cursorTarget, 0, false, true, "",""];
		};
	} else {
		player removeAction s_player_sleep;
		s_player_sleep = -1;
	};

	//Repairing Vehicles
	if ((dayz_myCursorTarget != _cursorTarget) and _isVehicle and !_isMan and _hasToolbox and (damage _cursorTarget < 1)) then {
		if (s_player_repair_crtl < 0) then {
			if (_isBike) then {
				dayz_myCursorTarget = _cursorTarget;
				_menu = dayz_myCursorTarget addAction [format[localize "str_action_repairbike", _text], "\z\addons\dayz_code\actions\repair_bike.sqf", _cursorTarget, 0, true, false, "", ""];
				s_player_repairActions set [count s_player_repairActions,_menu];
				s_player_repair_crtl = 1;
			} else {
				if (_isInspected) then {
					dayz_myCursorTarget = _cursorTarget;
					_menu = dayz_myCursorTarget addAction [format[localize "str_actions_rapairveh", _text], "\z\addons\dayz_code\actions\repair_vehicle.sqf",_cursorTarget, 0, true, false, "",""];
					s_player_repairActions set [count s_player_repairActions,_menu];
					s_player_repair_crtl = 1;
				if (!_isMotorBike and !_isTractor and !_isBike) then {
					_menu1 = dayz_myCursorTarget addAction [format[localize "str_actions_salvageveh", _text], "\z\addons\dayz_code\actions\salvage_vehicle.sqf",_cursorTarget, 0, true, false, "",""];
					s_player_repairActions set [count s_player_repairActions,_menu1];
					s_player_repair_crtl = 1;
				};
			};
		};	
	} else {
		{dayz_myCursorTarget removeAction _x} forEach s_player_repairActions;
			s_player_repairActions = [];
			s_player_repair_crtl = -1;
		};
	};
	
	//Study Body
	if (_isMan and !_isAlive and !_isZombie and !_isAnimal) then {
		if (s_player_studybody < 0) then {
			s_player_studybody = player addAction [localize "str_action_studybody", "\z\addons\dayz_code\actions\study_body.sqf",_cursorTarget, 0, false, true, "",""];
		};
	} else {
		player removeAction s_player_studybody;
		s_player_studybody = -1;
	};
} else {
	//Engineering
	{dayz_myCursorTarget removeAction _x} forEach s_player_repairActions;s_player_repairActions = [];
	player removeAction s_player_repair_crtl;
	s_player_repair_crtl = -1;
	dayz_myCursorTarget = objNull;
	//Others
	//player removeAction s_player_forceSave;
	//s_player_forceSave = -1;
	player removeAction s_player_flipveh;
	s_player_flipveh = -1;
	player removeAction s_player_sleep;
	s_player_sleep = -1;
	player removeAction s_player_deleteBuild;
	s_player_deleteBuild = -1;
	player removeAction s_player_butcher;
	s_player_butcher = -1;
	player removeAction s_player_cook;
	s_player_cook = -1;
	player removeAction s_player_boil;
	s_player_boil = -1;
	player removeAction s_player_fireout;
	s_player_fireout = -1;
	player removeAction s_player_packtent;
	s_player_packtent = -1;
	player removeAction s_player_fillfuel;
	s_player_fillfuel = -1;
	player removeAction s_player_studybody;
	s_player_studybody = -1;
	player removeAction s_player_chloroform;
	s_player_chloroform = -1;
	player removeAction s_player_igniteTent;
    s_player_igniteTent = -1;
	player removeAction s_player_igniteVehicle;
    s_player_igniteVehicle = -1;
	player removeAction s_player_inspectVehicle;
    s_player_inspectVehicle = -1;
	player removeAction s_player_search;
	s_player_search = -1;
	player removeAction s_player_drink;
	s_player_drink = -1;
	player removeAction s_player_milk;
	s_player_milk = -1;
	//fuel
	player removeAction s_player_fillfuel20;
	s_player_fillfuel20 = -1;
	player removeAction s_player_fillfuel5;
	s_player_fillfuel5 = -1;
	//Allow player to spihon vehicle fuel
	player removeAction s_player_siphonfuel;
	s_player_siphonfuel = -1;
	/*
	//Allow player o gather
	player removeAction s_player_gather;
	s_player_gather = -1;
	*/
};