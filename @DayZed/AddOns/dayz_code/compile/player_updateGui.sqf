private["_display","_ctrlBlood","_bloodVal","_ctrlFood","_ctrlThirst","_foodVal","_ctrlTemp","_tempVal","_staminaVal","_array"];
disableSerialization;

_bloodVal =		r_player_blood / r_player_bloodTotal;
_staminaVal = 	dayzed_stamina / dayzed_stamina_total;
_foodVal = 		1 - (dayz_hunger / SleepFood);
_thirstVal = 	1 - (dayz_thirst / SleepWater);
_tempVal 	= 	1 - ((dayz_temperatur - dayz_temperaturmin)/(dayz_temperaturmax - dayz_temperaturmin));	// Normalise to [0,1]


if (uiNamespace getVariable ['DZ_displayUI', 0] == 1) exitWith {
	_array = [_foodVal,_thirstVal];
	_array
};

_display = uiNamespace getVariable 'DAYZ_GUI_display';

_zedKills = player getVariable["zombieKills",0];
_humanKills = player getVariable["humanKills",0];
_banditKills = player getVariable["banditKills",0];
_allKills = _humanKills +_banditKills;

_ctrlBlood = 	_display displayCtrl 1300;
_ctrlBleed = 	_display displayCtrl 1303;
_ctrlFood = 	_display displayCtrl 1301;
_ctrlThirst = 	_display displayCtrl 1302;
_ctrlTemp 	= 	_display displayCtrl 1306;
_ctrlFracture = 	_display displayCtrl 1203;
_ctrlInfection = 	_display displayCtrl 1204;
_ctrlPain = 	_display displayCtrl 1209;
_ctrlStamina = 	_display displayCtrl 1310;

_ctrlBloodBorder  = _display displayCtrl 1200;
_ctrlFoodBorder   = _display displayCtrl 1201;
_ctrlThirstBorder = _display displayCtrl 1202;
_ctrlTempBorder   = _display displayCtrl 1208;
_ctrlStaminaBorder= _display displayCtrl 1210;

_ctrlBloodBorder  ctrlSetTextColor [0.92,0.92,0.92,1];
_ctrlFoodBorder   ctrlSetTextColor [0.92,0.92,0.92,1];
_ctrlThirstBorder ctrlSetTextColor [0.92,0.92,0.92,1];
_ctrlTempBorder ctrlSetTextColor [0.92,0.92,0.92,1];
_ctrlStaminaBorder ctrlSetTextColor [0.92,0.92,0.92,1];

_zedPic = _display displayCtrl 1211;
_killsPic = _display displayCtrl 1212;

_killsTxt = _display displayCtrl 1213;
_zedTxt = _display displayCtrl 1214;

_zedTxt ctrlSetStructuredText parseText format ["%1",_zedKills];
_killsTxt ctrlSetStructuredText parseText format ["%1",_allKills];

//Food/Water/Blood
_bloodColor = [(Dayz_GUI_R + (0.2 * (1-_bloodVal))),(Dayz_GUI_G * _bloodVal),(Dayz_GUI_B * _bloodVal), 1];
_ctrlBlood ctrlSetTextColor _bloodColor;

_foodColor = [(Dayz_GUI_R + (0.2 * (1-_foodVal))),(Dayz_GUI_G * _foodVal),(Dayz_GUI_B * _foodVal), 1];
_ctrlFood ctrlSetTextColor _foodColor;

_thirstColor = [(Dayz_GUI_R + (0.2 * (1-_thirstVal))),(Dayz_GUI_G * _thirstVal),(Dayz_GUI_B * _thirstVal), 1];
_ctrlThirst ctrlSetTextColor _thirstColor;

_tempColor = [(Dayz_GUI_R + (0.2 * _tempVal)),(Dayz_GUI_G * (1-_tempVal)),(Dayz_GUI_B * (1-_tempVal)), 1];
_ctrlTemp ctrlSetTextColor _tempColor;

_staminaColor = [(Dayz_GUI_R + (0.2 * _staminaVal)),(Dayz_GUI_G * (1-_staminaVal)),(Dayz_GUI_B * (1-_staminaVal)), 1];
_ctrlStamina ctrlSetTextColor _staminaColor;

_bloodText = "";
_bloodLevel = round(_bloodVal * 10);
if (_bloodLevel > 0) then {_bloodText = "\z\addons\dayz_code\gui\status\bloodlevel_" + str(_bloodLevel) + "_ca.paa"};
_ctrlBlood ctrlSetText _bloodText;

_foodLevel = round((_foodVal max 0) * 10);
_foodText = "\z\addons\dayz_code\gui\status\foodlevel_" + str(_foodLevel) + "_ca.paa";
_ctrlFood ctrlSetText _foodText;

_thirstText = "";
_thirstLevel = round((_thirstVal max 0) * 10);
if (_thirstLevel > 0) then {_thirstText = "\z\addons\dayz_code\gui\status\thirstlevel_" + str(_thirstLevel) + "_ca.paa"};
_ctrlThirst ctrlSetText _thirstText;


if (_bloodVal < 0.2) then {
	_ctrlBlood call player_guiControlFlash;
} else {
};

if (_thirstVal < 0.2) then {
	_ctrlThirst call player_guiControlFlash;
} else {
};

if (_foodVal < 0.2) then {
	_ctrlFood call player_guiControlFlash;
} else {
};

if (_tempVal > 0.8 ) then {
	_ctrlTemp call player_guiControlFlash;
} else {
	_ctrlTemp ctrlShow true;
};

if (_staminaVal > 0.8 ) then {
	_ctrlStamina call player_guiControlFlash;
} else {
	_ctrlStamina ctrlShow true;
};

if (r_player_injured) then {
	_ctrlBleed call player_guiControlFlash;
};

if (r_player_infected) then {
	_ctrlInfection ctrlShow true;
} else {
	_ctrlInfection ctrlShow false;

};

if (r_player_inpain) then {
	_ctrlPain ctrlShow true;
} else {
	_ctrlPain ctrlShow false;

};

if (!canStand player) then {
	if (!(ctrlShown _ctrlFracture)) then {
		r_fracture_legs = true;
		_ctrlFracture ctrlShow true;
	};
};

_array = [_foodVal,_thirstVal];
_array