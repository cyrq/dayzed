
waitUntil {!(isNil "CLAY_TopolkaDam_sound")};


while {CLAY_TopolkaDamFX} do
{
	waitUntil {CLAY_TopolkaDam_sound};

	_soundSource = "HeliHEmpty" createVehicleLocal [0,0,0];
	_soundSource setPosASL [10315.187,3607.5203,37.4];

	[_soundSource] spawn
	{
		_s = _this select 0;
		while {CLAY_TopolkaDam_sound} do
		{
			_s say "CLAY_WaterfallFX1";
			sleep 35;
		};
	};

	waitUntil {!(CLAY_TopolkaDam_sound) || !(CLAY_TopolkaDamFX)};

	sleep 1;
	CLAY_TopolkaDam_sound = false;
	deleteVehicle _soundSource;
};
