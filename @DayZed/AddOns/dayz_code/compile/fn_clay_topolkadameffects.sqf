/*********************************************************************
***
***	Title: Topolka Dam Effects
***
***
***	Description: Creates water and sound effects aswell as a
***		     small river with rapids at the Topolka Dam.
***
***
***	Author: Clayman	<worldofclay@gmx.de>
***
***	Version: 1.2
***
**********************************************************************/

CLAY_TopolkaDamFX = true;


_water1 = "MAP_pond_big_01" createVehicleLocal [0,0,0];
_water1 setPosASL [10315.187,3607.5203,37.4];
_water2 = "MAP_pond_big_01" createVehicleLocal [0,0,0];
_water2 setPosASL [10316.801,3538.1584,36.1];
_water3 = "MAP_pond_big_01" createVehicleLocal [0,0,0];
_water3 setPosASL [10313.91,3470.2805,36.1];
_water4 = "MAP_pond_big_28_03" createVehicleLocal [0,0,0];
_water4 setPosASL [10301.494,3397.4358,36.1];
_water5 = "MAP_pond_big_28_01" createVehicleLocal [0,0,0];
_water5 setPosASL [10306.504,3422.1577,36.1];
_water5 setDir 355;

_rock1 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock1 setPosASL [10309.365,3398.7043,5.885242];
_rock2 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock2 setPosASL [10289.212,3400.3198,12.0457];
_rock3 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock3 setPosASL [10294.988,3397.6316,5.743645];
_rock4 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock4 setPosASL [10306.603,3397.3662,8.251797];
_rock5 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock5 setPosASL [10300.585,3396.4177,10.969398];
_rock6 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock6 setPosASL [10304.021,3396.481,10.587006];
_rock7 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock7 setPosASL [10297.084,3397.292,7.810661];
_rock8 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock8 setPosASL [10290.266,3400.9587,12.590393];
_rock9 = "MAP_R2_Rock1" createVehicleLocal [0,0,0];
_rock9 setPosASL [10323.604,3570.0215,6.65696];

_boulder1 = "MAP_R2_Boulder1" createVehicleLocal [0,0,0];
_boulder1 setPosASL [10324.289,3568.2805,37.209103];
_boulder1 setDir 180;
_boulder2 = "MAP_R2_Boulder1" createVehicleLocal [0,0,0];
_boulder2 setPosASL [10321.835,3566.0767,36.486861];
_boulder3 = "MAP_R2_Boulder1" createVehicleLocal [0,0,0];
_boulder3 setPosASL [10306.476,3569.1716,36.904892];
_boulder4 = "MAP_R2_Boulder1" createVehicleLocal [0,0,0];
_boulder4 setPosASL [10307.284,3567.6069,36.230499];
_boulder4 setDir 270;
_boulder5 = "MAP_R2_Boulder1" createVehicleLocal [0,0,0];
_boulder5 setPosASL [10307.619,3569.6309,36.609241];


If (count _this == 5) Then
{
	CLAY_TopolkaDam_lights = _this select 0;

	_colors = _this select 1;
	If (count _colors == 3) Then
	{
		If (count (_colors select 0) == 3) Then
		{
			CLAY_TopolkaDam_lightCol1 = _colors select 0;
		}
		Else
		{
			CLAY_TopolkaDam_lightCol1 = [1,0,0];
		};
		If (count (_colors select 1) == 3) Then
		{
			CLAY_TopolkaDam_lightCol2 = _colors select 1;
		}
		Else
		{
			CLAY_TopolkaDam_lightCol2 = [0,1,0];
		};
		If (count (_colors select 2) == 3) Then
		{
			CLAY_TopolkaDam_lightCol3 = _colors select 2;
		}
		Else
		{
			CLAY_TopolkaDam_lightCol3 = [0,0,1];
		};
	}
	Else
	{
		CLAY_TopolkaDam_lightCol1 = [1,0,0];
		CLAY_TopolkaDam_lightCol2 = [0,1,0];
		CLAY_TopolkaDam_lightCol3 = [0,0,1];
	};

	CLAY_TopolkaDam_gate1 = _this select 2;
	CLAY_TopolkaDam_gate2 = _this select 3;
	CLAY_TopolkaDam_gate3 = _this select 4;
}
Else
{
	If (isNil "CLAY_TopolkaDam_lights") Then {CLAY_TopolkaDam_lights = true;};

	If (isNil "CLAY_TopolkaDam_lightCol1") Then {CLAY_TopolkaDam_lightCol1 = [1,0,0];};
	If (isNil "CLAY_TopolkaDam_lightCol2") Then {CLAY_TopolkaDam_lightCol2 = [0,1,0];};
	If (isNil "CLAY_TopolkaDam_lightCol3") Then {CLAY_TopolkaDam_lightCol3 = [0,0,1];};

	If (isNil "CLAY_TopolkaDam_gate1") Then {CLAY_TopolkaDam_gate1 = true;};
	If (isNil "CLAY_TopolkaDam_gate2") Then {CLAY_TopolkaDam_gate2 = true;};
	If (isNil "CLAY_TopolkaDam_gate3") Then {CLAY_TopolkaDam_gate3 = true;};
};


If (CLAY_TopolkaDam_lights) Then
{
	CLAY_TopolkaDam_light1 = "#lightpoint" createVehicleLocal [10309.4,3620,5.5];
	CLAY_TopolkaDam_light1 setLightBrightness 0.1;
	CLAY_TopolkaDam_light1 setLightColor CLAY_TopolkaDam_lightCol1;
	CLAY_TopolkaDam_light1 setLightAmbient CLAY_TopolkaDam_lightCol1;

	CLAY_TopolkaDam_light2 = "#lightpoint" createVehicleLocal [10315.5,3620,5.5];
	CLAY_TopolkaDam_light2 setLightBrightness 0.1;
	CLAY_TopolkaDam_light2 setLightColor CLAY_TopolkaDam_lightCol2;
	CLAY_TopolkaDam_light2 setLightAmbient CLAY_TopolkaDam_lightCol2;

	CLAY_TopolkaDam_light3 = "#lightpoint" createVehicleLocal [10321.9,3620,5.5];
	CLAY_TopolkaDam_light3 setLightBrightness 0.1;
	CLAY_TopolkaDam_light3 setLightColor CLAY_TopolkaDam_lightCol3;
	CLAY_TopolkaDam_light3 setLightAmbient CLAY_TopolkaDam_lightCol3;
};


[] execVM "\z\addons\dayz_code\compile\fn_clay_topolkadamsound.sqf";

_soundSource = "HeliHEmpty" createVehicleLocal [0,0,0];
_soundSource setPosASL [10315.187,3567.5203,36];

[_soundSource] spawn
{
	_s = _this select 0;
	while {CLAY_TopolkaDamFX} do
	{
		_s say "CLAY_WaterfallFX2";
		sleep 10;
	};
};


private ["_z", "_up"];
_z = 36.1;
_up = true;

while {CLAY_TopolkaDamFX} do
{

	If (CLAY_TopolkaDam_gate1 || CLAY_TopolkaDam_gate2 || CLAY_TopolkaDam_gate3) Then
	{
		CLAY_TopolkaDam_sound = true;
	}
	Else
	{
		CLAY_TopolkaDam_sound = false;
	};

	If (CLAY_TopolkaDam_lights) Then
	{
		If (isNull CLAY_TopolkaDam_light1) Then
		{
			CLAY_TopolkaDam_light1 = "#lightpoint" createVehicleLocal [10309.4,3620,5.5];
			CLAY_TopolkaDam_light1 setLightBrightness 0.1;
			CLAY_TopolkaDam_light1 setLightColor CLAY_TopolkaDam_lightCol1;
			CLAY_TopolkaDam_light1 setLightAmbient CLAY_TopolkaDam_lightColl1;
		};
		If (isNull CLAY_TopolkaDam_light2) Then
		{
			CLAY_TopolkaDam_light2 = "#lightpoint" createVehicleLocal [10315.5,3620,5.5];
			CLAY_TopolkaDam_light2 setLightBrightness 0.1;
			CLAY_TopolkaDam_light2 setLightColor CLAY_TopolkaDam_lightCol2;
			CLAY_TopolkaDam_light2 setLightAmbient CLAY_TopolkaDam_lightCol2;
		};
		If (isNull CLAY_TopolkaDam_light3) Then
		{
			CLAY_TopolkaDam_light3 = "#lightpoint" createVehicleLocal [10321.9,3620,5.5];
			CLAY_TopolkaDam_light3 setLightBrightness 0.1;
			CLAY_TopolkaDam_light3 setLightColor CLAY_TopolkaDam_lightCol3;
			CLAY_TopolkaDam_light3 setLightAmbient CLAY_TopolkaDam_lightCol3;
		};
	}
	Else
	{
		If (!(isNil "CLAY_TopolkaDam_light1")) Then {deleteVehicle CLAY_TopolkaDam_light1;};
		If (!(isNil "CLAY_TopolkaDam_light2")) Then {deleteVehicle CLAY_TopolkaDam_light2;};
		If (!(isNil "CLAY_TopolkaDam_light3")) Then {deleteVehicle CLAY_TopolkaDam_light3;};
	};

	If (_up) Then
	{
		If (_z < 36.2) Then
		{
			_z = _z + 0.005;
		}
		Else
		{
			_up = false;
		};
	}
	Else
	{
		If (_z > 36.1) Then
		{
			_z = _z - 0.005;
		}
		Else
		{
			_up = true;
		};
	};
	{
		_x setPosASL [getPosASL _x select 0, getPosASL _x select 1, _z];
	}
	forEach [_water2, _water3, _water4, _water5];

	If (CLAY_TopolkaDam_gate1) Then
	{
		drop ["\Ca\Data\cl_water", "", "Billboard", 1.3, 1.3, [10309.4,3620,5.5], [0,-10,0], 1, 100, 0.1, 0.1, [5,10], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", ""];
		drop ["\Ca\Data\cl_water", "", "Billboard", 1.3, 1.3, [10310.4,3620,5.5], [0,-10,0], 1, 100, 0.1, 0.1, [5,10], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", ""];
		drop ["\Ca\Data\cl_basic", "", "Billboard", 3, 3, [10309.4,3606.5,1], [0,0,0], 1, 0.25, 0.2, 0.001, [10,12], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", ""];
		drop ["\Ca\Data\cl_basic", "", "Billboard", 3, 3, [10310.4,3606.5,1], [0,0,0], 1, 0.25, 0.2, 0.001, [10,12], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", ""];
	};
	If (CLAY_TopolkaDam_gate2) Then
	{
		drop ["\Ca\Data\cl_water", "", "Billboard", 1.3, 1.3, [10314.5,3620,5.5], [0, -10, 0], 1, 100, 0.1, 0.1, [5, 10], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", ""];
		drop ["\Ca\Data\cl_water", "", "Billboard", 1.3, 1.3, [10315.5,3620,5.5], [0, -10, 0], 1, 100, 0.1, 0.1, [5, 10], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", ""];
		drop ["\Ca\Data\cl_water", "", "Billboard", 1.3, 1.3, [10316.5,3620,5.5], [0, -10, 0], 1, 100, 0.1, 0.1, [5, 10], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", ""];
		drop ["\Ca\Data\cl_basic", "", "Billboard", 3, 3, [10314.5,3606.5,1], [0,0,0], 1, 0.25, 0.2, 0.001, [10,12], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", ""];
		drop ["\Ca\Data\cl_basic", "", "Billboard", 3, 3, [10315.5,3606.5,1], [0,0,0], 1, 0.25, 0.2, 0.001, [10,12], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", ""];
		drop ["\Ca\Data\cl_basic", "", "Billboard", 3, 3, [10316.5,3606.5,1], [0,0,0], 1, 0.25, 0.2, 0.001, [10,12], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", ""];
	};
	If (CLAY_TopolkaDam_gate3) Then
	{
		drop ["\Ca\Data\cl_water", "", "Billboard", 1.3, 1.3, [10320.9,3620,5.5], [0, -10, 0], 1, 100, 0.1, 0.1, [5, 10], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", ""];
		drop ["\Ca\Data\cl_water", "", "Billboard", 1.3, 1.3, [10321.9,3620,5.5], [0, -10, 0], 1, 100, 0.1, 0.1, [5, 10], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", ""];
		drop ["\Ca\Data\cl_basic", "", "Billboard", 3, 3, [10320.9,3606.5,1], [0,0,0], 1, 0.25, 0.2, 0.001, [10,12], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", ""];
		drop ["\Ca\Data\cl_basic", "", "Billboard", 3, 3, [10321.9,3606.5,1], [0,0,0], 1, 0.25, 0.2, 0.001, [10,12], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", ""];
	};

	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-6, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-4, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-3, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-2, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-1, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [0, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [1, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [2, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [3, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [4, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [2, (3 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];

	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-6, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-5.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-4.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-4, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-3.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-3, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-2.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-2, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-1.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-1, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [-0.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [0, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [0.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [1, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [1.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [2, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [2.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [3, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [3.5, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];
	drop ["\Ca\Data\cl_water", "", "Billboard", 1, 1, [4, -(37.5 + random 1), -0.35], [0, -(5 + random 3), 0], 1, 100, 0.1, 0.1, [1, (1 + random 2)], [[1,1,1,0.9]], [0], 0.1, 0.01, "", "", _water1];

	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [-7, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [-6, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [-5, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [-4, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [-3, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [-2, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [-1, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [0, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [1, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [2, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [3, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [4, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];
	drop ["\Ca\Data\cl_basic", "", "Billboard", 2, 2, [5, -42.5, -1.35], [0, -1, 0], 1, 0.35, 0.2, 0.001, [2, 5], [[1,1,1,0.8],[1,1,1,0.4],[1,1,1,0.2],[1,1,1,0.2],[1,1,1,0.1],[1,1,1,0.1],[1,1,1,0]], [0], 0.1, 0.8, "", "", _water1];

	sleep 0.1;
};


deleteVehicle _water1;
deleteVehicle _water2;
deleteVehicle _water3;
deleteVehicle _water4;
deleteVehicle _water5;
deleteVehicle _rock1;
deleteVehicle _rock2;
deleteVehicle _rock3;
deleteVehicle _rock4;
deleteVehicle _rock5;
deleteVehicle _rock6;
deleteVehicle _rock7;
deleteVehicle _rock8;
deleteVehicle _rock9;
deleteVehicle _boulder1;
deleteVehicle _boulder2;
deleteVehicle _boulder3;
deleteVehicle _boulder4;
deleteVehicle _boulder5;
deleteVehicle _soundSource;
deleteVehicle CLAY_TopolkaDam_light1;
deleteVehicle CLAY_TopolkaDam_light2;
deleteVehicle CLAY_TopolkaDam_light3;
