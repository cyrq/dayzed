Flies_array = [];
Crows_array = [];
BodiesC_array = [];
BodiesF_array = [];
RYD_CFActive = true;
if (isNil "RYD_CF_Clear") then {RYD_CF_Clear = false};

RYD_CF_RandomAroundMM = 
	{//based on Muzzleflash' function
	private ["_pos","_xPos","_yPos","_a","_b","_dir","_angle","_mag","_nX","_nY","_temp","_level"];

	_pos = _this select 0;
	_a = _this select 1;
	_b = _this select 2;
	_level = _this select 3;

	_xPos = _pos select 0;
	_yPos = _pos select 1;

	_dir = random 360;

	_mag = _a + (sqrt ((random _b) * _b));
	_nX = _mag * (sin _dir);
	_nY = _mag * (cos _dir);

	_pos = [_xPos + _nX, _yPos + _nY,_level];  

	_pos	
	};

RYD_Crows = 
	{
	//scriptName "fn_crows.sqf";
	/*
		Author: Karel Moricky

		Modification: accepting only objects as position reference, that allows deleting crows when body is gone: Rydygier, 27-07-2012
		Modification: another spawning pos calculation way: Rydygier, 15-08-2013

		Description:
		Spawns flock of crows circling over the body (object).

		Parameter(s):
		_this select 0: object (body)
		_this select 1 (Optional): NUMBER - area size
		_this select 2 (Optional): NUMBER - number of crows

		Returns:
		ARRAY - list of all spawned crows
	*/
	private ["_body","_flockPos","_flockArea","_flockCount","_flockHeight","_wp0","_wp1","_wp2","_wp3","_wps","_crowList","_crow","_spawnPos"];

	_body = _this select 0;
	_flockArea = if (count _this > 1) then {_this select 1} else {50};
	_flockCount = if (count _this > 2) then {_this select 2} else {_flockArea / 5};
	_flockHeight = if (count _this > 3) then {_this select 3} else {30 + random 10};

	_flockPos = getPosATL _body;

	_flockPos set [2,_flockHeight];
	_wp0 = [_flockPos, _flockArea, 00] call BIS_fnc_relPos;
	_wp1 = [_flockPos, _flockArea, 090] call BIS_fnc_relPos;
	_wp2 = [_flockPos, _flockArea, 180] call BIS_fnc_relPos;
	_wp3 = [_flockPos, _flockArea, 270] call BIS_fnc_relPos;
	_wps = [_wp0,_wp1,_wp2,_wp3];


	_crowList = [];
	for "_i" from 1 to _flockCount do 
		{
		_spawnPos = [_flockPos,800,1500,_flockHeight] call RYD_CF_RandomAroundMM;

		_crow = "crow" camCreate _spawnPos;
			
		[_crow,_wp0,_wp1,_wp2,_wp3,_flockArea,_body] execFSM "\z\addons\dayz_code\compile\fn_crows.fsm";
		_crowList set [(count _crowList),_crow];
		};

	_crowList;
	};

RYD_Carrion = 
	{
	sleep 5;
	
	while {(RYD_CFActive)} do
		{

			{
			_body = _x;
			
			if ((_body isKindOf "Man") and !(_body isKindOf "zZombie_Base") and !(_body isKindOf "Animal") and !(alive _body)) then 
				{
				_isCrows = _body getVariable ["RYD_isCrows",false];
				_DT = _body getVariable ["RYD_DT",-1501 - (random 250)];
				if (({(_body in _x)} count BodiesF_array) > 4) then
					{
					_DT = time
					};
					
				if not (_isCrows) then
					{
					if (({(_x distance _body) < 100} count (Crows_array - [_body])) == 0) then
						{
						_body setVariable ["RYD_isCrows",true];
						Crows_array set [(count Crows_array),_body];
												
						_crows = [_body,25 + (random 15),8 + (round (random 8)),15 + (random 20)] call RYD_Crows;
						BodiesC_array set [(count BodiesC_array),[_body,_crows]];
						}
					};
					
				if ((time - _DT) > 1800) then
					{
					if (({(_x distance _body) < 10} count (Flies_array - [_body])) == 0) then
						{
						_body setVariable ["RYD_DT",time];
						Flies_array set [(count Flies_array),_body];
						
						_pos = getPosATL _body;
						
						_ps1 = (_body nearObjects ["#particlesource",0.1]) + (_body nearObjects ["#dynamicsound",0.1]);
						[_pos,0.1,1 + (random 1)] call bis_fnc_flies;
						_ps2 = (_body nearObjects ["#particlesource",0.1]) + (_body nearObjects ["#dynamicsound",0.1]);

						_flies = _ps2 - _ps1;
						
						BodiesF_array set [(count BodiesF_array),[_body,_flies]];
						}
					}
				}
			}
		foreach AllDead;
		
			{
			_body = _x select 0;
			if (isNull _body) then
				{
				+(_x select 1) spawn
					{
						{
						deleteVehicle _x; 
						sleep (3 + (random 3))
						} 
					foreach _this
					};
					
				BodiesF_array set [_foreachIndex, "Del"];
				}
			}
		foreach BodiesF_array;
		
		Flies_array = Flies_array - [objNull];
		BodiesF_array = BodiesF_array - ["Del"];
		
			{
			_body = _x select 0;
			if (isNull _body) then
				{
				_crows = _x select 1;

				_posC = getPosASL (_crows select 0);
					
				_endPos = [_posC,50000,150000,((_posC select 2) + ((random 10) - 5))] call RYD_CF_RandomAroundMM;

					{
					[_x,_endPos,_posC] spawn
						{
						private ["_crow","_endPos","_pos","_dis"];

						_crow = _this select 0;	
						_endPos = _this select 1;
						_pos = _this select 2;	


						sleep (3 + (random 3));

						_dis = _crow distance _endPos;
						_crow camSetPos _endPos;
						_crow camCommit (_dis * 1.5);

						waitUntil 
							{
							sleep 1;
							_dis = 1001;
							if not (isNull _crow) then
								{
								_dis = _crow distance _pos
								};
								
							(_dis > 1000)
							};

						deleteVehicle _crow
						}
					}
				foreach _crows;
					
				BodiesC_array set [_foreachIndex, "Del"];
				}
			}
		foreach BodiesC_array;
		
		Crows_array = Crows_array - [objNull];
		BodiesC_array = BodiesC_array - ["Del"];
		
		sleep 15
		};
		
	if (RYD_CF_Clear) then
		{
	
			{
				{
				deleteVehicle _x
				}
			foreach (_x select 1)
			}
		foreach (BodiesF_array + BodiesC_array);
		
		Flies_array = nil;
		Crows_array = nil;
		BodiesC_array = nil;
		BodiesF_array = nil
		};
	};