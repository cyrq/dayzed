/*
player_recieveMessage.sqf
Author: cyrq (cyrq1337@gmail.com).
Made for DayZed: http://dayzed.eu
 
You can use, modify and distribute this without any permissions.
Give credit where credit is due.
 
Description: Gets messages from other players that have a radio using publicVariable.
The messages are send by player_sendMessage.sqf
*/

private ["_player","_msg","_sender","_hasRadio"];

_msg = _this select 1;
_hasRadio = "ItemRadio" in items player;

if (_hasRadio) then {
	systemChat format ["Radio Transmission: %1", (toString(_msg))];
};