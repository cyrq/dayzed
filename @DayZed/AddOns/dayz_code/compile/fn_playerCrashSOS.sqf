//Name of the location
fa_coor2str = {
	private["_pos","_res","_nearestCity","_town"];

	_pos = +(_this);
	if (count _pos < 1) then { _pos = [0,0]; }
	else { if (count _pos < 2) then { _pos = [_pos select 0,0]; };
	};
	_nearestCity = nearestLocations [_pos, ["NameCityCapital","NameCity","NameVillage","NameLocal","Hill","NameMarine"],1750];
	_town = "Wilderness";
	if (count _nearestCity > 0) then {_town = text (_nearestCity select 0)};
	_res = format["%1", _town, round((_pos select 0)/100), round((15360-(_pos select 1))/100)];

	_res
};

heliData = _this select 0;
heliPos = heliData select 0;
heliModel = heliData select 1;
heliSOS = heliData select 2;
sosDistance = heliData select 3;

_nameHeliModel = getText(configFile >> "cfgVehicles" >> heliModel >> "displayName");

if(heliSOS && ("ItemRadio" in items player) && (player distance heliPos <= sosDistance)) then {
		[player,"sos",0,false] call dayz_zombieSpeak;
		[player,10,false,(getPosATL player)] spawn player_alertZombies;
		systemChat format["Radio Transmission: Mayday! Our %2 is going down! Last known position: %1. We... (transmission cut off)",(heliPos call fa_coor2str), _nameHeliModel];
        sleep 15;
};