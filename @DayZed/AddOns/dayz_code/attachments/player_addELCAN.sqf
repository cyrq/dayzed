//Weapon attachments by cyrq

private ["_hasToolbox","_hasAttachment","_wpn","_wpnName","_attachName","_vehicle","_inVehicle","_onLadder"];

_hasToolbox = "ItemToolbox" in items player; //check for toolbox
_hasAttachment = "ItemELCANSight" in magazines player; //check for attachment
_wpn = primaryWeapon player; //what wpn we have on us
_wpnName = getText (configFile >> "CfgWeapons" >> _wpn >> "displayName"); //name of the wpn
_attachName = getText (configFile >> "CfgMagazines" >> "ItemELCANSight" >> "displayName"); //name of the attach
_vehicle = vehicle player;
_inVehicle = (_vehicle != player);
_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;

if (!_hasToolbox) exitWith //doesn't have toolbox so exit script
{
closeDialog 1; //close gear
cutText ["You need a Toolbox to mount the attachments.", "PLAIN DOWN"]; //text
};

if (!(_wpn in AvailableWeaponsELCAN)) exitWith //doesn't have available wpn so exit script -- ^^^CASE SENSITIVE!!!^^^
{
closeDialog 1; //close gear
cutText [format["You can't mount %1 on a %2.",_attachName,_wpnName], "PLAIN DOWN"]; //text
};

if (_inVehicle) exitWith //in vehicle
{
closeDialog 1; //close gear
cutText ["You can't mount the attachments when inside a vehicle.", "PLAIN DOWN"]; //text
};

if (_onLadder) exitWith //on ladder
{
closeDialog 1; //close gear
cutText ["You can't mount the attachments when climbing a ladder.", "PLAIN DOWN"]; //text
};

if ((_hasToolbox) && (_hasAttachment)) then { //has all req - execute loop

player playActionNow "Medic"; //play animation

closeDialog 1; //close gear

cutText [format["Adding %1 to %2...",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen

//SoundFx
sleep 1;
_dis=20;
_sfx = "attachment";
[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
[player,_dis,true,(getPosATL player)] call player_alertZombies;

//Anim loop start

r_interrupt = false;
_animState = animationState player;
r_doLoop = true;
_started = false;
_finished = false;

while {r_doLoop} do {
_animState = animationState player;
_isMedic = ["medic",_animState] call fnc_inString;
if (_isMedic) then {
_started = true;
};
if (_started and !_isMedic) then {
r_doLoop = false;
_finished = true;
};
if (r_interrupt) then {
r_doLoop = false;
};
sleep 0.1;
};
r_doLoop = false;
if (_finished) then {

//Anim loop end

[player,_wpn] call BIS_fnc_invRemove; //remove current weapon

player removeMagazine "ItemELCANSight"; //remove attachment from inv

//Weapon attachment options

Switch true do {

//---------------------------------------------M4PACK----------------------------------------

case (_wpn == "RH_M249") : {
[player,"RH_M249elcan"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully attached %1 to your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_Mk48mod1") : {
[player,"RH_Mk48mod1elcan"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully attached %1 to your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

default {}; //don't touch!
};

//Automatic muzzle select for weapons with GP, M203 launchers etc.
if ( (primaryWeapon player) != "") then {
_type = primaryWeapon player;
_muzzles = getArray(configFile >> "cfgWeapons" >> _type >> "muzzles");
if ((_muzzles select 0) != "this") then {
player selectWeapon (_muzzles select 0);
} else {
player selectWeapon _type;
	};
};

//finished but...

} else {

//...player moved

r_interrupt = false;
[objNull, player, rSwitchMove,""] call RE; //cancel animation
player playActionNow "stop";
cutText [format["You failed to attach %1 to your %2. Stand Still!",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};
};//done