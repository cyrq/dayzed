//Weapon attachments (remove) by cyrq

private ["_hasToolbox","_freeSlots","_freeSlotsCount","_wpn","_wpnName","_attachName","_vehicle","_inVehicle","_onLadder"];

_hasToolbox = "ItemToolbox" in items player; //check for toolbox
_freeSlots = [player] call BIS_fnc_invSlotsEmpty; //check for empty inv slots
_freeSlotsCount = _freeSlots select 4; //count slots
_wpn = primaryWeapon player; //what wpn we have on us
_wpnName = getText (configFile >> "CfgWeapons" >> _wpn >> "displayName"); //name of the wpn
_attachName = getText (configFile >> "CfgMagazines" >> "ItemCCOSight" >> "displayName"); //name of the attach
_vehicle = vehicle player;
_inVehicle = (_vehicle != player);
_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;

if (!_hasToolbox) exitWith //doesn't have toolbox so exit script
{
closeDialog 1; //close gear
cutText ["You need a Toolbox to remove the attachments.", "PLAIN DOWN"]; //text
};

if (!(_wpn in WeaponsCCO)) exitWith //doesn't have available wpn in hand so exit script -- ^^^CASE SENSITIVE!!!^^^
{
closeDialog 1; //close gear
cutText [format["You don't have any weapon in hands, from which you can remove the %1.",_attachName], "PLAIN DOWN"]; //text
};

if (_freeSlotsCount < 1) exitWith //doesn't have enough slots
{
closeDialog 1; //close gear
cutText ["You don't have enough room in your inventory", "PLAIN DOWN"]; //text
};

if (_inVehicle) exitWith //in vehicle
{
closeDialog 1; //close gear
cutText ["You can't remove the attachments when inside a vehicle.", "PLAIN DOWN"]; //text
};

if (_onLadder) exitWith //on ladder
{
closeDialog 1; //close gear
cutText ["You can't remove the attachments when climbing a ladder.", "PLAIN DOWN"]; //text
};

if (_hasToolbox) then { //has all req - execute loop

player playActionNow "Medic"; //play animation

closeDialog 1; //close gear

cutText [format["Removing %1 from %2...",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen

//SoundFx
sleep 1;
_dis=20;
_sfx = "attachment";
[player,_sfx,0,false,_dis] call dayz_zombieSpeak;
[player,_dis,true,(getPosATL player)] call player_alertZombies;

//Anim loop start

r_interrupt = false;
_animState = animationState player;
r_doLoop = true;
_started = false;
_finished = false;

while {r_doLoop} do {
_animState = animationState player;
_isMedic = ["medic",_animState] call fnc_inString;
if (_isMedic) then {
_started = true;
};
if (_started and !_isMedic) then {
r_doLoop = false;
_finished = true;
};
if (r_interrupt) then {
r_doLoop = false;
};
sleep 0.1;
};
r_doLoop = false;
if (_finished) then {

//Anim loop end

[player,_wpn] call BIS_fnc_invRemove; //remove current weapon

player addMagazine "ItemCCOSight"; //add attachment to inv

//Weapon attachment options

Switch true do {

//---------------------------------------------M14PACK----------------------------------------

case (_wpn == "RH_m14aim") : {
[player,"RH_m14"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

//---------------------------------------------M4PACK----------------------------------------

case (_wpn == "RH_M4a1aim") : {
[player,"RH_M4a1"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_M4sbraim") : {
[player,"RH_M4sbr"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_M4sdaim") : {
[player,"RH_M4sd"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_M4a1glaim") : { 
[player,"RH_M4a1gl"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_M4sdglaim") : {
[player,"RH_M4sdgl"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_M16A4aim") : {
[player,"RH_M16a4"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_M16A4glaim") : {
[player,"RH_M16a4gl"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

//---------------------------------------------HK416PACK----------------------------------------

case (_wpn == "RH_hk416aim") : {
[player,"RH_hk416"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_hk416sdaim") : {
[player,"RH_hk416sd"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

/*case (_wpn == "RH_hk416glaim") : { // different grenade launcher types
[player,"RH_hk416gl"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_hk416sdglaim") : {
[player,"RH_hk416sdgl"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};*/

case (_wpn == "RH_hk416saim") : {
[player,"RH_hk416s"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_hk416sglaim") : {
[player,"RH_hk416sgl"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_acrbaim") : {
[player,"RH_acrb"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

//---------------------------------------------AKSPACK----------------------------------------

case (_wpn == "RH_Rk95aim") : {
[player,"RH_rk95"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_Rk95sdaim") : {
[player,"RH_Rk95sd"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

//---------------------------------------------SMGPACK----------------------------------------

case (_wpn == "RH_mp5a5aim") : {
[player,"RH_mp5a5"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_mp5sd6aim") : {
[player,"RH_mp5sd6"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_umpaim") : {
[player,"RH_UMP"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_umpsdaim") : {
[player,"RH_umpsd"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_P90aim") : {
[player,"RH_P90"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "RH_p90sdaim") : {
[player,"RH_P90sd"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

//---------------------------------------------FAMASPACK----------------------------------------

case (_wpn == "C1987_Famas_f1_aim") : {
[player,"C1987_Famas_f1"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

case (_wpn == "C1987_Famas_f1_aim_sd") : {
[player,"C1987_Famas_f1_sd"] call BIS_fnc_invAdd; //add new wpn
cutText [format["You have successfully removed %1 from your %2.",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};

default {}; //don't touch!
};

//Automatic muzzle select for weapons with GP, M203 launchers etc.
if ( (primaryWeapon player) != "") then {
_type = primaryWeapon player;
_muzzles = getArray(configFile >> "cfgWeapons" >> _type >> "muzzles");
if ((_muzzles select 0) != "this") then {
player selectWeapon (_muzzles select 0);
} else {
player selectWeapon _type;
	};
};

//finished but...

} else {

//...player moved

r_interrupt = false;
[objNull, player, rSwitchMove,""] call RE; //cancel animation
player playActionNow "stop";
cutText [format["You failed to remove %1 from your %2. Stand Still!",_attachName,_wpnName],"PLAIN DOWN"]; //info on screen
};
};//done