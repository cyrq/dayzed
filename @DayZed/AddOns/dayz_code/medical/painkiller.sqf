private ["_id","_unit","_dis","_sfx"];
_unit = (_this select 3) select 0;
_pain = _unit getVariable ["USEC_inPain", false];

closeDialog 0;

if !(_pain) exitWith {
	switch true do {
		case ((_unit == player) or (vehicle player != player)) : { 
			cutText ["You're not in pain.\nThere's no need to use a Painkiller.", "PLAIN DOWN"];
		};
	default {cutText ["The person is not in pain.\nThere's no need to use a Painkiller.", "PLAIN DOWN"];}
	};
};

_unit setVariable ["USEC_inPain", false, true];

call fnc_usec_medic_removeActions;
r_action = false;

if (vehicle player == player) then {
	//not in a vehicle
	player playActionNow "Gear";
};
[player,"painkiller",0,false] call dayz_zombieSpeak;

if ((_unit == player) or (vehicle player != player)) then {
	//Self Healing
	_id = [player,player] execVM "\z\addons\dayz_code\medical\publicEH\medPainkiller.sqf";
	cutText ["You've swallowed the Painkillers.\nYou're no longer in pain.", "PLAIN DOWN"];
} else {
	//Another Player
	[player,20] call player_humanityChange;
	cutText ["The person swallowed the Painkillers.\nHe's no longer in pain.", "PLAIN DOWN"];
	[player,_unit,"loc",rTITLETEXT, "You've swallowed the Painkillers.\nYou're no longer in pain.","PLAIN DOWN"] call RE;
};

player removeMagazine "ItemPainkiller";

sleep 1;

PVDZ_send = [_unit,"Painkiller",[_unit,player]];
publicVariableServer "PVDZ_send";