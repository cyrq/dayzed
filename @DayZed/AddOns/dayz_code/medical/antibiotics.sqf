private ["_id","_hasMeds","_unit"];
_hasMeds = "ItemAntibiotic" in magazines player;
_unit = (_this select 3) select 0;
_infected = _unit getVariable ["USEC_infected", false];

closeDialog 0;

if !(_infected) exitWith {
	switch true do {
		case ((_unit == player) or (vehicle player != player)) : { 
			cutText ["You're not sick.\nThere's no need to use Antibiotics.", "PLAIN DOWN"];
		};
	default {cutText ["The person is not sick.\nThere's no need to use Antibiotics.", "PLAIN DOWN"];}
	};
};

//remove infection
r_player_infected = false;
_unit setVariable["USEC_infected",false,true];

//remove option
call fnc_usec_medic_removeActions;
r_action = false;

[player,"painkiller",0,false] call dayz_zombieSpeak;

if ((_unit == player) or (vehicle player != player)) then {
	//Self Healing
	_id = [player,player] execVM "\z\addons\dayz_code\medical\publicEH\medAntibiotics.sqf";
	cutText ["You've swallowed the Antibiotics.\nYou're no longer sick.", "PLAIN DOWN"];
} else {
	//Another Player
	PVDZ_send = [_unit,"Antibiotics",[_unit,player]];
	publicVariableServer "PVDZ_send";
	[player,20] call player_humanityChange;
	cutText ["The person swallowed the Antibiotics.\nHe's no longer sick.", "PLAIN DOWN"];
	[player,_unit,"loc",rTITLETEXT, "You've swallowed the Antibiotics.\nYou're no longer sick.","PLAIN DOWN"] call RE;
};

player removeMagazine "ItemAntibiotic";