//medkit by cyrq

private ["_timeMedKit","_time","_healTime","_bloodAmount","_injured","_inPain","_display","_ctrlBlood","_ctrlPain"];

disableSerialization;

_onLadder = (getNumber (configFile >> "CfgMovesMaleSdr" >> "States" >> (animationState player) >> "onLadder")) == 1;
if (_onLadder) exitWith {cutText [(localize "str_player_21") , "PLAIN DOWN"]};

//Force players to wait 5 mins
_timeMedKit = time - dayz_lastMedkit;
if (_timeMedKit < 300) exitWith {cutText ["You've already used the First Aid Kit. A second one wonn't help.", "PLAIN DOWN"]};

_time = time;
_healTime = 16;

_bloodAmount = 3000;

_injured = player getVariable ["USEC_injured", false];
_inPain = player getVariable ["USEC_inPain", false];

player playActionNow "Medic";
closeDialog 1;
[player,"bandage",0,false] call dayz_zombieSpeak;
cutText [format["Using First Aid Kit. Stand Still.."], "PLAIN DOWN"];

r_interrupt = false;
_animState = animationState player;
r_doLoop = true;
_started = false;
_finished = false;
	
	while {r_doLoop} do {
		_animState = animationState player;
		_isMedic = ["medic",_animState] call fnc_inString;
			if (_isMedic) then {
				_started = true;
				};
					if(!_isMedic && !r_interrupt && (time - _time) < _healTime) then {
						player playActionNow "Medic";
							_isMedic = true;
						};
							if (_started && !_isMedic && (time - _time) > _healTime) then {
								r_doLoop = false;
									_finished = true;
								};
			if (r_interrupt) then {
				r_doLoop = false;
				};
		sleep 0.1;
	};
r_doLoop = false;

if (_finished) then {
	player removeMagazine "ItemMedKit";
		r_player_blood = r_player_blood + _bloodAmount;		
		if(r_player_blood > 12000) then {
			r_player_blood = 12000;
		};
		r_player_lowblood = false;
		player setVariable["USEC_BloodQty",r_player_blood,true];
			
		if (_injured) then {
			r_player_injured = false;
			r_player_handler = false;
			dayz_sourceBleeding = objNull;
			call fnc_usec_resetWoundPoints;
		};
		
		if (_inPain) then {
			player setVariable ["USEC_inPain", false, true];
			r_player_inpain = false;
		};
		
		player setVariable ["medForceUpdate",true];
		
		//Ensure Controls are visible
		_display = uiNamespace getVariable 'DAYZ_GUI_display';
		_ctrlBlood = _display displayCtrl 1300;
		_ctrlPain = _display displayCtrl 1303;
		_ctrlBlood ctrlShow true;
		_ctrlPain ctrlShow false;
		
		cutText [format["You have successfully used the First Aid Kit."], "PLAIN DOWN"];
		
		dayz_lastMedkit = time;
} else {
		r_interrupt = false;
		[objNull, player, rSwitchMove,""] call RE;
		player playActionNow "stop";
			cutText ["You moved. Stand still!", "PLAIN DOWN"];
	};