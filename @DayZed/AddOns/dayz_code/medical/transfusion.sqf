private ["_unit","_blood","_lowBlood","_injured","_inPain","_lastused","_text","_animState","_started","_finished","_timer","_isMedic","_isClose"];
// bleed.sqf
_unit = (_this select 3) select 0;
_blood = _unit getVariable ["USEC_BloodQty", 0];
_lowBlood = _unit getVariable ["USEC_lowBlood", false];
_injured = _unit getVariable ["USEC_injured", false];
_inPain = _unit getVariable ["USEC_inPain", false];
_lastused = _unit getVariable ["LastTransfusion", time];

//if (_lastused - time < 600) exitwith {cutText [format[(localize "str_actions_medical_18"),_text] , "PLAIN DOWN"]};

call fnc_usec_medic_removeActions;
r_action = false;

if (vehicle player == player) then {
	//not in a vehicle
	player playActionNow "Medic";
	diag_log format ["TRANSFUSION: starting blood transfusion (%1 > %2)", name player, name _unit];
	player removeMagazine "ItemBloodbag";
	cutText [localize "str_actions_medical_transfusion_start", "PLAIN DOWN"];
	[player,_unit,"loc",rTITLETEXT,localize "str_actions_medical_transfusion_start","PLAIN DOWN"] call RE;
};

r_interrupt = false;
_animState = animationState player;
r_doLoop = true;
_started = false;
_finished = false;

while {r_doLoop} do {
	_animState = animationState player;
	_isMedic = ["medic",_animState] call fnc_inString;

	if (_isMedic) then {
		_started = true;
	};
	
	if (_started and !_isMedic) then {
		r_doLoop = false;
		_finished = true;
	};
	
	_isClose = ((player distance _unit) < ((sizeOf typeOf _unit) / 2));

	if (r_interrupt or !_isClose) then {
			r_doLoop = false;
	};
	sleep 0.1;
};
r_doLoop = false;

	if (_finished) then {
			diag_log format ["TRANSFUSION: completed blood transfusion successfully.", _i];
			cutText [localize "str_actions_medical_transfusion_successful", "PLAIN DOWN"];
			[player,_unit,"loc",rTITLETEXT,localize "str_actions_medical_transfusion_successful","PLAIN DOWN"] call RE;
			_unit setVariable["LastTransfusion",time,true];
			PVDZ_send = [_unit,"Transfuse",[_unit,player,12000]];	
			publicVariableServer "PVDZ_send";
			[player,250] call player_humanityChange;		
	} else {
			r_interrupt = false;
			player switchMove "";
			player playActionNow "stop";
			diag_log format ["TRANSFUSION: transfusion was interrupted (r_interrupt: %1 | distance: %2 (%3)", r_interrupt, player distance _unit, _isClose, _i];
			cutText [localize "str_actions_medical_transfusion_interrupted", "PLAIN DOWN"];
			[player,_unit,"loc",rTITLETEXT,localize "str_actions_medical_transfusion_interrupted","PLAIN DOWN"] call RE;
};
