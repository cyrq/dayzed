class CfgVehicles {
	class All;
	class AllVehicles;
	class Land;
	class LandVehicle;
	class Motorcycle;
	class Old_moto_base; 
	class TT650_Base;
	#include "CfgVehicles\Land_Vehicles_Civilian\Motorcycles.hpp"
	class Bicycle;
	class Old_bike_base_EP1;
	class MMT_base;
	#include "CfgVehicles\Land_Vehicles_Civilian\Bikes.hpp"
	class Car;
	#include "CfgVehicles\Land_Vehicles_Civilian\Hilux.hpp"
	#include "CfgVehicles\Land_Vehicles_Civilian\VW.hpp"
	#include "CfgVehicles\Land_Vehicles_Civilian\Tractors.hpp"
	#include "CfgVehicles\Land_Vehicles_Civilian\Ikarus.hpp"
	#include "CfgVehicles\Land_Vehicles_Civilian\Skoda_Van.hpp"
	class Offroad_DSHKM_base;
	class Offroad_DSHKM_Gue;
	#include "CfgVehicles\Land_Vehicles_Armored\Hilux_armored.hpp"
	class ATV_Base_EP1;
	#include "CfgVehicles\Land_Vehicles_Military\Atv.hpp"
	class SkodaBase;
	#include "CfgVehicles\Land_Vehicles_Civilian\Skoda.hpp"
	#include "CfgVehicles\Land_Vehicles_Civilian\Sedan.hpp"
	#include "CfgVehicles\Land_Vehicles_Civilian\Hatchback.hpp"
	class Lada_base;
	#include "CfgVehicles\Land_Vehicles_Civilian\Lada.hpp"
	class Volha_TK_CIV_Base_EP1;
	#include "CfgVehicles\Land_Vehicles_Civilian\Volga.hpp"
	class SUV_Base_EP1;
	#include "CfgVehicles\Land_Vehicles_Civilian\Suv.hpp"
	class UAZ_Base;
	class UAZ_MG_Base;
	class UAZ_MG_CDF;
	#include "CfgVehicles\Land_Vehicles_Armored\Uaz_armored.hpp"
	class UAZ_Unarmed_Base;
	#include "CfgVehicles\Land_Vehicles_Military\Uaz.hpp"
	class LandRover_Base;
	class LandRover_CZ_EP1;
	class BAF_Offroad_D;
	#include "CfgVehicles\Land_Vehicles_Military\Landrover.hpp"
	class HMMWV_Base;
	#include "CfgVehicles\Land_Vehicles_Military\Hmmwv.hpp"
	class BTR40_base_EP1;
	#include "CfgVehicles\Land_Vehicles_Military\Btr.hpp"
	class Ural_Base;
	class UralOpen_Base;
	#include "CfgVehicles\Land_Vehicles_Trucks\Ural.hpp"
	class V3S_Base;	
	#include "CfgVehicles\Land_Vehicles_Trucks\V3S.hpp"
	class Kamaz_Base;
	class Kamaz;
	#include "CfgVehicles\Land_Vehicles_Trucks\Kamaz.hpp"
	class Air : AllVehicles {
		class NewTurret;
		class ViewPilot;
		class AnimationSources;
	};
	class Helicopter : Air {
		class HitPoints;
		class Turrets {
			class MainTurret : NewTurret {
				class Turrets;
				class ViewOptics;
			};
        };
	};
	#include "CfgVehicles\Air_Vehicles\MI17.hpp"
	#include "CfgVehicles\Air_Vehicles\UH1H.hpp"
	class AH6_Base_EP1;
	#include "CfgVehicles\Air_Vehicles\MH6J_DZ.hpp"
	#include "CfgVehicles\Air_Vehicles\Chinook_CH47F.hpp"
	class Plane;
	class An2_Base_EP1;
	#include "CfgVehicles\Air_Vehicles\AN2_DZ.hpp"
	class Ship;
	#include "CfgVehicles\Water_Vehicles\FishingBoat.hpp"
	#include "CfgVehicles\Water_Vehicles\SmallBoat.hpp"
	class Boat;
	class Rubberboat;
	#include "CfgVehicles\Water_Vehicles\Pbx.hpp"
	class House {
		class DestructionEffects;
	};
	class SpawnableWreck : House {};
	class Chinook_CH47FWreck_DZed : SpawnableWreck {
		model="\ca\air_e\CH47\CH_47FWreck.p3d";
		mapSize=20;
		heightAdjustment = 1;
		class AnimationSources {};
	};
	class Mi8Wreck_DZ : SpawnableWreck {
		model = "\Ca\air\MI8Wreck.p3d";
		icon = "ca\Misc_E\data\Icons\Icon_uh60_wreck_CA";
		mapSize = 15;
		displayName = $STR_VEH_NAME_MI8_WRECK;
		vehicleClass = "Wrecks";
		heightAdjustment = 1;
		class AnimationSources {};
	};
	class MV22Wreck_DZed : SpawnableWreck {
		model="\ca\air2\MV22\MV22Wreck.p3d";
		heightAdjustment = -0.50;
		class AnimationSources {};
	};
	class UH1Wreck_DZ : SpawnableWreck {
		model = "\ca\air2\UH1Y\UH1Y_Crashed.p3d";
		icon = "\ca\air2\data\UI\icon_UH1Y_CA.paa";
		mapSize = 15;
		displayName = $STR_VEH_NAME_UH1Y_WRECK;
		vehicleClass = "Wrecks";
		class AnimationSources {};
	};
	class UH60Wreck_DZ: SpawnableWreck {
		model = "\Ca\Misc_E\Wreck_UH60_EP1.p3d";
		icon = "ca\Misc_E\data\Icons\Icon_uh60_wreck_CA";
		mapSize = 15;
		displayName = $STR_VEH_NAME_UH60_WRECK;
		vehicleClass = "Wrecks";
		class AnimationSources {};
	};
	
	//OLD CONFIG:
	//class Bag_Base_EP1;
	//class Bag_Base_BAF;
	class Strategic;
	class NonStrategic;
	class Land_Fire;
	class Animal;
	class Pastor;

	#include "CfgVehicles\RepairParts.hpp" //names for all reapir parts. Needs moving to hitpoints
	//ZEDS
	#include "CfgVehicles\Zeds\Zeds.hpp" //old type zeds
	#include "CfgVehicles\Zeds\ViralZeds.hpp" //Viral type zeds
	#include "CfgVehicles\Zeds\WildZeds.hpp" //Viral type zeds
	//Survivor Skins
	#include "CfgVehicles\Skins.hpp"
	//Bags
	#include "CfgVehicles\Bags.hpp"
	//DZAnimal and DZ_Fin
	#include "CfgVehicles\Animal.hpp"
	//Blood
	#include "CfgVehicles\Blood.hpp"

	//Includes all Building Stuff
	// This parent class is made to make referring to these objects easier later with allMissionObjects
	#include "CfgVehicles\Buildings\HouseDZ.hpp"
	//Fire
	#include "CfgVehicles\Buildings\Land_Fire.hpp"
	//Buildings
	#include "CfgVehicles\Buildings\Land_A_Crane_02b.hpp"
	#include "CfgVehicles\Buildings\Land_A_FuelStation_Feed.hpp"
	#include "CfgVehicles\Buildings\Land_A_TVTower_Mid.hpp"
	#include "CfgVehicles\Buildings\Land_A_TVTower_Top.hpp"
	#include "CfgVehicles\Buildings\Land_Farm_WTower.hpp"
	#include "CfgVehicles\Buildings\Land_HouseB_Tenement.hpp"
	#include "CfgVehicles\Buildings\Land_Ind_MalyKomin.hpp"
	#include "CfgVehicles\Buildings\Land_komin.hpp"
	#include "CfgVehicles\Buildings\Land_majak.hpp"
	#include "CfgVehicles\Buildings\Land_Mil_ControlTower.hpp"
	#include "CfgVehicles\Buildings\Land_NAV_Lighthouse.hpp"
	#include "CfgVehicles\Buildings\Land_NavigLight.hpp"
	#include "CfgVehicles\Buildings\Land_Rail_Semafor.hpp"
	#include "CfgVehicles\Buildings\Land_Rail_Zavora.hpp"
	#include "CfgVehicles\Buildings\Land_runway_edgelight.hpp"
	#include "CfgVehicles\Buildings\Land_Stoplight.hpp"
	#include "CfgVehicles\Buildings\Land_telek1.hpp"
	#include "CfgVehicles\Buildings\Land_VASICore.hpp"
	#include "CfgVehicles\Buildings\Land_Vysilac_FM.hpp"
	#include "CfgVehicles\Flashlight.hpp"

	//camo
	#include "CfgVehicles\CamoNetting.hpp"
	
	//WeaponHolder
	#include "CfgVehicles\WeaponHolder.hpp"

	//itemBox's
	#include "CfgVehicles\CardboardBox.hpp"

	//Tents,storage
	#include "CfgVehicles\Storage.hpp"
	
	// Traps
	#include "CfgVehicles\Traps.hpp"

	//Antihack
	class HouseBase;
	#include "CfgVehicles\antihack_logic.hpp"
	#include "CfgVehicles\antihack_plants.hpp"
	
	//WorldPlants
	class Building;
	class Plant_Base: Building {
		scope = 2;
		displayname = "Plant Sphere 100cm";
		favouritezones = "(meadow) * (forest) * (1 - houses) * (1 - sea)";
	}
	class Dayz_Plant1: Plant_Base {
		displayname = $STR_ITEM_NAME_comfrey;
		output = "equip_comfreyleafs";
		outamount = "1";
		favouritezones = "(meadow) * (forest) * (1 - houses) * (1 - sea)";
		model = "z\addons\dayz_communityassets\models\comfrey_up_mid.p3d";
	};
	class Dayz_Plant2: Plant_Base {
		displayname = $STR_ITEM_NAME_comfrey;
		output = "equip_comfreyleafs";
		outamount = "1";
		favouritezones = "(meadow) * (forest) * (1 - houses) * (1 - sea)";
		model = "z\addons\dayz_communityassets\models\comfrey_up_small.p3d";
	};
	class Dayz_Plant3: Plant_Base {
		displayname = $STR_ITEM_NAME_comfrey;
		output = "equip_comfreyleafs";
		outamount = "1";
		favouritezones = "(meadow) * (forest) * (1 - houses) * (1 - sea)";
		model = "z\addons\dayz_communityassets\models\comfrey_up.p3d";
	};
};
class CfgNonAIVehicles {
	#include "CfgVehicles\StreetLamps.hpp"
};
