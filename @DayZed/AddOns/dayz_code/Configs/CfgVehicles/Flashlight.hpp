class StaticSEARCHLight;
	class SearchLight: StaticSEARCHLight
	{
		class Reflectors;
	};
	class pzn_NoSearchLight: SearchLight
	{
		access = 2;
		scope = 2;
		model = "\dayz_equip\models\searchlight_manual.p3d";
		simulation = "flagcarrier";
		class Reflectors
		{
			class main_reflector
			{
				color[] = {0.8,0.8,0.9,1.0};
				ambient[] = {0.1,0.1,0.1,1.0};
				position = "light";
				direction = "lightEnd";
				hitpoint = "light";
				selection = "light";
				size = 0.5;
				angle = 40;
				brightness = 0.5;
			};
		};
	};
	class pzn_NoSearchLight_t: pzn_NoSearchLight
	{
		scope = 2;
		class Reflectors
		{
			class main_reflector
			{
				color[] = {0.9,0.0,0.0,0.9};
				ambient[] = {0.1,0.0,0.0,1};
				position = "light";
				direction = "lightEnd";
				hitpoint = "light";
				selection = "light";
				size = 0.5;
				angle = 30;
				brightness = 0.3;
			};
		};
	};