class Offroad_DSHKM_Gue_DZed : Offroad_DSHKM_Gue {
	displayName="Offroad-DShKM";
	displayNameShort="Offroad";
	scope=2;
	side=2;
	faction="";
	crew="";
	typicalCargo[]={};
	class TransportMagazines {};
	maxSpeed=125;
	terrainCoef=2.5;
	turnCoef=2;
	preferRoads=1;
	transportMaxMagazines=35;
	transportMaxWeapons=6;
	transportMaxBackpacks=3;
	gunnerOpticsShowCursor=0;
	hiddenSelectionsTextures[]={"\ca\wheeled\hilux_armed\data\coyota_trup4_CO.paa"};
	class Damage {
		tex[]={};
		mat[]= {
			"ca\wheeled\hilux_armed\data\detailmapy\coyota_armed.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\coyota_armed.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\coyota_armed_destruct.rvmat",
			"ca\weapons\data\dshk.rvmat",
			"ca\weapons\data\dshk.rvmat",
			"ca\weapons\data\dshk_destruct.rvmat",
			"ca\weapons\data\tripod_dshk.rvmat",
			"ca\weapons\data\tripod_dshk.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\tripod_dshk_destruct.rvmat",
			"ca\weapons\data\pkm.rvmat",
			"ca\weapons\data\pkm.rvmat",
			"ca\weapons\data\pkm_destruct.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\coyota_trup3.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\coyota_trup3.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\coyota_trup3_destruct.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\drziaky.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\drziaky.rvmat",
			"ca\wheeled\hilux_armed\data\detailmapy\drziaky_destruct.rvmat",
			"ca\wheeled\data\detailmapy\coyota_kola.rvmat",
			"ca\wheeled\data\detailmapy\coyota_kola.rvmat",
			"ca\wheeled\data\detailmapy\coyota_kola_destruct.rvmat",
			"ca\wheeled\data\detailmapy\coyota_interier.rvmat",
			"ca\wheeled\data\detailmapy\coyota_interier.rvmat",
			"ca\wheeled\data\detailmapy\coyota_interier_destruct.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat"
		};
	};
	class NewTurret;
	class Turrets {
		class MainTurret : NewTurret {
			memoryPointGun="machinegun";
			body="mainTurret";
			gun="mainGun";
			outGunnerMayFire=1;
			forceHideGunner=1; // 0 
			castGunnerShadow=1;
			viewGunnerInExternal=1;
			gunnerOpticsModel="\ca\Weapons\optika_empty";
			gunnerForceOptics=0;
			weapons[]={"DSHKM"};
			soundServo[]={};
			magazines[]={"50Rnd_127x107_DSHKM"};
			gunnerAction="Hilux_Gunner";
			gunnerInAction="Hilux_Gunner";
			gunnerGetOutAction="GetOutLow";
			gunnerCompartments="Compartment2";
			gunBeg="usti hlavne";
			gunEnd="konec hlavne";
			ejectDeadGunner=1;
			minElev=-18;
			maxElev=60;
			minTurn=-360;
			maxTurn=360;
			initTurn=0;
			primaryGunner=1;
			enableManualFire=0;
			stabilizedInAxes=0;
			startEngine=0;
		};
	};
	class AnimationSources {
		class HitLFWheel {source="Hit";hitpoint="HitLFWheel";raw=1;};
		class HitRFWheel {source="Hit";hitpoint="HitRFWheel";raw=1;};
		class HitLBWheel {source="Hit";hitpoint="HitLBWheel";raw=1;};
		class HitRBWheel {source="Hit";hitpoint="HitRBWheel";raw=1;};
		class HitLF2Wheel {source="Hit";hitpoint="HitLF2Wheel";raw=1;};  
		class HitRF2Wheel {source="Hit";hitpoint="HitRF2Wheel";raw=1;};  
		class HitLMWheel {source="Hit";hitpoint="HitLMWheel";raw=1;};  
		class HitRMWheel {source="Hit";hitpoint="HitRMWheel";raw=1;};  
		class HitGlass1 {source="Hit";hitpoint="HitGlass1";raw=1;};
		class HitGlass2 {source="Hit";hitpoint="HitGlass2";raw=1;};
		class HitGlass3 {source="Hit";hitpoint="HitGlass3";raw=1;};
		class HitGlass4 {source="Hit";hitpoint="HitGlass4";raw=1;};
		class ReloadAnim {source="reload";weapon="DShKM";};
		class ReloadMagazine {source="reloadmagazine";weapon="DShKM";};
		class Revolving {source="revolving";weapon="DShKM";};
	};
	class HitPoints {
		class HitEngine {armor=0.5;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.5;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.25;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.25;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.25;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.25;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
	};
};