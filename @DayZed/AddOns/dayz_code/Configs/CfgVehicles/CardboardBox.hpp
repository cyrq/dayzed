class CardboardBox;
class FoodBox0 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_1;
	model = "\dayz_equip\models\cardboard_box.p3d";
	
	class transportmagazines {
		class _xx_FoodCanBakedBeans {
			magazine = "FoodCanBakedBeans";
			count = 6;
		};
		
		class _xx_FoodCanSardines {
			magazine = "FoodCanSardines";
			count = 5;
		};
		
		class _xx_FoodCanFrankBeans {
			magazine = "FoodCanFrankBeans";
			count = 4;
		};
		
		class _xx_FoodCanPasta {
			magazine = "FoodCanPasta";
			count = 3;
		};
	};
};

class FoodBox1 : FoodBox0 {};

class FoodBox2 : FoodBox0 {};

class MedBox1 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_2;
	model = "\dayz_equip\models\cardboard_box_med.p3d";
	
	class transportmagazines {
		class _xx_ItemBandage {
			magazine = "ItemBandage";
			count = 3;
		};
		
		class _xx_ItemEpinephrine {
			magazine = "ItemEpinephrine";
			count = 2;
		};
		
		class _xx_ItemMorphine {
			magazine = "ItemMorphine";
			count = 2;
		};
		
		class _xx_ItemBloodbag {
			magazine = "ItemBloodbag";
			count = 2;
		};
		
		class _xx_ItemPainkiller {
			magazine = "ItemPainkiller";
			count = 3;
		};
		
		class _xx_ItemAntibiotic {
			magazine = "ItemAntibiotic";
			count = 2;
		};
		class _xx_ItemChloroform {
			magazine = "ItemChloroform";
			count = 2;
		};
	};
};
class MedBoxCS1 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_2;
	model = "\dayz_equip\models\cardboard_box_med.p3d";
	
	class transportmagazines {
		class _xx_ItemBandage {
			magazine = "ItemBandage";
			count = 5;
		};
	};
};
class MedBoxCS2 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_2;
	model = "\dayz_equip\models\cardboard_box_med.p3d";
	
	class transportmagazines {
		class _xx_ItemPainkiller {
			magazine = "ItemPainkiller";
			count = 5;
		};
	};
};
class MedBoxCS3 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_2;
	model = "\dayz_equip\models\cardboard_box_med.p3d";
	
	class transportmagazines {
		class _xx_ItemEpinephrine {
			magazine = "ItemEpinephrine";
			count = 5;
		};
	};
};
class MedBoxCS4 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_2;
	model = "\dayz_equip\models\cardboard_box_med.p3d";
	
	class transportmagazines {
		class _xx_ItemMorphine {
			magazine = "ItemMorphine";
			count = 5;
		};
	};
};
class MedBoxCS5 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_2;
	model = "\dayz_equip\models\cardboard_box_med.p3d";
	
	class transportmagazines {
		class _xx_ItemBloodbag {
			magazine = "ItemBloodbag";
			count = 5;
		};
	};
};
class MedBoxCS6 : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_2;
	model = "\dayz_equip\models\cardboard_box_med.p3d";
	
	class transportmagazines {
		class _xx_ItemAntibiotic {
			magazine = "ItemAntibiotic";
			count = 5;
		};
	};
};
class FoodBoxINF : CardboardBox {
	scope = public;
	displayName = $STR_DAYZ_OBJ_1;
	model = "\dayz_equip\models\cardboard_box.p3d";
	
	class transportmagazines {
		class _xx_ItemCanOpener {
			magazine = "FoodCanBakedBeans";
			count = 1;
		};
		
		class _xx_FoodCanBakedBeans {
			magazine = "FoodCanBakedBeans";
			count = 2;
		};
		
		class _xx_FoodCanSardines {
			magazine = "FoodCanSardines";
			count = 2;
		};
		
		class _xx_FoodCanFrankBeans {
			magazine = "FoodCanFrankBeans";
			count = 2;
		};
		
		class _xx_FoodCanPasta {
			magazine = "FoodCanPasta";
			count = 2;
		};
	};
};