class Land_A_TVTower_Mid: House
	{
		scope = 1;
		destrType = "DestructNo";
		model = "\ca\Structures\A_TVTower\A_TVTower_Mid";
		featureSize = 150;
		class MarkerLights
		{
			class RedStill
			{
			name = "cerveny pozicni";
			color[] = {1, 0.1, 0.1, 1};
			ambient[] = {0.1, 0.01, 0.01, 1};
			brightness = 0.01;
			blinking = 0;
			};
		};
	};