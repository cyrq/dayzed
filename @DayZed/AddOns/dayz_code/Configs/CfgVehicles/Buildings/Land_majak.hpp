class Land_majak: House
	{
		scope = 1;
		model = "\ca\buildings\majak";
		displayName = "Lighthouse";
		animated = 1;
		ladders[] = {{"start","end"}};
		class DestructionEffects: DestructionEffects
		{
			class Ruin1
			{
				simulation = "ruin";
				type = "\ca\buildings\ruins\majak_ruins.p3d";
				position = "";
				intensity = 1;
				interval = 1;
				lifeTime = 1;
			};
		};
		armor = 1000;
		class MarkerLights
		{
			class RedBlinking
			{
			name = "cerveny pozicni blik";
			color[] = {0.64, 0.064, 0.064, 1};
			ambient[] = {0.1, 0.01, 0.01, 1};
			brightness = 0.02;
			blinking = 1;
			};
		};
		class Reflectors
		{
			class MainLight
			{
			color[] = {0.9, 0.8, 0.8, 1};
			ambient[] = {0.1, 0.1, 0.1, 1};
			position = "Source_MainLight";
			direction = "Direction_MainLight";
			hitpoint = "MainLight";
			selection = "LightFlare";
			size = 5;
			brightness = 3;
			period[] = {4.6, 2.4};
			};
		};
	};
class Land_majak2: Land_majak
	{
		model = "\ca\buildings\majak2";
		class Reflectors
		{
			class MainLight
			{
			color[] = {0.9, 0.8, 0.8, 1};
			ambient[] = {0.1, 0.1, 0.1, 1};
			position = "Source_MainLight";
			direction = "Direction_MainLight";
			hitpoint = "MainLight";
			selection = "LightFlare";
			size = 5;
			brightness = 3;
			period[] = {2.4, 4.6};
			};
		};
		class DestructionEffects: DestructionEffects
		{
			class Ruin1
			{
				simulation = "ruin";
				type = "\ca\buildings\ruins\majak_ruins.p3d";
				position = "";
				intensity = 1;
				interval = 1;
				lifeTime = 1;
			};
		};
	};