class Land_NavigLight: House
	{
		scope = 1;
		displayName = "";
		model = "\ca\buildings\Misc\NavigLight";
		armor = 50;
		class MarkerLights
		{
			class WhiteStill
			{
			name = "zluty pozicni";
			color[] = {0.99, 0.69, 0.17, 1};
			ambient[] = {0.099, 0.069, 0.017, 1};
			brightness = 0.02;
			blinking = 0;
			};
		};
	};