class Land_komin: House
	{
		ladders[] = {{"start","end"}};
		model = "\ca\buildings\komin";
		armor = 300;
		class DestructionEffects: DestructionEffects
		{
			class Ruin1
			{
				simulation = "ruin";
				type = "\ca\buildings\ruins\komin_ruins.p3d";
				position = "";
				intensity = 1;
				interval = 1;
				lifeTime = 1;
			};
		};
		class MarkerLights
		{
			class RedBlinking
			{
			name = "cerveny pozicni blik";
			color[] = {0.64, 0.064, 0.064, 1};
			ambient[] = {0.1, 0.01, 0.01, 1};
			brightness = 0.01;
			blinking = 1;
			};
		};
	};