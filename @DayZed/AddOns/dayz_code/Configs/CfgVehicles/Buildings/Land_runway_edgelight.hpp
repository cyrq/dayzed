class Land_runway_edgelight: House
	{
		scope = 1;
		displayName = "";
		model = "\ca\buildings\Misc\runway_edgelight";
		armor = 20;
		class MarkerLights
		{
			class RedStill
			{
			name = "zluty pozicni";
			color[] = {0.99, 0.69, 0.17, 1};
			ambient[] = {0.099, 0.069, 0.017, 1};
			brightness = 0.01;
			blinking = 0;
			};
		};
	};