class Land_Rail_Zavora: House
	{
		scope = 1;
		model = "\CA\Structures\Rail\Rail_Misc\rail_Zavora";
		destrType = "DestructTree";
		class MarkerLights
		{
			class WhiteBlinking
			{
			name = "bily pozicni blik";
			color[] = {0.01, 0.01, 0.03, 1};
			ambient[] = {0.01, 0.01, 0.0056, 1};
			brightness = 0.01;
			blinking = 1;
			};
		};
	};