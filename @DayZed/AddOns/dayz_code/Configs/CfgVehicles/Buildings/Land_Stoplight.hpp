class Land_Stoplight01: House
	{
		scope = 1;
		model = "\ca\buildings\Misc\stoplight01";
		armor = 50;
		class MarkerLights
		{
			class YellowTopBlinking
			{
				name = "Source_topLight";
				color[] = {0.075, 0.05, 0.007, 1};
				ambient[] = {0.03, 0.023, 0.0056, 1};
				brightness = 0.01;
				blinking = 1;
			};
			class YellowLowBlinking
			{
				name = "Source_lowLight";
				color[] = {0.075, 0.05, 0.007, 1};
				ambient[] = {0.03, 0.023, 0.0056, 1};
				brightness = 0.01;
				blinking = 1;
			};
		};
	};
	class Land_Stoplight02: Land_Stoplight01
	{
		model = "\ca\buildings\Misc\stoplight02";
		class MarkerLights
		{
			class YellowTopBlinking
			{
				name = "Source_topLight";
				color[] = {0.075, 0.05, 0.007, 1};
				ambient[] = {0.03, 0.023, 0.0056, 1};
				brightness = 0.01;
				blinking = 1;
			};
		};
	};