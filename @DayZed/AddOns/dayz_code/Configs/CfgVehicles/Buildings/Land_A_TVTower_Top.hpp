class Land_A_TVTower_Top: House
	{
		scope = 1;
		destrType = "DestructNo";
		model = "\ca\Structures\A_TVTower\A_TVTower_Top";
		featureSize = 150;
		class MarkerLights
		{
			class RedLight
			{
			name = "cerveny pozicni";
			color[] = {1, 0.1, 0.1, 1};
			ambient[] = {0.1, 0.01, 0.01, 1};
			brightness = 0.01;
			blinking = 1;
			};
		};
	};