class Land_Rail_Semafor: House
	{
		scope = 1;
		model = "\CA\Structures\Rail\Rail_Misc\rail_Semafor";
		destrType = "DestructTree";
		class MarkerLights
		{
			class GreenStill
			{
			name = "zeleny pozicni";
			color[] = {0.0042, 0.032, 0.003, 1};
			ambient[] = {0.01, 0.1, 0.01, 1};
			brightness = 0.01;
			blinking = 0;
			};
		};
	};