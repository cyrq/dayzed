class UAZ_RU : UAZ_Unarmed_Base {
		displayName="UAZ-469";
		maxSpeed=100;
		terrainCoef=2.25;
		turnCoef=1.5;
		preferRoads=0;
		transportMaxMagazines=45;
		transportMaxWeapons=8;
		transportMaxBackpacks=4;
		armor=40;
		damageResistance=0.00845;
		armorWheels=0.12;
		scope=2;
		side=0;
		faction="";
		crew="";
		typicalCargo[]={""};
		hiddenSelectionsTextures[]={"\ca\wheeled\data\Uaz_main_CO.paa"};
		class HitPoints {
			class HitEngine {
				armor = 0.4;
				material = -1;
				name = "motor";
				passthrough = 0.2;
				visual = "";
			};
			class HitFuel {
				armor = 0.3;
				material = -1;
				name = "palivo";
				passthrough = 0.5;
				visual = "";
			};
			class HitBody {
				armor=1;
				material=-1;
				name="karoserie";
				visual="";
				passThrough=1;
			};
			class HitLFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitLBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitGlass1 {
				armor = 0.05;
				material = -1;
				name = "glass1";
				passthrough = 0;
				visual = "glass1";
			};
			class HitGlass2 {
				armor = 0.05;
				material = -1;
				name = "glass2";
				passthrough = 0;
				visual = "glass2";
			};
			class HitGlass3 {
				armor = 0.05;
				material = -1;
				name = "glass3";
				passthrough = 0;
				visual = "glass3";
			};
			class HitGlass4 {
				armor = 0.05;
				material = -1;
				name = "glass4";
				passthrough = 0;
				visual = "glass4";
			};
		};
	};
	class UAZ_Unarmed_TK_CIV_EP1 : UAZ_Unarmed_Base {
		displayName="UAZ-469";
		maxSpeed=100;
		terrainCoef=2.25;
		turnCoef=1.5;
		preferRoads=0;
		transportMaxMagazines=45;
		transportMaxWeapons=8;
		transportMaxBackpacks=4;
		armor=40;
		damageResistance=0.00845;
		armorWheels=0.12;
		expansion=1;
		scope=2;
		side=3;
		accuracy=0.3;
		faction="";
		crew="";
		typicalCargo[]={};
		hiddenSelectionsTextures[]={"\CA\wheeled_E\UAZ\Data\Uaz_main_CIVIL_CO.paa"};
		class HitPoints {
			class HitEngine {
				armor = 0.4;
				material = -1;
				name = "motor";
				passthrough = 0.2;
				visual = "";
			};
			class HitFuel {
				armor = 0.3;
				material = -1;
				name = "palivo";
				passthrough = 0.5;
				visual = "";
			};
			class HitBody {
				armor=1;
				material=-1;
				name="karoserie";
				visual="";
				passThrough=1;
			};
			class HitLFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitLBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitGlass1 {
				armor = 0.05;
				material = -1;
				name = "glass1";
				passthrough = 0;
				visual = "glass1";
			};
			class HitGlass2 {
				armor = 0.05;
				material = -1;
				name = "glass2";
				passthrough = 0;
				visual = "glass2";
			};
			class HitGlass3 {
				armor = 0.05;
				material = -1;
				name = "glass3";
				passthrough = 0;
				visual = "glass3";
			};
			class HitGlass4 {
				armor = 0.05;
				material = -1;
				name = "glass4";
				passthrough = 0;
				visual = "glass4";
			};
		};
	};
	class UAZ_Unarmed_UN_EP1 : UAZ_Unarmed_Base {
		displayName="UAZ-469";
		maxSpeed=100;
		terrainCoef=2.25;
		turnCoef=1.5;
		preferRoads=0;
		transportMaxMagazines=45;
		transportMaxWeapons=8;
		transportMaxBackpacks=4;
		armor=40;
		damageResistance=0.00845;
		armorWheels=0.12;
		expansion=1;
		scope=2;
		side=2;
		accuracy=0.3;
		faction="";
		crew="";
		typicalCargo[]={};
		hiddenSelectionsTextures[]={"\CA\wheeled_E\UAZ\Data\Uaz_main_UN_CO.paa"};
		class TransportMagazines {};
		class TransportWeapons {};
		class HitPoints {
			class HitEngine {
				armor = 0.4;
				material = -1;
				name = "motor";
				passthrough = 0.2;
				visual = "";
			};
			class HitFuel {
				armor = 0.3;
				material = -1;
				name = "palivo";
				passthrough = 0.5;
				visual = "";
			};
			class HitBody {
				armor=1;
				material=-1;
				name="karoserie";
				visual="";
				passThrough=1;
			};
			class HitLFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitLBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitGlass1 {
				armor = 0.05;
				material = -1;
				name = "glass1";
				passthrough = 0;
				visual = "glass1";
			};
			class HitGlass2 {
				armor = 0.05;
				material = -1;
				name = "glass2";
				passthrough = 0;
				visual = "glass2";
			};
			class HitGlass3 {
				armor = 0.05;
				material = -1;
				name = "glass3";
				passthrough = 0;
				visual = "glass3";
			};
			class HitGlass4 {
				armor = 0.05;
				material = -1;
				name = "glass4";
				passthrough = 0;
				visual = "glass4";
			};
		};
	};
	class UAZ_CDF : UAZ_Unarmed_Base {
		displayName="UAZ-469";
		maxSpeed=100;
		terrainCoef=2.25;
		turnCoef=1.5;
		preferRoads=0;
		transportMaxMagazines=45;
		transportMaxWeapons=8;
		transportMaxBackpacks=4;
		armor=40;
		damageResistance=0.00845;
		armorWheels=0.12;
		scope=2;
		side=1;
		faction="";
		accuracy=0.3;
		crew="";
		typicalCargo[]={};
		hiddenSelectionsTextures[]={"\ca\wheeled\data\Uaz_main_002_CO.paa"};
		class HitPoints {
			class HitEngine {
				armor = 0.4;
				material = -1;
				name = "motor";
				passthrough = 0.2;
				visual = "";
			};
			class HitFuel {
				armor = 0.3;
				material = -1;
				name = "palivo";
				passthrough = 0.5;
				visual = "";
			};
			class HitBody {
				armor=1;
				material=-1;
				name="karoserie";
				visual="";
				passThrough=1;
			};
			class HitLFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitLBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitGlass1 {
				armor = 0.05;
				material = -1;
				name = "glass1";
				passthrough = 0;
				visual = "glass1";
			};
			class HitGlass2 {
				armor = 0.05;
				material = -1;
				name = "glass2";
				passthrough = 0;
				visual = "glass2";
			};
			class HitGlass3 {
				armor = 0.05;
				material = -1;
				name = "glass3";
				passthrough = 0;
				visual = "glass3";
			};
			class HitGlass4 {
				armor = 0.05;
				material = -1;
				name = "glass4";
				passthrough = 0;
				visual = "glass4";
			};
		};
	};
	class UAZ_Unarmed_TK_EP1 : UAZ_Unarmed_Base {
		displayName="UAZ-469";
		maxSpeed=100;
		terrainCoef=2.25;
		turnCoef=1.5;
		preferRoads=0;
		transportMaxMagazines=45;
		transportMaxWeapons=8;
		transportMaxBackpacks=4;
		armor=40;
		damageResistance=0.00845;
		armorWheels=0.12;
		expansion=1;
		scope=2;
		side=1;
		faction="";
		accuracy=0.3;
		crew="";
		typicalCargo[]={};
		class TransportMagazines {};
		class TransportWeapons {};
		hiddenSelectionsTextures[]={"\CA\wheeled_E\UAZ\Data\Uaz_main_IND_CO.paa"};
		class HitPoints {
			class HitEngine {
				armor = 0.4;
				material = -1;
				name = "motor";
				passthrough = 0.2;
				visual = "";
			};
			class HitFuel {
				armor = 0.3;
				material = -1;
				name = "palivo";
				passthrough = 0.5;
				visual = "";
			};
			class HitBody {
				armor=1;
				material=-1;
				name="karoserie";
				visual="";
				passThrough=1;
			};
			class HitLFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRFWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_1_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitLBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_1_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitRBWheel {
				armor = 0.35;
				material = -1;
				name = "wheel_2_2_steering";
				passThrough=0.3;
				visual = "";
			};
			class HitGlass1 {
				armor = 0.05;
				material = -1;
				name = "glass1";
				passthrough = 0;
				visual = "glass1";
			};
			class HitGlass2 {
				armor = 0.05;
				material = -1;
				name = "glass2";
				passthrough = 0;
				visual = "glass2";
			};
			class HitGlass3 {
				armor = 0.05;
				material = -1;
				name = "glass3";
				passthrough = 0;
				visual = "glass3";
			};
			class HitGlass4 {
				armor = 0.05;
				material = -1;
				name = "glass4";
				passthrough = 0;
				visual = "glass4";
			};
		};
	};	