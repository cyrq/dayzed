class HMMWV_DZ: HMMWV_Base {
	displayname = "Humvee";
	model = "ca\wheeled_E\HMMWV\HMMWV";
	maxSpeed=200;
	terrainCoef=2;
	turnCoef=2;
	preferRoads=0;
	transportMaxMagazines=60;
	transportMaxWeapons=10;
	transportMaxBackpacks=5;
	scope=2;
	enableGPS=1;
	armor=40;
	damageResistance=0.00562;
	crew="";
	typicalCargo[]={};
	hasgunner = 0;
	hiddenselections[] = {"Camo1"};
	hiddenselectionstextures[] = {"\ca\wheeled\hmmwv\data\hmmwv_body_co.paa"};
	class Turrets {};
	class Damage {
		mat[] = {
			"ca\wheeled\hmmwv\data\hmmwv_details.rvmat",
			"Ca\wheeled\HMMWV\data\hmmwv_details_damage.rvmat",
			"Ca\wheeled\HMMWV\data\hmmwv_details_destruct.rvmat",
			"ca\wheeled\hmmwv\data\hmmwv_body.rvmat",
			"Ca\wheeled\HMMWV\data\hmmwv_body_damage.rvmat",
			"Ca\wheeled\HMMWV\data\hmmwv_body_destruct.rvmat",
			"ca\wheeled\hmmwv\data\hmmwv_clocks.rvmat",
			"ca\wheeled\hmmwv\data\hmmwv_clocks.rvmat",
			"ca\wheeled\data\hmmwv_clocks_destruct.rvmat",
			"ca\wheeled\HMMWV\data\hmmwv_glass.rvmat",
			"ca\wheeled\HMMWV\data\hmmwv_glass_Half_D.rvmat",
			"ca\wheeled\HMMWV\data\hmmwv_glass_Half_D.rvmat",
			"ca\wheeled\HMMWV\data\hmmwv_glass_in.rvmat",
			"ca\wheeled\HMMWV\data\hmmwv_glass_in_Half_D.rvmat",
			"ca\wheeled\HMMWV\data\hmmwv_glass_in_Half_D.rvmat"
		};
		tex[] = {};
	};
	class HitPoints {
		class HitEngine {armor=0.5;material=-1;name="motor";visual="";passThrough=1;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.14;material=-1;name="fueltank";visual="";passThrough=1;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};