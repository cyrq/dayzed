class ATV_CZ_EP1 : ATV_Base_EP1 {
	displayName="Quad ATV";
	displayNameShort="ATV";
	maxSpeed=80;
	fuelCapacity=30;
	terrainCoef=2;
	turnCoef=1.5;
	preferRoads=0;
	transportMaxMagazines=20;
	transportMaxWeapons=2;
	transportMaxBackpacks=1;
	scope=2;
	side=1;
	faction="";
	crew="";
	typicalCargo[]={};
	class HitPoints {
		class HitBody {
			armor = 1;
			material = -1;
			name = "karoserie";
			passthrough = 1;
			visual = "";
		};
		class HitEngine {
			armor = 2;
			material = -1;
			name = "motor";
			passthrough = 0;
			visual = "motor";
		};
		class HitFuel {
			armor = 1;
			material = -1;
			name = "palivo";
			passthrough = 0;
			visual = "";
		};
		class HitLFWheel {
			armor = 1;
			material = -1;
			name = "wheel_1_1_steering";
			passthrough = 0.3;
			visual = "";
		};
		class HitLBWheel {
			armor = 1;
			material = -1;
			name = "wheel_1_2_steering";
			passthrough = 0.3;
			visual = "";
		};
		class HitRFWheel {
			armor = 1;
			material = -1;
			name = "wheel_2_1_steering";
			passthrough = 0.3;
			visual = "";
		};
		class HitRBWheel {
			armor = 1;
			material = -1;
			name = "wheel_2_2_steering";
			passthrough = 0.3;
			visual = "";
		};
	};
};