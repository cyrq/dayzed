class BTR40_DZed : BTR40_base_EP1 {
	displayname = "BTR-40";
	scope=2;
	side=2;
	faction="";
	crew="";
	maxSpeed=100;
	fuelCapacity=100;
	enableGPS = 0;
	typicalCargo[]={};
	hiddenSelectionsTextures[]={"ca\wheeled_e\btr40\data\btr40ext_co.paa"};
	terrainCoef=2;
	turnCoef=4;
	preferRoads=0;
	transportMaxMagazines=50;
	transportMaxWeapons=10;
	transportMaxBackpacks=5;
	armor=40;
	damageResistance=0.03241;
	class HitPoints {
		class HitEngine {
			armor=0.75;
			material=-1;
			name="motor";
			visual="";
			passThrough=0.2;
		};
		class HitFuel {
			armor=0.40;
			material=-1;
			name="palivo";
			visual="";
			passThrough=0.5;
		};
		class HitBody {
			armor=2;
			material=-1;
			name="karoserie";
			visual="";
			passThrough=1;
		};
		class HitLFWheel {
			armor=0.30;
			material=-1;
			name="wheel_1_1_steering";
			visual="";
			passThrough=0.3;
		};
		class HitLBWheel {
			armor=0.30;
			material=-1;
			name="wheel_1_2_steering";
			visual="";
			passThrough=0.3;
		};
		class HitRFWheel {
			armor=0.30;
			material=-1;
			name="wheel_2_1_steering";
			visual="";
			passThrough=0.3;
		};
		class HitRBWheel {
			armor=0.30;
			material=-1;
			name="wheel_2_2_steering";
			visual="";
			passThrough=0.3;
		};
	};
};