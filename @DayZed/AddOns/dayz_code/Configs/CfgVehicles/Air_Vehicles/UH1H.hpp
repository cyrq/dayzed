class UH1H_base : Helicopter {
	class Turrets : Turrets {
		class MainTurret : MainTurret {
			class ViewOptics: ViewOptics {};
			class Turrets: Turrets {};
		};
		class LeftDoorGun: MainTurret {
			class Turrets: Turrets {};
		};
	};
};
class UH1H_DZ : UH1H_base {
	displayName="UH-1H Huey";
	displayNameShort="UH-1H";
	scope = 2;
	side = 2;
	crew = "";
	typicalCargo[] = {};
	hiddenSelections[] = {};
	class TransportMagazines{};
	class TransportWeapons{};
	commanderCanSee = 2+16+32;
	gunnerCanSee = 2+16+32;
	driverCanSee = 2+16+32;
	transportMaxMagazines = 25;
	transportMaxWeapons = 5;
    transportmaxbackpacks = 5;		
	class Turrets : Turrets {
		class MainTurret : MainTurret {
			weapons[]={"M240_veh"};
			magazines[]={"100Rnd_762x51_M240"};
		};
		class LeftDoorGun : LeftDoorGun	{
			weapons[]={"M240_veh_2"};
			magazines[]={"100Rnd_762x51_M240"};
		};
	};
	class AnimationSources {
		class ReloadAnim {source="reload";weapon="M240_veh";};
		class ReloadMagazine {source="reloadmagazine";weapon="M240_veh";};
		class Revolving {source="revolving";weapon="M240_veh";};
		class ReloadAnim_2 {source="reload";weapon="M240_veh_2";};
		class ReloadMagazine_2 {source="reloadmagazine";weapon="M240_veh_2";};
		class Revolving_2 {source="revolving";weapon="M240_veh_2";};
	};
	class HitPoints {
		class HitHull {armor=1;material=51;name="NEtrup";visual="trup";passThrough=1;};
		class HitEngine {armor=0.25;material=51;name="motor";visual="motor";passThrough=1;};
		class HitAvionics {armor=0.15;material=51;name="elektronika";visual="elektronika";passThrough=1;};
		class HitVRotor {armor=0.3;material=51;name="mala vrtule";visual="mala vrtule staticka";passThrough=0.3;};
		class HitHRotor {armor=0.2;material=51;name="velka vrtule";visual="velka vrtule staticka";passThrough=0.1;};
		class HitGlass1 {armor=0.25;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.25;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.25;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.25;material=-1;name="glass4";visual="glass4";passThrough=0;};
		class HitGlass5 {armor=0.25;material=-1;name="glass5";visual="glass5";passThrough=0;};
	};
};
class UH1H_DZ2 : UH1H_DZ {
	displayName="UH-1H Huey Camo";
	displayNameShort="UH-1H";
	hiddenSelections[] = {"Camo1", "Camo2", "Camo_mlod"};
	hiddenSelectionsTextures[] = {"ca\air_E\UH1H\data\UH1D_TKA_CO.paa", "ca\air_E\UH1H\data\UH1D_in_TKA_CO.paa", "ca\air_E\UH1H\data\default_TKA_co.paa"};
	class Turrets : Turrets {
		class MainTurret : MainTurret {
			weapons[]={"M240_veh"};
			magazines[]={"100Rnd_762x51_M240"};
		};
		class LeftDoorGun : LeftDoorGun	{
			weapons[]={"M240_veh_2"};
			magazines[]={"100Rnd_762x51_M240"};
		};
	};
	class AnimationSources {
		class ReloadAnim {source="reload";weapon="M240_veh";};
		class ReloadMagazine {source="reloadmagazine";weapon="M240_veh";};
		class Revolving {source="revolving";weapon="M240_veh";};
		class ReloadAnim_2 {source="reload";weapon="M240_veh_2";};
		class ReloadMagazine_2 {source="reloadmagazine";weapon="M240_veh_2";};
		class Revolving_2 {source="revolving";weapon="M240_veh_2";};
	};
	class HitPoints {
		class HitHull {armor=1;material=51;name="NEtrup";visual="trup";passThrough=1;};
		class HitEngine {armor=0.25;material=51;name="motor";visual="motor";passThrough=1;};
		class HitAvionics {armor=0.15;material=51;name="elektronika";visual="elektronika";passThrough=1;};
		class HitVRotor {armor=0.3;material=51;name="mala vrtule";visual="mala vrtule staticka";passThrough=0.3;};
		class HitHRotor {armor=0.2;material=51;name="velka vrtule";visual="velka vrtule staticka";passThrough=0.1;};
		class HitGlass1 {armor=0.25;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.25;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.25;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.25;material=-1;name="glass4";visual="glass4";passThrough=0;};
		class HitGlass5 {armor=0.25;material=-1;name="glass5";visual="glass5";passThrough=0;};
	};
};