class CH47_base_EP1;
class CH_47F_EP1 : CH47_base_EP1 {};
class Chinook_CH47F : CH_47F_EP1 {
	displayname = "Chinook CH-47";
	displayNameShort = "Chinook CH-47";
	scope = 2;
	model="\ca\air_E\CH47\CH_47F";
	picture="\ca\air_e\data\UI\Picture_ch47f_CA.paa";
	icon="\ca\air_e\data\UI\Icon_ch47f_CA.paa";
	mapSize=24;
	crew = "";
	typicalCargo[] = {};
	class Turrets {};
};