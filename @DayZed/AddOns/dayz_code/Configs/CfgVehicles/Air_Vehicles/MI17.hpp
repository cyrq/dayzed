class Mi17_base : Helicopter {
	class Turrets : Turrets	{
		class MainTurret: MainTurret {
			class ViewOptics: ViewOptics {};
			class Turrets: Turrets {};
		};
		class BackTurret: MainTurret {
			class Turrets: Turrets {};
		};
	};
};
class Mi17_base_CDF : Mi17_base {};
class Mi17_DZ : Mi17_base_CDF {
	displayName="Mil Mi-17";
	displayNameShort="Mi-17";
	scope = 2;
	side = 2;
	crew = "";
	typicalCargo[] = {};
	hiddenSelections[] = {};
	class TransportMagazines{};
	class TransportWeapons{};
	commanderCanSee = 2+16+32;
	gunnerCanSee = 2+16+32;
	driverCanSee = 2+16+32;
	transportMaxWeapons = 20;
	transportMaxMagazines = 100;
	transportmaxbackpacks = 10;
	class Turrets : Turrets {
		class MainTurret : MainTurret {
			weapons[]={"PKT"};
			magazines[]={"100Rnd_762x54_PK"};
		};
		class BackTurret : BackTurret {
			weapons[]={"PKT_2"};
			magazines[]={"100Rnd_762x54_PK"};
		};
	};
	class AnimationSources {
		class ReloadAnim {source="reload";weapon="PKT";};
		class ReloadMagazine {source="reloadmagazine";weapon="PKT";};
		class Revolving {source="revolving";weapon="PKT";};
		class ReloadAnim_2 {source="reload";weapon="PKT_2";};
		class ReloadMagazine_2 {source="reloadmagazine";weapon="PKT_2";};
		class Revolving_2 {source="revolving";weapon="PKT_2";};
	};
	class HitPoints {
		class HitHull {armor=1;material=51;name="NEtrup";visual="trup";passThrough=1;};
		class HitEngine {armor=0.25;material=51;name="motor";visual="motor";passThrough=1;};
		class HitAvionics {armor=0.15;material=51;name="elektronika";visual="elektronika";passThrough=1;};
		class HitVRotor {armor=0.3;material=51;name="mala vrtule";visual="mala vrtule staticka";passThrough=0.3;};
		class HitHRotor {armor=0.2;material=51;name="velka vrtule";visual="velka vrtule staticka";passThrough=0.1;};
		class HitGlass1 {armor=0.12;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.12;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.12;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.12;material=-1;name="glass4";visual="glass4";passThrough=0;};
		class HitGlass5 {armor=0.12;material=-1;name="glass5";visual="glass5";passThrough=0;};
		class HitGlass6 {armor=0.12;material=-1;name="glass6";visual="glass6";passThrough=0;};
	};
};
class Mi17_Civilian_base_Ru;
class Mi17_Civilian : Mi17_Civilian_base_Ru {
	scope=2;
	vehicleClass="Air";
	side=3;
	displayName="Mi-17 Civilian";
	displayNameShort="Mi-17";
	faction="";
	model="\ca\air\Mi_8AMT";
	crew="";
	typicalCargo[]={};
	weapons[]={};
	magazines[]={};
	hasGunner=0;
	class Turrets {};
	threat[]={0,0,0};
	commanderCanSee = 2+16+32;
    gunnerCanSee = 2+16+32;
    driverCanSee = 2+16+32;
	transportMaxWeapons = 20;
	transportMaxMagazines = 100;
	transportmaxbackpacks = 10;
	hiddenSelectionsTextures[]={"\CA\air\data\mi8civil_body_g_CO.paa","\CA\air\data\mi8civil_det_g_CO.paa","ca\air\data\clear_empty.paa","ca\air\data\mi8_decals_ca.paa"};
	class HitPoints {
		class HitHull {armor=1;material=51;name="NEtrup";visual="trup";passThrough=1;};
		class HitEngine {armor=0.25;material=51;name="motor";visual="motor";passThrough=1;};
		class HitAvionics {armor=0.15;material=51;name="elektronika";visual="elektronika";passThrough=1;};
		class HitVRotor {armor=0.3;material=51;name="mala vrtule";visual="mala vrtule staticka";passThrough=0.3;};
		class HitHRotor {armor=0.2;material=51;name="velka vrtule";visual="velka vrtule staticka";passThrough=0.1;};
		class HitGlass1 {armor=0.12;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.12;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.12;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.12;material=-1;name="glass4";visual="glass4";passThrough=0;};
		class HitGlass5 {armor=0.12;material=-1;name="glass5";visual="glass5";passThrough=0;};
		class HitGlass6 {armor=0.12;material=-1;name="glass6";visual="glass6";passThrough=0;};
	};
};