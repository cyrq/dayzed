class Old_bike_TK_CIV_EP1 : Old_bike_base_EP1 {
	displayName="Old bike";
	displayNameShort="Bike";
	maxSpeed=35;
	terrainCoef=3;
	turnCoef=2;
	preferRoads=1;
	soundEngine[]={};		
	scope=2;
	side=3;
	faction="";
	crew="";
	typicalCargo[]={""};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0;};
		class HitFuel {armor=0.3;material=51;name="palivo";passThrough=0;};
		class HitBody {armor=1;material=51;name="karoserie";visual="";passThrough=1;};
		class HitFWheel {armor=1;material=-1;name="wheel_1_damper";visual="wheel_1";passThrough=0;};
		class HitBWheel {armor=1;material=-1;name="wheel_2_damper";visual="wheel_2";passThrough=0;};
	};
};
class MMT_Civ : MMT_base {
	displayName="Mountain bike";
	displayNameShort="Bike";
	maxSpeed=45;
	terrainCoef=2.5;
	turnCoef=2;
	preferRoads=1;
	soundEngine[]={};
	scope=2;
	side=3;
	faction="";
	crew="";
	typicalCargo[]={};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0;};
		class HitFuel {armor=0.3;material=51;name="palivo";passThrough=0;};
		class HitBody {armor=1;material=51;name="karoserie";visual="";passThrough=1;};
		class HitFWheel {armor=1;material=-1;name="wheel_1_damper";visual="wheel_1";passThrough=0;};
		class HitBWheel {armor=1;material=-1;name="wheel_2_damper";visual="wheel_2";passThrough=0;};
	};
};