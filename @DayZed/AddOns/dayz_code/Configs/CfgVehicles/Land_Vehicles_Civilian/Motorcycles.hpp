class M1030 : Motorcycle {
	displayName="M1030 Motorcycle";
	displayNameShort="M1030";
	fuelCapacity=50;
	turnCoef=1;
	terrainCoef=3;
	maxSpeed=120;
	preferRoads=1;
	transportMaxMagazines=5;
	transportMaxWeapons=1;
	transportMaxBackpacks=1;
	class HitPoints {
		class HitEngine {armor=1.2;material=60;name="engine";visual="engine";passThrough=1;};
		class HitFuel {armor=1.4;material=-1;name="palivo";visual="palivo";passThrough=1;};
		class HitBody {armor=0.4;material=-1;name="karoserie";visual="karoserie";passThrough=1;};
		class HitFWheel {armor=0.05;material=-1;name="Pravy predni tlumic";visual="Pravy predni";passThrough=1;};
		class HitBWheel {armor=0.05;material=-1;name="Pravy zadni tlumic";visual="Pravy zadni";passThrough=1;};
	};
};	
class Old_moto_TK_Civ_EP1 : Old_moto_base {
	displayName="Old Motorcycle";
	displayNameShort="Old Motorcycle";
	fuelCapacity=50;
	maxSpeed=120;
	turnCoef=3.5;
	terrainCoef=3;
	preferRoads=1;
	transportMaxMagazines=5;
	transportMaxWeapons=1;
	transportMaxBackpacks=1;
	expansion=1;
	scope=2;
	side=3;
	faction="";
	crew="";
	typicalCargo[]={};
	rarityUrban=0.5;
	class HitPoints {
		class HitEngine {armor=1.2;material=60;name="engine";visual="engine";passThrough=1;};
		class HitFuel {armor=1.4;material=-1;name="palivo";visual="palivo";passThrough=1;};
		class HitBody {armor=0.4;material=-1;name="karoserie";visual="karoserie";passThrough=1;};
		class HitFWheel {armor=0.05;material=-1;name="Pravy predni tlumic";visual="Pravy predni";passThrough=1;};
		class HitBWheel {armor=0.05;material=-1;name="Pravy zadni tlumic";visual="Pravy zadni";passThrough=1;};
	};
};

class TT650_Civ : TT650_Base {
	displayName="TT650 Motorcycle";
	displayNameShort="TT650";
	terrainCoef=3;
	turnCoef=2;
	fuelCapacity=50;
	maxSpeed=120;
	transportMaxMagazines=5;
	transportMaxWeapons=1;
	transportMaxBackpacks=1;
	expansion=1;
	scope=2;
	side=0;
	faction="";
	crew="";
	typicalCargo[]={};
	class HitPoints {
		class HitEngine {armor=1.2;material=60;name="engine";visual="engine";passThrough=1;};
		class HitFuel {armor=1.4;material=-1;name="palivo";visual="palivo";passThrough=1;};
		class HitBody {armor=0.4;material=-1;name="karoserie";visual="karoserie";passThrough=1;};
		class HitFWheel {armor=0.05;material=-1;name="Pravy predni tlumic";visual="Pravy predni";passThrough=1;};
		class HitBWheel {armor=0.05;material=-1;name="Pravy zadni tlumic";visual="Pravy zadni";passThrough=1;};
	};
};

class TT650_TK_EP1 : TT650_Civ {
	hiddenSelectionsTextures[]={"\CA\wheeled_E\TT650\Data\TT650_ECIV_CO.paa"};
};