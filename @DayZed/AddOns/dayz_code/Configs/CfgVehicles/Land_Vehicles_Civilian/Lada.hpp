class Lada1 : Lada_base {
	displayname="Lada Riva 1500";
	displayNameShort="Lada";
	maxSpeed=110;
	terrainCoef=5;
	turnCoef=2;
	preferRoads=1;
	transportMaxMagazines=30;
	transportMaxWeapons=5;
	transportMaxBackpacks=2;
	faction="";
	crew="";
	typicalCargo[]={};
	scope=2;
	accuracy=1000;
	hiddenSelections[]={};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};
class Lada2 : Lada_base {
	displayname="Lada Riva 1500";
	displayNameShort="Lada";
	maxSpeed=110;
	terrainCoef=6;
	turnCoef=2;
	preferRoads=1;
	transportMaxMagazines=30;
	transportMaxWeapons=5;
	transportMaxBackpacks=2;
	faction="";
	crew="";
	typicalCargo[]={};
	scope=2;
	accuracy=1000;
	hiddenSelections[]={"Camo1"};
	hiddenSelectionsTextures[]={"\ca\wheeled2\Lada\Data\Lada_red_CO.paa"};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};
class Lada1_TK_CIV_EP1 : Lada_base {
	displayname="Lada Riva 1500";
	displayNameShort="Lada";
	maxSpeed=110;
	terrainCoef=6;
	turnCoef=2;
	preferRoads=1;
	transportMaxMagazines=30;
	transportMaxWeapons=5;
	transportMaxBackpacks=2;
	expansion=1;
	faction="";
	crew="";
	typicalCargo[]={};
	scope=2;
	side=3;
	hiddenSelections[]={"Camo1","Camo2"};
	hiddenSelectionsTextures[]={"\CA\wheeled_E\Lada\Data\Lada_ECIV1_CO.paa","\Ca\wheeled_E\Lada\Data\Lada_glass_ECIV1_CA.paa"};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};
class LadaLM : Lada_base {
	scope=2;
	accuracy=1000;
	model="\CA\wheeled2\Lada\Lada_LM.p3d";
	displayname="Lada Militia";
	displayNameShort="Lada";
	maxSpeed=150;
	terrainCoef=3;
	turnCoef=2;
	preferRoads=1;
	transportMaxMagazines=30;
	transportMaxWeapons=5;
	transportMaxBackpacks=2;
	faction="";
	crew="";
	typicalCargo[]={};
	class Reflectors {};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};