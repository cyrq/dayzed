 class Tractor : Car{
	class SpeechVariants {
                        class Default
                                {
                                speechSingular[]={"veh_Tractor"};
                                speechPlural[]={"veh_Tractors"};
                                };

                        class EN : Default {};
                        class CZ
                                {
                                speechSingular[]={"veh_Tractor_CZ"};
                                speechPlural[]={"veh_Tractors_CZ"};
                                };

                        class CZ_Akuzativ
                                {
                                speechSingular[]={"veh_Tractor_CZ4P"};
                                speechPlural[]={"veh_Tractors_CZ4P"};
                                };

                        class RU
                                {
                                speechSingular[]={"veh_Tractor_RU"};
                                speechPlural[]={"veh_Tractors_RU"};
                                };

                        };

                TextPlural="Tractors";
                TextSingular="Tractor";
                nameSound="veh_Tractor";
                scope=2;
                faction="";
                side=3;
                model="\CA\wheeled\tractor_2.p3d";
                picture="\Ca\wheeled\data\ico\tractor_CA.paa";
                Icon="\Ca\wheeled\data\map_ico\icomap_traktor_CA.paa";
                mapSize=6;
                terrainCoef=1;
                wheelCircumference=3.768;
                soundGear[]={"",5.62341e-005,1};
                soundEngineOnInt[]={"ca\sounds\vehicles\Wheeled\Tractor\ext\ext-tractor-start-2",0.562341,1};
                soundEngineOnExt[]={"ca\sounds\vehicles\Wheeled\Tractor\ext\ext-tractor-start-2",0.562341,1,250};
                soundEngineOffInt[]={"ca\sounds\vehicles\Wheeled\Tractor\ext\ext-Tractor-stop-1",0.562341,1};
                soundEngineOffExt[]={"ca\sounds\vehicles\Wheeled\Tractor\ext\ext-Tractor-stop-1",0.562341,1,250};
                soundGetIn[]={"\Ca\sounds\Vehicles\Wheeled\URAL\ext\ext-ural-getout",0.1,1};
                soundGetOut[]={"\Ca\sounds\Vehicles\Wheeled\URAL\ext\ext-ural-getout",0.1,1,30};
                buildCrash0[]={"Ca\sounds\Vehicles\Crash\crash_building_01",0.707946,1,200};
                buildCrash1[]={"Ca\sounds\Vehicles\Crash\crash_building_02",0.707946,1,200};
                buildCrash2[]={"Ca\sounds\Vehicles\Crash\crash_building_03",0.707946,1,200};
                buildCrash3[]={"Ca\sounds\Vehicles\Crash\crash_building_04",0.707946,1,200};
                soundBuildingCrash[]={"buildCrash0",0.25,"buildCrash1",0.25,"buildCrash2",0.25,"buildCrash3",0.25};
                WoodCrash0[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_01",0.707946,1,200};
                WoodCrash1[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_02",0.707946,1,200};
                WoodCrash2[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_03",0.707946,1,200};
                WoodCrash3[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_04",0.707946,1,200};
                WoodCrash4[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_05",0.707946,1,200};
                WoodCrash5[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_06",0.707946,1,200};
                soundWoodCrash[]={"woodCrash0",0.166,"woodCrash1",0.166,"woodCrash2",0.166,"woodCrash3",0.166,"woodCrash4",0.166,"woodCrash5",0.166};
                ArmorCrash0[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_01",0.707946,1,200};
                ArmorCrash1[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_02",0.707946,1,200};
                ArmorCrash2[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_03",0.707946,1,200};
                ArmorCrash3[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_04",0.707946,1,200};
                soundArmorCrash[]={"ArmorCrash0",0.25,"ArmorCrash1",0.25,"ArmorCrash2",0.25,"ArmorCrash3",0.25};
                class SoundEvents
                        {
                        class AccelerationOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tractor\ext\ext-Tractor-acce-1",0.562341,1,280};
                                limit="0.5";
                                expression="engineOn*gmeterZ";
                                };

                        };

                class Sounds
                        {
                        class EngineLowOut
                                {
                                sound[]={"\ca\sounds\Vehicles\Wheeled\Tractor\ext\ext-tractor-low-4",1,1.1,300};
                                frequency="(randomizer*0.05+0.95)*rpm";
                                volume="engineOn*(thrust factor[0.7, 0.2])";
                                };

                        class EngineHighOut
                                {
                                sound[]={"\ca\sounds\Vehicles\Wheeled\Tractor\ext\ext-tractor-high-2a",1,0.8,350};
                                frequency="(randomizer*0.05+0.95)*rpm";
                                volume="engineOn*(thrust factor[0.5, 1.0])";
                                };

                        class IdleOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tractor\ext\ext-tractor-idle-5",0.562341,1,200};
                                frequency="1";
                                volume="engineOn*(rpm factor[0.35, 0])";
                                };

                        class TiresRockOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-rock2",0.316228,1,30};
                                frequency="1";
                                volume="rock*(speed factor[2, 20])";
                                };

                        class TiresSandOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-sand2",0.316228,1,30};
                                frequency="1";
                                volume="sand*(speed factor[2, 20])";
                                };

                        class TiresGrassOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-grass3",0.316228,1,30};
                                frequency="1";
                                volume="grass*(speed factor[2, 20])";
                                };

                        class TiresMudOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-mud2",0.316228,1,30};
                                frequency="1";
                                volume="mud*(speed factor[2, 20])";
                                };

                        class TiresGravelOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-gravel2",0.316228,1,30};
                                frequency="1";
                                volume="gravel*(speed factor[2, 20])";
                                };

                        class TiresAsphaltOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-asphalt3",0.316228,1,30};
                                frequency="1";
                                volume="asphalt*(speed factor[2, 20])";
                                };

                        class NoiseOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Noises\ext\noise2",0.316228,1,30};
                                frequency="1";
                                volume="(damper0 max 0.04)*(speed factor[0, 8])";
                                };

                        };

                class Damage
                        {
                        tex[]={};
                        mat[]={"CA\wheeled\data\traktor_2.rvmat","CA\wheeled\Data\traktor_2.rvmat","CA\wheeled\Data\traktor_2_destruct.rvmat","CA\wheeled\data\traktor_2_skla.rvmat","CA\wheeled\data\traktor_2_skla_destruct.rvmat","CA\wheeled\data\traktor_2_skla_destruct.rvmat"};
                        };

                typicalCargo[]={};
                crew="";
                displayName="Tractor";
                driverAction="Truck_Driver";
                castDriverShadow=1;
                class Library
                        {
                        libTextDesc="Agricultural tractor.";
                        };

                maxSpeed=40;
                hasGunner=0;
                class Turrets {};
                class Reflectors
                        {
                        class Left
                                {
                                color[]={0.9,0.8,0.8,1};
                                ambient[]={0.1,0.1,0.1,1};
                                position="L svetlo";
                                direction="konec L svetla";
                                hitpoint="L svetlo";
                                selection="L svetlo";
                                size=1;
                                brightness=0.5;
                                };

                        class Right : Left
                                {
                                position="P svetlo";
                                direction="konec P svetla";
                                hitpoint="P svetlo";
                                selection="P svetlo";
                                };

                        };

                rarityUrban=0.2;
                brakeDistance=2;
	transportMaxMagazines=24;
	transportMaxWeapons=4;
	transportMaxBackpacks=2;
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
                };
 
 class tractorOld : Tractor {
	scope=1;
	model="\ca\Wheeled\tractor";
	driverAction="Tractor_Driver";
	displayname="Old Tractor";
	displayNameShort="Tractor";
	transportMaxMagazines=24;
	transportMaxWeapons=4;
	transportMaxBackpacks=2;
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
	};
	class Damage {
		tex[]={};
		mat[]={"ca\wheeled\data\detailmapy\traktor.rvmat","ca\wheeled\data\detailmapy\traktor.rvmat","ca\wheeled\data\detailmapy\traktor_destruct.rvmat"};
	};
	class Reflectors {
		class Left {
			color[]={0.9,0.8,0.8,1};
			ambient[]={0.1,0.1,0.1,1};
			position="L svetlo";
			direction="konec L svetla";
			hitpoint="L svetlo";
			selection="L svetlo";
			size=1;
			brightness=0.5;
		};
	};
};