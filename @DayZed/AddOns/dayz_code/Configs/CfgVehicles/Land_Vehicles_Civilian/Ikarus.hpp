class Ikarus : Car {
wheelCircumference=3.233;
weapons[]={"TruckHorn2"};
scope=2;
faction="";
side=3;
picture="\Ca\wheeled\data\ico\bus_city_CA.paa";
model="ca\wheeled_E\Ikarus\Ikarus";
Icon="\Ca\wheeled\data\map_ico\icomap_Bus_CA.paa";
mapSize=11;
crew="";
typicalCargo[]={};
displayName="Ikarus Bus";
displayNameShort="Bus";
terrainCoef=5;
turnCoef=3.7;
maxSpeed=80;
transportMaxMagazines=50;
transportMaxWeapons=10;
transportMaxBackpacks=5;              
armor=20;
damageResistance=0.00262;
insideSoundCoef=0.9;
soundGear[]={"",5.62341e-005,1};
soundGetIn[]={"ca\sounds\vehicles\Wheeled\BUS\ext\ext-bus-getout-1",1,1};
soundGetOut[]={"ca\sounds\vehicles\Wheeled\BUS\ext\ext-bus-getout-1",1,1,40};
soundEngineOnInt[]={"ca\sounds\vehicles\Wheeled\BUS\int\int-bus-start-1",0.562341,1};
soundEngineOnExt[]={"ca\sounds\vehicles\Wheeled\BUS\ext\ext-bus-start-1",0.562341,1,280};
soundEngineOffInt[]={"ca\sounds\vehicles\Wheeled\BUS\int\int-bus-stop-1",0.562341,1};
soundEngineOffExt[]={"ca\sounds\vehicles\Wheeled\BUS\ext\ext-bus-stop-1",0.562341,1,280};
buildCrash0[]={"Ca\sounds\Vehicles\Crash\crash_building_01",0.707946,1,200};
buildCrash1[]={"Ca\sounds\Vehicles\Crash\crash_building_02",0.707946,1,200};
buildCrash2[]={"Ca\sounds\Vehicles\Crash\crash_building_03",0.707946,1,200};
buildCrash3[]={"Ca\sounds\Vehicles\Crash\crash_building_04",0.707946,1,200};
soundBuildingCrash[]={"buildCrash0",0.25,"buildCrash1",0.25,"buildCrash2",0.25,"buildCrash3",0.25};
WoodCrash0[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_01",0.707946,1,200};
WoodCrash1[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_02",0.707946,1,200};
WoodCrash2[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_03",0.707946,1,200};
WoodCrash3[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_04",0.707946,1,200};
WoodCrash4[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_05",0.707946,1,200};
WoodCrash5[]={"Ca\sounds\Vehicles\Crash\crash_mix_wood_06",0.707946,1,200};
soundWoodCrash[]={"woodCrash0",0.166,"woodCrash1",0.166,"woodCrash2",0.166,"woodCrash3",0.166,"woodCrash4",0.166,"woodCrash5",0.166};
ArmorCrash0[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_01",0.707946,1,200};
ArmorCrash1[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_02",0.707946,1,200};
ArmorCrash2[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_03",0.707946,1,200};
ArmorCrash3[]={"Ca\sounds\Vehicles\Crash\crash_vehicle_04",0.707946,1,200};
soundArmorCrash[]={"ArmorCrash0",0.25,"ArmorCrash1",0.25,"ArmorCrash2",0.25,"ArmorCrash3",0.25};
                class SoundEvents
                        {
                        class AccelerationIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\BUS\int\int-bus-acce-1",0.562341,1};
                                limit="0.15";
                                expression="engineOn*(1-camPos)*2*gmeterZ*((speed factor[1.5, 5]) min (speed factor[5, 1.5]))";
                                };

                        class AccelerationOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\BUS\ext\ext-bus-acce-1",0.562341,1,280};
                                limit="0.15";
                                expression="engineOn*camPos*2*gmeterZ*((speed factor[1.5, 5]) min (speed factor[5, 1.5]))";
                                };

                        };

                class Sounds
                        {
                        class Engine
                                {
                                sound[]={"\ca\sounds\Vehicles\Wheeled\BUS\ext\ext-bus-low-2",1,1,350};
                                frequency="(randomizer*0.05+0.95)*rpm";
                                volume="engineOn*camPos*(thrust factor[0.7, 0.2])";
                                };

                        class EngineHighOut
                                {
                                sound[]={"\ca\sounds\Vehicles\Wheeled\BUS\ext\ext-bus-high-4",1,1,450};
                                frequency="(randomizer*0.05+0.95)*rpm";
                                volume="engineOn*camPos*(thrust factor[0.5, 0.9])";
                                };

                        class IdleOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\BUS\ext\ext-bus-idle-1",0.316228,1,150};
                                frequency="1";
                                volume="engineOn*camPos*(rpm factor[0.4, 0])";
                                };

                        class TiresRockOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-rock2",0.177828,1,30};
                                frequency="1";
                                volume="camPos*rock*(speed factor[2, 20])";
                                };

                        class TiresSandOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-sand2",0.177828,1,30};
                                frequency="1";
                                volume="camPos*sand*(speed factor[2, 20])";
                                };

                        class TiresGrassOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-grass3",0.177828,1,30};
                                frequency="1";
                                volume="camPos*grass*(speed factor[2, 20])";
                                };

                        class TiresMudOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-mud2",0.177828,1,30};
                                frequency="1";
                                volume="camPos*mud*(speed factor[2, 20])";
                                };

                        class TiresGravelOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-gravel2",0.177828,1,30};
                                frequency="1";
                                volume="camPos*gravel*(speed factor[2, 20])";
                                };

                        class TiresAsphaltOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\ext\ext-tires-asphalt3",0.0562341,1,30};
                                frequency="1";
                                volume="camPos*asphalt*(speed factor[2, 20])";
                                };

                        class NoiseOut
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Noises\ext\ext-noise3",0.177828,1,30};
                                frequency="1";
                                volume="camPos*(damper0 max 0.04)*(speed factor[0, 8])";
                                };

                        class EngineLowIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\BUS\int\int-bus-low-2",1,1};
                                frequency="(randomizer*0.05+0.95)*rpm";
                                volume="((engineOn*thrust) factor[0.65, 0.2])*(1-camPos)";
                                };

                        class EngineHighIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\BUS\int\int-bus-high-4",1,0.95};
                                frequency="(randomizer*0.05+0.95)*rpm";
                                volume="((engineOn*thrust) factor[0.55, 1.0])*(1-camPos)";
                                };

                        class IdleIn
                                {
                                sound[]={"\ca\sounds\Vehicles\Wheeled\BUS\int\int-bus-idle-1",0.562341,1};
                                frequency="1";
                                volume="engineOn*(rpm factor[0.3, 0])*(1-camPos)";
                                };

                        class TiresRockIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\int\int-tires-rock2",0.177828,1};
                                frequency="1";
                                volume="(1-camPos)*rock*(speed factor[2, 20])";
                                };

                        class TiresSandIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\int\int-tires-sand2",0.177828,1};
                                frequency="1";
                                volume="(1-camPos)*sand*(speed factor[2, 20])";
                                };

                        class TiresGrassIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\int\int-tires-grass3",0.177828,1};
                                frequency="1";
                                volume="(1-camPos)*grass*(speed factor[2, 20])";
                                };

                        class TiresMudIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\int\int-tires-mud2",0.177828,1};
                                frequency="1";
                                volume="(1-camPos)*mud*(speed factor[2, 20])";
                                };

                        class TiresGravelIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\int\int-tires-gravel2",0.177828,1};
                                frequency="1";
                                volume="(1-camPos)*gravel*(speed factor[2, 20])";
                                };

                        class TiresAsphaltIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Tires\int\int-tires-asphalt3",0.1,1};
                                frequency="1";
                                volume="(1-camPos)*asphalt*(speed factor[2, 20])";
                                };

                        class NoiseIn
                                {
                                sound[]={"\ca\SOUNDS\Vehicles\Wheeled\Noises\int\int-noise3",0.316228,1};
                                frequency="1";
                                volume="(damper0 max 0.04)*(speed factor[0, 8])*(1-camPos)";
                                };

                        class Movement
                                {
                                sound="soundEnviron";
                                frequency="1";
                                volume="0";
                                };

                        };
	steerAheadSimul=0.5;
	steerAheadPlan=0.38;
	driverAction="Ikarus_Driver";
	class Damage {
		tex[]={};
		mat[]={"Ca\wheeled_E\Ikarus\Data\Bus_exterior.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_exterior_damage.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_exterior_destruct.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_interior.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_interior_damage.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_interior_destruct.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_windows.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_windows_damage.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_windows_damage.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_windows_in.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_windows_in_damage.rvmat","Ca\wheeled_E\Ikarus\Data\Bus_windows_in_damage.rvmat","Ca\Ca_E\data\default.rvmat","Ca\Ca_E\data\default.rvmat","Ca\Ca_E\data\default_destruct.rvmat"};
	};
	hiddenSelections[]={"Camo1"};
	hiddenSelectionsTextures[]={"\ca\wheeled2\ikarus\data\bus_exterior_co.paa"};
	cargoAction[]={"Truck_Cargo01","Truck_Cargo01","Truck_Cargo01","Truck_Cargo01","Truck_Cargo01","Truck_Cargo01","Truck_Cargo01","Truck_Cargo01","Truck_Cargo02","Truck_Cargo02","Truck_Cargo02","Truck_Cargo02","Truck_Cargo03","Truck_Cargo03","Truck_Cargo04","Truck_Cargo04","Truck_Cargo04","Truck_Cargo04","Truck_Cargo04","Ikarus_Cargo","Ikarus_Cargo","Ikarus_Cargo","Ikarus_Cargo"};
	transportSoldier=23;
	hasGunner=0;
	rarityUrban=0.9;
	class Turrets {};
	class Library {libTextDesc="Common bus for public transport.";};
	class AnimationSources {
		class HitLFWheel {source="Hit";hitpoint="HitLFWheel";raw=1;};
		class HitRFWheel {source="Hit";hitpoint="HitRFWheel";raw=1;};
		class HitLBWheel {source="Hit";hitpoint="HitLBWheel";raw=1;};
		class HitRBWheel {source="Hit";hitpoint="HitRBWheel";raw=1;};
		class HitLF2Wheel {source="Hit";hitpoint="HitLF2Wheel";raw=1;};  
		class HitRF2Wheel {source="Hit";hitpoint="HitRF2Wheel";raw=1;};  
		class HitLMWheel {source="Hit";hitpoint="HitLMWheel";raw=1;};  
		class HitRMWheel {source="Hit";hitpoint="HitRMWheel";raw=1;};  
		class HitGlass1 {source="Hit";hitpoint="HitGlass1";raw=1;};
		class HitGlass2 {source="Hit";hitpoint="HitGlass2";raw=1;};
		class HitGlass3 {source="Hit";hitpoint="HitGlass3";raw=1;};
		class HitGlass4 {source="Hit";hitpoint="HitGlass4";raw=1;};
		class HitGlass5 {source="Hit";hitpoint="HitGlass5";raw=1;};
		class HitGlass6 {source="Hit";hitpoint="HitGlass6";raw=1;};
	};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.20;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.20;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.20;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.20;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
		class HitGlass5 {armor=0.1;material=-1;name="glass5";visual="glass5";passThrough=0;};
		class HitGlass6 {armor=0.1;material=-1;name="glass6";visual="glass6";passThrough=0;};
	};
};

class Ikarus_TK_CIV_EP1 : Ikarus {
	expansion=1;
	displayName="Ikarus Bus (Red)";
	displayNameShort="Bus";
	hiddenSelections[]={"Camo1"};
	hiddenSelectionsTextures[]={"\ca\wheeled_E\Ikarus\Data\Bus_exterior_ECIV_CO"};
	rarityUrban=0.4;
	typicalCargo[]={""};
};