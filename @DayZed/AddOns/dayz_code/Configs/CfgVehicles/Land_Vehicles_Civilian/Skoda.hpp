class Skoda : SkodaBase {
	displayName="Skoda White";
	displayNameShort="Skoda";
	maxSpeed=100;
	terrainCoef=6;
	turnCoef=2;
	preferRoads=1;
	fuelCapacity=50;
	transportMaxMagazines=24;
	transportMaxWeapons=4;
	transportMaxBackpacks=2;
	scope=2;
	accuracy=1000;
	crew="";
	faction="";
	typicalCargo[]={};
	model="\ca\wheeled\skodovka";
	hiddenSelections[]={"Camo1","Camo2"};
	hiddenSelectionsTextures[]={"\ca\wheeled\data\skodovka_bila_co.paa","\ca\wheeled\data\skodovka_int_co.paa"};
	class Damage {
		tex[]={};
		mat[]={
			"ca\wheeled\data\skodovka.rvmat",
			"ca\wheeled\data\skodovka.rvmat",
			"ca\wheeled\data\skodovka_destruct.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat"
		};
	};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};
class SkodaBlue : SkodaBase {
	displayName="Skoda Blue";
	displayNameShort="Skoda";
	maxSpeed=110;
	terrainCoef=6;
	turnCoef=2;
	preferRoads=1;
	fuelCapacity=50;
	transportMaxMagazines=24;
	transportMaxWeapons=4;
	transportMaxBackpacks=2;
	scope=2;
	accuracy=1000;
	crew="";
	faction="";
	typicalCargo[]={};
	model="\ca\wheeled\skodovka_blue";
	class Damage {
		tex[]={};
		mat[]={
			"ca\wheeled\data\skodovka_modra.rvmat",
			"ca\wheeled\data\skodovka_modra.rvmat",
			"ca\wheeled\data\skodovka_destruct.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat"
		};
	};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};
class SkodaGreen : SkodaBase {
	displayName="Skoda Green";
	displayNameShort="Skoda";
	maxSpeed=110;
	terrainCoef=6;
	turnCoef=2;
	preferRoads=1;
	fuelCapacity=50;
	transportMaxMagazines=24;
	transportMaxWeapons=4;
	transportMaxBackpacks=2;
	scope=2;
	accuracy=1000;
	crew="";
	faction="";
	typicalCargo[]={};
	model="\ca\wheeled\skodovka_green";
	class Damage {
		tex[]={};
		mat[]={
			"ca\wheeled\data\skodovka_zelena.rvmat",
			"ca\wheeled\data\skodovka_zelena.rvmat",
			"ca\wheeled\data\skodovka_destruct.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat"
		};
	};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};
class SkodaRed : SkodaBase {
	displayName="Skoda Red";
	displayNameShort="Skoda";
	maxSpeed=110;
	terrainCoef=6;
	turnCoef=2;
	preferRoads=1;
	fuelCapacity=50;
	transportMaxMagazines=24;
	transportMaxWeapons=4;
	transportMaxBackpacks=2;
	scope=2;
	accuracy=1000;
	crew="";
	faction="";
	typicalCargo[]={};
	model="\ca\wheeled\skodovka_red";
	class Damage {
		tex[]={};
		mat[]={
			"ca\wheeled\data\skodovka.rvmat",
			"ca\wheeled\data\skodovka.rvmat",
			"ca\wheeled\data\skodovka_destruct.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat",
			"ca\wheeled\data\detailmapy\auta_skla_in_damage.rvmat"
		};
	};
	class HitPoints {
		class HitEngine {armor=0.4;material=-1;name="motor";visual="";passThrough=0.2;};
		class HitBody {armor=1; material=-1;name="karoserie";visual="";passThrough=1;};
		class HitFuel {armor=0.3;material=-1;name="palivo";visual="";passThrough=0.5;};
		class HitLFWheel {armor=0.15;material=-1;name="wheel_1_1_steering";visual="";passThrough=0.3;};
		class HitRFWheel {armor=0.15;material=-1;name="wheel_2_1_steering";visual="";passThrough=0.3;};
		class HitLBWheel {armor=0.15;material=-1;name="wheel_1_2_steering";visual="";passThrough=0.3;};
		class HitRBWheel {armor=0.15;material=-1;name="wheel_2_2_steering";visual="";passThrough=0.3;};
		class HitGlass1 {armor=0.1;material=-1;name="glass1";visual="glass1";passThrough=0;};
		class HitGlass2 {armor=0.1;material=-1;name="glass2";visual="glass2";passThrough=0;};
		class HitGlass3 {armor=0.1;material=-1;name="glass3";visual="glass3";passThrough=0;};
		class HitGlass4 {armor=0.1;material=-1;name="glass4";visual="glass4";passThrough=0;};
	};
};