class SUV_DZ : SUV_Base_EP1 {
	displayName="SUV Civilian";
	displayNameShort="SUV";
	crew = "";
	faction = "";
	scope = 2;
	side = 3;
	typicalcargo[] = {};
	transportSoldier=5;
	fuelCapacity=100;
	maxSpeed=200;
	enableGPS=0;
	hiddenSelections[] = {};
	terrainCoef=3;
	turnCoef=2;
	preferRoads=1;
	transportMaxMagazines=50;
	transportMaxWeapons=10;
	transportMaxBackpacks=5;
	class HitPoints {
		class HitEngine {
			armor = 0.65;
			material = -1;
			name = "motor";
			passthrough = 1;
			visual = "";
		};
		class HitFuel {
			armor = 0.55;
			material = -1;
			name = "palivo";
			passthrough = 1;
			visual = "";
		};
		class HitBody {
			armor=0.65;
			material=-1;
			name="karoserie";
			visual="";
			passThrough=1;
		};
		class HitLFWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_1_1_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitRFWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_2_1_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitLBWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_1_2_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitRBWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_2_2_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitGlass1 {
			armor = 0.1;
			material = -1;
			name = "glass1";
			passthrough = 0;
			visual = "glass1";
			};
		class HitGlass2 {
			armor = 0.1;
			material = -1;
			name = "glass2";
			passthrough = 0;
			visual = "glass2";
		};
		class HitGlass3 {
			armor = 0.1;
			material = -1;
			name = "glass3";
			passthrough = 0;
			visual = "glass3";
		};
		class HitGlass4 {
			armor = 0.1;
			material = -1;
			name = "glass4";
			passthrough = 0;
			visual = "glass4";
		};
	};
};
class SUV_DZed : SUV_DZ {
	displayName="SUV Camouflage";
	displayNameShort="SUV";
	terrainCoef=2;
	hiddenSelections[]={"camo"};
	hiddenSelectionsTextures[]={"\dayzed_weapons\CfgMISC\textures\camo10.paa"};
	class HitPoints {
		class HitEngine {
			armor = 0.65;
			material = -1;
			name = "motor";
			passthrough = 1;
			visual = "";
		};
		class HitFuel {
			armor = 0.55;
			material = -1;
			name = "palivo";
			passthrough = 1;
			visual = "";
		};
		class HitBody {
			armor=0.65;
			material=-1;
			name="karoserie";
			visual="";
			passThrough=1;
		};
		class HitLFWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_1_1_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitRFWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_2_1_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitLBWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_1_2_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitRBWheel {
			armor = 0.30;
			material = -1;
			name = "wheel_2_2_steering";
			passThrough=0.3;
			visual = "";
		};
		class HitGlass1 {
			armor = 0.1;
			material = -1;
			name = "glass1";
			passthrough = 0;
			visual = "glass1";
			};
		class HitGlass2 {
			armor = 0.1;
			material = -1;
			name = "glass2";
			passthrough = 0;
			visual = "glass2";
		};
		class HitGlass3 {
			armor = 0.1;
			material = -1;
			name = "glass3";
			passthrough = 0;
			visual = "glass3";
		};
		class HitGlass4 {
			armor = 0.1;
			material = -1;
			name = "glass4";
			passthrough = 0;
			visual = "glass4";
		};
	};
};