Farm[] = {
	{"2Rnd_shotgun_74Slug",0.05}, 
	{"2Rnd_shotgun_74Pellets",0.05},
	{"5x_22_LR_17_HMR",0.02},
	{"bwc_SKS_mag",0.03},
	{"8Rnd_9x19_p38",0.03},
	{"10x_303_DZed",0.02},
	{"15Rnd_W1866_Slug",0.04},
	{"WoodenArrow",0.05},
	{"ItemMilkbottleUnfilled",0.04},
	{"ItemMilkbottle",0.03},
	{"ItemSodaLemonade",0.02},
	{"FoodCanUnlabeled",0.02},
	{"FoodCanUnlabeledEmpty",0.03},
	{"FoodCanRusUnlabeled",0.02},
	{"FoodCanRusUnlabeledEmpty",0.03},
	{"TrashJackDaniels",0.04}
};