#include "CfgLoot.hpp"

class CfgBuildingLoot {
	class Default {
		zombieChance = 0.2;
		minRoaming = 0;
		maxRoaming = 2;
		zombieClass[] = {"zZombie_Base","z_hunter","z_teacher","z_suit1","z_suit2","z_worker1","z_worker2","z_worker3","z_villager1","z_villager2","z_villager3"};
		lootChance = 0;
		lootPos[] = {};
		lootType[] = {};
		hangPos[] = {};
		vehPos[] = {};
	};
	class Master {
		weapons[] = {"SMAW","Javelin","G36C","Stinger"};
	};
	class Church: Default {
		zombieChance = 0.9;
		minRoaming = 1;
		maxRoaming = 3;
		zombieClass[] = {"z_priest","z_priest","z_priest"};
		lootChance = 0.7;
		//lootChance = 0.5;
		lootPos[] = {};
		lootType[] = {
			{"ItemBookBible","magazine",0.15},
			{"","Church",0.25}
		};
	};
	class Castle: Default {
		zombieChance = 0.9;
		lootChance = 0.7;
		//lootChance = 0.5;
		zombieClass[] = {"zZombie_Base","z_hunter","z_teacher","z_villager1","z_villager2","z_villager3","z_new_villager2","z_new_villager3","z_new_villager4"};
		lootPos[] = {};
		lootType[] = {
			{"LeeEnfield_DZed","weapon",0.02},
			{"DZ_ALICE_Pack_EP1","object",0.02},
			{"ItemMap","weapon",0.02},	
			{"ItemFlashlight","weapon",0.02},			
			{"RH_m1911old","weapon",0.03},
			{"ItemWatch","weapon",0.03},
			{"RH_pm","weapon",0.03}, 
			{"Crossbow_DZ","weapon",0.03},
			{"","CastleLoot",0.25}
		};
	};	
	class Residential: Default {
		zombieChance = 0.9;
		maxRoaming = 2;
		zombieClass[] = {"zZombie_Base","z_hunter","z_teacher","z_villager1","z_villager2","z_villager3","z_new_villager2","z_new_villager3","z_new_villager4"};
		lootChance = 0.4;
		//lootChance = 0.2;
		lootPos[] = {};
		lootType[] = {
			{"bwc_SKS","weapon",0.01},
			{"RH_m1911old","weapon",0.01},
			{"Winchester1866","weapon",0.01},
			{"RH_pm","weapon",0.02}, 
			{"ItemMatchbox","weapon",0.02},
			{"ItemWatch","weapon",0.03},
			{"ItemCanOpener","weapon",0.03},
			{"ItemFlashlight","weapon",0.04},
			{"ItemBaseBallBat","weapon",0.04},	
			{"","Residential",0.60}
		};
	};
	
	class Office: Residential {
		zombieChance = 0.9;
		maxRoaming = 3;
		lootChance = 0.4;
		//lootChance = 0.2;
		zombieClass[] = {"z_suit1","z_suit2"};
		lootType[] = {
			{"bwc_SKS","weapon",0.01},
			{"ItemDomeTent","magazine",0.01},
			{"DZ_TK_Assault_Pack_EP1","object",0.01},
			{"Remington870_lamp","weapon",0.02},
			{"RH_pm","weapon",0.02}, 
			{"ItemTent","magazine",0.02},
			{"ItemMap","weapon",0.03},			
			{"ItemWatch","weapon",0.04},
			{"ItemCarpenterGuide","weapon",0.03},
			{"RH_m1911old","weapon",0.03},
			{"ItemCanOpener","weapon",0.03},
			{"ItemFlashlight","weapon",0.04},
			{"ItemMatchbox","weapon",0.02},
			{"ItemBaseBallBat","weapon",0.04},
			{"ItemCompass","weapon",0.02},										
			{"","Residential",0.60}
		};
	};
	
	class Farm: Default { 
		zombieChance = 0.9;
		maxRoaming = 3;
		zombieClass[] = {"zZombie_Base","z_hunter","z_hunter","z_hunter","z_villager1","z_villager2","z_villager3","z_new_villager2","z_new_villager3","z_new_villager4"};
		lootChance = 0.6;
		//lootChance = 0.4;
		lootPos[] = {};
		lootType[] = {			
			{"TrapBear","magazine",0.02},			
			{"WeaponHolder_ItemFuelcan","object",0.02},
			{"Winchester1866","weapon",0.02},			
			{"Crossbow_DZ","weapon",0.03},
			{"RH_p38","weapon",0.03},
			{"MR43","weapon",0.03},
			{"WeaponHolder_ItemHatchet","object",0.04},
			{"ItemMachete","weapon",0.04},
			{"PartWoodPile","magazine",0.04},
			{"","Farm",0.60}
		};
	};
	
	class Industrial: Default { 
		zombieChance = 0.9;
		zombieClass[] = {"z_worker1","z_worker2","z_worker3","z_new_worker2","z_new_worker3","z_new_worker4"};
		maxRoaming = 2;
		lootChance = 0.5;
		//lootChance = 0.2;
		lootPos[] = {};
		lootType[] = {
			{"ItemToolbox","weapon",0.02},			
			{"WeaponHolder_ItemCrowbar","object",0.04},
			{"WeaponHolder_PartFueltank","object",0.02},
			//{"ItemHammer","weapon",0.03},
			{"WeaponHolder_PartGlass","object",0.02},
			{"WeaponHolder_PartEngine","object",0.02},
			{"ItemWireCutter","weapon",0.03},
			{"WeaponHolder_ItemJerrycan","object",0.03},
			{"WeaponHolder_PartGeneric","object",0.04},
			//{"ItemLugWrench","weapon",0.02},
			{"WeaponHolder_PartWheel","object",0.04},
			{"","Industrial",0.50}
			//{"","Industrial",0.60}
		};
	};
	
	class Supermarket: Default {
		lootChance = 0.6;
		//lootChance = 0.3;
		minRoaming = 2;
		maxRoaming = 6;
		zombieChance = 0.9;
		zombieClass[] = {"zZombie_Base","zZombie_Base","z_teacher","z_suit1","z_suit2","z_new_villager2","z_new_villager3","z_new_villager4"};
		lootType[] = {
			{"DZ_Assault_Pack_EP1","object",0.01}, 
			{"ItemMatchbox","weapon",0.02},
			{"ItemCompass","weapon",0.01},
			{"ItemMap","weapon",0.02},	
			{"ItemFlashlight","weapon",0.02},			
			{"ItemWatch","weapon",0.02},	
			{"RH_p38","weapon",0.02},
			{"RH_pm","weapon",0.01}, 				
			{"","Supermarket",0.10},
			{"","trash",0.30}
			//{"","trash",0.40}
		};
	};	
	
	class Hospital: Default {
		zombieChance = 0.9;
		minRoaming = 2;
		maxRoaming = 6;
		zombieClass[] = {"z_doctor","z_doctor","z_doctor"};
		lootChance = 0.9;
		//lootChance = 0.7;
		lootPos[] = {};
		lootType[] = {
			{"","hospital",0.50}
		};
	};
	
	class Hunting: Default {
		zombieChance = 0.6;
		minRoaming = 1;
		maxRoaming = 3;
		zombieClass[] = {"z_hunter","z_hunter","z_hunter"};
		lootChance = 0.8;
		//lootChance = 0.5;
		lootPos[] = {};
		lootType[] = {
			{"DZed_SmallBag","object",0.01},
			{"huntingrifle","weapon",0.01},
			{"ItemGHILLIERag","magazine",0.01},
			{"ItemCompass","weapon",0.02},
			{"TrapBear","magazine",0.02},
			{"ItemMagnesiumRod","weapon",0.03},
			{"ItemMap","weapon",0.04},
			{"Crossbow_DZ","weapon",0.05},	
			{"ItemKnife","weapon",0.05},	
			{"","hunter",0.45}
			//{"","hunter",0.55}
		};
	};
	
		class HeliCrashWEST: Default {
		zombieChance = 1;
		maxRoaming = 8;
		zombieClass[] = {"z_soldier_pilot"};
		lootChance = 0.5;
		lootPos[] = {};
		lootType[] = {
			{"C1987_G3","weapon",0.01},
			{"DZed_GunBag","object",0.01}, 
			{"NVGoggles_Broken","weapon",0.01},	
			{"RH_M40A3","weapon",0.01},
			{"Skin_Sniper1_DZ","magazine",0.01},
			{"M24","weapon",0.01},
			{"ItemSniperScope","magazine",0.01},
			{"M200_Intervention_DZed","weapon",0.01},
			{"G36_C_SD_eotech","weapon",0.02},
			{"ItemELCANSight","magazine",0.02},
			{"C1987_Famas_f1","weapon",0.02},
			{"ItemRifleSuppressor","magazine",0.02},
			{"RH_acrb","weapon",0.02},
			{"ItemRadio","weapon",0.03},
			{"RH_UMP","weapon",0.03},
			{"ItemGPS_Broken","weapon",0.03},
			{"RH_M4sbr","weapon",0.03},
			{"","militaryWEST",0.10}
		};
	};
	class HeliCrashEAST: Default {
		zombieChance = 1;
		maxRoaming = 8;
		zombieClass[] = {"z_soldier_pilot"};
		lootChance = 0.5;
		lootPos[] = {};
		lootType[] = {
			{"RH_rpk74","weapon",0.01},
			{"Binocular_Vector","weapon",0.01},		
			{"ItemNightVisionScope","magazine",0.01},
			{"Skin_SG_GRU_Sniper_DZed","magazine",0.01},
			{"SVD_Ironsight_DayZed","weapon",0.01},
			{"ItemPSOScope","magazine",0.02},
			{"RH_rk95","weapon",0.02},
			{"ItemPistolSuppressor","magazine",0.02},
			{"RH_gr1","weapon",0.02},
			{"ItemRadio_Broken","weapon",0.02},
			{"RH_akms","weapon",0.02},
			{"ItemGrenadeLauncherRU","magazine",0.03},
			{"RH_akm","weapon",0.03},
			{"Item1P29Scope","magazine",0.03},
			{"RH_ak105","weapon",0.03},
			{"","militaryEAST",0.10}
		};
	};
	class HeliCrashMED: Default {
		zombieChance = 1;
		maxRoaming = 8;
		zombieClass[] = {"z_soldier_pilot"};
		lootChance = 0.5;
		lootPos[] = {};
		lootType[] = {
			{"MedBoxCS6","object",0.01},
			{"MedBoxCS5","object",0.02},
			{"MedBoxCS4","object",0.03},
			{"MedBoxCS3","object",0.04},
			{"MedBoxCS2","object",0.05},
			{"MedBoxCS1","object",0.06},		
			{"","crashMED",0.10}
		};
	};
	class HeliCrashUS: Default {
		zombieChance = 1;
		maxRoaming = 8;
		zombieClass[] = {"z_soldier_pilot"};
		lootChance = 0.5;
		lootPos[] = {};
		lootType[] = {
			{"RH_m14","weapon",0.01},
			{"NVGoggles","weapon",0.01},
			{"RH_M40A5","weapon",0.01},
			{"RH_Mk48mod1","weapon",0.01},
			{"Binocular_Vector_Broken","weapon",0.01},	
			{"BAF_LRR_scoped_W_DZed","weapon",0.01},
			{"RH_p90","weapon",0.02},
			{"PipeBomb","magazine",0.02},
			{"RH_M4sdaim_wdl","weapon",0.02},
			{"Skin_Sniper1_DZed","magazine",0.02},
			{"ItemACOGScope","magazine",0.02},
			{"ItemGPS","weapon",0.03},
			{"RH_hk416","weapon",0.03},
			{"ItemPistolSuppressor","magazine",0.03},
			{"RH_deagle","weapon",0.03},
			{"","militaryWEST",0.15}
		};
	};
	
	class Military: Default {
		zombieChance = 0.9;
		maxRoaming = 6;
		zombieClass[] = {"z_soldier","z_soldier_heavy","z_policeman","z_new_worker2","z_new_worker3","z_new_worker4"};
		lootChance = 0.6;
		lootPos[] = {};
		lootType[] = {			
			{"RH_akms","weapon",0.01},
			{"DZ_British_ACU","object",0.01},
			{"RH_ak74","weapon",0.03},
			{"ItemKOBRASight","magazine",0.01},
			{"M1014_DZed","weapon",0.02},
			{"ItemGrenadeLauncherRU","magazine",0.02},
			{"RH_vz61","weapon",0.02},
			{"RH_mp5a5","weapon",0.02},
			{"ItemCamoNet","magazine",0.02},	
			{"ItemMagnesiumRod","weapon",0.03},
			{"RH_an94","weapon",0.03},
			{"ItemGrenadeLauncherUS","magazine",0.03},
			{"Binocular","weapon",0.03},
			{"RH_bizon","weapon",0.03},				
			{"ItemFlashlightRed","weapon",0.04},
			{"RH_m1911old","weapon",0.04},	
			{"ItemKnife","weapon",0.04},
			{"RH_pm","weapon",0.04}, 	
			{"","militarycommon",0.60}
		};
	};
	class MilitarySpecial: Default {
		zombieChance = 0.9;
		minRoaming = 2;
		maxRoaming = 6;
		zombieClass[] = {"z_soldier","z_soldier_heavy","z_soldier_pilot"};
		lootChance = 0.5;
		lootPos[] = {};
		lootType[] = {
			{"C1987_Famas_f1","weapon",0.01},
			{"ItemRadio_Broken","weapon",0.01},
			{"RH_gr1","weapon",0.01},
			{"ItemPistolSuppressor","magazine",0.01},
			{"DZ_Backpack_EP1","object",0.01}, 	
			{"RH_M4a1","weapon",0.02},	
			{"RH_mk2","weapon",0.02},
			{"RH_ak105","weapon",0.02},
			{"ItemGHILLIERag","magazine",0.02},
			{"RH_usp","weapon",0.02},
			{"RH_akm","weapon",0.02},
			{"ItemHOLOSight","magazine",0.02},
			{"RH_hk416s","weapon",0.02},				
			{"RH_tec9","weapon",0.03},
			{"RH_bizon","weapon",0.03},				
			{"ItemFlashlightRed","weapon",0.04},
			{"RH_aks74u","weapon",0.04},
			{"RH_usp","weapon",0.04},			
			{"","militaryrare",0.80}
		};
	};		
	class MilitaryBarracksSmall: Default {
		zombieChance = 0.9;
		minRoaming = 2;
		maxRoaming = 6;
		zombieClass[] = {"z_soldier","z_soldier_heavy","z_soldier_pilot"};
		lootChance = 0.7;
		lootPos[] = {};
		lootType[] = {
			{"DZed_MedicPack","object",0.01},
			{"G36C","weapon",0.01},
			{"ItemRifleSuppressor","magazine",0.01},
			{"RH_M4sbr","weapon",0.02},
			{"RH_deagle","weapon",0.02},
			{"RH_akms","weapon",0.02},
			{"RH_mp5a5","weapon",0.03},
			{"ItemCCOSight","magazine",0.02},
			{"ItemMagnesiumRod","weapon",0.03},
			{"RH_AK107","weapon",0.03},	
			{"RH_aps","weapon",0.04},
			{"ItemKnife","weapon",0.04},
			{"RH_vz61","weapon",0.05},
			{"Binocular","weapon",0.05},
			{"","militaryrare",0.20},	
			{"","militarycommon",0.40}
		};
	};								
	class MilitaryC130: Default {
		zombieChance = 0.9;
		minRoaming = 2;
		maxRoaming = 6;
		zombieClass[] = {"z_soldier","z_soldier_heavy","z_soldier_pilot"};
		lootChance = 0.8;
		lootPos[] = {};
		lootType[] = {
			{"ItemRadio_Broken","weapon",0.01},
			{"RH_p90","weapon",0.01},
			{"Skin_SG_GRU_TeamLeader_DZed","magazine",0.01},
			{"ItemAIMSHOTSight","magazine",0.01},
			{"RH_M249","weapon",0.01},
			{"Skin_Soldier3_DZed","magazine",0.01},
			{"RH_MK12","weapon",0.01},
			{"ItemGPS_Broken","weapon",0.01},	
			{"DZ_CivilBackpack_EP1","object",0.01}, 
			{"C1987_Famas_f1","weapon",0.01},
			{"RH_mk2","weapon",0.02},
			{"RH_mk22","weapon",0.02},
			{"ItemCCOSight","magazine",0.02},
			{"RH_UMP","weapon",0.03},
			{"RH_M16a4","weapon",0.03},		
			{"RH_g17","weapon",0.03},
			{"","militaryrare",0.50}
		};
	};
	class MilitaryMilBarracks: Default {
		zombieChance = 0.9;
		maxRoaming = 6;
		zombieClass[] = {"z_soldier","z_soldier_heavy","z_soldier_pilot"};
		lootChance = 0.4;
		lootPos[] = {};
		lootType[] = {			
			{"RH_rk95","weapon",0.01},
			{"ItemGPS_Broken","weapon",0.01},
			{"G36C","weapon",0.01},
			{"DZ_TK_Assault_Pack_EP1","object",0.01},
			{"RH_aks74","weapon",0.02},
			{"LeeEnfield_DZed","weapon",0.02},
			{"RH_M4sbr","weapon",0.02},
			{"RH_ak105","weapon",0.02},
			{"ItemGrenadeLauncherRU","magazine",0.02},
			{"RH_deagle","weapon",0.02},
			{"RH_mk22","weapon",0.02},
			{"ItemKOBRASight","magazine",0.02},
			{"Binocular","weapon",0.03},
			{"RH_bizon","weapon",0.03},				
			{"ItemMap","weapon",0.04},
			{"RH_aps","weapon",0.04},
			{"","militaryrare",0.30},
			{"","militarycommon",0.50}
		};
	};
	class MilitaryMilHouse: Default {
		zombieChance = 0.9;
		maxRoaming = 6;
		zombieClass[] = {"z_soldier","z_soldier_heavy","z_soldier_pilot"};
		lootChance = 0.3;
		lootPos[] = {};
		lootType[] = {			
			{"SVD_Ironsight_DayZed","weapon",0.01},
			{"ItemRadio_Broken","weapon",0.01},
			{"RH_p90","weapon",0.01},
			{"ItemCCOSight","magazine",0.01},
			{"RH_UMP","weapon",0.01},
			{"DZ_ALICE_Pack_EP1","object",0.01},
			{"RH_akm","weapon",0.01},
			{"ItemGrenadeLauncherUS","magazine",0.02},
			{"RH_AK107","weapon",0.02},	
			{"RH_aks74u","weapon",0.02},	
			{"ItemFlashlightRed","weapon",0.02},
			{"RH_tec9","weapon",0.02},
			{"ItemHOLOSight","magazine",0.02},
			{"RH_mp5a5","weapon",0.03},
			{"ItemMagnesiumRod","weapon",0.03},
			{"RH_g17","weapon",0.03},
			{"ItemMap","weapon",0.04},
			{"RH_m1911old","weapon",0.04},			
			{"ItemKnife","weapon",0.04},
			{"","militaryrare",0.30},
			{"","militarycommon",0.50}
		};
	};
	class WildZeds: Default {
		zombieChance = 0.8;
		minRoaming = 3;
		maxRoaming = 6;	
		zombieClass[] = {"zZombie_Base","z_hunter","z_hunter","z_hunter","z_villager1","z_villager2","z_villager3","z_new_villager2","z_new_villager3","z_new_villager4"};
	};

#include "CfgBuildingPos.hpp"

};