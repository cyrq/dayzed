class CfgLoot {

	#include "BuildingLoot\Supermarket.hpp"
	#include "BuildingLoot\Farm.hpp"
	#include "BuildingLoot\Industrial.hpp"
	#include "BuildingLoot\CastleLoot.hpp"
	#include "BuildingLoot\Residential.hpp"
	#include "BuildingLoot\Church.hpp"
	#include "BuildingLoot\militaryEAST.hpp"
	#include "BuildingLoot\militaryWEST.hpp"
	#include "BuildingLoot\Hospital.hpp"
	#include "BuildingLoot\militarymagazinescommon.hpp"
	#include "BuildingLoot\militarymagazinesrare.hpp"
	#include "BuildingLoot\Hangar.hpp"
	#include "BuildingLoot\Hunter.hpp"
	#include "BuildingLoot\crashMED.hpp"
	#include "BuildingLoot\Shed.hpp"

	policeman[] = {
		{"ItemBandage",0.3},
		{"8Rnd_9x18_Makarov",0.02},
		{"8Rnd_9x19_p38",0.03},
		{"8Rnd_45cal_m1911",0.02},	
		{"15Rnd_W1866_Slug",0.03},
		{"8Rnd_B_Beneli_74Slug_DZed",0.03},
		{"ItemSodaCoke",0.01},
		{"ItemSodaPepsi",0.01},
		{"FoodCanBakedBeans",0.01},
		{"FoodCanPasta",0.02},
		{"TrashJackDaniels",0.05},
		{"ItemTrashRazor",0.06},
		{"ItemCards",0.05}
	};
	civilian[] = {
		{"ItemBandage",0.03},
		{"ItemPainkiller",0.03},
		{"8Rnd_9x18_Makarov",0.02},
		{"8Rnd_9x19_p38",0.03},
		{"8Rnd_45cal_m1911",0.02},	
		{"15Rnd_W1866_Slug",0.03},
		{"2Rnd_shotgun_74Pellets",0.03},
		{"ItemSodaCoke",0.01},
		{"ItemSodaPepsi",0.01},
		{"FoodCanBakedBeans",0.01},
		{"FoodCanPasta",0.02},
		{"TrashJackDaniels",0.05},
		{"ItemTrashRazor",0.06},
		{"ItemCards",0.05}
	};
	viralloot[] = {
		{"ItemBandage",0.02},
		{"8Rnd_9x18_Makarov",0.04},
		{"2Rnd_shotgun_74Pellets",0.08},
		{"ItemSodaPepsi",0.04},
		{"FoodCanFrankBeans",0.05},
		{"FoodCanPasta",0.05},
		{"FoodCanSardines",0.05}
	};
	food[] = {
		{"FoodCakeCremeCakeClean",0.01},
		{"ItemSodaMtngreen",0.01}
	};
	generic[] = {
		{"8Rnd_9x18_Makarov",0.02},
		{"8Rnd_9x19_p38",0.03},
		{"8Rnd_45cal_m1911",0.02},	
		{"8Rnd_9x19_Mk",0.01},		
		{"15Rnd_W1866_Slug",0.01},
		{"bwc_SKS_mag",0.02},
		{"8Rnd_B_Beneli_74Slug_DZed",0.02},
		{"8Rnd_B_Beneli_Pellets_DZed",0.01},
		{"2Rnd_shotgun_74Slug",0.03}, 
		{"2Rnd_shotgun_74Pellets",0.02},	
		{"10x_303_DZed",0.02},	
		{"HandChemGreen",0.04},
		{"HandChemBlue",0.04},
		{"HandChemRed",0.04},
		{"HandRoadFlare",0.04},	
		{"WoodenArrow",0.04},
		{"ItemSodaCoke",0.05},
		{"ItemSodaPepsi",0.05},	
		{"FoodCanBakedBeans",0.05},
		{"FoodCanSardines",0.05},
		{"FoodCanFrankBeans",0.05},
		{"FoodCanPasta",0.05},
		{"FoodCanUnlabeled",0.04},
		{"FoodCanRusUnlabeled",0.05},
		{"FoodCanRusStew",0.05},
		{"FoodCanRusPork",0.05},
		{"FoodCanRusPeas",0.05},
		{"FoodCanRusMilk",0.04},
		{"FoodCanRusCorn",0.05},
		{"FoodPistachio",0.03},
		{"FoodNutmix",0.04},
		{"ItemSodaCokeEmpty",0.05},
		{"ItemSodaPepsiEmpty",0.03},
		{"FoodCanUnlabeledEmpty",0.05},
		{"FoodCanRusUnlabeledEmpty",0.02},
		{"FoodCanRusPorkEmpty",0.02},
		{"FoodCanRusPeasEmpty",0.03},
		{"FoodCanRusMilkEmpty",0.02},
		{"FoodCanRusCornEmpty",0.05},
		{"FoodCanRusStewEmpty",0.03},
		{"TrashTinCan",0.05},
		{"TrashJackDaniels",0.02},
		{"ItemSodaEmpty",0.03},
		{"ItemTrashToiletpaper",0.01},
		{"ItemTrashRazor",0.01},
		{"ItemCards",0.02}
	};
	medical[] = {
		{"ItemBandage",0.60},
		{"ItemPainkiller",0.60},
		{"ItemAntibiotic",0.20},
		{"ItemMorphine",0.20},
		{"ItemMedKit",0.20},
		{"ItemBloodbag",0.20},
		{"ItemEpinephrine",0.20},
		{"ItemChloroform",0.20},
		{"ItemHeatPack",0.20},
		{"TrashTinCan",0.20},
		{"ItemSodaEmpty",0.20},
		{"ItemTrashToiletpaper",0.20},
		{"ItemTrashRazor",0.20}
	};
	trash[] = {
		{"ItemSodaCokeEmpty",0.05},
		{"ItemSodaPepsiEmpty",0.04},
		{"FoodCanUnlabeledEmpty",0.05},
		{"FoodCanRusUnlabeledEmpty",0.04},
		{"FoodCanRusPorkEmpty",0.02},
		{"FoodCanRusPeasEmpty",0.03},
		{"FoodCanRusMilkEmpty",0.02},
		{"FoodCanRusCornEmpty",0.05},
		{"FoodCanRusStewEmpty",0.03},
		{"TrashTinCan",0.05},
		{"TrashJackDaniels",0.03},
		{"ItemSodaEmpty",0.03},
		{"ItemTrashToiletpaper",0.04},
		{"ItemTrashRazor",0.03},
		{"ItemCards",0.02}
	};
};