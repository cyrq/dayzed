class ItemRadio: ItemCore {
		descriptionShort="Radio";
		model="z\addons\dayz_code\Configs\CfgWeapons\Item\radio.p3d";
		picture="\z\addons\dayz_code\Configs\CfgWeapons\Item\equip_radio.paa";
		class ItemActions
		{
			class Use
			{
				text = "Send Radio Message";
				script = "spawn player_sendMessage;";
			};
		};
	};
	
class ItemRadio_Broken: ItemRadio {
		displayName="Radio (Broken)";
		descriptionShort="Broken Radio";
		picture="\z\addons\dayz_code\Configs\CfgWeapons\Item\equip_broken_radio.paa";
			class ItemActions {
				delete Use;
				class Use2 {
					text = "Fix Radio";
					script = "spawn player_fixEQ;";
				};
			};
		};