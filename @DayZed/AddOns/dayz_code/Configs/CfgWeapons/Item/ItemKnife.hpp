class ItemKnife; //external class reference
class ItemKnife5 : ItemKnife {};
class ItemKnife4 : ItemKnife {};
class ItemKnife3 : ItemKnife {};
class ItemKnife2 : ItemKnife {};
class ItemKnife1 : ItemKnife {};
class ItemKnifeBlunt : ItemKnife {
	displayName = "Blunt Hunting Knife";
	descriptionShort = "Your Hunting Knife appears to be blunt.";
};