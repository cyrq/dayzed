class ItemToolbox; //external class

class ItemToolboxEmpty: ItemToolbox {
	displayName = "Toolbox (Empty)";
	descriptionShort = "Empty Toolbox that can be used for storing various tools and equipment.";
		class ItemActions {
			delete Use;
		};
	};