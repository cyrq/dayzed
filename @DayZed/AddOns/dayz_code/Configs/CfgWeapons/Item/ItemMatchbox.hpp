class ItemMatchbox; //external class

class ItemMatchbox5 : ItemMatchbox {
		descriptionShort = "Box of safety matches for starting a fire under controlled conditions. 5 Matches Left.";
		class ItemActions;
	};
	
class ItemMatchbox4 : ItemMatchbox {
		descriptionShort = "Box of safety matches for starting a fire under controlled conditions. 4 Matches Left.";
		class ItemActions;
	};
	
class ItemMatchbox3 : ItemMatchbox {
		descriptionShort = "Box of safety matches for starting a fire under controlled conditions. 3 Matches Left.";
		class ItemActions;
	};
	
class ItemMatchbox2 : ItemMatchbox {
		descriptionShort = "Box of safety matches for starting a fire under controlled conditions. 2 Matches Left.";
		class ItemActions;
	};
	
class ItemMatchbox1 : ItemMatchbox {
		descriptionShort = "Box of safety matches for starting a fire under controlled conditions. 1 Match Left.";
		class ItemActions;
	};
	
class ItemMatchboxEmpty : ItemMatchbox {
		displayName = "Empty Box of Matches";
		descriptionShort = "Empty Box of safety matches..";
		class ItemActions {
			delete Use;
		};
	};