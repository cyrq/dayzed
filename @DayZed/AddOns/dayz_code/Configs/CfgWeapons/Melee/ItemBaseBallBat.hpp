class ItemBaseBallBat: ItemCore
	{
		scope=2;
		model="\z\addons\dayz_communityassets\models\baseball_bat_weaponized.p3d";
		picture="\z\addons\dayz_communityassets\pictures\equip_baseball_bat_ca.paa";
		displayName=$STR_EQUIP_NAME_BASEBALLBAT;
		descriptionShort=$STR_EQUIP_DESC_BASEBALLBAT;
		class ItemActions
		{
			class Toolbelt
				{
				text=$STR_ACTIONS_RFROMTB;
				script="spawn player_addToolbelt;";
				use[]=
				{
					"ItemBaseBallBat"
				};
				output[]=
				{
					"MeleeBaseBallBat"
				};
		};
			class ToBack
			{
				text=$STR_ACTIONS_2BACK;
				script="spawn player_addtoBack;";
				use[]=
				{
					"ItemBaseBallBat"
				};
				output[]=
				{
					"MeleeBaseBallBat"
				};
			};
		};
	};
	