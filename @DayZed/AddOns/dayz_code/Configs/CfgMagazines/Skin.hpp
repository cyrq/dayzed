class SkinBase : CA_Magazine {
		scope = public;
		count = 1;
		type = 256;
		displayName = $STR_EQUIP_NAME_CLOTHES;
		descriptionShort = $STR_EQUIP_DESC_CLOTHES;
		model = "\dayz_equip\models\cloth_parcel.p3d";
		picture = "\dayz_equip\textures\equip_cloth_parcel_ca.paa";
		class ItemActions {
			class Use {
				text = $STR_EQUIP_TEXT_CLOTHES;
				script = "spawn player_wearClothes;";
			};
		};
	};
	class Skin_Sniper1_DZ : SkinBase {
		displayName = $STR_EQUIP_NAME_GHILLIE;
		descriptionShort = $STR_EQUIP_DESC_GHILLIE;
	};
	
	class Skin_Camo1_DZ : SkinBase {
		displayName = $STR_EQUIP_NAME_CAMO;
		descriptionShort = $STR_EQUIP_DESC_CAMO;
	};
	
	class Skin_Survivor2_DZ : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Soldier1_DZ : SkinBase {
		displayName = $STR_EQUIP_NAME_SOLDIER;
		descriptionShort = $STR_EQUIP_DESC_SOLDIER;
	}; 
	
	class Skin_Soldier1_DZed : SkinBase {
		displayName = "KSK Soldier Clothing";
		descriptionShort = "A German KSK Soldier Uniform. Can be unpacked and worn.";
	}; 
	
	class Skin_Soldier2_DZed : SkinBase {
		displayName = "Delta Force Clothing";
		descriptionShort = "A Delta Force Uniform. Can be unpacked and worn";		
	}; 

	class Skin_Soldier3_DZed : SkinBase {
		displayName = "Czech SF Clothing";
		descriptionShort = "A Czech Special Forces Uniform. Can be unpacked and worn.";
	}; 
	
	class Skin_Soldier4_DZed : SkinBase {
		displayName = "BAF Clothing";
		descriptionShort = "A British Armed Forces Clothing. Can be unpacked and worn.";
	}; 
	
	class Skin_Sniper1_DZed : SkinBase {
		displayName = "Shattered Ghillie";
		descriptionShort = $STR_EQUIP_DESC_GHILLIE;
	};
	
	class Skin_SG_GRU_TeamLeader_DZed : SkinBase {
		displayName = "GRU Leader Clothing";
		descriptionShort = "A Russian Spetsnaz GRU Team Leader Uniform. Can be unpacked and worn.";
	};
	
	class Skin_SG_GRU_Sniper_DZed : SkinBase {
		displayName = "GRU Sniper Clothing";
		descriptionShort = "A Russian Spetsnaz GRU Sniper Uniform. Can be unpacked and worn.";
	}; 	
	
	class Skin_SG_GRU_Marksman_W_DZed : SkinBase {
		displayName = "GRU Marksman Clothing";
		descriptionShort = "A Russian Spetsnaz GRU Marksman Uniform. Can be unpacked and worn.";
	};
	
	class Skin_SG_GRU_Assaultman_W_DZed : SkinBase {
		displayName = "GRU Assault Clothing";
		descriptionShort = "A Russian Spetsnaz GRU Assault Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Policeman_DZed : SkinBase {
		displayName = "Chernarus Police Clothing";
		descriptionShort = "A Chernarus Police Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Policeman_RU_DZed : SkinBase {
		displayName = "Chernarus Police Clothing";
		descriptionShort = "A Chernarus Police Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Civilian_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Civilian1_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Civilian2_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Civilian3_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Civilian_RU_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Civilian1_RU_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Civilian2_RU_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};
	
	class Skin_Civilian3_RU_DZed : SkinBase {
		displayName = $STR_EQUIP_NAME_CIV;
		descriptionShort = $STR_EQUIP_DESC_CIV;
	};

	class Skin_Worker_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Worker1_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Worker2_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Worker3_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Worker_RU_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Worker1_RU_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Worker2_RU_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Worker3_RU_DZed : SkinBase {
		displayName = "T.E.C. Worker Clothing";
		descriptionShort = "A T.E.C. Worker Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer1_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer2_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer3_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer_RU_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer1_RU_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer2_RU_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Profiteer3_RU_DZed : SkinBase {
		displayName = "Profiteer Clothing";
		descriptionShort = "A Profiteer Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander1_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander2_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander3_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander_RU_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander1_RU_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander2_RU_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Woodlander3_RU_DZed : SkinBase {
		displayName = "Woodlander Clothing";
		descriptionShort = "A Woodlander Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Functionary_DZed : SkinBase {
		displayName = "Functionary Clothing";
		descriptionShort = "A Functionary Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Functionary1_DZed : SkinBase {
		displayName = "Functionary Clothing";
		descriptionShort = "A Functionary Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Functionary_RU_DZed : SkinBase {
		displayName = "Functionary Clothing";
		descriptionShort = "A Functionary Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Functionary1_RU_DZed : SkinBase {
		displayName = "Functionary Clothing";
		descriptionShort = "A Functionary Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Priest_DZed : SkinBase {
		displayName = "Priest Clothing";
		descriptionShort = "A Priest Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Doctor_DZed : SkinBase {
		displayName = "Doctor Clothing";
		descriptionShort = "A Doctor Uniform. Can be unpacked and worn.";
	};
	
	class Skin_SchoolTeacher_DZed : SkinBase {
		displayName = "School Teacher Clothing";
		descriptionShort = "A School Teacher Uniform. Can be unpacked and worn.";
	};
	
	class Skin_SchoolTeacher_RU_DZed : SkinBase {
		displayName = "School Teacher Clothing";
		descriptionShort = "A School Teacher Uniform. Can be unpacked and worn.";
	};
	
	class Skin_Assistant_DZed : SkinBase {
		displayName = "Medical Assistant Clothing";
		descriptionShort = "A Medical Assistant. Can be unpacked and worn.";
	};
	
	class Skin_Tshirt_DZed : SkinBase {
		displayName = "Tourist Clothing (Blue)";
		descriptionShort = "A Tourist Clothing. Can be unpacked and worn.";
	};
	
	class Skin_Tshirt1_DZed : SkinBase {
		displayName = "Tourist Clothing (Black)";
		descriptionShort = "A Tourist Clothing. Can be unpacked and worn.";
	};
	
	class Skin_Tshirt2_DZed : SkinBase {
		displayName = "Tourist Clothing (Green)";
		descriptionShort = "A Tourist Clothing. Can be unpacked and worn.";
	};
	
	class Skin_Tshirt3_DZed : SkinBase {
		displayName = "Tourist Clothing (Orange)";
		descriptionShort = "A Tourist Clothing. Can be unpacked and worn.";
	};
	
	class Skin_Tshirt4_DZed : SkinBase {
		displayName = "Tourist Clothing (White)";
		descriptionShort = "A Tourist Clothing. Can be unpacked and worn.";
	};
	
	class Skin_Tshirt5_DZed : SkinBase {
		displayName = "Tourist Clothing (Orange Pepe)";
		descriptionShort = "A Tourist Clothing. Can be unpacked and worn.";
	};
	
	class Skin_Tshirt6_DZed : SkinBase {
		displayName = "Tourist Clothing (Black Star)";
		descriptionShort = "A Tourist Clothing. Can be unpacked and worn.";
	};