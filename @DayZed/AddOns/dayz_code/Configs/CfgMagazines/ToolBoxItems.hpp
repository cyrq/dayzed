class ItemHammer: CA_Magazine {
	scope = public;
	count = 1;
	type = 256;
	displayName = "Hammer";
	descriptionShort = "Basic tool of many professions.";
	model = "\dayz_equip\models\hammer.p3d";
	picture = "\dayz_equip\textures\hammer_equip.paa";
	class ItemActions {
		class Use {
			text = "Add to ToolBox";
			script = "spawn player_addToolbox";
		};
	};
};
class ItemLugWrench: CA_Magazine {
	scope = public;
	count = 1;
	type = 256;
	displayName = "Lug Wrench";
	descriptionShort = "Spider-type lug wrench used to loosen and tighten lug nuts on vehicles wheels.";
	model = "\dayz_equip\models\lug_wrench.p3d";
	picture = "\dayz_equip\textures\lug_wrench_equip.paa";
	class ItemActions {
		class Use {
			text = "Add to ToolBox";
			script = "spawn player_addToolbox";
		};
	};
};