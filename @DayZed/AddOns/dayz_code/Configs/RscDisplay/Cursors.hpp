class CfgWeaponCursors
{
	class RifleCursorCore;
	class LawCursorCore;
	class MGCursorCore
	{
		color[] = {0.95, 0.95, 0.95, 1};
	};
	class RifleCursor: RifleCursorCore
	{
		color[] = {0.95, 0.95, 0.95, 1};
	};
	class PistolCursor: RifleCursor
	{
		color[] = {0.95, 0.95, 0.95, 1};
	};
	class LawCursor: LawCursorCore
	{
		color[] = {0.95, 0.95, 0.95, 1};
	};
	class Cannon: LawCursorCore
	{
		color[] = {0.95, 0.95, 0.95, 1};
	};
};