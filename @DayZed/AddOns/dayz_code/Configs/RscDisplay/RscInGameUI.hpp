class RscIGUIText: RscText{};
class RscIGUIValue: RscIGUIText{};
class RscInGameUI
{
	colorReady[]={0.95, 0.95, 0.95, 1};
	colorPrepare[]={0.91, 0.67, 0,1};
	colorUnload[]={0.54, 0.05, 0.05,1};
	
	class RscUnitInfoNoHUD{};
	class RscUnitInfoSoldier
	{
		class CA_Mode: RscIGUIText
		{
			sizeEx = "0.026 * 1.2";
			style = 1;
			y = "0.14 + SafeZoneY";
		};
		class CA_Ammo: RscIGUIValue
		{
			sizeEx = "0.026 * 1.5";
			y = "0.015 + SafeZoneY";
		};
		class CA_Weapon: RscIGUIText
		{
			sizeEx = "0.022 * 3.0";
			style = 1;
			h = 0.08;
			x = "(SafeZoneW + SafeZoneX) - (1 - 0.461)";
			y = "0.065 + SafeZoneY";
			w = 0.514;
		};
	};
	class RscUnitInfoTank
	{
		class CA_Mode: RscIGUIText
		{
			sizeEx = "0.026 * 1.2";
			style = 1;
			y = "0.14 + SafeZoneY";
		};
		class CA_Ammo: RscIGUIValue
		{
			sizeEx = "0.026 * 1.5";
			y = "0.015 + SafeZoneY";
		};
		class CA_Weapon: RscIGUIText
		{
			sizeEx = "0.022 * 3.0";
			style = 1;
			h = 0.08;
			x = "(SafeZoneW + SafeZoneX) - (1 - 0.461)";
			y = "0.065 + SafeZoneY";
			w = 0.514;
		};
		class CA_GunnerWeapon: RscIGUIText
		{
			style = 1;
			y = "0.20 + SafeZoneY";
		};
	};
	class RscUnitVehicle
	{
		class CA_Mode: RscIGUIText
		{
			sizeEx = "0.026 * 1.2";
			style = 1;
			y = "0.14 + SafeZoneY";
		};
		class CA_Ammo: RscIGUIValue
		{
			sizeEx = "0.026 * 1.5";
			y = "0.015 + SafeZoneY";
		};
		class CA_Weapon: RscIGUIText
		{
			sizeEx = "0.022 * 3.0";
			style = 1;
			h = 0.08;
			x = "(SafeZoneW + SafeZoneX) - (1 - 0.461)";
			y = "0.065 + SafeZoneY";
			w = 0.514;
		};
		class CA_GunnerWeapon: RscIGUIText
		{
			style = 1;
			y = "0.20 + SafeZoneY";
		};
	};
	class RscUnitInfo
	{
		class CA_Mode: RscIGUIText
		{
			sizeEx = "0.026 * 1.2";
			style = 1;
			y = "0.14 + SafeZoneY";
		};
		class CA_Ammo: RscIGUIValue
		{
			sizeEx = "0.026 * 1.5";
			y = "0.015 + SafeZoneY";
		};
		class CA_Weapon: RscIGUIText
		{
			sizeEx = "0.022 * 3.0";
			style = 1;
			h = 0.08;
			x = "(SafeZoneW + SafeZoneX) - (1 - 0.461)";
			y = "0.065 + SafeZoneY";
			w = 0.514;
		};
		class CA_GunnerWeapon: RscIGUIText
		{
			style = 1;
			y = "0.20 + SafeZoneY";
		};
	};
	class RscUnitInfoAir: RscUnitInfo
	{
		class CA_Mode: RscIGUIText
		{
			sizeEx = "0.026 * 1.2";
			style = 1;
			y = "0.14 + SafeZoneY";
		};
		class CA_Ammo: RscIGUIValue
		{
			sizeEx = "0.026 * 1.5";
			y = "0.015 + SafeZoneY";
		};
		class CA_Weapon: RscIGUIText
		{
			sizeEx = "0.022 * 3.0";
			style = 1;
			h = 0.08;
			x = "(SafeZoneW + SafeZoneX) - (1 - 0.461)";
			y = "0.065 + SafeZoneY";
			w = 0.514;
		};
		class CA_GunnerWeapon: RscIGUIText
		{
			style = 1;
			y = "0.20 + SafeZoneY";
		};
	};
	class RscWeaponZeroing
	{
		class CA_ZeroingText: RscIGUIText
		{
			style = 1;
			y = "0.25 + SafeZoneY";
		};
		class CA_Zeroing: RscIGUIValue
		{
			y = "0.29 + SafeZoneY";
		};
	};
	class RscWeaponRangeZeroing
	{
		class CA_ZeroingText: RscIGUIText
		{
			style = 1;
			y = "0.25 + SafeZoneY";
		};
		class CA_Zeroing: RscIGUIValue
		{
			y = "0.29 + SafeZoneY";
		};
	};
};