class RscCombo;

class RscDisplayOptionsVideo
        {
        class controls
                {
                class G_VideoOptionsControls : RscControlsGroup
                        {
                        idc=1024;
                        x=0;
                        y=0;
                        w="SafeZoneW + 1";
                        h="SafeZoneH + 1";
                        class VScrollbar
                                {
                                autoScrollSpeed=-1;
                                autoScrollDelay=5;
                                autoScrollRewind=0;
                                color[]={1,1,1,0};
                                width=0.001;
                                };

                        class HScrollbar
                                {
                                color[]={1,1,1,0};
                                height=0.001;
                                };
								
                        class Controls
                                {
                                delete CA_TextGamma;
                                delete CA_ValueGamma;
                                delete CA_SliderGamma;
                                };

                        };

                };
		};