class RscDisplayMultiplayerSetup : RscStandardDisplay
{
	class controls
	{
	class CA_MP_roles_Title : CA_Title {
			text="Welcome to DayZed!";
	};
	class TextMission : RscText {
			text="Version:";
	};
	class CA_TextDescription : RscText {
		text="Information:";
	};

delete TextSide;
delete TextRoles;

	class TextPool : RscText {
		x="(30/100)	* SafeZoneW + SafeZoneX";
		y="(16.5/100) * SafeZoneH + SafeZoneY";
		text="Players:";
	};
		
delete CA_B_DSinterface;

	class CA_B_West : RscActiveText {
		colorActive[]={1,1,1,0};
		colorDisabled[]={1,1,1,0};
		colorShade[]={1,1,1,0};
		colorText[]={1,1,1,0};
		pictureWidth=0;
		pictureHeight=0;
		textHeight=0;
		sideToggle="";
		idc=104;
		color[]={1,1,1,0};
		text="";
		picture="";
	};

delete CA_B_East;
delete CA_B_Guerrila;
delete CA_B_Civilian;
delete CA_ValueRoles;

	class CA_ValuePool : RscIGUIListBox {
		x="(30/100)	* SafeZoneW + SafeZoneX";
		y="(20/100)	* SafeZoneH + SafeZoneY";
		canDrag=0;
	};
		
	class CA_ButtonContinue : RscShortcutButton
		{
		x="(53/100)	* SafeZoneW + SafeZoneX";
		y="(93/100)	* SafeZoneH + SafeZoneY";
	};

	class CA_ButtonCancel : RscShortcutButton
		{
		x="(38/100)	* SafeZoneW + SafeZoneX";
		y="(93/100)	* SafeZoneH + SafeZoneY";
	};
	
delete CA_B_EnableAll;
};

class controlsbackground
{
delete SidesBack;
delete SidesBorder;
delete ValueRolesBack;
delete ValueRolesBorder;

	class ValuePoolBack : RscText {
		x="(30/100)	* SafeZoneW + SafeZoneX";
		y="(20/100)	* SafeZoneH + SafeZoneY";
	};
	
	class ValuePoolBorder : RscText {
		x="(30/100)	* SafeZoneW + SafeZoneX";
		y="(20/100)	* SafeZoneH + SafeZoneY";
	};
};
};

class RscDisplayMPPlayers : RscStandardDisplay
{
	class controls
	{
		delete CA_TextSquad;
		delete CA_ValueSquad;
		delete CA_TextSquadName;
		delete CA_ValueSquadName;
		delete CA_TextSquadId;
		delete CA_ValueSquadId;
		delete CA_TextSquadMail;
		delete CA_ValueSquadMail;
		delete CA_TextSquadWeb;
		delete CA_ValueSquadWeb;
		delete CA_TextSquadPicture;
		delete CA_ValueSquadPicture;
		delete CA_TextSquadTitle;
		delete CA_ValueSquadTitle;
	};
};