		class ItemCarpenterGuide: ItemCore {
		scope = 2;
		displayName = "Woodworking Guide"; 
		descriptionShort = "Almost 50 pages covering the basics of Carpentering.";
		picture = "\dayz_equip\textures\basic_woodworking_guide_equip.paa";
		model = "\dayz_equip\models\basic_woodworking_guide.p3d"; 
		class ItemActions {
			class Craft0 {
				text = "Craft Storage Box";
				script = ";['Craft0', _id] spawn player_craftItem;";
				sfx = "craft_wood";
				complexTime = 17;
				failChance = 0.15;
				input[] = 
				{
					{"equip_nails","CfgMagazines",4},
					{"PartWoodPile","CfgMagazines",2}
				};
				output[] = 
				{
					{"ItemStorageBox","CfgMagazines",1}
				};
				required[] = 
				{
					{"ItemToolbox","CfgWeapons",1}
				};
			};
		};

	};	