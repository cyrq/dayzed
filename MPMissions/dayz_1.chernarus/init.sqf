startLoadingScreen ["","RscDisplayLoadCustom"];

/*Check ARMA Version*/

_ARMAv = productVersion;
if ((_ARMAv select 3) > 116523) exitWith
{
	startLoadingScreen ["PLEASE INSTALL ARMA V1.63 BETA PATCH #116523!", "DayZ_loadingScreen"];
	sleep 10;
	endMission "END1";
	endLoadingScreen;
};

cutText ["","BLACK OUT"];
enableSaving [false, false];

//Server Settings
dayZ_instance = 1; // The instance
dayZ_ATPList = ["68837958","81498438","125187"]; //Built in ATP UID exclusions

#include "\z\addons\dayz_code\system\mission\init.sqf"